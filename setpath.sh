#
# This script adds Mitsuba to the current path.
# It works with both Bash and Zsh.
#
# NOTE: this script must be sourced and not run, i.e.
#    . setpath.sh        for Bash
#    source setpath.sh   for Zsh or Bash
#

if [[ "${BASH_SOURCE[0]}" == "${0}" ]]; then
	echo "The setpath.sh script must be sourced, not executed. In other words, run\n"
	echo "$ source setpath.sh\n"
	exit 0
fi

root=$(dirname "$BASH_SOURCE")
path=$(cd "$root"; pwd)

alias collapser="$path"/build/collapser
alias textureFiltering="$path"/build/textureFiltering
alias voxels2vol="$path"/build/voxels2vol
