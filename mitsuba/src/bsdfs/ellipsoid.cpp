/*
    This file is part of Mitsuba, a physically based rendering system.

    Copyright (c) 2007-2014 by Wenzel Jakob and others.

    Mitsuba is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    Mitsuba is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <mitsuba/core/fresolver.h>
#include <mitsuba/core/sggx.h>
#include <mitsuba/render/bsdf.h>
#include <mitsuba/hw/basicshader.h>
#include "microfacet.h"
#include "ior.h"

MTS_NAMESPACE_BEGIN

class Ellipsoid : public BSDF {
public:
    Ellipsoid(const Properties &props) : BSDF(props) {
		ref<FileResolver> fResolver = Thread::getThread()->getFileResolver();

        m_diffuseReflectance = new ConstantSpectrumTexture(
            props.getSpectrum("diffuseReflectance", Spectrum(1.0f)));

		m_specularReflectance = new ConstantSpectrumTexture(
			props.getSpectrum("specularReflectance", Spectrum(1.0f)));

        m_sggx_sigma = new ConstantSpectrumTexture(
                    props.getSpectrum("sggxSigma", Spectrum(1.0f)));

        m_sggx_r = new ConstantSpectrumTexture(
                    props.getSpectrum("sggxR", Spectrum(0.5)));

		std::string materialName = props.getString("material", "Cu");

		Spectrum intEta, intK;
		if (boost::to_lower_copy(materialName) == "none") {
			intEta = Spectrum(0.0f);
			intK = Spectrum(1.0f);
		} else {
			intEta.fromContinuousSpectrum(InterpolatedSpectrum(
				fResolver->resolve("data/ior/" + materialName + ".eta.spd")));
			intK.fromContinuousSpectrum(InterpolatedSpectrum(
				fResolver->resolve("data/ior/" + materialName + ".k.spd")));
		}

		Float extEta = lookupIOR(props, "extEta", "air");

		m_eta = props.getSpectrum("eta", intEta) / extEta;
		m_k   = props.getSpectrum("k", intK) / extEta;

		MicrofacetDistribution distr(props);
		m_type = distr.getType();
		m_sampleVisible = distr.getSampleVisible();

		m_alphaU = new ConstantFloatTexture(distr.getAlphaU());
		if (distr.getAlphaU() == distr.getAlphaV())
			m_alphaV = m_alphaU;
		else
			m_alphaV = new ConstantFloatTexture(distr.getAlphaV());
	}

    Ellipsoid(Stream *stream, InstanceManager *manager)
	 : BSDF(stream, manager) {
		m_type = (MicrofacetDistribution::EType) stream->readUInt();
		m_sampleVisible = stream->readBool();
		m_alphaU = static_cast<Texture *>(manager->getInstance(stream));
		m_alphaV = static_cast<Texture *>(manager->getInstance(stream));
        m_sggx_sigma = static_cast<Texture *>(manager->getInstance(stream));
        m_sggx_r = static_cast<Texture *>(manager->getInstance(stream));

        m_diffuseReflectance = static_cast<Texture *>(manager->getInstance(stream));
		m_specularReflectance = static_cast<Texture *>(manager->getInstance(stream));
		m_eta = Spectrum(stream);
		m_k = Spectrum(stream);

		configure();
	}

	void serialize(Stream *stream, InstanceManager *manager) const {
		BSDF::serialize(stream, manager);

		stream->writeUInt((uint32_t) m_type);
		stream->writeBool(m_sampleVisible);
		manager->serialize(stream, m_alphaU.get());
		manager->serialize(stream, m_alphaV.get());
        manager->serialize(stream, m_sggx_sigma.get());
        manager->serialize(stream, m_sggx_r.get());
        manager->serialize(stream, m_diffuseReflectance.get());
		manager->serialize(stream, m_specularReflectance.get());
		m_eta.serialize(stream);
		m_k.serialize(stream);
	}

	void configure() {
		unsigned int extraFlags = 0;
		if (m_alphaU != m_alphaV)
			extraFlags |= EAnisotropic;

        if (!m_alphaU->isConstant() || !m_alphaV->isConstant() ||
            !m_diffuseReflectance->isConstant() || !m_sggx_sigma->isConstant())
            extraFlags |= ESpatiallyVarying;

		if (!m_alphaU->isConstant() || !m_alphaV->isConstant() ||
            !m_specularReflectance->isConstant() || !m_sggx_sigma->isConstant())
			extraFlags |= ESpatiallyVarying;

		m_components.clear();
		m_components.push_back(EGlossyReflection | EFrontSide | extraFlags);

        /* Verify the input parameters and fix them if necessary */
        m_diffuseReflectance = ensureEnergyConservation(
            m_diffuseReflectance, "diffuseReflectance", 1.0f);

		/* Verify the input parameters and fix them if necessary */
		m_specularReflectance = ensureEnergyConservation(
			m_specularReflectance, "specularReflectance", 1.0f);

        m_usesRayDifferentials =
                m_alphaU->usesRayDifferentials() ||
                m_alphaV->usesRayDifferentials() ||
                m_diffuseReflectance->usesRayDifferentials() ||
                m_specularReflectance->usesRayDifferentials();

		BSDF::configure();
	}

	/// Helper function: reflect \c wi with respect to a given surface normal
	inline Vector reflect(const Vector &wi, const Normal &m) const {
		return 2 * dot(wi, m) * Vector(m) - wi;
	}

    void toSGGX(Spectrum sigma, Spectrum r, Vector3f &diag, Vector3f &tri) const {
        SGGX S;
        S.fromSigmaR(sigma, r);
        diag = S.m_diag;
        tri = S.m_tri;

    }

    // ellipsoide en repère local
    // (u, v, w) = vecteurs unitaires global en local
    // donc S_local = (u,v,w) S (u,v,w)t
    void toLocalSpace(Vector3f diag,Vector3f tri,
                 Vector3f u,
                 Vector3f v,
                 Vector3f w,
                 Vector3f &diag_local,
                 Vector3f &tri_local) const {

        SGGX sggx(diag,tri);

        Matrix3x3 S = sggx.toMatrix();
        Matrix3x3 R(u,v,w); // avec vecteurs colonnes
        Matrix3x3 R_t;
        R.transpose(R_t);
        Matrix3x3 S_local = R * S * R_t;
        diag_local = Vector3f(S_local(0,0),S_local(1,1),S_local(2,2));
        tri_local = Vector3f(S_local(1,0),S_local(2,0),S_local(2,1));

        // DEBUG
        //diag_local = Vector3f(0.00005,0.00005,1.0);
        //tri_local = Vector3f(0.0,0.0,0.0);

    }

    Spectrum eval(const BSDFSamplingRecord &bRec, EMeasure measure) const {
        return evalDiff(bRec, measure) + evalSpec(bRec, measure);
    }

    Spectrum evalDiff(const BSDFSamplingRecord &bRec, EMeasure measure) const {
        /* Stop if this component was not requested */
        if (measure != ESolidAngle ||
                Frame::cosTheta(bRec.wi) <= 0 ||
                Frame::cosTheta(bRec.wo) <= 0 ||
                ((bRec.component != -1 && bRec.component != 0) ||
                 !(bRec.typeMask & EGlossyReflection)))
            return Spectrum(0.0f);

        /* Construct the microfacet distribution matching the
           roughness values at the current surface position. */

        Vector3f diag, tri, diag_local, tri_local;
        toSGGX(m_sggx_sigma->eval(bRec.its),m_sggx_r->eval(bRec.its),diag,tri);
        toLocalSpace(diag,tri,
                     bRec.its.toLocal(Vector3f(1.0,0.0,0.0)),
                     bRec.its.toLocal(Vector3f(0.0,1.0,0.0)),
                     bRec.its.toLocal(Vector3f(0.0,0.0,1.0)),
                     diag_local,
                     tri_local);

        if (bRec.sampler == NULL)
            Log(EError, "Need sampler here");

        MicrofacetDistribution distr(m_type, diag_local, tri_local, bRec.sampler);
        bRec.sampler->next2D();

        // From "Implementing a simple Anisotropic Rough Diffuse Material with Stochastic Evaluation"
        Float pdf;
        Normal m = distr.sample(bRec.wi, bRec.sampler->next2D(), pdf);

        Float clamped_dot = std::max(0.0f, dot(bRec.wo,m));

        return distr.smithG1(bRec.wo, m) * clamped_dot * INV_PI * m_diffuseReflectance->eval(bRec.its);
    }


    Spectrum evalSpec(const BSDFSamplingRecord &bRec, EMeasure measure) const {
        /* Stop if this component was not requested */
        if (measure != ESolidAngle ||
                Frame::cosTheta(bRec.wi) <= 0 ||
                Frame::cosTheta(bRec.wo) <= 0 ||
                ((bRec.component != -1 && bRec.component != 0) ||
                 !(bRec.typeMask & EGlossyReflection)))
            return Spectrum(0.0f);

        /* Calculate the reflection half-vector */
        Vector H = normalize(bRec.wo+bRec.wi);

        /* Construct the microfacet distribution matching the
           roughness values at the current surface position. */

        Vector3f diag, tri, diag_local, tri_local;
        toSGGX(m_sggx_sigma->eval(bRec.its),m_sggx_r->eval(bRec.its),diag,tri);
        toLocalSpace(diag,tri,
                     bRec.its.toLocal(Vector3f(1.0,0.0,0.0)),
                     bRec.its.toLocal(Vector3f(0.0,1.0,0.0)),
                     bRec.its.toLocal(Vector3f(0.0,0.0,1.0)),
                     diag_local,
                     tri_local);

        MicrofacetDistribution distr(m_type, diag_local, tri_local, bRec.sampler);

        /* Evaluate the microfacet normal distribution */
        const Float D = distr.eval(H);
        if (D == 0)
            return Spectrum(0.0f);

        /* Fresnel factor */
        const Spectrum F = fresnelConductorExact(dot(bRec.wi, H), m_eta, m_k);

        /* Smith's shadow-masking function */
        const Float G = distr.G(bRec.wi, bRec.wo, H);

        /* Calculate the total amount of reflection */
        Float model = D * G / (4.0f * Frame::cosTheta(bRec.wi));

        return F * model * m_specularReflectance->eval(bRec.its);
    }

    Float pdf(const BSDFSamplingRecord &bRec, EMeasure measure) const {

        if (bRec.sampler == NULL)
            Log(EError, "Need sampler here");

        Spectrum albedo_spec = m_specularReflectance->eval(bRec.its);
        Spectrum albedo_diff = m_diffuseReflectance->eval(bRec.its);

        Float max_spec = albedo_spec.max();
        Float max_diff = albedo_diff.max();

        if (max_spec+max_diff == 0.0)
            return 1.0; // doesnt matter since no reflectance?

        Float spec_weight = max_spec/(max_spec+max_diff);
        Float diff_weight = max_diff/(max_spec+max_diff);

        Float res = pdfDiff(bRec, measure) * diff_weight + pdfSpec(bRec, measure) * spec_weight;
        return res;
    }

    Float pdfDiff(const BSDFSamplingRecord &bRec, EMeasure measure) const {
        if (measure != ESolidAngle ||
            Frame::cosTheta(bRec.wi) <= 0 ||
            Frame::cosTheta(bRec.wo) <= 0 ||
            ((bRec.component != -1 && bRec.component != 0) ||
            !(bRec.typeMask & EGlossyReflection)))
            return 0.0f;

        return INV_PI * std::max(0.0f,dot(bRec.wo,Vector3f(0,0,1)));
    }

    Float pdfSpec(const BSDFSamplingRecord &bRec, EMeasure measure) const {
		if (measure != ESolidAngle ||
            Frame::cosTheta(bRec.wi) <= 0 ||
            Frame::cosTheta(bRec.wo) <= 0 ||
			((bRec.component != -1 && bRec.component != 0) ||
			!(bRec.typeMask & EGlossyReflection)))
			return 0.0f;

		/* Calculate the reflection half-vector */
		Vector H = normalize(bRec.wo+bRec.wi);

		/* Construct the microfacet distribution matching the
		   roughness values at the current surface position. */
        Vector3f diag, tri, diag_local, tri_local;
        toSGGX(m_sggx_sigma->eval(bRec.its),m_sggx_r->eval(bRec.its),diag,tri);
        toLocalSpace(diag,tri,
                     bRec.its.toLocal(Vector3f(1.0,0.0,0.0)),
                     bRec.its.toLocal(Vector3f(0.0,1.0,0.0)),
                     bRec.its.toLocal(Vector3f(0.0,0.0,1.0)),
                     diag_local,
                     tri_local);

        MicrofacetDistribution distr(m_type, diag_local, tri_local, bRec.sampler);

		if (m_sampleVisible)
			return distr.eval(H) * distr.smithG1(bRec.wi, H)
				/ (4.0f * Frame::cosTheta(bRec.wi));
		else
			return distr.pdf(bRec.wi, H) / (4 * absDot(bRec.wo, H));
	}

    Spectrum sample(BSDFSamplingRecord &bRec, const Point2 &sample_) const {
        Float pdf;
        Spectrum res = sample(bRec,pdf,sample_);
        return res;
    }

    Spectrum sampleDiff(BSDFSamplingRecord &bRec, const Point2 &sample) const {
        if (Frame::cosTheta(bRec.wi) <= 0)
            return Spectrum(0.0f);

        /* Construct the microfacet distribution matching the
           roughness values at the current surface position. */
        Vector3f diag, tri, diag_local, tri_local;
        toSGGX(m_sggx_sigma->eval(bRec.its),m_sggx_r->eval(bRec.its),diag,tri);
        toLocalSpace(diag,tri,
                     bRec.its.toLocal(Vector3f(1.0,0.0,0.0)),
                     bRec.its.toLocal(Vector3f(0.0,1.0,0.0)),
                     bRec.its.toLocal(Vector3f(0.0,0.0,1.0)),
                     diag_local,
                     tri_local);

        MicrofacetDistribution distr(m_type, diag_local, tri_local, bRec.sampler);

        /* Sample M, the microfacet normal */
        Float pdf;
        Normal m = distr.sample(bRec.wi, sample, pdf);

        /* Sample diffuse (cosine?) centered on m */
        Vector3f sampled = warp::squareToCosineHemisphere(bRec.sampler->next2D());

        if (sampled.length() < 0.95) {
            Log(EError, "Sampled is not normalized!");
        }

        // avoid bad cases
        if (m == Vector3f(0,0,1)) {
            bRec.wo = sampled;
        } else {

            // sampled arround m
            // rotated frame : z = m, x = cross(m,z), y = cross(x,z)
            // then rotated sampled, so that if sampled == (0,0,1) it becomes m
            // rotated_sampled = () * sampled
            Vector3f X = cross(m,Vector3f(1,0,0));
            X /= X.length();
            Vector3f Y = cross(X,m);
            Y /= Y.length(); // useless ?
            Matrix3x3 M(X,Y,m); // avec vecteurs colonnes
            bRec.wo = M*sampled;
            bRec.wo /= bRec.wo.length(); // just in case?
        }

        // clamping
        if (Frame::cosTheta(bRec.wo) < 0)
            return Spectrum(0.0f);

        /* Useless ?? */
        bRec.eta = 1.0f;
        bRec.sampledComponent = 0;
        bRec.sampledType = EGlossyReflection;

        // Do we need to divide by <wo, wg> ?
        return Spectrum(distr.smithG1(bRec.wo, m)) * m_diffuseReflectance->eval(bRec.its);; // This is G2(wi, wo, wm)/G1(wi, wm)
    }


    Spectrum sampleSpec(BSDFSamplingRecord &bRec, const Point2 &sample) const {
        if (Frame::cosTheta(bRec.wi) <= 0)
			return Spectrum(0.0f);

		/* Construct the microfacet distribution matching the
		   roughness values at the current surface position. */
        Vector3f diag, tri, diag_local, tri_local;
        toSGGX(m_sggx_sigma->eval(bRec.its),m_sggx_r->eval(bRec.its),diag,tri);
        toLocalSpace(diag,tri,
                     bRec.its.toLocal(Vector3f(1.0,0.0,0.0)),
                     bRec.its.toLocal(Vector3f(0.0,1.0,0.0)),
                     bRec.its.toLocal(Vector3f(0.0,0.0,1.0)),
                     diag_local,
                     tri_local);

        MicrofacetDistribution distr(m_type, diag_local, tri_local, bRec.sampler);

		/* Sample M, the microfacet normal */
		Float pdf;
		Normal m = distr.sample(bRec.wi, sample, pdf);

		if (pdf == 0)
			return Spectrum(0.0f);

		/* Perfect specular reflection based on the microfacet normal */
		bRec.wo = reflect(bRec.wi, m);
		bRec.eta = 1.0f;
		bRec.sampledComponent = 0;
		bRec.sampledType = EGlossyReflection;

		/* Side check */
		if (Frame::cosTheta(bRec.wo) <= 0)
			return Spectrum(0.0f);

        Spectrum F = fresnelConductorExact(dot(bRec.wi, m), m_eta, m_k);


		Float weight;
		if (m_sampleVisible) {
			weight = distr.smithG1(bRec.wo, m);
		} else {
			weight = distr.eval(m) * distr.G(bRec.wi, bRec.wo, m)
				* dot(bRec.wi, m) / (pdf * Frame::cosTheta(bRec.wi));
		}

		return F * weight * m_specularReflectance->eval(bRec.its);
	}

    Spectrum sample(BSDFSamplingRecord &bRec, Float &pdf, const Point2 &sample) const {

        Spectrum albedo_spec = m_specularReflectance->eval(bRec.its);
        Spectrum albedo_diff = m_diffuseReflectance->eval(bRec.its);

        // importance sampling
        Float max_spec = albedo_spec.max();
        Float max_diff = albedo_diff.max();

        int sampledComponent;
        Float sampledWeight;
        Float spec_weight = 0.f, diff_weight = 0.f;
        if (max_spec+max_diff == 0) {
            // quand par interpolation on trouve de la densité ici alors que pas
            // de donnée... Peut d'influance sur le look, juste un peu plus sombre.

            //std::cout << "What?? " << albedo_spec.toString() << " " << albedo_diff.toString() << std::endl;
            //std::cout << "density here " << densityAtT << std::endl;
            //std::cout << "density here again " << m_density->lookupFloat(mRec.p) << std::endl;

            sampledComponent = 1;
            sampledWeight = 1.0;
        } else {
            spec_weight = max_spec/(max_spec+max_diff);
            diff_weight = max_diff/(max_spec+max_diff);
            sampledComponent = (bRec.sampler->next1D() > spec_weight); // 0 -> spec, 1 -> diff
            sampledWeight = (sampledComponent) ? diff_weight : spec_weight;

        }

        Spectrum result(0.0);
        if (sampledComponent) {
            result = sampleDiff(bRec, pdf, sample);
        } else {
            result = sampleSpec(bRec, pdf, sample);
        }

        if (result.isZero()) // sampling failed
            return result;

        result *= pdf;
        pdf *= sampledWeight;

        if (pdf == 0.0)
            return result;

        EMeasure measure = BSDF::getMeasure(bRec.sampledType);
        if (sampledComponent) {
            // if diffuse has been sampled
            pdf += pdfSpec(bRec, measure) * spec_weight;
            result += evalSpec(bRec, measure);
        } else {
            pdf += pdfDiff(bRec, measure) * diff_weight;
            result += evalDiff(bRec, measure);
        }



        return result/pdf;
    }

    Spectrum sampleDiff(BSDFSamplingRecord &bRec, Float &pdf, const Point2 &sample) const {
        Spectrum res = sampleDiff(bRec,sample);
        pdf = INV_PI * std::max(0.0f,dot(bRec.wo,Vector3f(0,0,1)));
        return res;
    }

    Spectrum sampleSpec(BSDFSamplingRecord &bRec, Float &pdf, const Point2 &sample) const {
        if (Frame::cosTheta(bRec.wi) <= 0 ||
			((bRec.component != -1 && bRec.component != 0) ||
			!(bRec.typeMask & EGlossyReflection)))
			return Spectrum(0.0f);

		/* Construct the microfacet distribution matching the
		   roughness values at the current surface position. */
        Vector3f diag, tri, diag_local, tri_local;
        toSGGX(m_sggx_sigma->eval(bRec.its),m_sggx_r->eval(bRec.its),diag,tri);
        toLocalSpace(diag,tri,
                     bRec.its.toLocal(Vector3f(1.0,0.0,0.0)),
                     bRec.its.toLocal(Vector3f(0.0,1.0,0.0)),
                     bRec.its.toLocal(Vector3f(0.0,0.0,1.0)),
                     diag_local,
                     tri_local);

        MicrofacetDistribution distr(m_type, diag_local, tri_local, bRec.sampler);

		/* Sample M, the microfacet normal */
		Normal m = distr.sample(bRec.wi, sample, pdf);

		if (pdf == 0)
			return Spectrum(0.0f);

		/* Perfect specular reflection based on the microfacet normal */
		bRec.wo = reflect(bRec.wi, m);
		bRec.eta = 1.0f;
		bRec.sampledComponent = 0;
		bRec.sampledType = EGlossyReflection;

		/* Side check */
		if (Frame::cosTheta(bRec.wo) <= 0)
			return Spectrum(0.0f);

        Spectrum F = /*fresnelConductorExact(dot(bRec.wi, m),
            m_eta, m_k) */ m_specularReflectance->eval(bRec.its);

		Float weight;
		if (m_sampleVisible) {
			weight = distr.smithG1(bRec.wo, m);
		} else {
			weight = distr.eval(m) * distr.G(bRec.wi, bRec.wo, m)
				* dot(bRec.wi, m) / (pdf * Frame::cosTheta(bRec.wi));
		}

		/* Jacobian of the half-direction mapping */
		pdf /= 4.0f * dot(bRec.wo, m);

		return F * weight;
	}

	void addChild(const std::string &name, ConfigurableObject *child) {
		if (child->getClass()->derivesFrom(MTS_CLASS(Texture))) {
			if (name == "alpha")
				m_alphaU = m_alphaV = static_cast<Texture *>(child);
			else if (name == "alphaU")
				m_alphaU = static_cast<Texture *>(child);
			else if (name == "alphaV")
				m_alphaV = static_cast<Texture *>(child);
            else if (name == "diffuseReflectance")
                m_diffuseReflectance = static_cast<Texture *>(child);
			else if (name == "specularReflectance")
				m_specularReflectance = static_cast<Texture *>(child);
            else if (name == "sggxSigma")
                m_sggx_sigma = static_cast<Texture *>(child);
            else if (name == "sggxR")
                m_sggx_r = static_cast<Texture *>(child);

			else
				BSDF::addChild(name, child);
		} else {
			BSDF::addChild(name, child);
		}
	}

	Float getRoughness(const Intersection &its, int component) const {
		return 0.5f * (m_alphaU->eval(its).average()
			+ m_alphaV->eval(its).average());
	}

	std::string toString() const {
		std::ostringstream oss;
        oss << "Ellipsoid[" << endl
			<< "  id = \"" << getID() << "\"," << endl
			<< "  distribution = " << MicrofacetDistribution::distributionName(m_type) << "," << endl
			<< "  sampleVisible = " << m_sampleVisible << "," << endl
			<< "  alphaU = " << indent(m_alphaU->toString()) << "," << endl
			<< "  alphaV = " << indent(m_alphaV->toString()) << "," << endl
            << "  diffuseReflectance = " << indent(m_diffuseReflectance->toString()) << "," << endl
			<< "  specularReflectance = " << indent(m_specularReflectance->toString()) << "," << endl
			<< "  eta = " << m_eta.toString() << "," << endl
			<< "  k = " << m_k.toString() << endl
			<< "]";
		return oss.str();
	}

	Shader *createShader(Renderer *renderer) const;

	MTS_DECLARE_CLASS()
private:
	MicrofacetDistribution::EType m_type;
    ref<Texture> m_diffuseReflectance;
	ref<Texture> m_specularReflectance;
	ref<Texture> m_alphaU, m_alphaV;
    ref<Texture> m_sggx_sigma;
    ref<Texture> m_sggx_r;
	bool m_sampleVisible;
	Spectrum m_eta, m_k;
};

/**
 * GLSL port of the rough conductor shader. This version is much more
 * approximate -- it only supports the Ashikhmin-Shirley distribution,
 * does everything in RGB, and it uses the Schlick approximation to the
 * Fresnel reflectance of conductors. When the roughness is lower than
 * \alpha < 0.2, the shader clamps it to 0.2 so that it will still perform
 * reasonably well in a VPL-based preview.
 */
class EllipsoidShader : public Shader {
public:
    EllipsoidShader(Renderer *renderer, const Texture *diffuseReflectance,
			const Texture *alphaU, const Texture *alphaV, const Spectrum &eta,
			const Spectrum &k) : Shader(renderer, EBSDFShader),
            m_diffuseReflectance(diffuseReflectance), m_alphaU(alphaU), m_alphaV(alphaV){
        m_diffuseReflectanceShader = renderer->registerShaderForResource(m_diffuseReflectance.get());
		m_alphaUShader = renderer->registerShaderForResource(m_alphaU.get());
		m_alphaVShader = renderer->registerShaderForResource(m_alphaV.get());

		/* Compute the reflectance at perpendicular incidence */
		m_R0 = fresnelConductorExact(1.0f, eta, k);
	}

	bool isComplete() const {
        return m_diffuseReflectanceShader.get() != NULL &&
			   m_alphaUShader.get() != NULL &&
			   m_alphaVShader.get() != NULL;
	}

	void putDependencies(std::vector<Shader *> &deps) {
        deps.push_back(m_diffuseReflectanceShader.get());
		deps.push_back(m_alphaUShader.get());
		deps.push_back(m_alphaVShader.get());
	}

	void cleanup(Renderer *renderer) {
        renderer->unregisterShaderForResource(m_diffuseReflectance.get());
		renderer->unregisterShaderForResource(m_alphaU.get());
		renderer->unregisterShaderForResource(m_alphaV.get());
	}

	void resolve(const GPUProgram *program, const std::string &evalName, std::vector<int> &parameterIDs) const {
		parameterIDs.push_back(program->getParameterID(evalName + "_R0", false));
	}

	void bind(GPUProgram *program, const std::vector<int> &parameterIDs, int &textureUnitOffset) const {
		program->setParameter(parameterIDs[0], m_R0);
	}

	void generateCode(std::ostringstream &oss,
			const std::string &evalName,
			const std::vector<std::string> &depNames) const {
		oss << "uniform vec3 " << evalName << "_R0;" << endl
			<< endl
			<< "float " << evalName << "_D(vec3 m, float alphaU, float alphaV) {" << endl
			<< "    float ct = cosTheta(m), ds = 1-ct*ct;" << endl
			<< "    if (ds <= 0.0)" << endl
			<< "        return 0.0f;" << endl
			<< "    alphaU = 2 / (alphaU * alphaU) - 2;" << endl
			<< "    alphaV = 2 / (alphaV * alphaV) - 2;" << endl
			<< "    float exponent = (alphaU*m.x*m.x + alphaV*m.y*m.y)/ds;" << endl
			<< "    return sqrt((alphaU+2) * (alphaV+2)) * 0.15915 * pow(ct, exponent);" << endl
			<< "}" << endl
			<< endl
			<< "float " << evalName << "_G(vec3 m, vec3 wi, vec3 wo) {" << endl
			<< "    if ((dot(wi, m) * cosTheta(wi)) <= 0 || " << endl
			<< "        (dot(wo, m) * cosTheta(wo)) <= 0)" << endl
			<< "        return 0.0;" << endl
			<< "    float nDotM = cosTheta(m);" << endl
			<< "    return min(1.0, min(" << endl
			<< "        abs(2 * nDotM * cosTheta(wo) / dot(wo, m))," << endl
			<< "        abs(2 * nDotM * cosTheta(wi) / dot(wi, m))));" << endl
			<< "}" << endl
			<< endl
			<< "vec3 " << evalName << "_schlick(float ct) {" << endl
			<< "    float ctSqr = ct*ct, ct5 = ctSqr*ctSqr*ct;" << endl
			<< "    return " << evalName << "_R0 + (vec3(1.0) - " << evalName << "_R0) * ct5;" << endl
			<< "}" << endl
			<< endl
			<< "vec3 " << evalName << "(vec2 uv, vec3 wi, vec3 wo) {" << endl
			<< "   if (cosTheta(wi) <= 0 || cosTheta(wo) <= 0)" << endl
			<< "    	return vec3(0.0);" << endl
			<< "   vec3 H = normalize(wi + wo);" << endl
			<< "   vec3 reflectance = " << depNames[0] << "(uv);" << endl
			<< "   float alphaU = max(0.2, " << depNames[1] << "(uv).r);" << endl
			<< "   float alphaV = max(0.2, " << depNames[2] << "(uv).r);" << endl
			<< "   float D = " << evalName << "_D(H, alphaU, alphaV)" << ";" << endl
			<< "   float G = " << evalName << "_G(H, wi, wo);" << endl
			<< "   vec3 F = " << evalName << "_schlick(1-dot(wi, H));" << endl
			<< "   return reflectance * F * (D * G / (4*cosTheta(wi)));" << endl
			<< "}" << endl
			<< endl
			<< "vec3 " << evalName << "_diffuse(vec2 uv, vec3 wi, vec3 wo) {" << endl
			<< "    if (cosTheta(wi) < 0.0 || cosTheta(wo) < 0.0)" << endl
			<< "    	return vec3(0.0);" << endl
			<< "    return " << evalName << "_R0 * inv_pi * inv_pi * cosTheta(wo);"<< endl
			<< "}" << endl;
	}
	MTS_DECLARE_CLASS()
private:

    ref<const Texture> m_diffuseReflectance;
	ref<const Texture> m_alphaU;
	ref<const Texture> m_alphaV;


    ref<Shader> m_diffuseReflectanceShader;
	ref<Shader> m_alphaUShader;
	ref<Shader> m_alphaVShader;
	Spectrum m_R0;
};

Shader *Ellipsoid::createShader(Renderer *renderer) const {
    return new EllipsoidShader(renderer,
        m_diffuseReflectance.get(), m_alphaU.get(), m_alphaV.get(), m_eta, m_k);
}

MTS_IMPLEMENT_CLASS(EllipsoidShader, false, Shader)
MTS_IMPLEMENT_CLASS_S(Ellipsoid, false, BSDF)
MTS_EXPORT_PLUGIN(Ellipsoid, "Ellipsoid BRDF");
MTS_NAMESPACE_END
