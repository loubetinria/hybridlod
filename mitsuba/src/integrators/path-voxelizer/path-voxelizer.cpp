/*
    This file is part of Mitsuba, a physically based rendering system.

    Copyright (c) 2007-2014 by Wenzel Jakob and others.

    Mitsuba is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    Mitsuba is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <mitsuba/render/scene.h>
#include <mitsuba/core/statistics.h>
#include <mitsuba/core/sggx.h>
#include <mitsuba/core/warp.h>
#include <mitsuba/render/texture.h>
#include <mitsuba/hw/basicshader.h>
#include <mitsuba/render/bsdf.h>

#include <boost/lexical_cast.hpp>
#include <boost/thread.hpp>
#include <boost/filesystem.hpp>
#include <ios>
#include <fstream>
#include <iostream>
#include <string>

// Number of rays sampled per voxel for estimating
// local opacity and average reflectance parameters
#define NB_SAMPLES_DIR 200

MTS_NAMESPACE_BEGIN

static StatsCounter avgPathLength("Path tracer", "Average path length", EAverage);
/*
    Thia hacky integrator voxelized the scene by casting many rays and collecting
    data (reflectance, normals). Ideally it should not be implemented here as
    an integrator.

    The collected voxel data is writen in files (.voxels) that are used by other
    tools of the hybrid lod pipeline.
 */
class MIPathTracerVoxelizer : public MonteCarloIntegrator {
public:
    MIPathTracerVoxelizer(const Properties &props)
        : MonteCarloIntegrator(props) {

        m_resolution = props.getSize("resolution", 100);
        m_gridSize[0] = props.getSize("gridX", m_resolution);
        m_gridSize[1] = props.getSize("gridY", m_resolution);
        m_gridSize[2] = props.getSize("gridZ", m_resolution);
        m_voxelSide = props.getFloat("voxelSide", 1.0);
        m_minCorner = props.getVector("minCorner", Vector3f(0.0));

        m_diffuse_front = new ConstantSpectrumTexture(
            props.getSpectrum("diffuse_front", Spectrum(1.0f)));

        m_specular_front = new ConstantSpectrumTexture(
            props.getSpectrum("specular_front", Spectrum(0.0f)));

        m_sggx_sigma_front = new ConstantSpectrumTexture(
                    props.getSpectrum("sggx_sigma_front", Spectrum(1.0f)));

        m_sggx_r_front = new ConstantSpectrumTexture(
                    props.getSpectrum("sggx_r_front", Spectrum(0.5)));

        std::ostringstream convert;
        convert << "Resolution: " << m_resolution << ", ";
        convert << "Grid: " << m_gridSize.toString() << ", ";
        convert << "voxelSide: " << m_voxelSide << ".";


        Log(EInfo, convert.str().c_str());

    }

	/// Unserialize from a binary data stream
    MIPathTracerVoxelizer(Stream *stream, InstanceManager *manager)
        : MonteCarloIntegrator(stream, manager) {
        m_resolution = stream->readSize();
        m_gridSize[0] = stream->readInt();
        m_gridSize[1] = stream->readInt();
        m_gridSize[2] = stream->readInt();
        m_voxelSide = stream->readFloat();
        m_minCorner[0] = stream->readFloat();
        m_minCorner[1] = stream->readFloat();
        m_minCorner[2] = stream->readFloat();
    }

    void serialize(Stream *stream, InstanceManager *manager) const {
        MonteCarloIntegrator::serialize(stream, manager);
        stream->writeSize(m_resolution);
        stream->writeInt(m_gridSize[0]);
        stream->writeInt(m_gridSize[1]);
        stream->writeInt(m_gridSize[2]);
        stream->writeFloat(m_voxelSide);
        stream->writeFloat(m_minCorner[0]);
        stream->writeFloat(m_minCorner[1]);
        stream->writeFloat(m_minCorner[2]);
    }

    void addChild(const std::string &name, ConfigurableObject *child) {
        if (child->getClass()->derivesFrom(MTS_CLASS(Texture))
                && (name == "diffuse_front")) {
            m_diffuse_front = static_cast<Texture *>(child);
        } else if (child->getClass()->derivesFrom(MTS_CLASS(Texture))
                && (name == "specular_front")) {
            m_specular_front = static_cast<Texture *>(child);
        } else if (child->getClass()->derivesFrom(MTS_CLASS(Texture))
                && (name == "sggx_sigma_front")) {
            m_sggx_sigma_front = static_cast<Texture *>(child);
        } else if (child->getClass()->derivesFrom(MTS_CLASS(Texture))
                && (name == "sggx_r_front")) {
            m_sggx_r_front = static_cast<Texture *>(child);
        }
    }

    // Sample a ray around a voxel center for occlusion estimation.
    // They are various ways to do this, some have better properties,
    // see details in the paper.
    RayDifferential sampleRayKernel(Point3f voxelCenter, Sampler *sampler, 
                                    float voxelSize, float time) const {

        // Sample random directions
        Vector3f dir = warp::squareToUniformSphere(sampler->next2D());

        // Sample ray origin in the convolution of the uniform pdf
        // on some cube and a 3D gaussian.
        Point3f middle(sampler->next1D()-0.5,sampler->next1D()-0.5,sampler->next1D()-0.5);
        float stddev = 0.6;
        Point2 gauss1 = warp::squareToStdNormal(sampler->next2D()) * stddev;
        Point2 gauss2 = warp::squareToStdNormal(sampler->next2D()) * stddev;
        Point3f gauss(gauss1[0],gauss1[1], gauss2[0]);
        return RayDifferential(voxelCenter + (middle+gauss)*voxelSize, dir, time);
    }


	Spectrum Li(const RayDifferential &r, RadianceQueryRecord &rRec) const {

		/* Some aliases and local variables */
		const Scene *scene = rRec.scene;
        int nbSample = NB_SAMPLES_DIR;

        /* get bounding box */
        //Vector3f boxSides = Vector3f(m_gridSize)*m_voxelSide;

        // This integrator is hacky, it requires that the spp is 1
        // and the resolution of the film should match the X and Y 
        // dimensions of the volume grid. Each pixel correspond
        // to a row of the 3D grid.
        Point2i pixel = r.pixel;
        if (! (pixel[0] < m_gridSize[0] && pixel[1] < m_gridSize[1])) {
            Log(EError, "Problem, pixels should correspond to X and Y dims");
        }

        std::vector<float> densityMean_vec;
        std::vector<Point3i> voxelId_vec;
        std::vector<Spectrum> diffuse_front_vec;
        std::vector<Spectrum> specular_front_vec;
        std::vector<Vector3f> sggx_diag_front_vec;
        std::vector<Vector3f> sggx_tri_front_vec;

        /* Loop on the voxels for this pixel */
        for (int voxel = 0; voxel < m_gridSize[2]; voxel++) {
        
            Point3i voxelId;
            voxelId.x = pixel[0];
            voxelId.y = pixel[1];
            voxelId.z = voxel;

            Point3f voxelCenter(m_minCorner.x + voxelId.x * m_voxelSide + m_voxelSide/2.0,
                                m_minCorner.y + voxelId.y * m_voxelSide + m_voxelSide/2.0,
                                m_minCorner.z + voxelId.z * m_voxelSide + m_voxelSide/2.0);

            Intersection &its = rRec.its;
            float opacityMean = 0.0;
            bool foundAtLeastOneInVoxel = false;
            std::vector<SGGX> SGGX_vec;
            std::vector<Float> SGGX_vec_weights;
            Float ray_length = m_voxelSide; // radius x 2

            Spectrum diffuse_front(0.0);
            int norm_diffuse_front = 0;

            Spectrum specular_front(0.0);
            int norm_specular_front = 0;

            // Sampling

            for (int i = 0; i < nbSample; i++) {

                float LDO_voxel = 0.0;

                RayDifferential ray_i = sampleRayKernel(voxelCenter,rRec.sampler, m_voxelSide, r.time);
                ray_i.maxt = ray_length;

                if (scene->rayIntersect(ray_i, its)) {
                    foundAtLeastOneInVoxel = true;
                    LDO_voxel += 1.0;

                    // Gathering diffuse
                    diffuse_front += m_diffuse_front->eval(its, false);
                    Spectrum s(m_diffuse_front->eval(its, false));
                    float R, G, B;
                    s.toLinearRGB(R,G,B);
                    norm_diffuse_front++;

                    // Gathering specular
                    specular_front += m_specular_front->eval(its, false);
                    norm_specular_front++;

                    // Gathering SGGX
                    Spectrum sigma = m_sggx_sigma_front->eval(its, false);
                    Spectrum r = m_sggx_r_front->eval(its, false);
                    SGGX sss(sigma,r);

                    SGGX_vec.push_back(sss);
                    SGGX_vec_weights.push_back(1.0);

                }

                opacityMean += LDO_voxel;

                if (float(i) > float(nbSample)*0.5  && !foundAtLeastOneInVoxel)
                    break; // stop sampling this voxel seems empty.

            }

            opacityMean /= Float (nbSample);

            if (!foundAtLeastOneInVoxel) {
                if (voxel+1 == m_gridSize[2])
                    recordSample(voxelId_vec,
                                 densityMean_vec,
                                 diffuse_front_vec,
                                 specular_front_vec,
                                 sggx_diag_front_vec,
                                 sggx_tri_front_vec);
                continue; // next voxel;
            }

            if (norm_diffuse_front > 0) {
                diffuse_front /= float(norm_diffuse_front);
            } else {
                diffuse_front = Spectrum(0.0);
            }

            if (norm_specular_front > 0) {
                specular_front /= norm_specular_front;
            } else {
                specular_front = Spectrum(0.0);
            }

            SGGX filteredSGGX;
            filteredSGGX.fromFilteredSGGX(SGGX_vec, SGGX_vec_weights);
            filteredSGGX.normalizeSGGX();

            Float densityMean = filteredSGGX.optimalDensity2(1.0 - opacityMean*0.9999, rRec.sampler);
            densityMean /= ray_length;

            densityMean_vec.push_back(densityMean);

            sggx_diag_front_vec.push_back(filteredSGGX.m_diag);
            sggx_tri_front_vec.push_back(filteredSGGX.m_tri);
            diffuse_front_vec.push_back(diffuse_front);
            specular_front_vec.push_back(specular_front);
            voxelId_vec.push_back(voxelId);

            if (densityMean_vec.size() > 1000 || voxel+1 == m_gridSize[2]) {
                recordSample(voxelId_vec,
                             densityMean_vec,
                             diffuse_front_vec,
                             specular_front_vec,
                             sggx_diag_front_vec,
                             sggx_tri_front_vec);
            }

        }
        return Spectrum(0.0);
	}

	inline Float miWeight(Float pdfA, Float pdfB) const {
		pdfA *= pdfA;
		pdfB *= pdfB;
		return pdfA / (pdfA + pdfB);
	}

	std::string toString() const {
		std::ostringstream oss;
        oss << "MIPathTracerVoxelizer[" << endl
			<< "  maxDepth = " << m_maxDepth << "," << endl
			<< "  rrDepth = " << m_rrDepth << "," << endl
			<< "  strictNormals = " << m_strictNormals << endl
			<< "]";
		return oss.str();
	}

    // Wants a non null vector
    Vector ortho(Vector vec) const {
        if (vec.x != 0.0 || vec.y != 0) {
            return normalize(Vector(-vec.y, vec.x, 0.0));
        } else {
            return normalize(Vector(0.0, -vec.z, vec.y));
        }
    }

    void recordSample(std::vector<Point3i> &voxelId_vec,
                      std::vector<float> &densityMean_vec,
                      std::vector<Spectrum> &diffuse_front_vec,
                      std::vector<Spectrum> &specular_front_vec,
                      std::vector<Vector3f> &sggx_diag_front_vec,
                      std::vector<Vector3f> &sggx_tri_front_vec) const {

        boost::filesystem::create_directory("voxelized");

        std::stringstream file;
        file << "voxelized/";
        file << getThreadId() << ".voxels";

        if (voxelId_vec.size() != densityMean_vec.size()
           || voxelId_vec.size() != diffuse_front_vec.size()
           || voxelId_vec.size() != specular_front_vec.size()
           || voxelId_vec.size() != sggx_diag_front_vec.size()
           || voxelId_vec.size() != sggx_tri_front_vec.size()) {

           Log(EError, "Wrong size here!");
        }

        FILE* pFile;
        pFile = fopen(file.str().c_str(), "a");

        if (pFile == nullptr) {
            SLog(EError, "Unable to open file for saving voxel data, file %s", file.str().c_str());
        }

        for (unsigned data = 0; data < voxelId_vec.size(); data++) {
            float x, y, z;
            std::vector<float> toWrite;

            toWrite.push_back(voxelId_vec[data][0]);
            toWrite.push_back(voxelId_vec[data][1]);
            toWrite.push_back(voxelId_vec[data][2]);

            toWrite.push_back(densityMean_vec[data]);

            diffuse_front_vec[data].toLinearRGB(x,y,z);
            toWrite.push_back(x);
            toWrite.push_back(y);
            toWrite.push_back(z);

            specular_front_vec[data].toLinearRGB(x,y,z);
            toWrite.push_back(x);
            toWrite.push_back(y);
            toWrite.push_back(z);

            // write SGGX
            toWrite.push_back(sggx_diag_front_vec[data][0]);
            toWrite.push_back(sggx_diag_front_vec[data][1]);
            toWrite.push_back(sggx_diag_front_vec[data][2]);
            toWrite.push_back(sggx_tri_front_vec[data][0]);
            toWrite.push_back(sggx_tri_front_vec[data][1]);
            toWrite.push_back(sggx_tri_front_vec[data][2]);

            size_t sz = toWrite.size();
            fwrite(reinterpret_cast<const char*>(&toWrite[0]), 1, sz*sizeof(float), pFile);
        }
        fclose(pFile);

        voxelId_vec.clear();
        densityMean_vec.clear();
        diffuse_front_vec.clear();
        specular_front_vec.clear();
        sggx_diag_front_vec.clear();
        sggx_tri_front_vec.clear();
    }

    unsigned long getThreadId() const {
        std::string threadId = boost::lexical_cast<std::string>(boost::this_thread::get_id());
        unsigned long threadNumber = 0;
        sscanf(threadId.c_str(), "%lx", &threadNumber);
        return threadNumber;
    }


    MTS_DECLARE_CLASS()

private:
    int m_resolution;
    Vector3i m_gridSize;
    float m_voxelSide;
    Vector3f m_minCorner;

    ref<Texture> m_diffuse_front;
    ref<Texture> m_specular_front;
    ref<Texture> m_sggx_sigma_front;
    ref<Texture> m_sggx_r_front;


};

MTS_IMPLEMENT_CLASS_S(MIPathTracerVoxelizer, false, MonteCarloIntegrator)
MTS_EXPORT_PLUGIN(MIPathTracerVoxelizer, "Hybrid LOD voxelizer");
MTS_NAMESPACE_END
