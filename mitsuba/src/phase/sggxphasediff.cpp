/*
    This file is part of Mitsuba, a physically based rendering system.

    Copyright (c) 2007-2014 by Wenzel Jakob and others.

    Mitsuba is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License Version 3
    as published by the Free Software Foundation.

    Mitsuba is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <mitsuba/core/frame.h>
#include <mitsuba/render/phase.h>
#include <mitsuba/render/medium.h>
#include <mitsuba/render/sampler.h>

/// The following file implements the micro-flake distribution
/// for rough fibers

MTS_NAMESPACE_BEGIN


class SGGXDiffusePhaseFunction : public PhaseFunction {
public:
    SGGXDiffusePhaseFunction(const Properties &props) : PhaseFunction(props) {
	}

    SGGXDiffusePhaseFunction(Stream *stream, InstanceManager *manager)
		: PhaseFunction(stream, manager) {
		configure();
	}

    virtual ~SGGXDiffusePhaseFunction() { }

	void configure() {
		PhaseFunction::configure();
	}

	void serialize(Stream *stream, InstanceManager *manager) const {
		PhaseFunction::serialize(stream, manager);
	}

	Float eval(const PhaseFunctionSamplingRecord &pRec) const {
        Log(EError, "The SGGX microflakes distribution needs a sampler for eval()");
        return 0.0;
	}

    Float eval(const PhaseFunctionSamplingRecord &pRec, Sampler *sampler) const {
        Point2 sample3(sampler->next2D());
        float pdf =  pRec.mRec.sggx.eval_diffuse(pRec.wi, pRec.wo, sample3[0], sample3[1]);
        //float pdf =  pRec.mRec.sggx.eval_specular(pRec.wi, pRec.wo);
        return pdf;
    }

	inline Float sample(PhaseFunctionSamplingRecord &pRec, Sampler *sampler) const {
        Point2 sample(sampler->next2D());
        Point2 sample2(sampler->next2D());
        pRec.wo = pRec.mRec.sggx.sample_diffuse(pRec.wi, sample[0], sample[1], sample2[0], sample2[1]);
        //pRec.wo = pRec.mRec.sggx.sample_specular(pRec.wi, sample[0], sample[1]);
		return 1.0f;
	}

    Float sample(PhaseFunctionSamplingRecord &pRec,
			Float &pdf, Sampler *sampler) const {
        Point2 sample(sampler->next2D());
        Point2 sample2(sampler->next2D());
        Point2 sample3(sampler->next2D());
        pRec.wo = pRec.mRec.sggx.sample_diffuse(pRec.wi, sample[0], sample[1], sample2[0], sample2[1]);
        pdf =  pRec.mRec.sggx.eval_diffuse(pRec.wi, pRec.wo, sample3[0], sample3[1]);
        //pRec.wo = pRec.mRec.sggx.sample_specular(pRec.wi, sample[0], sample[1]);
        //pdf =  pRec.mRec.sggx.eval_specular(pRec.wi, pRec.wo);
        return 1.0;
    }
    
    Float pdf(const PhaseFunctionSamplingRecord &pRec) const {
        return eval(pRec);
    }

    Float pdf(const PhaseFunctionSamplingRecord &pRec, Sampler *sampler) const {
        return eval(pRec, sampler);
    }


	bool needsDirectionallyVaryingCoefficients() const { return true; }

	std::string toString() const {
		std::ostringstream oss;
        oss << "SGGXDiffusePhaseFunction[" << endl
			<< "]";
		return oss.str();
	}

	MTS_DECLARE_CLASS()
private:

};

MTS_IMPLEMENT_CLASS_S(SGGXDiffusePhaseFunction, false, PhaseFunction)
MTS_EXPORT_PLUGIN(SGGXDiffusePhaseFunction, "Microflake SGGX phase function");
MTS_NAMESPACE_END
