#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <vector>
#include <assert.h>
#include <limits>
#include <string>
#include <fstream>
#include <cstdio>
#include <cstdlib>

#include <boost/thread.hpp>
#include <iostream>

void readFloatFile(const char * file, float **data, int &gridX, int &gridY, int &gridZ,
                   float &minX, float &minY, float &minZ,
                   float &maxX, float &maxY, float &maxZ) {
    FILE *pFile;

    pFile=fopen(file,"rb");
    assert(pFile);

    if (pFile) {

        // read header
        int V, O, L, chanels;

        fread(reinterpret_cast<char*>(&V), 1, sizeof(char), pFile);
        fread(reinterpret_cast<char*>(&O), 1, sizeof(char), pFile);
        fread(reinterpret_cast<char*>(&L), 1, sizeof(char), pFile);

        // Byte 4 File format version number (currently 3)
        int version;
        fread(reinterpret_cast<char*>(&version), 1, sizeof(char), pFile);

        // Bytes 5-8 Encoding identifier (32-bit integer). The following choices are available:
        //  1. Dense float32-based representation
        //  2. Dense float16-based representation (currently not supported by this implementation)
        //  3. Dense uint8-based representation (The range 0..255 will be mapped to 0..1)
        //  4. Dense quantized directions. The directions are stored in spherical coordinates with a total storage cost of 16 bit per entry.
        int type; //float32
        fread(&type, 1, sizeof(int), pFile);

        // Bytes 9-12 Number of cells along the X axis (32 bit integer)
        // Bytes 13-16 Number of cells along the Y axis (32 bit integer)
        // Bytes 17-20 Number of cells along the Z axis (32 bit integer)
        fread(&gridX, 1, sizeof(int), pFile);
        fread(&gridY, 1, sizeof(int), pFile);
        fread(&gridZ, 1, sizeof(int), pFile);

        //std::cout << "Grid size: " << gridX << " " << gridY << " " << gridZ << std::endl;

        // Bytes 21-24 Number of channels (32 bit integer, supported values: 1 or 3)
        fread(&chanels, 1, sizeof(int), pFile);
        assert(chanels == 1);

        // Bytes 25-48 Axis-aligned bounding box of the data stored in single precision (order: xmin, ymin, zmin, xmax, ymax, zmax)
        // bbox
        fread(&minX, 1, sizeof(float), pFile);
        fread(&minY, 1, sizeof(float), pFile);
        fread(&minZ, 1, sizeof(float), pFile);
        fread(&maxX, 1, sizeof(float), pFile);
        fread(&maxY, 1, sizeof(float), pFile);
        fread(&maxZ, 1, sizeof(float), pFile);

        (*data) = new float[gridX*gridY*gridZ];
        /*
        for (int z = 0; z < gridZ; z++) {
            for (int y = 0; y < gridY; y++) {
                for (int x = 0; x < gridX; x++) {
                    float toRead;
                    fread(&toRead, 1, sizeof(float), pFile);
                    (*data)[x + y * gridX + z * gridX * gridY] = toRead;
                    //std::cout << (*data)[x + y * gridX + z * gridX * gridY];
                }
            }
        }*/
        fread((*data), gridX * gridY * gridZ, sizeof(float), pFile);

    }
    fclose(pFile);
}

void readChar3File(const char * file, unsigned char **data, int &gridX, int &gridY, int &gridZ,
                    float &minX, float &minY, float &minZ,
                    float &maxX, float &maxY, float &maxZ) {
    FILE *pFile;

    pFile=fopen(file,"rb");
    assert(pFile);

    if (pFile) {

        // read header
        int V, O, L, chanels;

        fread(reinterpret_cast<char*>(&V), 1, sizeof(char), pFile);
        fread(reinterpret_cast<char*>(&O), 1, sizeof(char), pFile);
        fread(reinterpret_cast<char*>(&L), 1, sizeof(char), pFile);

        // Byte 4 File format version number (currently 3)
        int version;
        fread(reinterpret_cast<char*>(&version), 1, sizeof(char), pFile);

        // Bytes 5-8 Encoding identifier (32-bit integer). The following choices are available:
        //  1. Dense float32-based representation
        //  2. Dense float16-based representation (currently not supported by this implementation)
        //  3. Dense uint8-based representation (The range 0..255 will be mapped to 0..1)
        //  4. Dense quantized directions. The directions are stored in spherical coordinates with a total storage cost of 16 bit per entry.
        int type; //float32
        fread(&type, 1, sizeof(int), pFile);

        // Bytes 9-12 Number of cells along the X axis (32 bit integer)
        // Bytes 13-16 Number of cells along the Y axis (32 bit integer)
        // Bytes 17-20 Number of cells along the Z axis (32 bit integer)
        fread(&gridX, 1, sizeof(int), pFile);
        fread(&gridY, 1, sizeof(int), pFile);
        fread(&gridZ, 1, sizeof(int), pFile);


        //std::cout << "Grid size: " << grid_size << std::endl;

        // Bytes 21-24 Number of channels (32 bit integer, supported values: 1 or 3)
        fread(&chanels, 1, sizeof(int), pFile);
        assert(chanels == 3);

        // Bytes 25-48 Axis-aligned bounding box of the data stored in single precision (order: xmin, ymin, zmin, xmax, ymax, zmax)
        // bbox
        fread(&minX, 1, sizeof(float), pFile);
        fread(&minY, 1, sizeof(float), pFile);
        fread(&minZ, 1, sizeof(float), pFile);
        fread(&maxX, 1, sizeof(float), pFile);
        fread(&maxY, 1, sizeof(float), pFile);
        fread(&maxZ, 1, sizeof(float), pFile);

        (*data) = new unsigned char[gridX*gridY*gridZ*3];
        /*
        for (int z = 0; z < gridZ; z++)
            for (int y = 0; y < gridY; y++) {
                for (int x = 0; x < gridX; x++) {
                    int id = x + y * gridX + z * gridX * gridY;
                    fread(&((*data_1)[id]), 1, sizeof(float), pFile);
                    fread(&((*data_2)[id]), 1, sizeof(float), pFile);
                    fread(&((*data_3)[id]), 1, sizeof(float), pFile);
                }
            }
        */
        fread((*data), gridX * gridY * gridZ * 3, sizeof(unsigned char), pFile);

    }
    fclose(pFile);
}

void writeChar3File(const char * file, unsigned char **data, int gridX, int gridY, int gridZ,
                    float minX, float minY, float minZ,
                    float maxX, float maxY, float maxZ) {
    FILE *pFile;

    std::remove(file);

    pFile=fopen(file,"a");
    assert(pFile);

    if (pFile) {

        // Bytes 1-3 ASCII Bytes ’V’, ’O’, and ’L’
        int V = 86, O = 79, L = 76;
        fwrite(reinterpret_cast<const char*>(&V), 1, sizeof(char), pFile);
        fwrite(reinterpret_cast<const char*>(&O), 1, sizeof(char), pFile);
        fwrite(reinterpret_cast<const char*>(&L), 1, sizeof(char), pFile);

        // Byte 4 File format version number (currently 3)
        int version = 3;
        fwrite(reinterpret_cast<const char*>(&version), 1, sizeof(char), pFile);

        // Bytes 5-8 Encoding identifier (32-bit integer). The following choices are available:
        //  1. Dense float32-based representation
        //  2. Dense float16-based representation (currently not supported by this implementation)
        //  3. Dense uint8-based representation (The range 0..255 will be mapped to 0..1)
        //  4. Dense quantized directions. The directions are stored in spherical coordinates with a total storage cost of 16 bit per entry.
        int type = 3; //uint8
        fwrite(reinterpret_cast<const char*>(&type), 1, sizeof(int), pFile);

        // Bytes 9-12 Number of cells along the X axis (32 bit integer)
        // Bytes 13-16 Number of cells along the Y axis (32 bit integer)
        // Bytes 17-20 Number of cells along the Z axis (32 bit integer)
        fwrite(reinterpret_cast<const char*>(&gridX), 1, sizeof(int), pFile);
        fwrite(reinterpret_cast<const char*>(&gridY), 1, sizeof(int), pFile);
        fwrite(reinterpret_cast<const char*>(&gridZ), 1, sizeof(int), pFile);

        int chanels = 3;
        // Bytes 21-24 Number of channels (32 bit integer, supported values: 1 or 3)
        fwrite(reinterpret_cast<const char*>(&chanels), 1, sizeof(int), pFile);

        // Bytes 25-48 Axis-aligned bounding box of the data stored in single precision (order: xmin, ymin, zmin, xmax, ymax, zmax)
        // bbox
        fwrite(reinterpret_cast<const char*>(&minX), 1, sizeof(int), pFile);
        fwrite(reinterpret_cast<const char*>(&minY), 1, sizeof(int), pFile);
        fwrite(reinterpret_cast<const char*>(&minZ), 1, sizeof(int), pFile);
        fwrite(reinterpret_cast<const char*>(&maxX), 1, sizeof(int), pFile);
        fwrite(reinterpret_cast<const char*>(&maxY), 1, sizeof(int), pFile);
        fwrite(reinterpret_cast<const char*>(&maxZ), 1, sizeof(int), pFile);

        /*
        for (int z = 0; z < gridZ; z++) {
            for (int y = 0; y < gridY; y++) {
                for (int x = 0; x < gridX; x++) {
                    int id = x + y * gridX + z * gridX * gridY;
                    float toWrite1 = (*data_1)[id];
                    float toWrite2 = (*data_2)[id];
                    float toWrite3 = (*data_3)[id];
                    fwrite(reinterpret_cast<const char*>(&toWrite1),    1, sizeof(float), pFile);
                    fwrite(reinterpret_cast<const char*>(&toWrite2),    1, sizeof(float), pFile);
                    fwrite(reinterpret_cast<const char*>(&toWrite3),    1, sizeof(float), pFile);
                }
            }
        }
        */
        fwrite(reinterpret_cast<const char*>(*data),    gridX * gridY * gridZ * 3, sizeof(char), pFile);

    }
    fclose(pFile);
}

void writeFloatFile(const char * file, float **data, int gridX, int gridY, int gridZ,
                    float minX, float minY, float minZ,
                    float maxX, float maxY, float maxZ) {
    FILE *pFile;

    std::remove(file);

    pFile=fopen(file,"a");
    assert(pFile);

    if (pFile) {

        // Bytes 1-3 ASCII Bytes ’V’, ’O’, and ’L’
        int V = 86, O = 79, L = 76;
        fwrite(reinterpret_cast<const char*>(&V), 1, sizeof(char), pFile);
        fwrite(reinterpret_cast<const char*>(&O), 1, sizeof(char), pFile);
        fwrite(reinterpret_cast<const char*>(&L), 1, sizeof(char), pFile);

        // Byte 4 File format version number (currently 3)
        int version = 3;
        fwrite(reinterpret_cast<const char*>(&version), 1, sizeof(char), pFile);

        // Bytes 5-8 Encoding identifier (32-bit integer). The following choices are available:
        //  1. Dense float32-based representation
        //  2. Dense float16-based representation (currently not supported by this implementation)
        //  3. Dense uint8-based representation (The range 0..255 will be mapped to 0..1)
        //  4. Dense quantized directions. The directions are stored in spherical coordinates with a total storage cost of 16 bit per entry.
        int type = 1; //float32
        fwrite(reinterpret_cast<const char*>(&type), 1, sizeof(int), pFile);

        // Bytes 9-12 Number of cells along the X axis (32 bit integer)
        // Bytes 13-16 Number of cells along the Y axis (32 bit integer)
        // Bytes 17-20 Number of cells along the Z axis (32 bit integer)
        fwrite(reinterpret_cast<const char*>(&gridX), 1, sizeof(int), pFile);
        fwrite(reinterpret_cast<const char*>(&gridY), 1, sizeof(int), pFile);
        fwrite(reinterpret_cast<const char*>(&gridZ), 1, sizeof(int), pFile);

        int chanels = 1;
        // Bytes 21-24 Number of channels (32 bit integer, supported values: 1 or 3)
        fwrite(reinterpret_cast<const char*>(&chanels), 1, sizeof(int), pFile);

        // Bytes 25-48 Axis-aligned bounding box of the data stored in single precision (order: xmin, ymin, zmin, xmax, ymax, zmax)
        // bbox
        fwrite(reinterpret_cast<const char*>(&minX), 1, sizeof(int), pFile);
        fwrite(reinterpret_cast<const char*>(&minY), 1, sizeof(int), pFile);
        fwrite(reinterpret_cast<const char*>(&minZ), 1, sizeof(int), pFile);
        fwrite(reinterpret_cast<const char*>(&maxX), 1, sizeof(int), pFile);
        fwrite(reinterpret_cast<const char*>(&maxY), 1, sizeof(int), pFile);
        fwrite(reinterpret_cast<const char*>(&maxZ), 1, sizeof(int), pFile);

        /*
        for (int z = 0; z < gridZ; z++) {
            for (int y = 0; y < gridY; y++) {
                for (int x = 0; x < gridX; x++) {
                    float toWrite = (*data)[x + y * gridX + z * gridX * gridY];
                    fwrite(reinterpret_cast<const char*>(&toWrite),    1, sizeof(float), pFile);
                }
            }
        }*/
        fwrite(reinterpret_cast<const char*>(*data), gridX*gridY*gridZ, sizeof(float), pFile);

    }
    fclose(pFile);
}

void thread_task(int j, int gridX,
                 int gridX_bis,
                 float *data1, float *data2, float *datares, float factor) {
    //std::cout << "I handle " << j*gridZ/8 << " to "<< (j+1)*gridZ/8 << std::endl;
    if (gridX == gridX_bis) {
        assert(gridX == 512);
        for (int x = 0; x < gridX; x++) {
            for (int y = 0; y < gridX; y++) {
                for (int z = j*gridX/8; z < (j+1)*gridX/8; z++) {
                    int id = x + y * gridX + z * gridX * gridX;
                    datares[id] = data2[id]*factor;
                }
            }
        }

    } else {
        assert(gridX_bis*2 == gridX);
        for (int x = 0; x < gridX; x++) {
            for (int y = 0; y < gridX; y++) {
                for (int z = j*gridX/8; z < (j+1)*gridX/8; z++) {
                    int id1 = x + y * gridX + z * gridX * gridX;
                    int id2 = x/2 + (y/2) * gridX/2 + (z/2) * gridX * gridX/4;
                    datares[id1] = data1[id1]*(1.0-factor)+data2[id2]*factor;
                    //std::cout << data1[id1] << std::endl;
                    //std::cout << "x: " << x << " " << x/2 << std::endl;
                    //std::cout << "y: " << y << " " << y/2 << std::endl;
                    //std::cout << "z: " << z << " " << z/2 << std::endl;


                    // TODO REMOVE
                    //data1[id1] = 100.0;
                    //assert(data1[id1] == 2.5);
                }
            }
        }
    }
}

void thread_task3(int j, int gridX,
                  unsigned char *data1, unsigned char *data2,
                  float *density1, float *density2,
                  float factor) {

    //std::cout << "I handle " << j*gridZ/8 << " to "<< (j+1)*gridZ/8 << std::endl;
    for (int x = 0; x < gridX; x++) {
        for (int y = 0; y < gridX; y++) {
            for (int z = j*gridX/8; z < (j+1)*gridX/8; z++) {
                int id1 = x + y * gridX + z * gridX * gridX;
                int id2 = x/2 + (y/2) * gridX/2 + (z/2) * gridX * gridX/4;

                if (density1[id1] == 0) {
                    data1[id1*3] = data2[id2*3];
                    data1[id1*3+1] = data2[id2*3+1];
                    data1[id1*3+2] = data2[id2*3+2];
                } else if (density2[id2] == 0) {
                    // nothing, data already in data1
                } else {
                    // Not very precise interpolation !
                    data1[id1*3] = (unsigned char)(float(data1[id1*3])*(1.0-factor) + float(data2[id2*3])*factor);
                    data1[id1*3+1] = (unsigned char)(float(data1[id1*3+1])*(1.0-factor) + float(data2[id2*3+1])*factor);
                    data1[id1*3+2] = (unsigned char)(float(data1[id1*3+2])*(1.0-factor) + float(data2[id2*3+2])*factor);
                }
                // TODO REMOVE
                //assert(data1[id1*3] == 2.5);
                //assert(data1[id1*3+1] == 2.5);
                //assert(data1[id1*3+2] == 2.5);

                //data1[id1*3] = 3.0;
                //data1[id1*3+1] = 3.0;
                //data1[id1*3+2] = 3.0;

            }
        }
    }
}

void thread_task3_sggx(int j, int gridX,
                  unsigned char *data1_sigma, unsigned char *data2_sigma,
                  unsigned char *data1_r, unsigned char *data2_r,
                  float *density1, float *density2,
                  float factor) {

    //std::cout << "I handle " << j*gridZ/8 << " to "<< (j+1)*gridZ/8 << std::endl;
    for (int x = 0; x < gridX; x++) {
        for (int y = 0; y < gridX; y++) {
            for (int z = j*gridX/8; z < (j+1)*gridX/8; z++) {
                int id1 = x + y * gridX + z * gridX * gridX;
                int id2 = x/2 + (y/2) * gridX/2 + (z/2) * gridX * gridX/4;

                if (density1[id1] == 0 && density2[id2] != 0) {
                    data1_sigma[id1*3] = data2_sigma[id2*3];
                    data1_sigma[id1*3+1] = data2_sigma[id2*3+1];
                    data1_sigma[id1*3+2] = data2_sigma[id2*3+2];
                    data1_r[id1*3] = data2_r[id2*3];
                    data1_r[id1*3+1] = data2_r[id2*3+1];
                    data1_r[id1*3+2] = data2_r[id2*3+2];
                } else if (density1[id1] == 0 && density2[id2] == 0) {
                    data1_sigma[id1*3] = (unsigned char)(254);
                    data1_sigma[id1*3+1] = (unsigned char)(253);
                    data1_sigma[id1*3+2] = (unsigned char)(255);
                    data1_r[id1*3] = (unsigned char)(127);
                    data1_r[id1*3+1] = (unsigned char)(127);
                    data1_r[id1*3+2] = (unsigned char)(128);
                } else if (density2[id2] == 0) {
                    // nothing, data already in data1
                } else {
                    // Not very precise interpolation !
                    // on doit repasser en diag / tri pour interpoler
                    float sigma1_x = float(data1_sigma[id1*3])/255.0;
                    float sigma1_y = float(data1_sigma[id1*3+1])/255.0;
                    float sigma1_z = float(data1_sigma[id1*3+2])/255.0;
                    float r1_xy = (float(data1_r[id1*3])/255.0*2.0-1.0);
                    float r1_xz = (float(data1_r[id1*3+1])/255.0*2.0-1.0);
                    float r1_yz = (float(data1_r[id1*3+2])/255.0*2.0-1.0);
                    float Sxy1 = r1_xy*sigma1_x*sigma1_y;
                    float Sxz1 = r1_xz*sigma1_x*sigma1_z;
                    float Syz1 = r1_yz*sigma1_y*sigma1_z;
                    float Sxx1 = sigma1_x*sigma1_x;
                    float Syy1 = sigma1_y*sigma1_y;
                    float Szz1 = sigma1_z*sigma1_z;
                    float sigma2_x = float(data2_sigma[id2*3])/255.0;
                    float sigma2_y = float(data2_sigma[id2*3+1])/255.0;
                    float sigma2_z = float(data2_sigma[id2*3+2])/255.0;
                    float r2_xy = (float(data2_r[id2*3])/255.0*2.0-1.0);
                    float r2_xz = (float(data2_r[id2*3+1])/255.0*2.0-1.0);
                    float r2_yz = (float(data2_r[id2*3+2])/255.0*2.0-1.0);
                    float Sxy2 = r2_xy*sigma2_x*sigma2_y;
                    float Sxz2 = r2_xz*sigma2_x*sigma2_z;
                    float Syz2 = r2_yz*sigma2_y*sigma2_z;
                    float Sxx2 = sigma2_x*sigma2_x;
                    float Syy2 = sigma2_y*sigma2_y;
                    float Szz2 = sigma2_z*sigma2_z;
                    Sxy1 = Sxy1*(1.0-factor) + Sxy2*factor;
                    Sxz1 = Sxz1*(1.0-factor) + Sxz2*factor;
                    Syz1 = Syz1*(1.0-factor) + Syz2*factor;
                    Sxx1 = Sxx1*(1.0-factor) + Sxx2*factor;
                    Syy1 = Syy1*(1.0-factor) + Syy2*factor;
                    Szz1 = Szz1*(1.0-factor) + Szz2*factor;

                    float sigma_x_n = std::sqrt(Sxx1);
                    float sigma_y_n = std::sqrt(Syy1);
                    float sigma_z_n = std::sqrt(Szz1);
                    float r_xy_n = Sxy1 / (sigma_x_n*sigma_y_n);
                    float r_xz_n = Sxz1 / (sigma_x_n*sigma_z_n);
                    float r_yz_n = Syz1 / (sigma_y_n*sigma_z_n);


                    data1_sigma[id1*3] = (unsigned char)(sigma_x_n*255.0);
                    data1_sigma[id1*3+1] = (unsigned char)(sigma_y_n*255.0);
                    data1_sigma[id1*3+2] = (unsigned char)(sigma_z_n*255.0);
                    data1_r[id1*3] = (unsigned char)((r_xy_n*0.5+0.5)*255.0);
                    data1_r[id1*3+1] = (unsigned char)((r_xz_n*0.5+0.5)*255.0);
                    data1_r[id1*3+2] = (unsigned char)((r_yz_n*0.5+0.5)*255.0);

                    //if(data1_r[id1*3] != (unsigned char)((r_xy_n*0.5+0.5)*255.0))

                    //if(data1_r[id1*3+1] != (unsigned char)((r_xz_n*0.5+0.5)*255.0))

                    //if(data1_r[id1*3+2] != (unsigned char)((r_yz_n*0.5+0.5)*255.0))


                    // todo : back to sigma r


                    //data1_sigma[id1*3] = char(255);
                    //data1_sigma[id1*3+1] = char(255);
                    //data1_sigma[id1*3+2] = char(255);
                    //data1_r[id1*3] = char(200);
                    //data1_r[id1*3+1] = char(200);
                    //data1_r[id1*3+2] = char(200);

                }

                // TODO REMOVE
                //assert(data1[id1*3] == 2.5);
                //assert(data1[id1*3+1] == 2.5);
                //assert(data1[id1*3+2] == 2.5);

                //data1[id1*3] = 3.0;
                //data1[id1*3+1] = 3.0;
                //data1[id1*3+2] = 3.0;
                //data1_sigma[id1*3] = char(254);
                //data1_sigma[id1*3+1] = char(253);
                //data1_sigma[id1*3+2] = char(252);
                //data1_r[id1*3] = char(127);
                //data1_r[id1*3+1] = char(127);
                //data1_r[id1*3+2] = char(128);
            }
        }
    }
}



/* argv[1] = mesh 1, genre /disc/loubet/Models/LoD/Lenin/
 * argv[2] = res
 * argv[3] = factor, entre 0 et 1
 */
int main(int argc, char *argv[])
{

    assert(argc == 4);

    std::string::size_type sz;     // alias of size_t
    std::string root(argv[1]);

    float factor = std::stof(std::string(argv[3]),&sz);
    assert(factor >= 0.f && factor <= 1.f);
    int res = std::stoi(std::string(argv[2]),&sz);

    int gridX, gridY, gridZ;
    int gridX_bis, gridY_bis, gridZ_bis;
    float minX, minY, minZ, maxX, maxY, maxZ;


    /* DENSITY */

    std::string density1_path = root + "volume/lod_density" + std::to_string(res*2) + ".mtsvol";
    std::string density2_path = root + "volume/lod_density" + std::to_string(res) + ".mtsvol";
    std::string density_int_path = root + "volume/lod_density" + std::to_string(res) + "_int.mtsvol";
    float *density1 = NULL;
    float *density2 = NULL;
    float *densityres = NULL;
    if (res < 512) {
        std::cout << "reading " << density1_path << std::endl;
        readFloatFile(density1_path.c_str(), &density1, gridX, gridY, gridZ, minX, minY, minZ, maxX, maxY, maxZ);
    } else {
        gridX = 512; gridY = 512; gridZ = 512;
        density1 = new float[gridX*gridY*gridZ];
        for (int i = 0; i < gridX*gridY*gridZ; i++)
            density1[i] = 0;
    }
    densityres = new float[gridX*gridY*gridZ];
    std::cout << "reading " << density2_path << std::endl;
    readFloatFile(density2_path.c_str(), &density2, gridX_bis, gridY_bis, gridZ_bis, minX, minY, minZ, maxX, maxY, maxZ);
    std::cout << "interpolating... " << std::endl;
    boost::thread_group group;
    for(int j = 0; j < 8; j++) {
        group.create_thread(boost::bind(&thread_task, j, gridX, gridX_bis, density1, density2, densityres, factor));
    }
    group.join_all();

    std::cout << "writing " << density_int_path << std::endl;
    writeFloatFile(density_int_path.c_str(), &densityres, gridX, gridY, gridZ, minX, minY, minZ, maxX, maxY, maxZ);
    //delete[] density1;
    //delete[] density2;

    // test: interp = densityres, ref = density2
    /*
    for (int x = 0; x < gridX; x++) {
        for (int y = 0; y < gridX; y++) {
            for (int z = 0; z < gridX; z++) {
                int id1 = x + y * gridX + z * gridX * gridX;
                int id2 = x/2 + (y/2) * gridX/2 + (z/2) * gridX * gridX/4;
                if(densityres[id1] != density2[id2]) {
                    std::cout << densityres[id1] << " != " << density2[id2] << std::endl;
                }
            }
        }
    }
    std::cout << "test density OK" << std::endl;
    */
    delete[] densityres;


    /* DIFFUSE */

    std::string diffuse1_path = root + "volume/lod_diffuse" + std::to_string(res*2) + ".mtsvol";
    std::string diffuse2_path = root + "volume/lod_diffuse" + std::to_string(res) + ".mtsvol";
    std::string diffuse_int_path = root + "volume/lod_diffuse" + std::to_string(res) + "_int.mtsvol";
    unsigned char *diffuse1 = NULL;
    unsigned char *diffuse2 = NULL;
    if (res < 512) {
        std::cout << "reading " << diffuse1_path << std::endl;
        readChar3File(diffuse1_path.c_str(), &diffuse1, gridX, gridY, gridZ, minX, minY, minZ, maxX, maxY, maxZ);
        std::cout << "reading " << diffuse2_path << std::endl;
        readChar3File(diffuse2_path.c_str(), &diffuse2, gridX_bis, gridY_bis, gridZ_bis, minX, minY, minZ, maxX, maxY, maxZ);
        assert(gridX == gridX_bis*2);
        assert(gridY == gridY_bis*2);
        assert(gridZ == gridZ_bis*2);
        std::cout << "interpolating... " << std::endl;
        boost::thread_group group2;
        for(int j = 0; j < 8; j++) {
            group2.create_thread(boost::bind(&thread_task3, j, gridX, diffuse1, diffuse2, density1, density2, factor));
        }
        group2.join_all();
        std::cout << "writing " << diffuse_int_path << std::endl;
        writeChar3File(diffuse_int_path.c_str(), &diffuse1, gridX, gridY, gridZ, minX, minY, minZ, maxX, maxY, maxZ);
        delete[] diffuse1;
        delete[] diffuse2;
    } else {
        // just copy
        std::cout << "reading " << diffuse2_path << std::endl;
        readChar3File(diffuse2_path.c_str(), &diffuse2, gridX_bis, gridY_bis, gridZ_bis, minX, minY, minZ, maxX, maxY, maxZ);
        std::cout << "writing " << diffuse_int_path << std::endl;
        writeChar3File(diffuse_int_path.c_str(), &diffuse2, gridX_bis, gridY_bis, gridZ_bis, minX, minY, minZ, maxX, maxY, maxZ);
        delete[] diffuse2;
    }

    /* SPECULAR */

    std::string specular1_path = root + "volume/lod_specular" + std::to_string(res*2) + ".mtsvol";
    std::string specular2_path = root + "volume/lod_specular" + std::to_string(res) + ".mtsvol";
    std::string specular_int_path = root + "volume/lod_specular" + std::to_string(res) + "_int.mtsvol";
    unsigned char *specular1 = NULL;
    unsigned char *specular2 = NULL;
    if (res < 512) {
        std::cout << "reading " << specular1_path << std::endl;
        readChar3File(specular1_path.c_str(), &specular1, gridX, gridY, gridZ, minX, minY, minZ, maxX, maxY, maxZ);
        std::cout << "reading " << specular2_path << std::endl;
        readChar3File(specular2_path.c_str(), &specular2, gridX_bis, gridY_bis, gridZ_bis, minX, minY, minZ, maxX, maxY, maxZ);
        assert(gridX == gridX_bis*2);
        assert(gridY == gridY_bis*2);
        assert(gridZ == gridZ_bis*2);
        std::cout << "interpolating... " << std::endl;
        boost::thread_group group3;
        for(int j = 0; j < 8; j++) {
            group3.create_thread(boost::bind(&thread_task3, j, gridX, specular1, specular2, density1, density2, factor));
        }
        group3.join_all();
        std::cout << "writing " << specular_int_path << std::endl;
        writeChar3File(specular_int_path.c_str(), &specular1, gridX, gridY, gridZ, minX, minY, minZ, maxX, maxY, maxZ);
        delete[] specular1;
        delete[] specular2;
    } else {
        // just copy
        std::cout << "reading " << specular2_path << std::endl;
        readChar3File(specular2_path.c_str(), &specular2, gridX_bis, gridY_bis, gridZ_bis, minX, minY, minZ, maxX, maxY, maxZ);
        std::cout << "writing " << specular_int_path << std::endl;
        writeChar3File(specular_int_path.c_str(), &specular2, gridX_bis, gridY_bis, gridZ_bis, minX, minY, minZ, maxX, maxY, maxZ);
        delete[] specular2;
    }


    /* SGGX */

    std::string sggx_sigma1_path = root + "volume/lod_sggx_sigma" + std::to_string(res*2) + ".mtsvol";
    std::string sggx_sigma2_path = root + "volume/lod_sggx_sigma" + std::to_string(res) + ".mtsvol";
    std::string sggx_sigma_int_path = root + "volume/lod_sggx_sigma" + std::to_string(res) + "_int.mtsvol";
    std::string sggx_r1_path = root + "volume/lod_sggx_r" + std::to_string(res*2) + ".mtsvol";
    std::string sggx_r2_path = root + "volume/lod_sggx_r" + std::to_string(res) + ".mtsvol";
    std::string sggx_r_int_path = root + "volume/lod_sggx_r" + std::to_string(res) + "_int.mtsvol";

    unsigned char *sigma1 = NULL;
    unsigned char *sigma2 = NULL;
    unsigned char *r1 = NULL;
    unsigned char *r2 = NULL;
    if (res < 512) {
        std::cout << "reading " << sggx_sigma1_path << std::endl;
        readChar3File(sggx_sigma1_path.c_str(), &sigma1, gridX, gridY, gridZ, minX, minY, minZ, maxX, maxY, maxZ);
        std::cout << "reading " << sggx_sigma2_path << std::endl;
        readChar3File(sggx_sigma2_path.c_str(),  &sigma2, gridX_bis, gridY_bis, gridZ_bis, minX, minY, minZ, maxX, maxY, maxZ);
        assert(gridX == gridX_bis*2);
        assert(gridY == gridY_bis*2);
        assert(gridZ == gridZ_bis*2);
        std::cout << "reading " << sggx_r1_path << std::endl;
        readChar3File(sggx_r1_path.c_str(), &r1, gridX, gridY, gridZ, minX, minY, minZ, maxX, maxY, maxZ);
        std::cout << "reading " << sggx_r2_path << std::endl;
        readChar3File(sggx_r2_path.c_str(), &r2, gridX_bis, gridY_bis, gridZ_bis, minX, minY, minZ, maxX, maxY, maxZ);
        assert(gridX == gridX_bis*2);
        assert(gridY == gridY_bis*2);
        assert(gridZ == gridZ_bis*2);
        std::cout << "interpolating... " << std::endl;
        boost::thread_group group4;
        for(int j = 0; j < 8; j++) {
            group4.create_thread(boost::bind(&thread_task3_sggx, j, gridX, sigma1, sigma2, r1, r2, density1, density2, factor));
        }
        group4.join_all();
        std::cout << "writing " << sggx_sigma_int_path << std::endl;
        writeChar3File(sggx_sigma_int_path.c_str(), &sigma1, gridX, gridY, gridZ, minX, minY, minZ, maxX, maxY, maxZ);
        std::cout << "writing " << sggx_r_int_path << std::endl;
        writeChar3File(sggx_r_int_path.c_str(), &r1, gridX, gridY, gridZ, minX, minY, minZ, maxX, maxY, maxZ);
        delete[] sigma1;
        delete[] sigma2;
        delete[] r1;
        delete[] r2;
    } else {
        // just copy
        std::cout << "reading " << sggx_sigma2_path << std::endl;
        readChar3File(sggx_sigma2_path.c_str(), &sigma2, gridX_bis, gridY_bis, gridZ_bis, minX, minY, minZ, maxX, maxY, maxZ);
        std::cout << "writing " << sggx_sigma_int_path << std::endl;
        writeChar3File(sggx_sigma_int_path.c_str(), &sigma2, gridX_bis, gridY_bis, gridZ_bis, minX, minY, minZ, maxX, maxY, maxZ);
        delete[] sigma2;
        std::cout << "reading " << sggx_r2_path << std::endl;
        readChar3File(sggx_r2_path.c_str(), &r2, gridX_bis, gridY_bis, gridZ_bis, minX, minY, minZ, maxX, maxY, maxZ);
        std::cout << "writing " << sggx_r_int_path << std::endl;
        writeChar3File(sggx_r_int_path.c_str(), &r2, gridX_bis, gridY_bis, gridZ_bis, minX, minY, minZ, maxX, maxY, maxZ);
        delete[] r2;
    }

    delete[] density1;
    delete[] density2;

}
