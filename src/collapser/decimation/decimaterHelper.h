#ifndef DECIMATER_HELPER_H
#define DECIMATER_HELPER_H

#include "../mesh/mesh.h"
#include "../textures/textures.h"

#include <OpenMesh/Tools/Decimater/Observer.hh>
#include <OpenMesh/Tools/Decimater/DecimaterT.hh>
#include <OpenMesh/Tools/Decimater/ModQuadricT.hh>
#include <OpenMesh/Tools/Decimater/ModEdgeLengthT.hh>
#include <OpenMesh/Tools/Decimater/ModNormalFlippingT.hh>
#include <OpenMesh/Tools/Decimater/ModProgMeshT.hh>
#include <OpenMesh/Tools/Decimater/ModAspectRatioT.hh>

// custom decimation methods

#include "../../openmesh/ModAreaT.hh"
#include "../../openmesh/ModMaxEdgeLengthT.hh"
#include "../../openmesh/ModMiddleT.hh"
#include "../../openmesh/ModOptimT.hh"
//#include <OpenMesh/Tools/Decimater/ModVolumeT.hh"
#include "../../openmesh/ModSurfaceT.hh"
#include "../../openmesh/ModSurfaceLinearT.hh"
#include "../../openmesh/ModQuadraticT.hh"


// Decimater
typedef OpenMesh::Decimater::DecimaterT< Mesh >   Decimater;
typedef OpenMesh::Decimater::ModQuadricT< Mesh >::Handle HModQuadric;
typedef OpenMesh::Decimater::ModEdgeLengthT< Mesh >::Handle HModEdgeLength;
typedef OpenMesh::Decimater::ModAreaT< Mesh >::Handle HModArea;
typedef OpenMesh::Decimater::ModNormalFlippingT< Mesh >::Handle HModNormalFlipping;
typedef OpenMesh::Decimater::ModProgMeshT< Mesh >::Handle HModProgMesh;
typedef OpenMesh::Decimater::ModAspectRatioT< Mesh >::Handle HModAspectRatio;
typedef OpenMesh::Decimater::ModMaxEdgeLengthT< Mesh >::Handle HModMaxEdgeLength;
typedef OpenMesh::Decimater::ModMiddleT< Mesh >::Handle HModMiddle;
typedef OpenMesh::Decimater::ModOptimT< Mesh >::Handle HModOptim;
//typedef OpenMesh::Decimater::ModVolumeT< Mesh >::Handle HModVolume;
typedef OpenMesh::Decimater::ModSurfaceT< Mesh >::Handle HModSurface;
typedef OpenMesh::Decimater::ModSurfaceLinearT< Mesh >::Handle HModSurfaceLinear;
typedef OpenMesh::Decimater::ModQuadraticT< Mesh >::Handle HModQuadratic;

struct DecimaterHelper {

    Decimater *decimater;
    HModQuadric hModQuadric; 
    //HModNormalFlipping hModNormalFlipping;
    HModEdgeLength hModEdgeLength;
    //HModArea hModArea; 
    HModProgMesh hModProgMesh; 
    //HModAspectRatio hModAspectRatio;
    HModMaxEdgeLength hModMaxEdgeLength; 
    HModMiddle hModMiddle;
    HModOptim hModOptim;
    //HModOptim hModVolume;
    HModSurface hModSurface;
    HModSurfaceLinear hModSurfaceLinear;
    HModQuadratic hModQuadratic;

    DecimaterHelper(Decimater *d) {
        decimater = d;
    }

    void addModules(float voxelSide, TexturesList *_texturesList, Mesh *mesh_final) {
        decimater->add( hModQuadric );  // register module at the decimater
        //decimater->add( hModNormalFlipping ); 
        //decimater->add( hModArea ); 
        decimater->add( hModProgMesh );
        //decimater->add( hModAspectRatio );
        //decimater->module( hModAspectRatio ).set_binary(false);
        decimater->add( hModEdgeLength );
        //decimater->add( hModMaxEdgeLength );
        decimater->module( hModQuadric ).unset_max_err();
        //decimater->module( hModArea ).set_area_min(voxelSide*voxelSide*MIN_AREA);
        //decimater->module( hModArea ).set_area_max(voxelSide*voxelSide*MAX_AREA);
        //decimater->module( hModAspectRatio ).set_aspect_ratio(ASPECT_RATIO);
        decimater->module( hModEdgeLength ).set_edge_length(voxelSide);
        //decimater->module( hModMaxEdgeLength ).set_max_edge_length(voxelSide);

        //decimater->add( hModVolume );
        //decimater->add( hModSurface );

        decimater->add( hModQuadratic );
        decimater->module( hModQuadratic ).set_voxelSize(voxelSide);
        decimater->module( hModQuadratic ).set_texturesList(_texturesList);
        decimater->module( hModQuadratic ).set_meshFinal(mesh_final);
    }

    void decimate() {
        std::cout << "Init..." << std::endl;
        decimater->initialize();       // let the decimater initialize the mesh and the
                                      // modules
        std::cout << "Decimate..." << std::endl;
        decimater->decimate();         // do decimation
    }

    unsigned getInfoListSize() {
        return decimater->module( hModProgMesh ).infolist().size();
    }

    void getInfoListV0V1(int i, Mesh::VertexHandle &v0, Mesh::VertexHandle &v1) {
        OpenMesh::Decimater::ModProgMeshT< Mesh >::Info info =  decimater->module( hModProgMesh ).infolist()[i];
        v0 = info.v0;
        v1 = info.v1;
    }

};


#endif // DECIMATER_HELPER_H
