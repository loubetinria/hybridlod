#ifndef GRID_INFO_H
#define GRID_INFO_H

#include "../mesh/mesh.h"
#include <sys/stat.h>
#include <unistd.h>

class GridInfo {

public:




    GridInfo(Mesh &mesh, int res) {
        resolution = res;
        bbmin = Mesh::Point(-1,-1,0);
        voxelSide = 2.0/float(resolution);
        gridX = resolution;
        gridY = resolution;
        gridZ = resolution;
        std::cout << "Grid size : " << gridX << " " << gridY << " " << gridZ << std::endl;
        //computeBBox(mesh);
    }

    GridInfo nextLevel();
    GridInfo() {}

    int nbVoxels() {
        return gridX*gridY*gridZ;
    }

    GridInfo(const GridInfo& copy) {
        voxelSide = copy.voxelSide;
        resolution = copy.resolution;
        gridX = copy.gridX;
        gridY = copy.gridY;
        gridZ = copy.gridZ;
        bbmin = copy.bbmin;
    }

    int toId(int x, int y, int z) {
        int res = x + y * gridX + z * gridX * gridY;
        assert(res >= 0 && res < gridX*gridY*gridZ);
        return res;
    }


    float voxelSide;
    int resolution;
    int gridX, gridY, gridZ;
    Mesh::Point bbmin;


protected:


    void computeBBox(Mesh &mesh);

};

#endif // GRID_INFO_H
