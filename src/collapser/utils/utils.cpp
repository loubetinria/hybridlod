#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <assert.h>
#include <limits>
#include <string>     // std::string, std::to_string

#include "utils.h"
#define THRESHOLD_UTILS 10e-3

float norm(const Mesh::Point &p1) {
    return std::sqrt(p1[0]*p1[0]
                     + p1[1]*p1[1]
                     + p1[2]*p1[2]);
}

bool areSame(Mesh::VertexHandle va, Mesh::VertexHandle vb, Mesh &mesh) {
    if (norm(mesh.point(va)-mesh.point(vb)) < 0.001)
        return true;
    return false;

}

bool areOverlapping(Mesh::FaceHandle fa, Mesh::FaceHandle fb, Mesh &mesh) {

    std::vector<Mesh::VertexHandle> a_vertices;
    std::vector<Mesh::VertexHandle> b_vertices;

    for (Mesh::FaceVertexIter fv_it=mesh.fv_iter(fa); fv_it.is_valid(); ++fv_it)
        a_vertices.push_back(*fv_it);

    for (Mesh::FaceVertexIter fv_it=mesh.fv_iter(fb); fv_it.is_valid(); ++fv_it)
        b_vertices.push_back(*fv_it);

    if (areSame(a_vertices[0],a_vertices[1],mesh) || areSame(a_vertices[0],a_vertices[2],mesh))
        return false;

    if (areSame(b_vertices[0],b_vertices[1],mesh) || areSame(b_vertices[0],b_vertices[2],mesh))
        return false;

    // faces égales : comme les faces ne sont pas dégénérée, il faut que tous les vertex de a
    // soient égal à un des vertex de b
    if (!(areSame(a_vertices[0],b_vertices[0],mesh)
          || areSame(a_vertices[0],b_vertices[1],mesh)
          || areSame(a_vertices[0],b_vertices[3],mesh)))
        return false;

    if (!(areSame(a_vertices[1],b_vertices[0],mesh)
          || areSame(a_vertices[1],b_vertices[1],mesh)
          || areSame(a_vertices[1],b_vertices[2],mesh)))
        return false;

    if (!(areSame(a_vertices[2],b_vertices[0],mesh)
          || areSame(a_vertices[2],b_vertices[1],mesh)
          || areSame(a_vertices[2],b_vertices[2],mesh)))
        return false;

    return true;
}


bool hasEdgeSmallerThat(Mesh &mesh, Mesh::FaceHandle fh, float size) {
    for(Mesh::FaceEdgeIter fe_it=mesh.fe_iter(fh); fe_it.is_valid(); ++fe_it) {
        Mesh::HalfedgeHandle he = mesh.halfedge_handle(*fe_it,0);
        Mesh::Point p1 = mesh.point(mesh.to_vertex_handle(he));
        Mesh::Point p2 = mesh.point(mesh.from_vertex_handle(he));
        if (norm(p1-p2) < size) {
            return true;
        }
    }
    return false;
}

bool has3EdgeSmallerThat(Mesh &mesh, Mesh::FaceHandle fh, float size) {
    int n = 0;
    for(Mesh::FaceEdgeIter fe_it=mesh.fe_iter(fh); fe_it.is_valid(); ++fe_it) {
        Mesh::HalfedgeHandle he = mesh.halfedge_handle(*fe_it,0);
        Mesh::Point p1 = mesh.point(mesh.to_vertex_handle(he));
        Mesh::Point p2 = mesh.point(mesh.from_vertex_handle(he));
        if (norm(p1-p2) < size) {
            n++;
        }
    }
    return (n >= 3);
}

float aspectRatio(Mesh &mesh, Mesh::FaceHandle fh) {

    Mesh::Point _v0, _v1, _v2;
    Mesh::FaceVertexIter fv_it=mesh.fv_iter(fh);
    _v0 = mesh.point(*fv_it); fv_it++;
    _v1 = mesh.point(*fv_it); fv_it++;
    _v2 = mesh.point(*fv_it);

    Mesh::Point d0 = _v0 - _v1;
    Mesh::Point d1 = _v1 - _v2;

    // finds the max squared edge length
    Mesh::Scalar l2, maxl2 = d0.sqrnorm();
    if ((l2 = d1.sqrnorm()) > maxl2)
      maxl2 = l2;
    // keep searching for the max squared edge length
    d1 = _v2 - _v0;
    if ((l2 = d1.sqrnorm()) > maxl2)
      maxl2 = l2;

    // squared area of the parallelogram spanned by d0 and d1
    Mesh::Scalar a2 = (d0 % d1).sqrnorm();

    if (a2 == 0.0)
        return 1000.0;

    // the area of the triangle would be
    // sqrt(a2)/2 or length * height / 2
    // aspect ratio = length / height
    //              = length * length / (2*area)
    //              = length * length / sqrt(a2)

    // returns the length of the longest edge
    //         divided by its corresponding height
    return std::sqrt((maxl2 * maxl2) / a2);
}



float area(Mesh &mesh, Mesh::FaceHandle fh) {

    Mesh::Point _v0, _v1, _v2;
    Mesh::FaceVertexIter fv_it=mesh.fv_iter(fh);
    _v0 = mesh.point(*fv_it); fv_it++;
    _v1 = mesh.point(*fv_it); fv_it++;
    _v2 = mesh.point(*fv_it);

    Mesh::Point d0 = _v0 - _v1;
    Mesh::Point d1 = _v1 - _v2;

    // finds the max squared edge length
    Mesh::Scalar l2, maxl2 = d0.sqrnorm();
    if ((l2 = d1.sqrnorm()) > maxl2)
      maxl2 = l2;
    // keep searching for the max squared edge length
    d1 = _v2 - _v0;
    if ((l2 = d1.sqrnorm()) > maxl2)
      maxl2 = l2;

    // squared area of the parallelogram spanned by d0 and d1
    return (d0 % d1).norm()*0.5;

}


std::string to_string(int i) {
    std::stringstream ss;
    ss << i;
    return ss.str();
}


bool exists(const std::string& name) {
  struct stat buffer;
  return (stat (name.c_str(), &buffer) == 0);
}

// get vertex handle for a face
void getVertexHandles(Mesh &mesh, Mesh::FaceHandle fh,
                      Mesh::VertexHandle &v0,
                      Mesh::VertexHandle &v1,
                      Mesh::VertexHandle &v2) {
    Mesh::FaceVertexIter fv_it=mesh.fv_iter(fh);
    v0 = *fv_it;
    fv_it++;
    v1 = *fv_it;
    fv_it++;
    v2 = *fv_it;
}

// get vertex handle for a face
// TODO: test
void getVertexAndFacesHandles(Mesh &mesh, Mesh::FaceHandle fh,
                              Mesh::VertexHandle &v0,
                              Mesh::VertexHandle &v1,
                              Mesh::VertexHandle &v2,
                              Mesh::FaceHandle &fv0v1,
                              Mesh::FaceHandle &fv1v2,
                              Mesh::FaceHandle &fv2v0) {

    Mesh::HalfedgeHandle v0v1 = mesh.halfedge_handle(fh);
    Mesh::HalfedgeHandle v1v2 = mesh.next_halfedge_handle (v0v1);
    Mesh::HalfedgeHandle v2v0 = mesh.next_halfedge_handle(v1v2);
    assert(mesh.next_halfedge_handle(v2v0) == v0v1);
    v0 = mesh.from_vertex_handle(v0v1);
    v1 = mesh.from_vertex_handle(v1v2);
    v2 = mesh.from_vertex_handle(v2v0);
    fv0v1 = mesh.opposite_face_handle(v0v1);
    fv1v2 = mesh.opposite_face_handle(v1v2);
    fv2v0 = mesh.opposite_face_handle(v2v0);

    /*
    Mesh::VertexHandle t0, t1, t2;
    getVertexHandles(mesh, fh, t0, t1, t2);
    std::cout << "Testing : " << v0.idx() << " " << v1.idx() << " " << v2.idx()
                            << t0.idx() << " " << t1.idx() << " " << t2.idx() << std::endl;





    std::cout << "Testing : " << fv0v1.idx() << " " << fv1v2.idx() << " " << fv2v0.idx() << " ";
    for(Mesh::FaceFaceIter ff_it=mesh.ff_iter(fh); ff_it.is_valid(); ++ff_it) {
                            std::cout << ff_it->idx() << " ";
    }
    std::cout << std::endl;
    */
}

void getVertexPoints(Mesh &mesh, Mesh::FaceHandle fh,
                     Mesh::Point &p0,
                     Mesh::Point &p1,
                     Mesh::Point &p2) {
    Mesh::FaceVertexIter fv_it=mesh.fv_iter(fh);
    p0 = mesh.point(*fv_it);
    fv_it++;
    p1 = mesh.point(*fv_it);
    fv_it++;
    p2 = mesh.point(*fv_it);
}


void getTriangleCenterAndRadius(const Mesh::Point &p0, const Mesh::Point &p1, const Mesh::Point &p2,
                                Mesh::Point &tri_center, float &tri_radius) {

    float d01 = norm(p0-p1), d02 = norm(p0-p2), d12 = norm(p1-p2);
    if (d01 >= d02 && d01 >= d12) {
         tri_center = (p0 + p1)*0.5;
         tri_radius = d01*0.5;
    } else if (d02 >= d01 && d02 >= d12) {
        tri_center = (p0 + p2)*0.5;
        tri_radius = d02*0.5;
    } else {
        tri_center = (p1 + p2)*0.5;
        tri_radius = d12*0.5;
    }
}

bool hasFace(std::set<int> &set, int fh) {
    return (set.find(fh) != set.end());
}

void lookForNewNeighboors(Mesh &mesh, Mesh::FaceHandle current_fh,
                          std::set<int> &s_visited_faces,
                          std::set<int> &s_to_explore) {

    s_visited_faces.insert(current_fh.idx());
    for (Mesh::FaceFaceIter ff_it=mesh.ff_iter(current_fh); ff_it.is_valid(); ++ff_it) {
        if (!hasFace(s_visited_faces, ff_it->idx()) && !hasFace(s_to_explore, ff_it->idx()))
            s_to_explore.insert(ff_it->idx());
    }
}

void findNeighboringFaces(Mesh &mesh, Mesh::FaceHandle fh, std::vector<Mesh::FaceHandle> &neighboors) {

    std::set<int> s_visited_faces;
    std::set<int> s_to_explore;

    for (Mesh::FaceFaceIter ff_it=mesh.ff_iter(fh); ff_it.is_valid(); ++ff_it) {
        s_to_explore.insert(ff_it->idx());
    }
    s_visited_faces.insert(fh.idx()); // ajoute la première

    while (s_to_explore.size() > 0) {
        int current = *(s_to_explore.begin());
        s_to_explore.erase(s_to_explore.begin());
        lookForNewNeighboors(mesh, Mesh::FaceHandle(current), s_visited_faces, s_to_explore);
    }

    // pas compris pourquoi on peut pas supprimer ça
    //s_visited_faces.erase(s_visited_faces.begin());

    for (std::set<int>::iterator it = s_visited_faces.begin(); it != s_visited_faces.end(); it++) {
        neighboors.push_back(Mesh::FaceHandle(*it));
    }
}

float rayTriangleIntersection(Ray ray, Triangle triangle) {
    // vector(a,b,c) : a = b-c

    float a,f,u,v;
    Mesh::Point e1 = triangle.p1 - triangle.p0;
    Mesh::Point e2 = triangle.p2 - triangle.p0;

    Mesh::Point h = cross(ray.d,e2);
    a = dot(e1,h);

    if (a > -0.00001 && a < 0.00001)
        return -1.0;

    f = 1.0f/a;
    Mesh::Point s = ray.o - triangle.p0;
    u = f * dot(s,h);

    if (u < 0.0 || u > 1.0)
        return -1.0;

    Mesh::Point q = cross(s,e1);
    v = f * dot(ray.d,q);

    if (v < 0.0 || u + v > 1.0)
        return -1.0;

    // at this stage we can compute t to find out where
    // the intersection point is on the line
    float t = f * dot(e2,q);

    if (t > 0.000001f)
        return t;

    return -1.0f;

}

void test_rayTriangleIntersection() {

    Triangle t;
    Ray r;

    t.p0 = Mesh::Point(0.0,0.0,0.0);
    t.p1 = Mesh::Point(1.0,0.0,0.0);
    t.p2 = Mesh::Point(0.0,1.0,0.0);
    r.o  = Mesh::Point(0.2,0.2,1.0);
    r.d  = Mesh::Point(0.0,0.0,-1.0);
    assertIsAlmost(rayTriangleIntersection(r,t), 1.0, 0.00001);
    r.o  = Mesh::Point(0.2,0.2,2.0);
    assertIsAlmost(rayTriangleIntersection(r,t), 2.0, 0.00001);
    r.d  = Mesh::Point(0.0,0.0,1.0);
    assertIsAlmost(rayTriangleIntersection(r,t), -1.0, 0.00001);

}

void assertIsAlmost(float test, float expected, float margin) {
    if (margin < 0)
        margin = -margin;

    if (test >= expected + margin || test <= expected - margin) {
        std::cout << "Expected " << expected << ", found " << test << std::endl;
    }

    assert(test < expected + margin);
    assert(test > expected - margin);

}


float distPointTriangle(typename Mesh::Point p,
                        typename Mesh::Point p0,
                        typename Mesh::Point p1,
                        typename Mesh::Point p2,
                        float &s,
                        float &t,
                        float voxelSize_) {

    // from David Eberly
    // triangle T(s,t) = B +sE0 +tE1
    typename Mesh::Point B = p0;
    typename Mesh::Point E0 = p1 - p0;
    typename Mesh::Point E1 = p2 - p0;
    typename Mesh::Point D = B - p;     //D = B − P
    float a = dot(E0, E0);              //a = E0 · E0
    float b = dot(E0, E1);              //b = E0 · E1
    float c = dot(E1, E1);              //c = E1 · E1
    float d = dot(E0, D);               //d = E0 · D
    float e = dot(E1, D);               //e = E1 · D
    //float f = dot(D, D);                //f = D · D

    // check degenerated triangles
    if ((p0-p1).norm() < voxelSize_ * THRESHOLD_UTILS) {
        if ((p1-p2).norm() < voxelSize_ * THRESHOLD_UTILS) {
            s = 0;
            t = 0;
            return (p - p0).norm();
        } else {
            // distance au segment p2 --- p1/p0
            s = 0;
            t = clampf(dot((p-p0),(p2-p0)/(p2-p0).sqrnorm()));
            return (B + s*E0 + t*E1 - p).norm();
        }
    } else if ((p1-p2).norm() < voxelSize_ * THRESHOLD_UTILS) {
        if ((p0-p2).norm() < voxelSize_ * THRESHOLD_UTILS) {
            // should not happen
            s = 0;
            t = 0;
            return (p - p0).norm();
        } else {
            // distance au segment p0 --- p1/p2
            s = 0;
            t = clampf(dot((p-p0),(p1-p0)/(p1-p0).sqrnorm()));
            return (B + s*E0 + t*E1 - p).norm();
        }
    } else if ((p0-p2).norm() < voxelSize_ * THRESHOLD_UTILS) {
        if ((p1-p2).norm() < voxelSize_ * THRESHOLD_UTILS) {
            // should not happen
            s = 0;
            t = 0;
            return (p - p0).norm();
        } else {
            // distance au segment p0/p2 --- p1
            t = 0;
            s = clampf(dot((p-p0),(p1-p0)/(p1-p0).sqrnorm()));
            return (B + s*E0 + t*E1 - p).norm();
        }
    }

    float det = a*c-b*b;
    if (det < 0.f) {
        //std::cout << "Det should be >= 0 ?? got " << det << std::endl;
        det = -det;
    }
    s = b*e-c*d;
    t = b*d-a*e ;

    if (s+t <= det) {
        if (s < 0.f) {
            if (t < 0.f) {
                // region4
                if (d < 0.f) {
                    t = 0.f;
                    s = (-d >= a) ? 1.f : -d/a;
                } else {
                    s = 0;
                    t = (e >= 0.f) ? 0.f : ((-e >= c) ? 1.f : -e/c);
                }
                // end of region 4
            } else {
                // region 3
                s = 0.f;
                t = (e >= 0.f) ? 0.f : ((-e >= c) ? 1.f : -e/c);
                // end of region 3
            }
        } else if (t < 0.f) {
                // region 5
                t = 0.f;
                s = (d >= 0.f) ? 0.f : ((-d >= a) ? 1.f : -d/a);
                // end of region 5
            } else {
                // region 0
                if (det <= 0.f) {
                    //std::cout << "problem with null det" << std::endl;
                    //std::cout << (p0-p1).norm() << std::endl;
                    //std::cout << (p1-p2).norm() << std::endl;
                    //std::cout << (p2-p0).norm() << std::endl;
                    s = 0;
                    t = 0;

                    return std::min((p-p0).norm() , std::min((p-p1).norm(),(p-p2).norm()));
                    //assert(0);
                }
                float invDet = 1.f/det;
                s = s*invDet;
                t = t*invDet;
                // end of region 0
            }
        } else {
        if (s < 0.f) {
            // region 2
            float tmp0 = b + d;
            float tmp1 = c + e;
            if (tmp1 > tmp0 ) { // minimum on edge s+t=1
                float numer = tmp1 - tmp0;
                float denom = a - 2.f*b + c;
                s = (numer >= denom) ? 1.f : numer/denom;
                t = 1.f-s;
            } else {          // minimum on edge s=0
                s = 0.f;
                t = (tmp1 <= 0.f) ? 1.f : ((e >= 0.f) ? 0.f : -e/c);
            }
            // end of region 2
        } else if (t < 0.f) {
            // region6
            float tmp0 = b + e;
            float tmp1 = a + d;
            if (tmp1 > tmp0) {
                float numer = tmp1 - tmp0;
                float denom = a-2.f*b+c;
                t = (numer >= denom) ? 1.f : numer/denom;
                s = 1.f - t;
            } else {
                t = 0.f;
                s = (tmp1 <= 0.f) ? 1.f : ((d >= 0) ? 0.f : -d/a);
            }
            // end region 6
        } else {
            // region 1
            float numer = c + e - b - d;
            if (numer <= 0.f) {
                s = 0.f;
            } else {
                float denom = a - 2.f*b + c;
                s = (numer >= denom) ? 1.f : numer/denom;
            }
            t = 1.f-s;
            // end of region 1
        }
    }

    assert(t >= 0.0 && t <= 1.0);
    assert(s >= 0.0 && s <= 1.0);

    //if (s+t > 1.f) {
        //std::cout << "problem with s+t > 1.f : " << s+t << std::endl;
        //assert(s+t <= 1.0);
    //}
    return (B + s*E0 + t*E1 - p).norm();

}

float clampf(float val) {
    if (val < 0.f)
        return 0.f;
    if (val > 1.f)
        return 1.f;
    return val;
}

Mesh::Point incenter(Mesh::Point A, Mesh::Point B, Mesh::Point C) {
    // from http://math.stackexchange.com/questions/740111/incenter-of-triangle-in-3d
    float a = norm(B-C);
    float b = norm(A-C);
    float c = norm(A-B);
    float sum = a+b+c;

    /*
    std::cout << "A = " << A << std::endl;
    std::cout << "B = " << B << std::endl;
    std::cout << "C = " << C << std::endl;
    std::cout << "I = " << (A * a + B * b + C * c)/sum << std::endl;
    */

    if (sum > 0) {
        return (A * a + B * b + C * c)/sum;
    } else {
        return A;
    }
}

Mesh::Point squareToUniformSphere(float u, float v) {
    float z = 1.0f - 2.0f * v;
    float r = std::sqrt(1.0f - z*z);
    float sinPhi = sin(2.0f * M_PI * u), cosPhi = cos(2.0f * M_PI * u);
    return Mesh::Point(r * cosPhi, r * sinPhi, z);
}

void addPointBBox(Mesh::Point &bbmin, Mesh::Point  &bbmax, Mesh::Point p) {
    bbmin[0] = std::min(bbmin[0], p[0]);
    bbmin[1] = std::min(bbmin[1], p[1]);
    bbmin[2] = std::min(bbmin[2], p[2]);
    bbmax[0] = std::max(bbmax[0], p[0]);
    bbmax[1] = std::max(bbmax[1], p[1]);
    bbmax[2] = std::max(bbmax[2], p[2]);
}
