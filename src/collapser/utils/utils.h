#ifndef UTILS_H
#define UTILS_H

#include "../mesh/mesh.h"
#include <sys/stat.h>
#include <unistd.h>

struct Triangle {
    Mesh::Point p0, p1, p2;
    Triangle(Mesh::Point p0,Mesh::Point p1,Mesh::Point p2) : p0(p0), p1(p1) , p2(p2) {}
    Triangle() {}

};

struct Ray {
    Mesh::Point o, d;
};

struct RGB {
    float r, g, b;
    RGB(float R, float G, float B) : r(R), g(G), b(B) {}
};

float norm(const Mesh::Point &p1);

bool areSame(Mesh::VertexHandle va, Mesh::VertexHandle vb, Mesh &mesh);

bool areOverlapping(Mesh::FaceHandle fa, Mesh::FaceHandle fb, Mesh &mesh);

bool hasEdgeSmallerThat(Mesh &mesh, Mesh::FaceHandle fh, float size);

bool has3EdgeSmallerThat(Mesh &mesh, Mesh::FaceHandle fh, float size);

float aspectRatio(Mesh &mesh, Mesh::FaceHandle fh);

float area(Mesh &mesh, Mesh::FaceHandle fh);

std::string to_string(int i);

bool exists(const std::string& name);

void getVertexHandles(Mesh &mesh, Mesh::FaceHandle fh,
                      Mesh::VertexHandle &v0,
                      Mesh::VertexHandle &v1,
                      Mesh::VertexHandle &v2);

// get face vertices and neighboring faces
void getVertexAndFacesHandles(Mesh &mesh, Mesh::FaceHandle fh,
                              Mesh::VertexHandle &v0,
                              Mesh::VertexHandle &v1,
                              Mesh::VertexHandle &v2,
                              Mesh::FaceHandle &fv0v1,
                              Mesh::FaceHandle &fv1v2,
                              Mesh::FaceHandle &fv2v0);

void getVertexPoints(Mesh &mesh, Mesh::FaceHandle fh,
                     Mesh::Point &p0,
                     Mesh::Point &p1,
                     Mesh::Point &p2);

void getTriangleCenterAndRadius(const Mesh::Point &p0, const Mesh::Point &p1, const Mesh::Point &p2,
                                Mesh::Point &tri_center, float &tri_radius);

void findNeighboringFaces(Mesh &mesh, Mesh::FaceHandle fh, std::vector<Mesh::FaceHandle> &neighboors);

bool hasFace(std::set<int> &set, int fh);


// returns a positive distance if intersection, -1 if no intersection
float rayTriangleIntersection(Ray ray, Triangle triangle);

void test_rayTriangleIntersection();


void assertIsAlmost(float test, float expected, float margin);

float distPointTriangle(typename Mesh::Point p,
                        typename Mesh::Point p0,
                        typename Mesh::Point p1,
                        typename Mesh::Point p2,
                        float &s,
                        float &t,
                        float voxelSide_);

float clampf(float val);

Mesh::Point incenter(Mesh::Point A, Mesh::Point B, Mesh::Point C);

Mesh::Point squareToUniformSphere(float u, float v);

void addPointBBox(Mesh::Point &bbmin, Mesh::Point &bbmax, Mesh::Point p);


#endif // UTILS_H
