#ifndef SCRIPTS_H
#define SCRIPTS_H

#include "../mesh/mesh.h"
#include <sys/stat.h>
#include <unistd.h>


void writeVoxelizerScript(std::string file,
                          std::string dstdir,
                          std::string scriptPath,
                          std::string root,
                          int resolution,
                          int gridX, int gridY, int gridZ,
                          Mesh::Point bbmin, Mesh::Point bbmax,
                          float voxelSide);

void writeRenderScript(std::string file, int resolution,
                       int gridX, int gridY, int gridZ,
                       Mesh::Point bbmin, Mesh::Point bbmax,
                       float voxelSide);

#endif // SCRIPTS_H
