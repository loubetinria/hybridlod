#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <assert.h>
#include <limits>
#include <string>     // std::string, std::to_string
#include "scripts.h"

void writeVoxelizerScript(std::string file,
                          std::string dstdir,
                          std::string scriptPath,
                          std::string root,
                          int resolution,
                          int gridX, int gridY, int gridZ,
                          Mesh::Point bbmin, Mesh::Point bbmax,
                          float voxelSide) {

    std::cout << "Writing " << scriptPath << std::endl;

    std::remove(scriptPath.c_str()); // delete file

    root.pop_back(); // remove last '/'

    std::ofstream myfile;
    myfile.open (scriptPath.c_str());
    myfile << "rm -rf voxelized/*voxels\n";
    myfile << "rm -rf voxelized/*"<< resolution <<".mtsvol\n";
    myfile << "mitsuba";
    myfile << " -b 2 -Dresolution=" << resolution;
    myfile << " -Dmodel=" << file;
    myfile << " -Droot=" << root;
    myfile << " -DgridX=" << gridX << " -DgridY=" << gridY << " -DgridZ=" << gridZ;
    myfile << " -DminCornerX=" << bbmin[0] << " -DminCornerY=" << bbmin[1];
    myfile << " -DminCornerZ=" << bbmin[2];
    myfile << " -DvoxelSide=" << voxelSide << " rendering/voxelize.xml" << std::endl;
    myfile << "cat voxelized/*voxels > voxelized/all.voxels \n";
    myfile << "voxels2vol " << "voxelized/all.voxels " << root << "/ " << gridX << " " << gridY << " " << gridZ << " ";
    myfile << bbmin[0] << " " << bbmin[1] << " " << bbmin[2] << " ";
    myfile << bbmax[0] << " " << bbmax[1] << " " << bbmax[2] << " " << 0 << "\n";

}

void writeRenderScript(std::string file, int resolution,
                       int gridX, int gridY, int gridZ,
                       Mesh::Point bbmin, Mesh::Point bbmax,
                       float voxelSide) {

    std::remove("render.sh"); // delete file

    std::ofstream myfile;
    myfile.open ("render.sh");
    myfile << "cd ..\n";
    myfile << "mitsuba mesures/matpreview-path-voxelizer/density.xml ";
    myfile << "-DhalfSizeX=" << (bbmax[0] - bbmin[0])*0.5;
    myfile << " -DhalfSizeY=" << (bbmax[1] - bbmin[1])*0.5;
    myfile << " -DhalfSizeZ=" << (bbmax[2] - bbmin[2])*0.5;
    myfile << " -DcornerX=" << bbmin[0];
    myfile << " -DcornerY=" << bbmin[1];
    myfile << " -DcornerZ=" << bbmin[2];
    myfile << " -Dlod=" << resolution;
    myfile << " -Ddensity=/disc/loubet/mitsuba-sampling/voxelized/density\n";
    myfile.close();

}
