#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <assert.h>
#include <limits>
#include <string>     // std::string, std::to_string

#include "gridInfo.h"

GridInfo GridInfo::nextLevel() {
    GridInfo next(*this);
    assert(gridX % 2 == 0);
    assert(gridY % 2 == 0);
    assert(gridZ % 2 == 0);
    assert(resolution % 2 == 0);
    next.gridX /= 2;
    next.gridY /= 2;
    next.gridZ /= 2;
    next.resolution /= 2;
    next.voxelSide *= 2.0;
    return next;
}

void GridInfo::computeBBox(Mesh &mesh) {
    Mesh::Point bbmax = mesh.point(Mesh::VertexHandle(0));
    bbmin = mesh.point(Mesh::VertexHandle(0));

    for (Mesh::VertexIter v_it=mesh.vertices_begin(); v_it!=mesh.vertices_end(); ++v_it) {
        Mesh::Point p = mesh.point(*v_it);
        bbmin[0] = std::min(bbmin[0], p[0]);
        bbmin[1] = std::min(bbmin[1], p[1]);
        bbmin[2] = std::min(bbmin[2], p[2]);
        bbmax[0] = std::max(bbmax[0], p[0]);
        bbmax[1] = std::max(bbmax[1], p[1]);
        bbmax[2] = std::max(bbmax[2], p[2]);
    }
    Mesh::Point bbsize = bbmax - bbmin;
    // take small margin
    bbmin -= bbsize*0.01;
    bbmax += bbsize*0.01;
    bbsize = bbmax - bbmin;
    voxelSide = std::max( bbsize[0]/resolution, std::max( bbsize[1]/resolution, bbsize[2]/resolution ) );
    //gridX = std::ceil(bbsize[0]/voxelSide);
    //gridY = std::ceil(bbsize[1]/voxelSide);
    //gridZ = std::ceil(bbsize[2]/voxelSide);
    gridX = resolution;
    gridY = resolution;
    gridZ = resolution;
    std::cout << "Grid size : " << gridX << " " << gridY << " " << gridZ << std::endl;
}
