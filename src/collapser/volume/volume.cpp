#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <list>

#include <assert.h>
#include <limits>
#include <string>     // std::string, std::to_string
#include <random>

#include "volume.h"


void Volume::test() {
    std::cout << "Volume : test " << std::endl;

    // test ray triangle intersection
    test_rayTriangleIntersection();

    std::cout << "Random points on sphere: " << std::endl;

    for (int i = 0; i < 200; i++)  {
        std::cout << squareToUniformSphere(dis(gen),dis(gen)) << std::endl;
    }
    std::cout << "Volume : test OK!" << std::endl;
}

Volume::Volume(GridInfo gridInfo, TexturesList *texturesList, std::string root) {

    gen = std::mt19937(rd());
    dis = std::uniform_real_distribution<>(0,1);

    //test();

    _gridInfo = GridInfo(gridInfo);
    _texturesList = texturesList;
    _root = root;
    _octree = new Octree< std::vector<int> >(_gridInfo.resolution);

}

Volume::Volume(std::string root) {

    gen = std::mt19937(rd());
    dis = std::uniform_real_distribution<>(0,1);

    _texturesList = NULL;
    _root = root;
    _octree = NULL;

}

Volume::~Volume() {
    delete _octree;
}


void Volume::write() {
    if (_texturesList->has_diffuse_front)
        writeDiffuse();

    if (_texturesList->has_sggx_front)
        writeSGGX();
}

void Volume::writeHeader(FILE *pFile, int type, int chanels, int resolution) {

    // Bytes 1-3 ASCII Bytes ’V’, ’O’, and ’L’
    int V = 86, O = 79, L = 76;
    fwrite(reinterpret_cast<const char*>(&V), 1, sizeof(char), pFile);
    fwrite(reinterpret_cast<const char*>(&O), 1, sizeof(char), pFile);
    fwrite(reinterpret_cast<const char*>(&L), 1, sizeof(char), pFile);

    // Byte 4 File format version number (currently 3)
    int version = 3;
    fwrite(reinterpret_cast<const char*>(&version), 1, sizeof(char), pFile);

    // Bytes 5-8 Encoding identifier (32-bit integer). The following choices are available:
    //  1. Dense float32-based representation
    //  2. Dense float16-based representation (currently not supported by this implementation)
    //  3. Dense uint8-based representation (The range 0..255 will be mapped to 0..1)
    //  4. Dense quantized directions. The directions are stored in spherical coordinates with a total storage cost of 16 bit per entry.
    fwrite(reinterpret_cast<const char*>(&type), 1, sizeof(int), pFile);

    // Bytes 9-12 Number of cells along the X axis (32 bit integer)
    // Bytes 13-16 Number of cells along the Y axis (32 bit integer)
    // Bytes 17-20 Number of cells along the Z axis (32 bit integer)
    fwrite(reinterpret_cast<const char*>(&resolution), 1, sizeof(int), pFile);
    fwrite(reinterpret_cast<const char*>(&resolution), 1, sizeof(int), pFile);
    fwrite(reinterpret_cast<const char*>(&resolution), 1, sizeof(int), pFile);

    // Bytes 21-24 Number of channels (32 bit integer, supported values: 1 or 3)
    fwrite(reinterpret_cast<const char*>(&chanels), 1, sizeof(int), pFile);

    // Bytes 25-48 Axis-aligned bounding box of the data stored in single precision (order: xmin, ymin, zmin, xmax, ymax, zmax)
    // bbox
    Mesh::Point bbmax_vol(1,1,2);
    Mesh::Point bbmin_vol(-1,-1,0);

    fwrite(reinterpret_cast<const char*>(&bbmin_vol[0]), 1, sizeof(int), pFile);
    fwrite(reinterpret_cast<const char*>(&bbmin_vol[1]), 1, sizeof(int), pFile);
    fwrite(reinterpret_cast<const char*>(&bbmin_vol[2]), 1, sizeof(int), pFile);
    fwrite(reinterpret_cast<const char*>(&bbmax_vol[0]), 1, sizeof(int), pFile);
    fwrite(reinterpret_cast<const char*>(&bbmax_vol[1]), 1, sizeof(int), pFile);
    fwrite(reinterpret_cast<const char*>(&bbmax_vol[2]), 1, sizeof(int), pFile);

}

void Volume::writeFloat(std::string file, const std::vector<float> &data, int resolution) {

    //std::string file = _root + "Volume/diffuse" + to_string(_gridInfo.resolution) + ".mtsvol";
    std::remove(file.c_str());

    FILE *pFile;
    pFile=fopen(file.c_str(),"a");
    assert(pFile);

    assert(int(data.size()) == resolution*resolution*resolution);

    std::cout << "Writing " << file << std::endl;

    writeHeader(pFile, 1 /*float*/, 1 /*channels*/, resolution);

    for (int z = 0; z < resolution; z++) {
        for (int y = 0; y < resolution; y++) {
            for (int x = 0; x < resolution; x++) {
                float toWrite = data[x + y * resolution + z * resolution * resolution];
                fwrite(reinterpret_cast<const char*>(&toWrite),    1, sizeof(float), pFile);
            }
        }
    }

    fclose(pFile);
}

void Volume::writeDiffuse() {

    std::string file = _root + "volume/diffuse" + to_string(_gridInfo.resolution) + ".mtsvol";
    std::remove(file.c_str());

    FILE *pFile;
    pFile=fopen(file.c_str(),"a");
    assert(pFile);

    std::cout << "Writing " << file << std::endl;

    writeHeader(pFile, 1 /*float*/, 3 /*channels*/, _gridInfo.gridX);

    assert(int(diffuse.size()) == _gridInfo.nbVoxels());
    for (int z = 0; z < _gridInfo.gridZ; z++) {
        for (int y = 0; y < _gridInfo.gridY; y++) {
            for (int x = 0; x < _gridInfo.gridX; x++) {
                float toWriteR = diffuse[_gridInfo.toId(x,y,z)][0];
                float toWriteG = diffuse[_gridInfo.toId(x,y,z)][1];
                float toWriteB = diffuse[_gridInfo.toId(x,y,z)][2];

                fwrite(reinterpret_cast<const char*>(&toWriteR),    1, sizeof(float), pFile);
                fwrite(reinterpret_cast<const char*>(&toWriteG),    1, sizeof(float), pFile);
                fwrite(reinterpret_cast<const char*>(&toWriteB),    1, sizeof(float), pFile);
            }
        }
    }

    fclose(pFile);
}


void Volume::writeSGGX() {
    std::string file_tri = _root + "volume/sggx_tri" + to_string(_gridInfo.resolution) + ".mtsvol";
    std::string file_diag = _root + "volume/sggx_diag" + to_string(_gridInfo.resolution) + ".mtsvol";

    std::remove(file_tri.c_str());
    std::remove(file_diag.c_str());

    FILE *pFile_tri;
    FILE *pFile_diag;

    pFile_tri=fopen(file_tri.c_str(),"a");
    assert(pFile_tri);
    pFile_diag=fopen(file_diag.c_str(),"a");
    assert(pFile_diag);

    std::cout << "Writing " << file_tri << std::endl;
    std::cout << "Writing " << file_diag << std::endl;

    writeHeader(pFile_tri, 1 /*float*/, 3 /*channels*/, _gridInfo.gridX);
    writeHeader(pFile_diag, 1 /*float*/, 3 /*channels*/, _gridInfo.gridX);

    assert(int(sggx.size()) == _gridInfo.nbVoxels());
    for (int z = 0; z < _gridInfo.gridZ; z++) {
        for (int y = 0; y < _gridInfo.gridY; y++) {
            for (int x = 0; x < _gridInfo.gridX; x++) {
                float toWriteTri0 = sggx[_gridInfo.toId(x,y,z)]._tri[0];
                float toWriteTri1 = sggx[_gridInfo.toId(x,y,z)]._tri[1];
                float toWriteTri2 = sggx[_gridInfo.toId(x,y,z)]._tri[2];
                float toWriteDiag0 = sggx[_gridInfo.toId(x,y,z)]._diag[0];
                float toWriteDiag1 = sggx[_gridInfo.toId(x,y,z)]._diag[1];
                float toWriteDiag2 = sggx[_gridInfo.toId(x,y,z)]._diag[2];

                fwrite(reinterpret_cast<const char*>(&toWriteTri0),    1, sizeof(float), pFile_tri);
                fwrite(reinterpret_cast<const char*>(&toWriteTri1),    1, sizeof(float), pFile_tri);
                fwrite(reinterpret_cast<const char*>(&toWriteTri2),    1, sizeof(float), pFile_tri);

                fwrite(reinterpret_cast<const char*>(&toWriteDiag0),    1, sizeof(float), pFile_diag);
                fwrite(reinterpret_cast<const char*>(&toWriteDiag1),    1, sizeof(float), pFile_diag);
                fwrite(reinterpret_cast<const char*>(&toWriteDiag2),    1, sizeof(float), pFile_diag);
            }
        }
    }

    fclose(pFile_tri);
    fclose(pFile_diag);

}

void Volume::readFloatFile(const char * file, std::vector<float> &data, int &gridX, int &gridY, int &gridZ,
                           float &minX, float &minY, float &minZ,
                           float &maxX, float &maxY, float &maxZ) {
    FILE *pFile;

    pFile=fopen(file,"rb");

    if (!pFile)
        std::cout << "Cannot read " << file << std::endl;

    assert(pFile);

    if (pFile) {

        std::cout << "Reading " << file << std::endl;

        // read header
        int V, O, L, chanels;

        fread(reinterpret_cast<char*>(&V), 1, sizeof(char), pFile);
        fread(reinterpret_cast<char*>(&O), 1, sizeof(char), pFile);
        fread(reinterpret_cast<char*>(&L), 1, sizeof(char), pFile);

        // Byte 4 File format version number (currently 3)
        int version;
        fread(reinterpret_cast<char*>(&version), 1, sizeof(char), pFile);

        // Bytes 5-8 Encoding identifier (32-bit integer). The following choices are available:
        //  1. Dense float32-based representation
        //  2. Dense float16-based representation (currently not supported by this implementation)
        //  3. Dense uint8-based representation (The range 0..255 will be mapped to 0..1)
        //  4. Dense quantized directions. The directions are stored in spherical coordinates with a total storage cost of 16 bit per entry.
        int type; //float32
        fread(&type, 1, sizeof(int), pFile);

        // Bytes 9-12 Number of cells along the X axis (32 bit integer)
        // Bytes 13-16 Number of cells along the Y axis (32 bit integer)
        // Bytes 17-20 Number of cells along the Z axis (32 bit integer)
        fread(&gridX, 1, sizeof(int), pFile);
        fread(&gridY, 1, sizeof(int), pFile);
        fread(&gridZ, 1, sizeof(int), pFile);

        std::cout << "Grid size: " << gridX << " " << gridY << " " << gridZ << std::endl;

        // Bytes 21-24 Number of channels (32 bit integer, supported values: 1 or 3)
        fread(&chanels, 1, sizeof(int), pFile);
        assert(chanels == 1);

        // Bytes 25-48 Axis-aligned bounding box of the data stored in single precision (order: xmin, ymin, zmin, xmax, ymax, zmax)
        // bbox
        fread(&minX, 1, sizeof(float), pFile);
        fread(&minY, 1, sizeof(float), pFile);
        fread(&minZ, 1, sizeof(float), pFile);
        fread(&maxX, 1, sizeof(float), pFile);
        fread(&maxY, 1, sizeof(float), pFile);
        fread(&maxZ, 1, sizeof(float), pFile);


        data.resize(gridX*gridY*gridZ);

        for (int z = 0; z < gridZ; z++)
            for (int y = 0; y < gridY; y++) {
                for (int x = 0; x < gridX; x++) {
                    float toRead;
                    fread(&toRead, 1, sizeof(float), pFile);
                    data[x + y * gridX + z * gridX * gridY] = toRead;
                    //std::cout << (*data)[x + y * gridX + z * gridX * gridY];
                }
            }
    }
    fclose(pFile);
}

void Volume::readFloat3File(const char * file, std::vector<Mesh::Point> &data, int &gridX, int &gridY, int &gridZ,
                            float &minX, float &minY, float &minZ,
                            float &maxX, float &maxY, float &maxZ) {
    FILE *pFile;

    pFile=fopen(file,"rb");

    if (!pFile)
        std::cout << "Cannot read " << file << std::endl;

    assert(pFile);

    if (pFile) {

        std::cout << "Reading " << file << std::endl;

        // read header
        int V, O, L, chanels;

        fread(reinterpret_cast<char*>(&V), 1, sizeof(char), pFile);
        fread(reinterpret_cast<char*>(&O), 1, sizeof(char), pFile);
        fread(reinterpret_cast<char*>(&L), 1, sizeof(char), pFile);

        // Byte 4 File format version number (currently 3)
        int version;
        fread(reinterpret_cast<char*>(&version), 1, sizeof(char), pFile);

        // Bytes 5-8 Encoding identifier (32-bit integer). The following choices are available:
        //  1. Dense float32-based representation
        //  2. Dense float16-based representation (currently not supported by this implementation)
        //  3. Dense uint8-based representation (The range 0..255 will be mapped to 0..1)
        //  4. Dense quantized directions. The directions are stored in spherical coordinates with a total storage cost of 16 bit per entry.
        int type; //float32
        fread(&type, 1, sizeof(int), pFile);

        // Bytes 9-12 Number of cells along the X axis (32 bit integer)
        // Bytes 13-16 Number of cells along the Y axis (32 bit integer)
        // Bytes 17-20 Number of cells along the Z axis (32 bit integer)
        fread(&gridX, 1, sizeof(int), pFile);
        fread(&gridY, 1, sizeof(int), pFile);
        fread(&gridZ, 1, sizeof(int), pFile);


        //std::cout << "Grid size: " << grid_size << std::endl;

        // Bytes 21-24 Number of channels (32 bit integer, supported values: 1 or 3)
        fread(&chanels, 1, sizeof(int), pFile);
        assert(chanels == 3);

        // Bytes 25-48 Axis-aligned bounding box of the data stored in single precision (order: xmin, ymin, zmin, xmax, ymax, zmax)
        // bbox
        fread(&minX, 1, sizeof(float), pFile);
        fread(&minY, 1, sizeof(float), pFile);
        fread(&minZ, 1, sizeof(float), pFile);
        fread(&maxX, 1, sizeof(float), pFile);
        fread(&maxY, 1, sizeof(float), pFile);
        fread(&maxZ, 1, sizeof(float), pFile);

        data.resize(gridX*gridY*gridZ);

        for (int z = 0; z < gridZ; z++)
            for (int y = 0; y < gridY; y++) {
                for (int x = 0; x < gridX; x++) {
                    float f0, f1, f2;
                    fread(&f0, 1, sizeof(float), pFile);
                    fread(&f1, 1, sizeof(float), pFile);
                    fread(&f2, 1, sizeof(float), pFile);
                    data[x + y * gridX + z * gridX * gridY] = Mesh::Point(f0,f1,f2);
                }
            }
    }
    fclose(pFile);
}

// si on ajoute, vérifier avant que le triangle est contenu dans le voxel
// c'est à dire, pas de vérification au dernier niveau.
int Volume::addTriInOctree(Triangle tri,
                            Mesh::Point &tri_center, float tri_radius, // in world
                            int level, int X, int Y, int Z, int tri_id) {
    int added = 0;
    //  level 0 = 1*1*1, level 1 = 2*2*2, level 2 = 4*4*4, level i = 2^i * 2^i * 2^i
    int levelWidth = 1 << level;
    float voxelWidth = _gridInfo.voxelSide * float(_gridInfo.resolution / levelWidth);
    int x,y,z;
    float s = 0,t = 0;
    bool lastLevel = (levelWidth == _gridInfo.resolution) ? true : false;

    if (lastLevel) {
        assert(_gridInfo.resolution / levelWidth == 1);
        (*_octree)(X,Y,Z).push_back(tri_id); // create new node if necessary
        added++;
    } else {
        for (int k = 0; k < 2; k++) {
            for (int j = 0; j < 2; j++) {
                for (int i = 0; i < 2; i++) {
                    // coordonnées du voxel dans la résolution d'après
                    x = X*2 + i;
                    y = Y*2 + j;
                    z = Z*2 + k;

                    Mesh::Point vox_center = _gridInfo.bbmin + Mesh::Point(x+0.5,y+0.5,z+0.5)*voxelWidth*0.5;
                    float vox_radius = (std::sqrt(3.0)) * 0.5 * 0.5 * voxelWidth; // 0.5 pour radius, 0.5 pour level suivant
                    float dist = distPointTriangle(vox_center, tri.p0, tri.p1, tri.p2, s,t, _gridInfo.voxelSide);
                    if ( dist < vox_radius*(1 + sqrt(3))) { // to avoid propagation
                        added += addTriInOctree(tri, tri_center, tri_radius, level+1, x,y,z, tri_id);
                    }
                }
            }
        }
    }

    return added;

}

Mesh::Point Volume::squareToUniformSphere(float u, float v) {
    float z = 1.0f - 2.0f * v;
    float r = std::sqrt(1.0f - z*z);
    float sinPhi = sin(2.0f * M_PI * u), cosPhi = cos(2.0f * M_PI * u);
    return Mesh::Point(r * cosPhi, r * sinPhi, z);
}

float Volume::intersection(Ray &ray, std::vector<Triangle> &tri_list) {
    float dist = -1;
    for (unsigned i = 0; i < tri_list.size(); i++) {
        float d = rayTriangleIntersection(ray, tri_list[i]);
        if (d >= 0) {
            dist = (dist >= 0) ? std::min(d, dist) : d;
        }
    }
    return dist;
}

/*
 * On doit pouvoir diffuser toutes les grilles en même temps
 */
void Volume::propagateData(std::list<OpenMesh::Vec3i> &emptyVoxels) {

    if (emptyVoxels.size() == 0)
        return;

    std::cout << "Propagating..." << std::endl;
    for (int dif = 0; dif < 1; dif++) {

        std::vector<int> voxelIds;
        std::vector<Mesh::Point> prop_diffuse_list;
        std::vector<SGGX> prop_sggx_list;

        std::list<OpenMesh::Vec3i>::iterator i = emptyVoxels.begin();
        while (i != emptyVoxels.end())
        {
            int x = (*i)[0];
            int y = (*i)[1];
            int z = (*i)[2];

            float sumWeights = 0;

            Mesh::Point new_diffuse(0,0,0);
            SGGX new_sggx;
            new_sggx /= 1.0f;

            // trouver des voisins
            for (int nz = z-1; nz < z+2; nz++) {
                for (int ny = y-1; ny < y+2; ny++) {
                    for (int nx = x-1; nx < x+2; nx++) {
                        if (nz < 0 || ny < 0 || nx < 0
                                || nz >= _gridInfo.gridZ
                                || ny >= _gridInfo.gridY
                                || nx >= _gridInfo.gridX) {
                            continue;
                        }
                        if (nx == x && ny == y && nz == z)
                            continue;

                        bool goodNeighbour = false;

                        if(_texturesList->has_diffuse_front) {
                            Mesh::Point neighbour_diffuse = diffuse[_gridInfo.toId(nx,ny,nz)];
                            if (neighbour_diffuse[0] >= 0) {
                                goodNeighbour = true;
                                new_diffuse += neighbour_diffuse;
                            }
                        }

                        if(_texturesList->has_sggx_front) {
                            // sggx déjà en repère monde
                            SGGX neighbour_sggx = sggx[_gridInfo.toId(nx,ny,nz)];
                            if (!neighbour_sggx.isNull()) {
                                goodNeighbour = true;
                                new_sggx += neighbour_sggx;
                            }
                        }

                        if (goodNeighbour)
                            sumWeights += 1;
                    }
                }
            }

            if (sumWeights > 0) {
                voxelIds.push_back(_gridInfo.toId(x,y,z));
                if(_texturesList->has_diffuse_front) {
                    prop_diffuse_list.push_back(new_diffuse / sumWeights);
                }
                if (_texturesList->has_sggx_front) {
                    new_sggx /= sumWeights;
                    prop_sggx_list.push_back( new_sggx );
                }
                emptyVoxels.erase(i++);  // alternatively, i = items.erase(i);

            } else {
                ++i;
            }
        }

        if (voxelIds.size() == 0)
            break;

        for (unsigned i = 0; i < voxelIds.size(); i++) {
            if(_texturesList->has_diffuse_front) {
                diffuse[voxelIds[i]] = prop_diffuse_list[i];
            }
            if (_texturesList->has_sggx_front) {
                sggx[voxelIds[i]] = prop_sggx_list[i];
            }
        }

        std::cout << "Size of the list: " << emptyVoxels.size() << std::endl;
    }
}


void Volume::voxelize(Mesh &mesh) {


    Mesh::Point p0, p1, p2, tri_center;
    float tri_radius;

    std::cout << "Preprocessing volume..." << std::endl;
    std::cout << mesh.n_faces() << " faces" << std::endl;

    _octree->setEmptyValue(std::vector<int>(0));

    float *voxelCount = new float[mesh.n_faces() + 1];

    int nf = 0;
    for (Mesh::FaceIter f_it=mesh.faces_sbegin(); f_it!=mesh.faces_end(); ++f_it) {
        // calculer la bounding sphere, si la bounding sphère du voxel et celle du triangle se touchent, add
        getVertexPoints(mesh, *f_it, p0, p1, p2);
        getTriangleCenterAndRadius(p0, p1, p2,tri_center, tri_radius);

        int added = addTriInOctree(Triangle(p0,p1,p2),
                                   tri_center, tri_radius,
                                   0, 0,0,0, f_it->idx());
        assert(f_it->idx() >= 0);
        voxelCount[f_it->idx()] = float(added);

        if (mesh.n_faces()/20 > 0)
            if (nf % (mesh.n_faces()/20) == 0)
                std::cout << int(float(nf) / float(mesh.n_faces()) * 100 + 0.5) << "% "<< std::endl;

        nf++;

    }
    std::cout << "Preprocess volume done!!" << std::endl;

    if(_texturesList->has_diffuse_front) {
        diffuse.resize(_gridInfo.nbVoxels());
    }

    if(_texturesList->has_sggx_front) {
        sggx.resize(_gridInfo.nbVoxels());
    }

    //std::list<OpenMesh::Vec3i> emptyVoxels;

    std::cout << "Filtering..." << std::endl;
    for (int z = 0; z < _gridInfo.gridZ; z++) {
        for (int y = 0; y < _gridInfo.gridY; y++) {
            for (int x = 0; x < _gridInfo.gridX; x++) {
                // regarder si il y a des triangles
                float sumWeights = 0;

                // init values
                if(_texturesList->has_diffuse_front)
                    diffuse[_gridInfo.toId(x,y,z)] = Mesh::Point(-1,-1,-1);

                if(_texturesList->has_sggx_front)
                    sggx[_gridInfo.toId(x,y,z)] = SGGX();

                Mesh::Point filteredDiffuseColor(0,0,0);
                SGGX filteredDiffuseSGGX;

                for (unsigned t = 0; t < _octree->at(x,y,z).size(); t++) {
                    Mesh::FaceHandle fh(_octree->at(x,y,z)[t]);
                    Mesh::FaceHalfedgeIter fhe_it=mesh.fh_iter(fh);
                    Mesh::TexCoord2D tc = mesh.texcoord2D(*fhe_it);
                    float weight = area(mesh, fh) / voxelCount[fh.idx()];

                    // add values
                    if(_texturesList->has_diffuse_front) {
                        filteredDiffuseColor += _texturesList->diffuse_front.getValueDetached(tc) * weight;
                    }

                    // add values
                    if(_texturesList->has_sggx_front) {
                        filteredDiffuseSGGX += _texturesList->sggx_front.getValueDetached(tc) * weight;
                    }

                    sumWeights += weight;
                }

                if (sumWeights > 0) {
                    if(_texturesList->has_diffuse_front)
                        diffuse[_gridInfo.toId(x,y,z)] = filteredDiffuseColor / sumWeights;



                    if(_texturesList->has_sggx_front)
                        sggx[_gridInfo.toId(x,y,z)] = filteredDiffuseSGGX / sumWeights;


                } else {
                    //emptyVoxels.push_back(OpenMesh::Vec3i(x,y,z));
                }
            }
        }
    }

    //propagateData(emptyVoxels);
    write();

    delete[] voxelCount;

}

void Volume::optimalDensity(int resolution) {
    // clear data
    density.clear();
    sggx.clear();

    std::string density_path = _root + "volume/lod_density" + to_string(resolution) + ".mtsvol";
    std::string sggx_tri_path = _root + "volume/sggx_tri" + to_string(resolution) + ".mtsvol";
    std::string sggx_diag_path = _root + "volume/sggx_diag" + to_string(resolution) + ".mtsvol";

    // read data

    float minX, minY, minZ, maxX, maxY, maxZ;
    int gridX, gridY, gridZ;
    std::vector<Mesh::Point> sggx_diag, sggx_tri;

    readFloat3File(sggx_diag_path.c_str(), sggx_diag, gridX, gridY, gridZ,
                   minX, minY, minZ,
                   maxX, maxY, maxZ);

    readFloat3File(sggx_tri_path.c_str(), sggx_tri, gridX, gridY, gridZ,
                   minX, minY, minZ,
                   maxX, maxY, maxZ);

    readFloatFile(density_path.c_str(), density, gridX, gridY, gridZ,
                  minX, minY, minZ,
                  maxX, maxY, maxZ);

    std::cout << "Optimizing density..." << std::endl;
    for (unsigned i = 0; i < density.size(); i++) {
        if (density[i] < 0) {
            std::cout << "Density negative !" << std::endl;
            density[i] = 0;
        }
        if (density[i] == 0)
            continue;

        SGGX sggx(sggx_diag[i], sggx_tri[i]);
        // on doit avoir une min transparency sur une distance de un voxel ?
        float voxelSize = 2.0 / float(resolution);
        float meanT = std::exp(-density[i]*voxelSize);

        float better_density = sggx.optimalDensity(meanT, gen, dis);
        //std::cout << "Density old " << density[i] << ", new " << better_density / voxelSize << std::endl;

        density[i] = better_density / voxelSize;
    }

    // write files
    std::string file = _root + "volume/density_mf" + to_string(resolution) + ".mtsvol";
    writeFloat(file, density, resolution);


}
