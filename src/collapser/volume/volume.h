#ifndef VOLUME_H
#define VOLUME_H

#include "../mesh/mesh.h"
#include "../sggx/sggx.h"
#include "../utils/gridInfo.h"
#include "../octree_code/octree.h"
#include "../utils/gridInfo.h"
#include "../utils/utils.h"
#include "../textures/textures.h"

#include <sys/stat.h>
#include <unistd.h>
#include <list>



class Volume {

public:

    GridInfo _gridInfo;
    TexturesList *_texturesList;
    std::string _root;

    // data
    std::vector<Mesh::Point> diffuse;
    std::vector<SGGX> sggx;
    std::vector<float> density;

    Volume(GridInfo gridInfo, TexturesList *texturesList, std::string root);
    Volume(std::string root);

    ~Volume();

    void write();
    void writeDensity();
    void writeSGGX();
    void writeDiffuse();
    void writeHeader(FILE *pFile, int type, int chanels, int resolution);

    void voxelize(Mesh &mesh);

    void propagateData(std::list<OpenMesh::Vec3i> &emptyVoxels);
    void optimalDensity(int resolution);

    void readFloat3File(const char * file, std::vector<Mesh::Point> &data, int &gridX, int &gridY, int &gridZ,
                        float &minX, float &minY, float &minZ,
                        float &maxX, float &maxY, float &maxZ);

    void readFloatFile(const char * file, std::vector<float> &data, int &gridX, int &gridY, int &gridZ,
                       float &minX, float &minY, float &minZ,
                       float &maxX, float &maxY, float &maxZ);

    void writeFloat(std::string file, const std::vector<float> &data, int resolution);


private:

    std::random_device rd;
    std::mt19937 gen;
    std::uniform_real_distribution<> dis;

    Octree< std::vector<int> > *_octree;

    int  addTriInOctree(Triangle tri,
                        Mesh::Point &tri_center, float tri_radius,
                        int level, int X, int Y, int Z, int tri_id);

    float intersection(Ray &ray, std::vector<Triangle> &tri_list);

    Mesh::Point squareToUniformSphere(float u, float v);

    void test();


};

#endif // VOLUME_H
