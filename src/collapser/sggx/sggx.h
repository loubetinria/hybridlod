#ifndef SGGX_H
#define SGGX_H

#include "../mesh/mesh.h"
#include <sys/stat.h>
#include <unistd.h>
#include <Eigen/Core>     // std::string, std::to_string
#include <cstddef>
#include <random>

class SGGX {
public:

    Mesh::Point _diag;
    Mesh::Point _tri;

    SGGX() {
        _diag = Mesh::Point(0,0,0);
        _tri = Mesh::Point(0,0,0);
    }

    SGGX(Mesh::Point d, Mesh::Point t) {
        _diag = d;
        _tri = t;

    }

    bool isNull() const {
        return (_diag == Mesh::Point(0,0,0) && _tri == Mesh::Point(0,0,0));
    }

    float optimalDensity(float meanTransparency, std::mt19937 &gen,
                         std::uniform_real_distribution<> &dis);

    // Operators

    SGGX& operator+=(const SGGX &other) {
        _diag += other._diag;
        _tri += other._tri;
        return *this;
    }

    SGGX& operator/=(float f) {
        _diag /= f;
        _tri /= f;
        return *this;
    }

    SGGX& operator*=(float f) {
        _diag *= f;
        _tri *= f;
        return *this;
    }

private:

    Eigen::Matrix3f toMatrix() const;

    float projectedArea(Mesh::Point view) const;

};

inline SGGX operator*(SGGX lhs, float rhs) {
  lhs *= rhs;
  return lhs;
}

inline SGGX operator*(float lhs, SGGX rhs) {
  rhs *= lhs;
  return rhs;
}

inline SGGX operator/(SGGX lhs, float rhs) {
  lhs /= rhs;
  return lhs;
}

#endif // SGGX_H
