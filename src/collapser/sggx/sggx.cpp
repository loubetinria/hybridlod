#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <assert.h>
#include <limits>
#include <string>     // std::string, std::to_string
#include <Eigen/Core>     // std::string, std::to_string

#include "sggx.h"
#include "../utils/utils.h"

float SGGX::optimalDensity(float meanTransparency, std::mt19937 &gen,
                           std::uniform_real_distribution<> &dis) {

    assert(meanTransparency > 0 && meanTransparency <= 1);

    float initial = -std::log(meanTransparency);

    if (isNull())
        return initial;

    assert(initial > 0);

    Eigen::Matrix3f sggx;

    sggx << _diag[0], _tri[0], _tri[1],
            _tri[0], _diag[1], _tri[2],
            _tri[1], _tri[2], _diag[2];

    /*
    float eigenvalues[3];
    bool success =  Eigen::eig3(sggx, eigenvalues); // remplace sggx by eigen vectors
    if (!success) {
        std::cout << "Eigen failled, no optimal density!" << std::endl;
        return initial;
    }

    for (int e = 0; e < 3; e++) {
        if (eigenvalues[e] < 0)
            eigenvalues[e] = 0;

        eigenvalues[e] = std::sqrt(eigenvalues[e]);
    }
    */

    float density = initial;
    float eps = 0.001;
    float currentT;
    float current_dT_dx;
    int step = 0;

    std::vector<float> projectedAreas;
    for (int i = 0; i < 30 ; i++)  {
        Mesh::Point view = squareToUniformSphere(dis(gen), dis(gen));
        projectedAreas.push_back(projectedArea(view));
        //projectedAreas.push_back(1.0);
    }

    for (int i = 0; i < 100 ; i++) {
        currentT = 0;
        current_dT_dx = 0;

        for (unsigned v = 0; v < projectedAreas.size(); v++) {
            currentT += std::exp(-density*projectedAreas[v]);
            current_dT_dx += -projectedAreas[v] * std::exp(-density*projectedAreas[v]);
        }

        currentT /= projectedAreas.size();
        current_dT_dx /= projectedAreas.size();

        if (std::abs(currentT - meanTransparency) < eps)
            break;

        // calcul de la dérivée en x = density
        // sum d/dx

        // tangent at the curve f(x) = currentT - meanTransparency
        // next density : solve current_dT_dx*x + b = 0
        // current_dT_dx*density + b = currentT - meanTransparency
        // b = currentT - meanTransparency - (current_dT_dx*density)
        // x = - (currentT - meanTransparency -(current_dT_dx*density))/current_dT_dx
        // x =  - (currentT - meanTransparency)/current_dT_dx + density

        /*
        if ((currentT - meanTransparency) < 0) {
            // on est trop transparent => augmenter la densité
            density += -std::log(meanTransparency/currentT);
        } else {
            // on est pas assez transparent => baisser la densité
            density += -std::log(meanTransparency/currentT);
        }
        */
        density = -(currentT - meanTransparency)/current_dT_dx + density;

        if (i > 75) {
            std::cout << currentT  << "(" << i << ")" << std::endl;
        }

        step++;
    }

    if (std::abs(currentT - meanTransparency) < eps) {
        //std::cout << "steps : " << step << std::endl;
        //Log(EInfo, "Converged");
    } else {
        std::cout << "Not converged : target = " << meanTransparency << ", got " << currentT << std::endl;
    }

    //std::cout << "meanTransparency = " << meanTransparency << ", currentT = " << currentT << " density = " << density << std::endl;

    return density;
}

float SGGX::projectedArea(Mesh::Point view) const {

    if (isNull())
        return 1.0; // sinon retourne 0!

    if (!(view.norm() > 0.99 && view.norm()< 1.01)) {
        //std::cout << view.toString() << std::endl;
        //Log(EInfo, "Got not normalized view vector!");
        view /= view.length();
    }

    Eigen::Vector3f v(view[0], view[1], view[2]);
    return std::sqrt((toMatrix() * v).dot(v));
}

Eigen::Matrix3f SGGX::toMatrix() const {
    Eigen::Matrix3f res;
    res << _diag[0], _tri[0], _tri[1],
            _tri[0], _diag[1], _tri[2],
            _tri[1], _tri[2], _diag[2];
    return res;
}
