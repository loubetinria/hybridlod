#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <assert.h>
#include <limits>
#include <string>     // std::string, std::to_string

#include "lod/lod.h"


#include <deque>
#include <list>
#include <sys/stat.h>
#include <unistd.h>


int main(int argc, char *argv[])
{
    if(argc != 2) {
        std::cout << "Usage: collapser path/to/data/root/" << std::endl;
        std::cout << "       with path/to/data/root/ the directory "
                  << "containing src/src.obj" << std::endl;
        std::cout << "Example: collapser data/lod/trunk/" << std::endl;        
        return 0;
    }


    std::string root(argv[1]);

    Lod lod512Helper(root, 512);
    lod512Helper.process();

    Lod lod256Helper(lod512Helper);
    lod256Helper.process();

    Lod lod128Helper(lod256Helper);
    lod128Helper.process();

    Lod lod64Helper(lod128Helper);
    lod64Helper.process();

    Lod lod32Helper(lod64Helper);
    lod32Helper.process();

    Lod lod16Helper(lod32Helper);
    lod16Helper.process();

    Lod lod8Helper(lod16Helper);
    lod8Helper.process();

}
