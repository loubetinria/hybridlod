#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <assert.h>
#include <limits>
#include <string>     // std::string, std::to_string

#include "lod/lod.h"


#include <deque>
#include <list>
#include <sys/stat.h>
#include <unistd.h>




/*
 * argv[1] = mesh 1, genre /disc/loubet/Models/LoD/Lenin/
 * argv[2] = res
 * argv[3] = factor, entre 0 et 1
 */
int main(int argc, char *argv[])
{

    assert(argc == 4);

    std::string::size_type sz;     // alias of size_t
    std::string root(argv[1]);

    float factor = std::stof(std::string(argv[3]),&sz);
    assert(factor >= 0.f && factor <= 1.f);
    //int res = std::stoi(std::string(argv[2]),&sz);

    std::string mesh1_path = root + "geometry/lod" + std::string(argv[2]) + "_0.obj";
    std::string mesh2_path = root + "geometry/lod" + std::string(argv[2]) + "_100.obj";
    std::string mesh_int_path = root + "geometry/lod" + std::string(argv[2]) + "_int.obj";


    std::cout << "factor: " << factor << std::endl;
    std::cout << "reading " << mesh1_path << std::endl;
    std::cout << "reading " << mesh2_path << std::endl;

    Mesh mesh1, mesh2;
    mesh1.request_halfedge_texcoords2D();
    mesh2.request_halfedge_texcoords2D();
    read(mesh1, mesh1_path.c_str());
    read(mesh2, mesh2_path.c_str());

    // interpolation dans mesh1
    for (Mesh::VertexIter v_it=mesh1.vertices_begin(); v_it!=mesh1.vertices_end(); ++v_it) {
        Mesh::Point source = mesh1.point(*v_it);
        Mesh::Point target = mesh2.point(*v_it);

        mesh1.set_point(*v_it, source*(1-factor) + target*factor);
        //mesh1.property(vSource,*v_it) = mesh_final.point(*v_it);
    }
    std::cout << "writing " << mesh_int_path << std::endl;

    writeNewObj(mesh1, mesh_int_path);

    // write mask
    std::string tex_mask_path = root + "maps/lod" + std::string(argv[2]) + "_mask.png";
    std::string tex_mask_int_path = root + "maps/lod" + std::string(argv[2]) + "_mask_int.png";

    pngwriter tex_mask_png;
    tex_mask_png.readfromfile(tex_mask_path.c_str()); // taille: même que les 0 et 1
    tex_mask_png.pngwriter_rename(tex_mask_int_path.c_str());
    for (int x = 1; x <= tex_mask_png.getwidth(); x++) {
        for (int y = 1; y <= tex_mask_png.getheight(); y++) {
            double R = double(tex_mask_png.read(x, y, 1))/65535.0;
            double G = double(tex_mask_png.read(x, y, 2))/65535.0;
            double B = double(tex_mask_png.read(x, y, 3))/65535.0;
            tex_mask_png.plot(x, y, 1.0*(1.0-factor) + R*factor,
                              1.0*(1.0-factor) + G*factor,
                              1.0*(1.0-factor) + B*factor);
        }
    }
    std::cout << "Writing " << tex_mask_int_path << std::endl;
    tex_mask_png.close();

}
