#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <assert.h>
#include <limits>
#include <string>     // std::string, std::to_string

#include <pngwriter.h>

#include "mesh/mesh.h"
#include "macrosurface/macrosurface.h"
#include "utils/utils.h"
#include "utils/gridInfo.h"


#include <deque>
#include <list>
#include <sys/stat.h>
#include <unistd.h>


OpenMesh::VPropHandleT<bool> vIsMacro2;

void request_attributes(Mesh &mesh) {
    mesh.add_property(vIsMacro2);
    mesh.request_vertex_colors();
}

/*
 * argv[1] = root, genre /disc/loubet/Models/LoD/Lenin/
 * argv[2] = input, in root + Geometry/, sans .obj
 * argv[3] = resolution
 */
int main(int argc, char *argv[])
{
    assert(argc == 4);

    int resolution = std::atoi(argv[3]);
    assert(resolution > 0);

    Mesh mesh;
    request_attributes(mesh);

    std::string root(argv[1]);
    std::string mesh_name(argv[2]);
    std::string mesh_path = root + "geometry/" + mesh_name + ".obj";
    std::string input_path = root + "geometry/input.obj";

    read(mesh, mesh_path.c_str());

    GridInfo gridInfo(mesh, resolution);


    std::cout << "Test macro surfaces..." << std::endl;
    testMacroSurfaces();

    std::cout << "Compute macro surfaces..." << std::endl;
    int n = 0;
    for (Mesh::VertexIter v_it=mesh.vertices_sbegin(); v_it!=mesh.vertices_end(); ++v_it) {
        bool isMacro = isOnMacroSurface(mesh, *v_it, gridInfo.voxelSide);
        //std::cout << "isMacro : " <<  int(isMacro) << std::endl;
        mesh.property(vIsMacro2,*v_it) = isMacro;
        Mesh::Color c = (isMacro) ? Mesh::Color(0,255,0) : Mesh::Color(255,0,0);
        mesh.set_color(*v_it, c);
        if (n % (mesh.n_vertices()/20) == 0)
            std::cout << int(float(n) / float(mesh.n_vertices()) * 100) + 1 << "%" << std::endl;
        n++;

    }
    std::cout << "Write macro test in mesh_macro_test.ply..." << std::endl;
    OpenMesh::IO::Options opt(OpenMesh::IO::Options::VertexColor);
    OpenMesh::IO::write_mesh(mesh, "mesh_macro_test.ply",opt);

	return 0;
}
