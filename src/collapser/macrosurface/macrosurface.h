#ifndef MACROSURFACE_H
#define MACROSURFACE_H

#include "../mesh/mesh.h"

bool isOnMacroSurface(Mesh &mesh, Mesh::VertexHandle vh, float resolution);
bool isOnMacroSurface(Mesh &mesh, Mesh::FaceHandle fh, Mesh::Point point, float resolution);
bool hasNonMacroEdgeSurface(Mesh &mesh, Mesh::FaceHandle fh, Mesh::HalfedgeHandle heh, float resolution);

// for testing

unsigned getNbIntersection(Mesh::Point p1, Mesh::Point p2, Mesh::Point center, float radius);

void testMacroSurfaces();

#endif // MACROSURFACE_H
