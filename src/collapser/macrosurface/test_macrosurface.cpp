#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <assert.h>
#include <limits>
#include <string>     // std::string, std::to_string

#include "macrosurface.h"

void test_isOnMacroSurface_cube() {
    Mesh mesh;

    // generate vertices

    Mesh::VertexHandle vhandle[8];

    vhandle[0] = mesh.add_vertex(Mesh::Point(-1, -1,  1));
    vhandle[1] = mesh.add_vertex(Mesh::Point( 1, -1,  1));
    vhandle[2] = mesh.add_vertex(Mesh::Point( 1,  1,  1));
    vhandle[3] = mesh.add_vertex(Mesh::Point(-1,  1,  1));
    vhandle[4] = mesh.add_vertex(Mesh::Point(-1, -1, -1));
    vhandle[5] = mesh.add_vertex(Mesh::Point( 1, -1, -1));
    vhandle[6] = mesh.add_vertex(Mesh::Point( 1,  1, -1));
    vhandle[7] = mesh.add_vertex(Mesh::Point(-1,  1, -1));

    std::vector<Mesh::VertexHandle>  face_vhandles;

    face_vhandles.clear();
    face_vhandles.push_back(vhandle[0]);
    face_vhandles.push_back(vhandle[1]);
    face_vhandles.push_back(vhandle[2]);
    face_vhandles.push_back(vhandle[3]);
    mesh.add_face(face_vhandles);

    face_vhandles.clear();
    face_vhandles.push_back(vhandle[7]);
    face_vhandles.push_back(vhandle[6]);
    face_vhandles.push_back(vhandle[5]);
    face_vhandles.push_back(vhandle[4]);
    mesh.add_face(face_vhandles);

    face_vhandles.clear();
    face_vhandles.push_back(vhandle[1]);
    face_vhandles.push_back(vhandle[0]);
    face_vhandles.push_back(vhandle[4]);
    face_vhandles.push_back(vhandle[5]);
    mesh.add_face(face_vhandles);

    face_vhandles.clear();
    face_vhandles.push_back(vhandle[2]);
    face_vhandles.push_back(vhandle[1]);
    face_vhandles.push_back(vhandle[5]);
    face_vhandles.push_back(vhandle[6]);
    mesh.add_face(face_vhandles);

    face_vhandles.clear();
    face_vhandles.push_back(vhandle[3]);
    face_vhandles.push_back(vhandle[2]);
    face_vhandles.push_back(vhandle[6]);
    face_vhandles.push_back(vhandle[7]);
    mesh.add_face(face_vhandles);

    face_vhandles.clear();
    face_vhandles.push_back(vhandle[0]);
    face_vhandles.push_back(vhandle[3]);
    face_vhandles.push_back(vhandle[7]);
    face_vhandles.push_back(vhandle[4]);
    mesh.add_face(face_vhandles);

    assert(isOnMacroSurface(mesh, Mesh::VertexHandle(0), 0.1));
    assert(isOnMacroSurface(mesh, Mesh::VertexHandle(1), 0.1));
    assert(isOnMacroSurface(mesh, Mesh::VertexHandle(2), 0.1));
    assert(isOnMacroSurface(mesh, Mesh::VertexHandle(3), 0.1));
    assert(isOnMacroSurface(mesh, Mesh::VertexHandle(4), 0.1));
    assert(isOnMacroSurface(mesh, Mesh::VertexHandle(5), 0.1));
    assert(isOnMacroSurface(mesh, Mesh::VertexHandle(6), 0.1));
    assert(isOnMacroSurface(mesh, Mesh::VertexHandle(7), 0.1));

    assert(!isOnMacroSurface(mesh, Mesh::VertexHandle(0), 3.5));
    assert(!isOnMacroSurface(mesh, Mesh::VertexHandle(1), 3.5));
    assert(!isOnMacroSurface(mesh, Mesh::VertexHandle(2), 3.5));
    assert(!isOnMacroSurface(mesh, Mesh::VertexHandle(3), 3.5));
    assert(!isOnMacroSurface(mesh, Mesh::VertexHandle(4), 3.5));
    assert(!isOnMacroSurface(mesh, Mesh::VertexHandle(5), 3.5));
    assert(!isOnMacroSurface(mesh, Mesh::VertexHandle(6), 3.5));
    assert(!isOnMacroSurface(mesh, Mesh::VertexHandle(7), 3.5));

}

void test_isOnMacroSurface_cylinder() {

    Mesh mesh;
    bool statusOK = true;
    try
    {
        if ( !OpenMesh::IO::read_mesh(mesh, "test_cylinder.obj") )
        {
            std::cerr << "Cannot read mesh test_cylinder.obj" << std::endl;
            statusOK = false;
        } else {
            std::cout << "Read mesh test_cylinder.obj" << std::endl;
        }
    }
    catch( std::exception& x )
    {
        std::cerr << x.what() << std::endl;
        statusOK = false;
    }
    assert(statusOK);

    // generate vertices

    assert(isOnMacroSurface(mesh, Mesh::VertexHandle(0), 0.1));
    assert(isOnMacroSurface(mesh, Mesh::VertexHandle(1), 0.1));
    assert(isOnMacroSurface(mesh, Mesh::VertexHandle(2), 0.1));
    assert(isOnMacroSurface(mesh, Mesh::VertexHandle(3), 0.1));
    assert(isOnMacroSurface(mesh, Mesh::VertexHandle(4), 0.1));
    assert(isOnMacroSurface(mesh, Mesh::VertexHandle(5), 0.1));
    assert(isOnMacroSurface(mesh, Mesh::VertexHandle(6), 0.1));
    assert(isOnMacroSurface(mesh, Mesh::VertexHandle(7), 0.1));
    assert(isOnMacroSurface(mesh, Mesh::VertexHandle(8), 0.1));
    assert(isOnMacroSurface(mesh, Mesh::VertexHandle(9), 0.1));
    assert(isOnMacroSurface(mesh, Mesh::VertexHandle(10), 0.1));
    assert(isOnMacroSurface(mesh, Mesh::VertexHandle(11), 0.1));

    assert(!isOnMacroSurface(mesh, Mesh::VertexHandle(8), 3.5));
    assert(!isOnMacroSurface(mesh, Mesh::VertexHandle(9), 3.5));
    assert(!isOnMacroSurface(mesh, Mesh::VertexHandle(10), 3.5));
    assert(!isOnMacroSurface(mesh, Mesh::VertexHandle(11), 3.5));

    assert(isOnMacroSurface(mesh, Mesh::VertexHandle(8), 1.0));
    assert(isOnMacroSurface(mesh, Mesh::VertexHandle(9), 1.0));
    assert(isOnMacroSurface(mesh, Mesh::VertexHandle(10), 1.0));
    assert(isOnMacroSurface(mesh, Mesh::VertexHandle(11), 1.0));

}


void test_getNbIntersection_simple() {
    // Testing edge and sphère intersection

    unsigned t1 = getNbIntersection(Mesh::Point(-2.0,0.0,0.0),
                                    Mesh::Point(2.0,0.0,0.0),
                                    Mesh::Point(0.0,0.0,0.0),
                                    1.0);

    assert(t1 == 2);

    unsigned t2 = getNbIntersection(Mesh::Point(0.0,0.0,0.0),
                                    Mesh::Point(5.0,5.0,5.0),
                                    Mesh::Point(1.0,1.0,1.0),
                                    0.5);

    assert(t2 == 2);

    unsigned t3 = getNbIntersection(Mesh::Point(5.0,-2.0,1.0),
                                    Mesh::Point(4.0,2.0,0.1),
                                    Mesh::Point(-1.0,1.0,3.0),
                                    2.5);

    assert(t3 == 0);
}

void test_getNbIntersection_half() {
    // Testing edge and sphère intersection

    unsigned t1 = getNbIntersection(Mesh::Point(-2.0,-2.0,-2.0),
                                    Mesh::Point(0.1,0.1,0.1),
                                    Mesh::Point(0.0,0.0,0.0),
                                    1.0);

    assert(t1 == 1);

    unsigned t2 = getNbIntersection(Mesh::Point(0.8,0.8,0.8),
                                    Mesh::Point(0.1,-6.0,6.3),
                                    Mesh::Point(1.0,1.0,1.0),
                                    1.0);

    assert(t2 == 1);

}


void testMacroSurfaces() {
    std::cout << "Testing Macro Surfaces..." << std::endl;

    test_getNbIntersection_simple();
    test_getNbIntersection_half();
    test_isOnMacroSurface_cube();
    test_isOnMacroSurface_cylinder();
}

