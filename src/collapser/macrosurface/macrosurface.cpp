#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <assert.h>
#include <limits>
#include <string>     // std::string, std::to_string
#include <cmath>     // std::string, std::to_string

#include "macrosurface.h"
#include "../utils/utils.h"

#define CONNECTED 0

struct BorderVertex {
    int v_a;
    int v_b;
    int type; // 0 = middle, 1 = near a, 2 = near b

    BorderVertex() {
        v_a = -1;
        v_b = -1;
        type = 0;
    }

    // initialize and sort edge vertices in increasing order
    BorderVertex(int v1, int v2, int t = 0) {
        assert(v1 != v2);
        v_a = (v1 < v2) ? v1 : v2 ;
        v_b = (v1 < v2) ? v2 : v1 ;
        type = t;
    }
};

struct BorderEdges {
    unsigned v_a, v_b;

    // unsorted
    BorderEdges(unsigned va, unsigned vb) : v_a(va), v_b(vb) {}
};

struct BorderGraph {

    BorderGraph() {
        vertices.clear();
        edges.clear();
    }

    std::vector<BorderVertex> vertices;
    std::vector<BorderEdges> edges;

    // look for vertex (of the graph, = intersection) with
    // vertices of the edge, sorted, and the type of
    // intersection
    bool has_vertex(int v0, int v1, int type, unsigned &v) {

        assert(v0 >= 0 && v1 > v0);
        assert(type >= 0 && type <= 2);

        for (unsigned i = 0; i < vertices.size(); i++) {
            if (vertices[i].v_a == v0
                    && vertices[i].v_b == v1
                    && vertices[i].type == type) {
                v = i;
                return true;
            }
        }
        return false;
    }

    // add new vertex, should not be in the list already
    unsigned add_vertex(int v0, int v1, int type) {

        assert(v0 >= 0 && v1 > v0);
        assert(type >= 0 && type <= 2);
        unsigned v;

        assert(!has_vertex(v0, v1, type, v)); // could be removed
        vertices.push_back(BorderVertex(v0, v1, type));
        return vertices.size() - 1;
    }

    // add an edge, not ordered
    void add_edge(unsigned v_a, unsigned v_b) {
        edges.push_back(BorderEdges(v_a, v_b));
    }

    // get a vertex directly linked with v by a edge
    int getNextVertex(unsigned v) {
        for (unsigned i = 0; i < edges.size(); i++) {
            if (edges[i].v_a == v)
                return edges[i].v_b;
            if (edges[i].v_b == v)
                return edges[i].v_a;
        }
        assert(0); // on devrait trouver
        return 0;
    }

    // return the next vertex after v (opposite direction to prev)
    int getNextVertex(unsigned v, unsigned prev) {
        for (unsigned i = 0; i < edges.size(); i++) {
            if (edges[i].v_a == v && edges[i].v_b != prev)
                return edges[i].v_b;
            if (edges[i].v_b == v && edges[i].v_a != prev)
                return edges[i].v_a;
        }
        assert(0); // should be albe to find
        return 0;
    }

    // return the next vertex after v (opposite direction to prev)
    int lookForNextVertex(unsigned v, unsigned prev) {
        for (unsigned i = 0; i < edges.size(); i++) {
            if (edges[i].v_a == v && edges[i].v_b != prev)
                return edges[i].v_b;
            if (edges[i].v_b == v && edges[i].v_a != prev)
                return edges[i].v_a;
        }
        return -1;
    }

    // return true if only one loop in the graph
    bool hasSingleLoop() {

        // algorithm : start at one point, visit and count the visited points.
        // if not closed loop: error
        // if closed and visited all: return true
        // else, retour false

        if (vertices.size() == 0) {
            return false; // no loop, no macro surface
        }

        assert(vertices.size() >= 3);
        if (edges.size() != vertices.size()) {
            //std::cout << "edges.size() " << edges.size() << " vertices.size() " << vertices.size() << std::endl;
            //print();
        }
        assert(edges.size() == vertices.size());

        unsigned visited = 2;
        int prev = 0;
        int current_i = getNextVertex(prev);

        while (true) {
            int next = getNextVertex(current_i, prev);
            if (next != 0) {
                visited++;
                prev = current_i;
                current_i = next;
            } else {
                if (visited == vertices.size()) {
                    //std::cout << " Done" << std::endl;
                    return true;
                } else if (visited < vertices.size()) {
                    //std::cout << " Done" << std::endl;
                    return false;
                } else {
                    assert(0); // should not
                }
            }
        }
        //std::cout << " Done" << std::endl;
        return false;
    }

    bool hasSingleComponent() {

        // algorithm : start at one point, visit and count the visited points.
        // if not closed loop: error
        // if closed and visited all: return true
        // else, retour false

        if (vertices.size() == 0) {
            return false; // no loop, no macro surface
        }

        assert(vertices.size() >= 2 );
        unsigned visited = 2;
        int first = 0;
        int second = getNextVertex(first);
        int prev = first;
        int current_i = second;

        int nextForward=0;
        while (nextForward >= 0) {
            nextForward = lookForNextVertex(current_i, prev);
            if (nextForward > 0) {
                visited++;
                prev = current_i;
                current_i = nextForward;
            } else if (nextForward == 0) {
                //std::cout << " Done, visited " << visited << std::endl;
                return (visited == vertices.size());
            }
        }

        // loop stop, explore back.
        int nextBackward=0;
        prev = second;
        current_i = 0;
        while (nextBackward >= 0) {
            nextBackward = lookForNextVertex(current_i, prev);
            if (nextBackward > 0) {
                visited++;
                prev = current_i;
                current_i = nextBackward;
            }
        }
        return (visited == vertices.size());

    }

    void print() {
        std::cout << "Vertices : " << std::endl;
        for (unsigned i = 0; i < vertices.size(); i++) {
            std::cout << vertices[i].v_a << " " << vertices[i].v_b << " " << vertices[i].type << std::endl;
        }
        std::cout << "Edges : " << std::endl;
        for (unsigned i = 0; i < edges.size(); i++) {
            std::cout << edges[i].v_a << " " << edges[i].v_b << std::endl;
        }
    }

};

// intersection sphère / edge
unsigned getNbIntersection(Mesh::Point p1, Mesh::Point p2, Mesh::Point center, float radius) {
    // point on segment : a*p1 + (1-a)*p2, avec a entre 0 et 1
    // equation : a*p1 + (1-a)*p2 is on the sphere of radius radius and center center

    float a = (p1-p2).sqrnorm();
    float b = 2.0*dot(center - p2, p2 - p1);
    float c = (p2-center).sqrnorm() - radius*radius;
    float delta = b*b-4.0*a*c;

    if (delta < 0)
        return 0;

    float sqrt_delta = std::sqrt(delta);

    if (sqrt_delta == 0.0) {
        // segment touches, no intersection
        return 0;
        float sol = -b/(2.0*a);
        if (sol >= 0.0 && sol <= 1.0) {
            return 1;
        } else {
            return 0;
        }
    }
    float sol1 = (-b -sqrt_delta)/(2.0*a);
    float sol2 = (-b +sqrt_delta)/(2.0*a);
    int nbSol = 0;
    if (sol1 >= 0.0 && sol1 <= 1.0)
        nbSol++;
    if (sol2 >= 0.0 && sol2 <= 1.0)
        nbSol++;
    return nbSol;
}

/*
 * v1 and v2 not sorted
 * must look for v1 and v2 in point already added,
 * connect then or create them.
 * For the search: per point, sorting
 */
void addBorder(BorderGraph &border_graph,
               Mesh::VertexHandle v0_a, Mesh::VertexHandle v1_a, int type_a,
               Mesh::VertexHandle v0_b, Mesh::VertexHandle v1_b, int type_b) {

    // ordonner pour a
    assert(v0_a.idx() != v1_a.idx());
    assert(v0_b.idx() != v1_b.idx());

    assert(v0_a.idx() >= 0);
    assert(v1_a.idx() >= 0);
    assert(v0_b.idx() >= 0);
    assert(v1_b.idx() >= 0);
    assert(type_a >= 0 && type_a <= 2);
    assert(type_b >= 0 && type_b <= 2);

    if (v0_a.idx() > v1_a.idx()) {
        Mesh::VertexHandle aux = v0_a;
        v0_a = v1_a;
        v1_a = aux;
        if (type_a == 1) {
            type_a = 2;
        } else if (type_a == 2) {
            type_a = 1;
        }
    }

    // sort for b
    assert(v0_b.idx() != v1_b.idx());
    if (v0_b.idx() > v1_b.idx()) {
        Mesh::VertexHandle aux = v0_b;
        v0_b = v1_b;
        v1_b = aux;
        if (type_b == 1) {
            type_b = 2;
        } else if (type_b == 2) {
            type_b = 1;
        }
    }


    unsigned v_a, v_b;
    bool found_v_a = border_graph.has_vertex(v0_a.idx(), v1_a.idx(), type_a, v_a);
    bool found_v_b = border_graph.has_vertex(v0_b.idx(), v1_b.idx(), type_b, v_b);
    if (!found_v_a) {
        v_a = border_graph.add_vertex(v0_a.idx(), v1_a.idx(), type_a);
    }
    if (!found_v_b) {
        v_b = border_graph.add_vertex(v0_b.idx(), v1_b.idx(), type_b);
    }

    border_graph.add_edge(v_a, v_b);
}

bool hasInternBoundary(Mesh &mesh, Mesh::FaceHandle fh, Mesh::Point center, float radius) {
    for (Mesh::FaceEdgeIter fe_it=mesh.fe_iter(fh); fe_it.is_valid(); ++fe_it) {
        if (mesh.is_boundary(*fe_it)) {
            Mesh::HalfedgeHandle heh = mesh.halfedge_handle(*fe_it,0);
            Mesh::Point p1 = mesh.point(mesh.to_vertex_handle(heh));
            Mesh::Point p2 = mesh.point(mesh.from_vertex_handle(heh));
            if (getNbIntersection(p1,p2,center,radius) > 0) {
                // intersection et boundary
                return true;
            }
        }
    }

    return false;
}

/*
 * return true if macro (= big face with 0 intersection and vertices outside) or error
 *
 * add faces to study in the list if they haven't been studied
 * add borders if any.
 *
 * assume that the face is connected => either adjacent to the base vertex,
 * or add because neighbor of a face connected internaly
 * or add because neighbor of a face connected containing one intersection
 *
 */
bool studyFace(Mesh &mesh, BorderGraph &borderGraph, Mesh::FaceHandle fh, Mesh::Point center, float radius,
                std::set<int> &s_visited_faces,
                std::set<int> &s_to_explore) {

    // compute intersections
    Mesh::VertexHandle v0, v1, v2;
    Mesh::FaceHandle fv0v1, fv1v2, fv2v0;

    getVertexAndFacesHandles(mesh, fh,
                             v0, v1, v2,
                             fv0v1, fv1v2, fv2v0);


#if  CONNECTED
    // look if there is a hole or a border => edge partly intern and boundary
    if (hasInternBoundary(mesh,fh, center, radius))
        return true;
#endif

    // condition for flagging the triangle as all inside or all outside:
    // all the vertices outside or cube of size 2*radius,

    unsigned nb_its_v0_v1 = getNbIntersection(mesh.point(v0), mesh.point(v1), center, radius);
    unsigned nb_its_v0_v2 = getNbIntersection(mesh.point(v0), mesh.point(v2), center, radius);
    unsigned nb_its_v1_v2 = getNbIntersection(mesh.point(v1), mesh.point(v2), center, radius);

    unsigned sum_its = nb_its_v0_v1 + nb_its_v0_v2 + nb_its_v1_v2;

    if (sum_its == 2) {
        // either on faces, or the same, 6 cases
        if (nb_its_v0_v1 == 1 && nb_its_v0_v2 == 1) {
            addBorder(borderGraph, v0, v1, 0, v0, v2, 0);

        } else if (nb_its_v0_v1 == 1 && nb_its_v1_v2 == 1) {
            addBorder(borderGraph, v0, v1, 0, v1, v2, 0);

        } else if (nb_its_v0_v2 == 1 && nb_its_v1_v2 == 1) {
            addBorder(borderGraph, v0, v2, 0, v1, v2, 0);

        } else if (nb_its_v0_v1 == 2) {
            addBorder(borderGraph, v0, v1, 1, v0, v1, 2);

        } else if (nb_its_v0_v2 == 2) {
            addBorder(borderGraph, v0, v2, 1, v0, v2, 2);

        } else if (nb_its_v1_v2 == 2) {
            addBorder(borderGraph, v1, v2, 1, v1, v2, 2);
        }

    } else if (sum_its == 4) {
        // either 2 and 2 or 2 1 1
        if (nb_its_v0_v1 == 0) {
            addBorder(borderGraph, v2, v1, 1, v2, v0, 1);
            addBorder(borderGraph, v2, v1, 2, v2, v0, 2);

        } else if (nb_its_v0_v2 == 0) {
            addBorder(borderGraph, v1, v0, 1, v1, v2, 1);
            addBorder(borderGraph, v1, v0, 2, v1, v2, 2);

        } else if (nb_its_v1_v2 == 0) {
            addBorder(borderGraph, v0, v1, 1, v0, v2, 1);
            addBorder(borderGraph, v0, v1, 2, v0, v2, 2);

        } else if (nb_its_v0_v1 == 2) {
            addBorder(borderGraph, v0, v2, 0, v0, v1, 1);
            addBorder(borderGraph, v1, v2, 0, v1, v0, 1);

        } else if (nb_its_v0_v2 == 2) {
            addBorder(borderGraph, v0, v1, 0, v0, v2, 1);
            addBorder(borderGraph, v2, v1, 0, v2, v0, 1);

        } else if (nb_its_v1_v2 == 2) {
            addBorder(borderGraph, v1, v0, 0, v1, v2, 1);
            addBorder(borderGraph, v2, v0, 0, v2, v1, 1);
        }


    } else if (sum_its == 6) {
        addBorder(borderGraph, v0, v1, 1, v0, v2, 1);
        addBorder(borderGraph, v2, v0, 1, v2, v1, 1);
        addBorder(borderGraph, v1, v0, 1, v1, v2, 1);

    } else {
        if (sum_its != 0) {
            //std::cout << "Error, sum_its=" << sum_its << std::endl;
            return true;
        }
    }

    // if no intersection, add if edge is intern
    bool isV0intern = ((center-mesh.point(v0)).norm() < radius );
    bool isV1intern = ((center-mesh.point(v1)).norm() < radius );
    bool isV2intern = ((center-mesh.point(v2)).norm() < radius );

    if (sum_its == 0 && !isV0intern && !isV1intern && !isV2intern)
        return true;

    // add faces for visiting
    s_visited_faces.insert(fh.idx());

    // add faces if intersections on edges
    if (nb_its_v0_v1 > 0 && fv0v1.idx() != -1) {
        if (!hasFace(s_visited_faces, fv0v1.idx()) && !hasFace(s_to_explore, fv0v1.idx()))
            s_to_explore.insert(fv0v1.idx());
    }
    if (nb_its_v1_v2 > 0 && fv1v2.idx() != -1) {
        if (!hasFace(s_visited_faces, fv1v2.idx()) && !hasFace(s_to_explore, fv1v2.idx()))
            s_to_explore.insert(fv1v2.idx());
    }
    if (nb_its_v0_v2 > 0 && fv2v0.idx() != -1) {
        if (!hasFace(s_visited_faces, fv2v0.idx()) && !hasFace(s_to_explore, fv2v0.idx()))
            s_to_explore.insert(fv2v0.idx());
    }

    if ((nb_its_v0_v1 == 0) && (fv0v1.idx() != -1) && isV0intern) {
        if (!hasFace(s_visited_faces, fv0v1.idx()) && !hasFace(s_to_explore, fv0v1.idx()))
            s_to_explore.insert(fv0v1.idx());
    }
    if ((nb_its_v1_v2 == 0) && (fv1v2.idx() != -1) && isV1intern) {
        if (!hasFace(s_visited_faces, fv1v2.idx()) && !hasFace(s_to_explore, fv1v2.idx()))
            s_to_explore.insert(fv1v2.idx());
    }
    if ((nb_its_v0_v2 == 0) && (fv2v0.idx() != -1) && isV2intern) {
        if (!hasFace(s_visited_faces, fv2v0.idx()) && !hasFace(s_to_explore, fv2v0.idx()))
            s_to_explore.insert(fv2v0.idx());
    }

    return false;
}


bool isOnMacroSurface(Mesh &mesh, Mesh::VertexHandle vh, float resolution) {

    //std::vector<Mesh::FaceHandle> visited_faces;
    //std::vector<Mesh::FaceHandle> to_explore;

    std::set<int> s_visited_faces;
    std::set<int> s_to_explore;

    BorderGraph borderGraph;

    // conditions of border : if edge boundary inside or  partly inside
    // in the sphere => return false
    for (Mesh::VertexFaceIter vf_it=mesh.vf_iter(vh); vf_it.is_valid(); ++vf_it) {
        s_to_explore.insert(vf_it->idx());
    }

    while (s_to_explore.size() > 0) {
        int current = *(s_to_explore.begin());
        s_to_explore.erase(s_to_explore.begin());
        bool macroOrError = studyFace(mesh, borderGraph, Mesh::FaceHandle(current), mesh.point(vh), resolution, s_visited_faces, s_to_explore);
        if (macroOrError) {
            //std::cout << "macro or error" << std::endl;
            return true;
        }
    }

    // graph finished
#if CONNECTED
    return borderGraph.hasSingleLoop();
#else
    return borderGraph.hasSingleComponent();
#endif
}

// id. but with a point somewhere is the middle of an edge
// we give the edge and the position
bool isOnMacroSurface(Mesh &mesh, Mesh::FaceHandle fh, Mesh::Point point, float resolution) {

    //std::vector<Mesh::FaceHandle> visited_faces;
    //std::vector<Mesh::FaceHandle> to_explore;

    //int nb_explored = 0;

    std::set<int> s_visited_faces;
    std::set<int> s_to_explore;

    BorderGraph borderGraph;

    // conditions of border : if edge boundary inside or  partly inside
    // in the sphere => return false
    // init with one face
    s_to_explore.insert(fh.idx());

    while (s_to_explore.size() > 0) {
        int current = *(s_to_explore.begin());
        s_to_explore.erase(s_to_explore.begin());
        bool macroOrError = studyFace(mesh, borderGraph, Mesh::FaceHandle(current), point, resolution, s_visited_faces, s_to_explore);
        //nb_explored++;
        if (macroOrError) {
            //std::cout << "macro ou erreur" << std::endl;
            return true;
        }
    }
    //std::cout << "explored : " << nb_explored << std::endl;
    // graph finished
#if CONNECTED
    return borderGraph.hasSingleLoop();
#else
    return borderGraph.hasSingleComponent();
#endif
}

// this assumes that none of the vertices of fh are on a macro surface
bool hasNonMacroEdgeSurface(Mesh &mesh, Mesh::FaceHandle fh, Mesh::HalfedgeHandle heh, float resolution) {
    // is size edge < resolution
    Mesh::Point p1 = mesh.point(mesh.to_vertex_handle(heh));
    Mesh::Point p2 = mesh.point(mesh.from_vertex_handle(heh));
    float dist_p12 = norm(p1 - p2);
    if (dist_p12 < resolution*2.f)
        return false;
    float cur_dist = resolution;
    while (cur_dist < dist_p12) {
        Mesh::Point current_point = p1 + (p2 - p1) / dist_p12 * cur_dist; // dist of cur_dist from p1
        if (!isOnMacroSurface(mesh, fh, current_point, resolution)) {
            return true;
        }
        cur_dist += resolution;
    }
    return false;
}
