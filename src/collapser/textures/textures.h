#ifndef TEXTURES_H
#define TEXTURES_H

#include "../mesh/mesh.h"
#include "../utils/utils.h"
#include "../sggx/sggx.h"

#include <sys/stat.h>
#include <unistd.h>

#include <pngwriter.h>


struct Texture {
    std::string tex_source_path;
    std::string tex_0_path;
    std::string tex_1_path;
    std::string tex_mesh_path;
    std::string tex_source_detached_path;
    std::string tex_detached_path;

    pngwriter tex_source_png;
    pngwriter tex_0_png;
    pngwriter tex_1_png;
    pngwriter tex_mesh_png;
    pngwriter tex_source_detached_png;
    pngwriter tex_detached_png;


    void init();

    bool init(std::string root, std::string mesh_name, std::string tex_name, std::string resolution);
    bool initMesh(int nb_faces_mesh);

    void close();

    void filter(Mesh &mesh, Mesh &mesh_final, const std::vector<int> &mesh_final_faces, std::vector<float> &weight,
                int nb_faces_mesh, int num_face_mesh, int resolution);

    void copy0to1(Mesh::TexCoord2D texcoord);
    void copySourceToDetached();

    void fill(double f);

    Mesh::Point getValueSource(Mesh::TexCoord2D tc);
    Mesh::Point getValueDetached(Mesh::TexCoord2D tc);

};



struct TextureSGGX {
    Texture sggx_sigma;
    Texture sggx_r;

    void init();
    bool init(std::string root, std::string mesh_name, std::string tex_name, std::string resolution);
    bool initMesh(int nb_faces_mesh);

    void close();

    void filter(Mesh &mesh, Mesh &mesh_final, const std::vector<int> &mesh_final_faces, std::vector<float> &weight,
                int nb_faces_mesh, int num_face_mesh, int resolution);

    void copy0to1(Mesh::TexCoord2D texcoord);

    SGGX getValueSource(Mesh::TexCoord2D tc);
    SGGX getValueDetached(Mesh::TexCoord2D tc);


};

struct TexturesList {
    bool has_diffuse_front;
    Texture diffuse_front;

    bool has_specular_front;
    Texture specular_front;

    bool has_sggx_front;
    TextureSGGX sggx_front;

    void copy0to1(Mesh::TexCoord2D texcoord) {
        if (has_diffuse_front)
            diffuse_front.copy0to1(texcoord);

        if (has_specular_front)
            specular_front.copy0to1(texcoord);

        if (has_sggx_front)
            sggx_front.copy0to1(texcoord);
    }

};

// Textures utils

void initTextures(TexturesList &texturesList, std::string root, std::string mesh_name, int resolution);
void initTexturesMesh(TexturesList &texturesList, int nb_faces_mesh);

void closeTextures(TexturesList &texturesList);

void filterTextures(TexturesList &texturesList,
                    Mesh &mesh, Mesh &mesh_final, const std::vector<int> &mesh_final_faces, std::vector<float> &weight,
                    int nb_faces_mesh, int num_face_mesh, int resolution);


#endif // TEXTURES_H
