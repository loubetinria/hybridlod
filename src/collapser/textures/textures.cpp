#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <assert.h>
#include <limits>
#include <string>     // std::string, std::to_string

#include "textures.h"

// struct Texture

void Texture::init() {
    tex_source_png.readfromfile(tex_source_path.c_str());
    assert(tex_source_png.getheight()*tex_source_png.getwidth() > 1);
    std::cout << "Reading " << tex_source_path << std::endl;
    tex_0_png.readfromfile(tex_source_path.c_str());
    tex_0_png.pngwriter_rename(tex_0_path.c_str());
    tex_1_png.readfromfile(tex_source_path.c_str());
    tex_1_png.pngwriter_rename(tex_1_path.c_str());

    //tex_1_png = pngwriter(4096, tex_source_png.getheight(), 0, tex_1_path.c_str());
    //tex_mesh_png = pngwriter(4096, nb_faces_mesh/4096+1, 0, tex_mesh_path.c_str());
    if (exists(tex_source_detached_path)) {
        std::cout << "Reading " << tex_source_detached_path << std::endl;
        tex_source_detached_png.readfromfile(tex_source_detached_path.c_str());
        tex_detached_png.readfromfile(tex_source_detached_path.c_str());
        tex_detached_png.pngwriter_rename(tex_detached_path.c_str());
    }
}

bool Texture::init(std::string root, std::string mesh_name, std::string tex_name, std::string resolution) {
    std::string tex_path = root + "maps/" + mesh_name + "_" + tex_name + ".png";
    if (exists(tex_path)) {
        tex_source_path = tex_path;
        tex_0_path = root + "maps/lod" + resolution + "_0_" + tex_name + ".png";
        tex_1_path = root + "maps/lod" + resolution + "_1_" + tex_name + ".png";
        tex_mesh_path = root + "maps/lod" + resolution + "_" + tex_name + ".png";
        tex_source_detached_path = root + "maps/" + mesh_name + "_detached_" + tex_name + ".png";
        tex_detached_path = root + "maps/lod" + resolution + "_detached_" + tex_name + ".png";
        init();
        return true;
    } else {
        std::cout << "Not found " << tex_path << std::endl;
        return false;
    }
    return false;
}

bool Texture::initMesh(int nb_faces_mesh) {
    if (exists(tex_source_path)) {
        tex_mesh_png = pngwriter(4096, nb_faces_mesh/4096+1, 0, tex_mesh_path.c_str());
        return true;
    } else {
        return false;
    }
    return false;
}

void Texture::close() {
    tex_0_png.close();
    tex_1_png.close();
    tex_mesh_png.close();
    std::cout << "Writing " << tex_0_path << std::endl;
    std::cout << "Writing " << tex_1_path << std::endl;
    std::cout << "Writing " << tex_mesh_path << std::endl;
}

void Texture::filter(Mesh &mesh, Mesh &mesh_final, const std::vector<int> &mesh_final_faces, std::vector<float> &weight,
                     int nb_faces_mesh, int num_face_mesh, int resolution) {

    double R = 0.0, G = 0.0, B = 0.0;
    std::vector<int> x_coords;
    std::vector<int> y_coords;



    for (unsigned i = 0; i < mesh_final_faces.size(); i++) {
        Mesh::FaceHalfedgeIter fhe_it=mesh_final.fh_iter(Mesh::FaceHandle(mesh_final_faces[i]));
        Mesh::TexCoord2D coord = mesh_final.texcoord2D(*fhe_it);
        //coord = coordonnées de texture entre 0 et 1 du matériau du triangle de l'input
        // dans tex_source_png
        // TODO : lecture exacte pour pas perdre en précision
        x_coords.push_back(int(coord[0] * 4096) + 1);
        y_coords.push_back(int(coord[1] * tex_source_png.getheight()) + 1);
        //std::cout << coord[0] * 4096 << " " << coord[1] * tex_source_png.getheight() << std::endl;
        //std::cout << x_coords[i] << " " << y_coords[i] << std::endl;

        // carefull, dread doesnt seam to work well as it doesn't respect the spec
        R += weight[i] * float(tex_source_png.read(x_coords[i], y_coords[i], 1))/65535.0;
        G += weight[i] * float(tex_source_png.read(x_coords[i], y_coords[i], 2))/65535.0;
        B += weight[i] * float(tex_source_png.read(x_coords[i], y_coords[i], 3))/65535.0;
    }

    //std::cout << "filtered " << R << " " << G << " " << B << std::endl;

    // écrire tex_1_png
    for (unsigned i = 0; i < mesh_final_faces.size(); i++) {
        tex_1_png.plot(x_coords[i], y_coords[i], R, G, B);
    }

    // écrire tex_mesh_png
    int x = num_face_mesh % 4096;
    int y = num_face_mesh / 4096;
    tex_mesh_png.plot(x+1, y+1, R, G, B);
    // todo : set tex coord pour mesh
}

void Texture::copy0to1(Mesh::TexCoord2D texcoord) {
    int x = int(texcoord[0] * 4096) + 1;
    int y = int(texcoord[1] * tex_source_png.getheight()) + 1;

    int R = tex_0_png.read(x,y,1);
    int G = tex_0_png.read(x,y,2);
    int B = tex_0_png.read(x,y,3);
    tex_1_png.plot(x, y, R, G, B);
}

void Texture::copySourceToDetached() {
    for (int x = 1; x <= tex_source_detached_png.getwidth(); x++) {
        for (int y = 1; y <= tex_source_detached_png.getheight(); y++) {
            int R = tex_source_detached_png.read(x,y,1);
            int G = tex_source_detached_png.read(x,y,2);
            int B = tex_source_detached_png.read(x,y,3);
            tex_detached_png.plot(x, y, R, G, B);
        }
    }
}



void Texture::fill(double f) {
    for (int i = 1; i <= tex_0_png.getwidth(); i++)
        for (int j = 1; j <= tex_0_png.getheight(); j++)
            tex_0_png.plot(i,j,f,f,f);

    for (int i = 1; i <= tex_1_png.getwidth(); i++)
        for (int j = 1; j <= tex_1_png.getheight(); j++)
            tex_1_png.plot(i,j,f,f,f);

    for (int i = 1; i <= tex_mesh_png.getwidth(); i++)
        for (int j = 1; j <= tex_mesh_png.getheight(); j++)
            tex_mesh_png.plot(i,j,f,f,f);

}

Mesh::Point Texture::getValueSource(Mesh::TexCoord2D tc) {

    int x_coord = int(tc[0] * 4096) + 1;
    int y_coord = int(tc[1] * tex_source_png.getheight()) + 1;

    Mesh::Point res(0,0,0);

    // carefull, dread doesnt seam to work well as it doesn't respect the spec
    res[0] = float(tex_source_png.read(x_coord, y_coord, 1))/65535.0;
    res[1] = float(tex_source_png.read(x_coord, y_coord, 2))/65535.0;
    res[2] = float(tex_source_png.read(x_coord, y_coord, 3))/65535.0;
    return res;
}

Mesh::Point Texture::getValueDetached(Mesh::TexCoord2D tc) {

    int x_coord = int(tc[0] * 4096) + 1;
    int y_coord = int(tc[1] * tex_detached_png.getheight()) + 1;

    Mesh::Point res(0,0,0);


    res[0] = float(tex_detached_png.read(x_coord, y_coord, 1))/65535.0;
    res[1] = float(tex_detached_png.read(x_coord, y_coord, 2))/65535.0;
    res[2] = float(tex_detached_png.read(x_coord, y_coord, 3))/65535.0;
    assert(res[0] <= 1.0);
    assert(res[1] <= 1.0);
    assert(res[2] <= 1.0);

    return res;
}

// struct TextureSGGX

void TextureSGGX::init() {
    sggx_sigma.init();
    sggx_r.init();
}

bool TextureSGGX::initMesh(int nb_faces_mesh) {
    return sggx_sigma.initMesh(nb_faces_mesh) && sggx_r.initMesh(nb_faces_mesh);
}

bool TextureSGGX::init(std::string root, std::string mesh_name, std::string tex_name, std::string resolution) {
    return sggx_sigma.init(root,mesh_name, "sggx_sigma" + tex_name, resolution)
            && sggx_r.init(root,mesh_name, "sggx_r" + tex_name, resolution);
}

void TextureSGGX::close() {
    // Remove : just for tests
    //sggx_sigma.fill(1.0);
    //sggx_r.fill(0.5);

    sggx_sigma.close();
    sggx_r.close();
}

void TextureSGGX::filter(Mesh &mesh, Mesh &mesh_final, const std::vector<int> &mesh_final_faces, std::vector<float> &weight,
                         int nb_faces_mesh, int num_face_mesh, int resolution) {

    float Sxx = 0.0, Syy = 0.0, Szz = 0.0;
    float Sxy = 0.0, Sxz = 0.0, Syz = 0.0;

    std::vector<int> x_coords;
    std::vector<int> y_coords;

    for (unsigned i = 0; i < mesh_final_faces.size(); i++) {
        Mesh::FaceHalfedgeIter fhe_it=mesh_final.fh_iter(Mesh::FaceHandle(mesh_final_faces[i]));
        Mesh::TexCoord2D coord = mesh_final.texcoord2D(*fhe_it);
        //coord = coordonnées de texture entre 0 et 1 du matériau du triangle de l'input
        // dans tex_source_png
        // TODO : lecture exacte pour pas perdre en précision
        x_coords.push_back(int(coord[0] * 4096) + 1);
        y_coords.push_back(int(coord[1] * sggx_sigma.tex_source_png.getheight()) + 1);
        //std::cout << coord[0] * 4096 << " " << coord[1] * tex_source_png.getheight() << std::endl;
        //std::cout << x_coords[i] << " " << y_coords[i] << std::endl;

        // déjà entre 0 et 1
        float sigma_x = float(sggx_sigma.tex_source_png.read(x_coords[i], y_coords[i], 1))/65535.0;
        float sigma_y = float(sggx_sigma.tex_source_png.read(x_coords[i], y_coords[i], 2))/65535.0;
        float sigma_z = float(sggx_sigma.tex_source_png.read(x_coords[i], y_coords[i], 3))/65535.0;

        // from 0 .. 1 to -1 .. 1
        float r_xy = float(sggx_r.tex_source_png.read(x_coords[i], y_coords[i], 1))/65535.0*2.0 - 1.0;
        float r_xz = float(sggx_r.tex_source_png.read(x_coords[i], y_coords[i], 2))/65535.0*2.0 - 1.0;
        float r_yz = float(sggx_r.tex_source_png.read(x_coords[i], y_coords[i], 3))/65535.0*2.0 - 1.0;

        Sxx += weight[i] * sigma_x * sigma_x;
        Syy += weight[i] * sigma_y * sigma_y;
        Szz += weight[i] * sigma_z * sigma_z;
        Sxy += weight[i] * r_xy * sigma_x * sigma_y;
        Sxz += weight[i] * r_xz * sigma_x * sigma_z;
        Syz += weight[i] * r_yz * sigma_y * sigma_z;
    }

    //std::cout << "filtered " << R << " " << G << " " << B << std::endl;

    // repasser en sigma et r
    double f_sigma_x = std::sqrt(Sxx);
    double f_sigma_y = std::sqrt(Syy);
    double f_sigma_z = std::sqrt(Szz);
    double f_r_xy = Sxy / (f_sigma_x * f_sigma_y);
    double f_r_xz = Sxz / (f_sigma_x * f_sigma_z);
    double f_r_yz = Syz / (f_sigma_y * f_sigma_z);


    // écrire tex_1_png
    for (unsigned i = 0; i < mesh_final_faces.size(); i++) {
        sggx_sigma.tex_1_png.plot(x_coords[i], y_coords[i], f_sigma_x, f_sigma_y, f_sigma_z);
        sggx_r.tex_1_png.plot(x_coords[i], y_coords[i], f_r_xy*0.5+0.5, f_r_xz*0.5+0.5, f_r_yz*0.5+0.5);
    }

    // écrire tex_mesh_png
    int x = num_face_mesh % 4096;
    int y = num_face_mesh / 4096;
    sggx_sigma.tex_mesh_png.plot(x+1, y+1, f_sigma_x, f_sigma_y, f_sigma_z);
    sggx_r.tex_mesh_png.plot(x+1, y+1, f_r_xy*0.5+0.5, f_r_xz*0.5+0.5, f_r_yz*0.5+0.5);
    // todo : set tex coord pour mesh

}

void TextureSGGX::copy0to1(Mesh::TexCoord2D texcoord) {
    sggx_sigma.copy0to1(texcoord);
    sggx_r.copy0to1(texcoord);
}

SGGX TextureSGGX::getValueSource(Mesh::TexCoord2D tc) {

    int x_coord = int(tc[0] * 4096) + 1;
    int y_coord = int(tc[1] * sggx_sigma.tex_source_png.getheight()) + 1;

    // déjà entre 0 et 1
    float sigma_x = float(sggx_sigma.tex_source_png.read(x_coord, y_coord, 1))/65535.0;
    float sigma_y = float(sggx_sigma.tex_source_png.read(x_coord, y_coord, 2))/65535.0;
    float sigma_z = float(sggx_sigma.tex_source_png.read(x_coord, y_coord, 3))/65535.0;

    // from 0 .. 1 to -1 .. 1
    float r_xy = float(sggx_r.tex_source_png.read(x_coord, y_coord, 1))/65535.0*2.0 - 1.0;
    float r_xz = float(sggx_r.tex_source_png.read(x_coord, y_coord, 2))/65535.0*2.0 - 1.0;
    float r_yz = float(sggx_r.tex_source_png.read(x_coord, y_coord, 3))/65535.0*2.0 - 1.0;

    float Sxx = sigma_x * sigma_x;
    float Syy = sigma_y * sigma_y;
    float Szz = sigma_z * sigma_z;
    float Sxy = r_xy * sigma_x * sigma_y;
    float Sxz = r_xz * sigma_x * sigma_z;
    float Syz = r_yz * sigma_y * sigma_z;

    return SGGX(Mesh::Point(Sxx,Syy,Szz),Mesh::Point(Sxy,Sxz,Syz));
}

SGGX TextureSGGX::getValueDetached(Mesh::TexCoord2D tc) {

    int x_coord = int(tc[0] * 4096) + 1;
    int y_coord = int(tc[1] * sggx_sigma.tex_detached_png.getheight()) + 1;

    // déjà entre 0 et 1
    float sigma_x = float(sggx_sigma.tex_detached_png.read(x_coord, y_coord, 1))/65535.0;
    float sigma_y = float(sggx_sigma.tex_detached_png.read(x_coord, y_coord, 2))/65535.0;
    float sigma_z = float(sggx_sigma.tex_detached_png.read(x_coord, y_coord, 3))/65535.0;

    // from 0 .. 1 to -1 .. 1
    float r_xy = float(sggx_r.tex_detached_png.read(x_coord, y_coord, 1))/65535.0*2.0 - 1.0;
    float r_xz = float(sggx_r.tex_detached_png.read(x_coord, y_coord, 2))/65535.0*2.0 - 1.0;
    float r_yz = float(sggx_r.tex_detached_png.read(x_coord, y_coord, 3))/65535.0*2.0 - 1.0;

    float Sxx = sigma_x * sigma_x;
    float Syy = sigma_y * sigma_y;
    float Szz = sigma_z * sigma_z;
    float Sxy = r_xy * sigma_x * sigma_y;
    float Sxz = r_xz * sigma_x * sigma_z;
    float Syz = r_yz * sigma_y * sigma_z;

    return SGGX(Mesh::Point(Sxx,Syy,Szz),Mesh::Point(Sxy,Sxz,Syz));
}

// Textures utils


void initTextures(TexturesList &texturesList, std::string root, std::string mesh_name, int resolution) {
    texturesList.has_diffuse_front = texturesList.diffuse_front.init(root,mesh_name, "diffuse_front",
                                                                     to_string(resolution));
    texturesList.has_specular_front = texturesList.specular_front.init(root,mesh_name, "specular_front",
                                                                       to_string(resolution));
    texturesList.has_sggx_front = texturesList.sggx_front.init(root,mesh_name, "_front",
                                                               to_string(resolution));
}

void initTexturesMesh(TexturesList &texturesList, int nb_faces_mesh) {
    texturesList.has_diffuse_front = texturesList.diffuse_front.initMesh(nb_faces_mesh);
    texturesList.has_specular_front = texturesList.specular_front.initMesh(nb_faces_mesh);
    texturesList.has_sggx_front = texturesList.sggx_front.initMesh(nb_faces_mesh);
}

void closeTextures(TexturesList &texturesList) {
    if (texturesList.has_diffuse_front)
        texturesList.diffuse_front.close();
    if (texturesList.has_specular_front)
        texturesList.specular_front.close();
    if (texturesList.has_sggx_front)
        texturesList.sggx_front.close();
}

void filterTextures(TexturesList &texturesList,
                    Mesh &mesh, Mesh &mesh_final, const std::vector<int> &mesh_final_faces, std::vector<float> &weight,
                    int nb_faces_mesh, int num_face_mesh, int resolution) {

    // filtrer chaque texture
    // ecrire le filtrage dans num_face_mesh, et dans lodN_1 dans les cases de toutes les faces de mesh_final_faces

    if (texturesList.has_diffuse_front)
        texturesList.diffuse_front.filter(mesh,mesh_final,mesh_final_faces,weight,nb_faces_mesh,num_face_mesh,resolution);

    if (texturesList.has_specular_front)
        texturesList.specular_front.filter(mesh,mesh_final,mesh_final_faces,weight,nb_faces_mesh,num_face_mesh,resolution);

    if (texturesList.has_sggx_front)
        texturesList.sggx_front.filter(mesh,mesh_final,mesh_final_faces,weight,nb_faces_mesh,num_face_mesh,resolution);

}

