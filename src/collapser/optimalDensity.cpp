#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <assert.h>
#include <limits>
#include <string>  
#include "volume/volume.h"
#include <deque>
#include <list>
#include <sys/stat.h>
#include <unistd.h>




/*
 * argv[1] = root, genre /disc/loubet/Models/LoD/Lenin/
 * argv[2] = input, in root + Geometry/, sans .obj
 */
int main(int argc, char *argv[])
{
    assert(argc == 2);
    std::string root(argv[1]);

    Volume volume(root);
    //volume.optimalDensity(256);
    volume.optimalDensity(128);
    volume.optimalDensity(64);
    volume.optimalDensity(32);
    volume.optimalDensity(16);
    volume.optimalDensity(8);



}
