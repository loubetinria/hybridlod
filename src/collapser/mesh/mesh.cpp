#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <assert.h>
#include <limits>
#include <string>     // std::string, std::to_string

#include "mesh.h"
#include "../utils/utils.h"

void read(Mesh &mesh, const char *name) {
    bool statusOK = true;
    try
    {
        OpenMesh::IO::Options opt(OpenMesh::IO::Options::FaceTexCoord);

        if ( !OpenMesh::IO::read_mesh(mesh, name, opt) )
        {
            std::cerr << "Cannot read mesh" << name << std::endl;
            statusOK = false;
        }
    }
    catch( std::exception& x )
    {
        std::cerr << x.what() << std::endl;
        statusOK = false;
    }
    assert(statusOK);
}

void readLocked(Mesh &mesh, const char *name) {
    std::string filename_locked(name);
    std::ifstream myfile_locked;
    std::cout << "Reading " << filename_locked << "... ";
    myfile_locked.open(filename_locked);
    int v_id;
    int count = 0;
    while (myfile_locked >> v_id) {
        // commence à 0?
        mesh.status(Mesh::VertexHandle(v_id-1)).set_locked(true);
        count++;
    }
    myfile_locked.close();
    std::cout <<  count << " locked" << std::endl;
}

void writeLocked(Mesh &mesh, std::string root, int gridsize) {

    std::stringstream ss_locked;
    ss_locked << root << "geometry/lod"<< gridsize <<"_locked.list";
    std::string filename_locked = ss_locked.str();
    std::remove(filename_locked.c_str()); // delete file
    std::ofstream myfile_locked;

    std::cout << "Writing " << filename_locked << std::endl;


    myfile_locked.open(filename_locked);

    int vcount = 1;
    for (Mesh::VertexIter v_it=mesh.vertices_begin(); v_it!=mesh.vertices_end(); ++v_it) {
        if (mesh.status(*v_it).locked())
            myfile_locked << vcount << std::endl;
        vcount++;
    }
    myfile_locked.close();

}

void writeNewObj(Mesh &mesh, std::string path) {


    std::remove(path.c_str());

    std::ofstream myfile;
    myfile.open(path.c_str());
    for (Mesh::VertexIter v_it=mesh.vertices_begin(); v_it!=mesh.vertices_end(); ++v_it) {
        Mesh::Point p = mesh.point(*v_it);
        myfile << "v " << p[0] << " " << p[1] << " " << p[2] << std::endl;

    }

    for (Mesh::FaceIter f_it=mesh.faces_begin(); f_it!=mesh.faces_end(); ++f_it) {
        Mesh::FaceHalfedgeIter fhe_it=mesh.fh_iter(*f_it);
        Mesh::TexCoord2D texCoord2D = mesh.texcoord2D(*fhe_it);
        myfile << "vt " << texCoord2D[0] << " " << texCoord2D[1] << std::endl;
    }

    for (Mesh::FaceIter f_it=mesh.faces_begin(); f_it!=mesh.faces_end(); ++f_it) {
        Mesh::FaceVertexIter fv_it=mesh.fv_iter(*f_it);
        int v0 = fv_it->idx(); fv_it++;
        int v1 = fv_it->idx(); fv_it++;
        int v2 = fv_it->idx();
        myfile << "f " << v0+1 << "/" << f_it->idx()+1 << " "
                       << v1+1 << "/" << f_it->idx()+1 << " "
                       << v2+1 << "/" << f_it->idx()+1 << " " << std::endl;
    }

    myfile.close();
}

void copyMesh(Mesh &src, Mesh &target, bool lock) {
    for (Mesh::VertexIter v_it=src.vertices_begin(); v_it!=src.vertices_end(); ++v_it) {
        target.add_vertex(src.point(*v_it));
        if (lock)
            if (src.status(*v_it).locked())
                target.status(*v_it).set_locked(true);
    }

    std::vector<Mesh::VertexHandle> v(3);
    for (Mesh::FaceIter f_it=src.faces_begin(); f_it!=src.faces_end(); ++f_it) {
        getVertexHandles(src, *f_it, v[0], v[1], v[2]);
        target.add_face(v);

        // get tex coord
        Mesh::FaceHalfedgeIter fhe_it=src.fh_iter(*f_it);
        Mesh::TexCoord2D tex = src.texcoord2D(*fhe_it);

        // set tex coord
        Mesh::FaceHalfedgeIter fhe_it2=target.fh_iter(*f_it);
        target.set_texcoord2D(*fhe_it2, tex);
        fhe_it2++;
        target.set_texcoord2D(*fhe_it2, tex);
        fhe_it2++;
        target.set_texcoord2D(*fhe_it2, tex);

    }
}
