#ifndef MESH_H
#define MESH_H



#include <OpenMesh/Core/IO/MeshIO.hh>
#include <OpenMesh/Core/Mesh/PolyMesh_ArrayKernelT.hh>
#include <OpenMesh/Core/Mesh/TriMesh_ArrayKernelT.hh>


//Mesh
typedef OpenMesh::TriMesh_ArrayKernelT<>  Mesh;

void read(Mesh &mesh, const char *name);

void readLocked(Mesh &mesh, const char *name);

void writeNewObj(Mesh &mesh, std::string path);

void writeLocked(Mesh &mesh, std::string root, int gridsize);

void copyMesh(Mesh &src, Mesh &target, bool lock);


#endif // MESH_H
