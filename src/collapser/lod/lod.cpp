#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <assert.h>
#include <limits>
#include <string>     // std::string, std::to_string

#include "lod.h"
#include "../mesh/mesh.h"
#include "../macrosurface/macrosurface.h"
#include "../utils/utils.h"
#include "../utils/scripts.h" // useless
#include "../textures/textures.h"
#include "../decimation/decimaterHelper.h"
#include "../volume/volume.h"
#include "../utils/gridInfo.h"

#include <boost/filesystem.hpp>

#define ASPECT_RATIO 5.0
#define MIN_AREA 0.3
#define MAX_AREA 1.5

//OpenMesh::VPropHandleT<int> vGroup;

Lod::Lod(std::string root, int res) {

    /*
    vGroup = OpenMesh::VPropHandleT<int>();
    vTarget = OpenMesh::VPropHandleT<Mesh::Point>() ;
    vTargetId = OpenMesh::VPropHandleT<int>();
    vSource = OpenMesh::VPropHandleT<Mesh::Point>();
    vOrigin = OpenMesh::VPropHandleT<Mesh::Point> ();
    fTargetId = OpenMesh::FPropHandleT<int> ();
    vIsMacro = OpenMesh::VPropHandleT<bool> ();
    */

    _root = root;
    _mesh_name = "input";
    _mesh_path = root + "geometry/input.obj";
    request_attributes(_mesh, _mesh_final, _mesh_detached);
    request_properties(_mesh, _mesh_final, _mesh_detached);

    _targetResolution = res;

    std::cout << "Reading "  << _mesh_path << std::endl;
    read(_mesh, _mesh_path.c_str());
    //std::cout << "Copying in mesh_final..." << std::endl;
    //copyMesh(_mesh, _mesh_final, false);

    //read(_mesh, _mesh_path.c_str());
    //readLocked(_mesh, _locked_path.c_str());
    read(_mesh_final, _mesh_path.c_str());

    _gridInfo = GridInfo(_mesh, _targetResolution);
}

Lod::Lod(Lod &previous) {

    _root = previous._root;
    std::stringstream ss;
    ss << "lod" << previous._targetResolution;
    _mesh_name = ss.str();
    _mesh_path = _root + "geometry/" + _mesh_name + ".obj";
    _locked_path = _root + "geometry/" + _mesh_name + "_locked.list";
    request_attributes(_mesh, _mesh_final, _mesh_detached);
    request_properties(_mesh, _mesh_final, _mesh_detached);

    _targetResolution = previous._targetResolution / 2;

    //read(_mesh, _mesh_path.c_str());
    //readLocked(_mesh, _locked_path.c_str());
    //read(_mesh_final, _mesh_path.c_str());
    std::cout << "Copying mesh from previous..." << std::endl;
    copyMesh(previous._mesh, _mesh, true);
    std::cout << "Copying mesh_final from previous..." << std::endl;
    copyMesh(previous._mesh, _mesh_final, false);
    std::cout << "Copying mesh_detached from previous..." << std::endl;
    copyMesh(previous._mesh_detached, _mesh_detached, false);

    _gridInfo=previous._gridInfo.nextLevel();

}

void Lod::process() {

    // init textures : read, prepare,
    // all but "mesh" texture because needs final number of triangles
    initTextures(_texturesList, _root, _mesh_name, _targetResolution);

    // compute macro surface and remove cylinders
    std::cout << "Voxel side = radius : " << _gridInfo.voxelSide << std::endl;
    computeMacro(_mesh, _gridInfo.voxelSide);
    detachMacro(_mesh, _mesh_final, _gridInfo.voxelSide);

    // init target mesh_final
    for (Mesh::VertexIter v_it=_mesh_final.vertices_begin(); v_it!=_mesh_final.vertices_end(); ++v_it) {
        _mesh_final.property(vTarget,*v_it) = Mesh::Point(-1,-1,-1);
        _mesh_final.property(vTargetId,*v_it) = -1;
    }

    for (Mesh::VertexIter v_it=_mesh.vertices_begin(); v_it!=_mesh.vertices_end(); ++v_it) {
        _mesh.property(vOrigin,*v_it) = _mesh.point(*v_it);
    }

    Decimater decimater(_mesh);
    DecimaterHelper decimaterHelper(&decimater);
    decimaterHelper.addModules(_gridInfo.voxelSide, &_texturesList, &_mesh_final);
    decimaterHelper.decimate();

    std::cout << "Find faces to delete..." << std::endl;
    std::vector<DeletedTriangle> deletedTriangles;
    findFacesToDetach(_mesh, deletedTriangles);

    std::cout << "Also delete neighboring faces..." << std::endl;
    findNeighboringFacesOfDetached(_mesh_final, deletedTriangles);

    std::cout << "Detach faces for interpolation..." << std::endl;
    detachFaces(_mesh_final, deletedTriangles);

    // we can delete triangles that have been separated
    std::cout << "Targeting..." << std::endl;
    targeting(_mesh_final, _mesh, decimaterHelper);

    std::cout << "Filtering..." << std::endl;
    //filterMaterials(_mesh_final, _mesh, _root, _mesh_name, _targetResolution);
    filterMaterials2(_mesh_final, _mesh, _root, _mesh_name, _targetResolution);

    std::cout << "Add detached faces to mesh_detached" << std::endl;
    addDetachedFaces(_mesh_detached, deletedTriangles);
    //assert(mesh_detached.n_faces() == deletedTriangles);
    //assert(detached >= nonMacro);

    _mesh_final.garbage_collection();

    for (int f = 0; f <= 10; f += 10) { // just 0 and 1

        // interpolate
        for (Mesh::VertexIter v_it=_mesh_final.vertices_begin(); v_it!=_mesh_final.vertices_end(); ++v_it) {
            Mesh::Point target = _mesh_final.property(vTarget,*v_it);
            Mesh::Point source = _mesh_final.property(vSource,*v_it);
            _mesh_final.set_point(*v_it, source*(1-float(f*0.1)) + target*float(f*0.1));
        }

        std::string filename;
        std::stringstream ss;
        ss << _root << "geometry/lod"<< _targetResolution <<"_" << f*10 <<".obj";
        filename = ss.str();

        try
        {
           writeNewObj(_mesh_final, filename);
           std::cout << "Writing " << filename << std::endl;

            if (f == 10) {
                std::string filename2;
                std::stringstream ss2;
                ss2 << _root << "geometry/lod"<< _targetResolution <<".obj";
                filename2 = ss2.str();
                _mesh.garbage_collection();
                writeNewObj(_mesh, filename2);
                writeLocked(_mesh, _root, _targetResolution);

                std::cout << "Writing " << filename2 << std::endl;

                // write also detached
                std::string filename3;
                std::stringstream ss3;
                ss3 << _root << "geometry/lod"<< _targetResolution <<"_detached.obj";
                filename3 = ss3.str();
                _mesh_detached.garbage_collection();
                std::cout << "Writing " << filename3 << std::endl;
                writeNewObj(_mesh_detached, filename3);
            }
        }
        catch( std::exception& x )
        {
            std::cerr << x.what() << std::endl;
            exit(1);
        }
    }

    if(_texturesList.has_diffuse_front) {
        std::cout << "Writing " << _texturesList.diffuse_front.tex_detached_path
                  << " (" << _texturesList.diffuse_front.tex_detached_png.getheight()
                  << "x" <<  _texturesList.diffuse_front.tex_detached_png.getwidth() << ") "<< std::endl;
        _texturesList.diffuse_front.tex_detached_png.close();
    }

    if(_texturesList.has_specular_front) {
        std::cout << "Writing " << _texturesList.specular_front.tex_detached_path
                  << " (" << _texturesList.specular_front.tex_detached_png.getheight()
                  << "x" <<  _texturesList.specular_front.tex_detached_png.getwidth() << ") "<< std::endl;
         _texturesList.specular_front.tex_detached_png.close();
    }

    if(_texturesList.has_sggx_front) {
        std::cout << "Writing " << _texturesList.sggx_front.sggx_sigma.tex_detached_path
                  << " (" << _texturesList.sggx_front.sggx_sigma.tex_detached_png.getheight()
                  << "x" <<  _texturesList.sggx_front.sggx_sigma.tex_detached_png.getwidth() << ") "<< std::endl;
        std::cout << "Writing " << _texturesList.sggx_front.sggx_r.tex_detached_path
                  << " (" << _texturesList.sggx_front.sggx_r.tex_detached_png.getheight()
                  << "x" <<  _texturesList.sggx_front.sggx_r.tex_detached_png.getwidth() << ") "<< std::endl;
        _texturesList.sggx_front.sggx_sigma.tex_detached_png.close();
        _texturesList.sggx_front.sggx_r.tex_detached_png.close();
    }

    std::stringstream ss_detached;
    ss_detached << _root << "geometry/lod"<< _targetResolution <<"_detached.obj";
    std::string filename_detached = ss_detached.str();

    std::stringstream ss_dd;
    ss_dd << _root << "volume/";
    std::string filename_dd = ss_dd.str();

    boost::filesystem::create_directory(_root + "volume");
    boost::filesystem::create_directory(_root + "scripts");

    std::stringstream ss_name_script;
    ss_name_script << _root << "scripts/voxelize"<< _targetResolution <<".sh";
     std::string name_script = ss_name_script.str();
    writeVoxelizerScript(filename_detached,
                         filename_dd,
                         name_script,
                         _root,
                         _targetResolution,
                         _gridInfo.gridX,
                         _gridInfo.gridX,
                         _gridInfo.gridX,
                         _gridInfo.bbmin,
                         _gridInfo.bbmin + Mesh::Point(_gridInfo.gridX,_gridInfo.gridX,_gridInfo.gridX)*_gridInfo.voxelSide,
                         _gridInfo.voxelSide);

}

void Lod::request_attributes(Mesh &mesh, Mesh &mesh_final, Mesh &mesh_detached) {

    mesh.request_vertex_status();
    mesh.request_edge_status();
    mesh.request_face_status();
    mesh.request_halfedge_texcoords2D();
    mesh.request_halfedge_status();
    //mesh.request_vertex_colors();

    mesh_final.request_vertex_status();
    mesh_final.request_edge_status();
    mesh_final.request_face_status();
    //mesh_final.request_vertex_colors();
    mesh_final.request_halfedge_texcoords2D();

    mesh_detached.request_halfedge_texcoords2D();
    mesh_detached.request_vertex_status();
    mesh_detached.request_edge_status();
    mesh_detached.request_face_status();
}

void Lod::request_properties(Mesh &mesh, Mesh &mesh_final, Mesh &mesh_detached) {

    mesh.add_property(fIsMacro);
    mesh.add_property(vOrigin);
    mesh_final.add_property(vTarget);
    mesh_final.add_property(vTargetId);
    mesh_final.add_property(vSource);
    mesh_final.add_property(fTargetId);

    /*
    vGroup = OpenMesh::VPropHandleT<int>();
    vTarget = OpenMesh::VPropHandleT<Mesh::Point>() ;
    vTargetId = OpenMesh::VPropHandleT<int>();
    vSource = OpenMesh::VPropHandleT<Mesh::Point>();
    vOrigin = OpenMesh::VPropHandleT<Mesh::Point> ();
    fTargetId = OpenMesh::FPropHandleT<int> ();
    vIsMacro = OpenMesh::VPropHandleT<bool> ();
    */
}



bool Lod::isGood(Mesh &mesh, Mesh::FaceHandle fh, float size) {
    //return (!hasEdgeSmallerThat(mesh, fh, size) && (aspectRatio(mesh,fh) < ASPECT_RATIO));
    //return ((aspectRatio(mesh,fh) < ASPECT_RATIO));
    return (area(mesh,fh) > size*size*MIN_AREA); // two times too small
}

/*
    Look in mesh faces that must be deleted.
    Delete them.
    Get a list for re-adding them separately.
 */
unsigned Lod::findFacesToDetach(Mesh &mesh, std::vector<DeletedTriangle> &deletedTriangles) {

    for (Mesh::FaceIter f_it=mesh.faces_sbegin(); f_it!=mesh.faces_end(); ++f_it) {

        if (!mesh.status(*f_it).deleted() /*&& !isGood(mesh, *f_it, voxelSide)*/) {

            bool oneGood = false;
            for(Mesh::FaceFaceIter ff_it=mesh.ff_iter(*f_it); ff_it.is_valid(); ++ff_it) {
                if (!mesh.status(*ff_it).deleted() /*&& isGood(mesh, *ff_it, voxelSide)*/) {
                    oneGood = true;
                    break;
                }
            }
            if (!oneGood) {
                DeletedTriangle deletedTri;
                int nnn = 0;
                for (Mesh::FaceVertexIter fv_it=mesh.fv_iter(*f_it); fv_it.is_valid(); ++fv_it) {
                    if (nnn==0) {
                       deletedTri.p0_origin = mesh.property(vOrigin,*fv_it);
                    }
                    if (nnn==1) {
                        deletedTri.p1_origin = mesh.property(vOrigin,*fv_it);
                    }
                    if (nnn==2) {
                        deletedTri.p2_origin = mesh.property(vOrigin,*fv_it);
                    }
                    nnn++;
                }
                assert(nnn == 3);
                deletedTri.fh = *f_it;
                Mesh::FaceHalfedgeIter fhe_it=mesh.fh_iter(*f_it);
                deletedTri.texCoord2D = mesh.texcoord2D(*fhe_it);
                deletedTriangles.push_back(deletedTri);
            }
        }
    }

    std::cout << "Detaching faces : " << deletedTriangles.size() << std::endl;

    for (unsigned i = 0; i < deletedTriangles.size(); i++) {
        mesh.delete_face(deletedTriangles[i].fh);
    }

    return deletedTriangles.size();
}


void Lod::findNeighboringFacesOfDetached(Mesh &mesh_final, std::vector<DeletedTriangle> &deletedTriangles) {

    std::vector<DeletedTriangle> new_deletedTriangles;

    for (unsigned i = 0; i < deletedTriangles.size(); i++) {
        std::vector<Mesh::FaceHandle> neighboors;
        findNeighboringFaces(mesh_final, deletedTriangles[i].fh,neighboors);
        for (unsigned i = 0; i < neighboors.size(); i++) {
            DeletedTriangle dt;
            dt.fh = neighboors[i];
            int nnn = 0;
            for (Mesh::FaceVertexIter fv_it=mesh_final.fv_iter(dt.fh); fv_it.is_valid(); ++fv_it) {
                if (nnn==0) {
                    dt.p0_origin = mesh_final.point(*fv_it);
                }
                if (nnn==1) {
                    dt.p1_origin = mesh_final.point(*fv_it);
                }
                if (nnn==2) {
                    dt.p2_origin = mesh_final.point(*fv_it);
                }
                nnn++;
            }
            assert(nnn == 3);
            Mesh::FaceHalfedgeIter fhe_it=mesh_final.fh_iter(neighboors[i]);
            dt.texCoord2D = mesh_final.texcoord2D(*fhe_it);
            new_deletedTriangles.push_back(dt);
        }
    }

    // add new ones to old ones
    for (unsigned i = 0; i < new_deletedTriangles.size(); i++) {
        deletedTriangles.push_back(new_deletedTriangles[i]);
    }

}

/*
    Add as detached in final mesh (make them disapear)
 */
void Lod::detachFaces(Mesh &mesh_final, std::vector<DeletedTriangle> &deletedTriangles) {


    std::vector<DeletedTriangle> cleaned_deletedTriangles;

    for (unsigned i = 0; i < deletedTriangles.size(); i++) {

        DeletedTriangle dt = deletedTriangles[i];

        if (mesh_final.status(dt.fh).deleted())
            continue;

        int nnn = 0;
        for (Mesh::FaceVertexIter fv_it=mesh_final.fv_iter(dt.fh); fv_it.is_valid(); ++fv_it) {
            if (nnn==0) {
                dt.p0_origin = mesh_final.point(*fv_it);
                deletedTriangles[i].p0_origin = mesh_final.point(*fv_it);
            }
            if (nnn==1) {
                dt.p1_origin = mesh_final.point(*fv_it);
                deletedTriangles[i].p1_origin = mesh_final.point(*fv_it);
            }
            if (nnn==2) {
                dt.p2_origin = mesh_final.point(*fv_it);
                deletedTriangles[i].p2_origin = mesh_final.point(*fv_it);
            }
            nnn++;
        }

        cleaned_deletedTriangles.push_back(deletedTriangles[i]);

        // add separated faces in mesh_final
        std::vector<Mesh::VertexHandle>  face_vhandles_final;
        Mesh::VertexHandle vhandle_final[3];

        mesh_final.delete_face(dt.fh);

        vhandle_final[0] = mesh_final.add_vertex(dt.p0_origin);
        vhandle_final[1] = mesh_final.add_vertex(dt.p1_origin);
        vhandle_final[2] = mesh_final.add_vertex(dt.p2_origin);
        face_vhandles_final.clear();
        face_vhandles_final.push_back(vhandle_final[0]);
        face_vhandles_final.push_back(vhandle_final[1]);
        face_vhandles_final.push_back(vhandle_final[2]);
        Mesh::FaceHandle fh = mesh_final.add_face(face_vhandles_final);
        //Mesh::Point vmean = (dt.p0_origin+dt.p1_origin+dt.p2_origin)/3.0;
        //mesh_final.property(vTarget,face_vhandles_final[0]) = vmean;
        //mesh_final.property(vTarget,face_vhandles_final[1]) = vmean;
        //mesh_final.property(vTarget,face_vhandles_final[2]) = vmean;
        mesh_final.property(vTarget,face_vhandles_final[0]) = dt.p0_origin;
        mesh_final.property(vTarget,face_vhandles_final[1]) = dt.p1_origin;
        mesh_final.property(vTarget,face_vhandles_final[2]) = dt.p2_origin;
        mesh_final.property(vTargetId,face_vhandles_final[0]) = -2;
        mesh_final.property(vTargetId,face_vhandles_final[1]) = -2;
        mesh_final.property(vTargetId,face_vhandles_final[2]) = -2;

        Mesh::FaceHalfedgeIter fhe_it=mesh_final.fh_iter(fh);
        mesh_final.set_texcoord2D(*fhe_it, dt.texCoord2D);
        fhe_it++;
        mesh_final.set_texcoord2D(*fhe_it, dt.texCoord2D);
        fhe_it++;
        mesh_final.set_texcoord2D(*fhe_it, dt.texCoord2D);

        // Here, as faces may have been filtered, need to copy 0 into 1
        // to get the unfiltered data (since we keep the initial geometry)
        _texturesList.copy0to1(dt.texCoord2D);

        Mesh::Point _v0, _v1, _v2;
        int nn = 0;

        for (Mesh::FaceVertexIter fv_it=mesh_final.fv_iter(fh); fv_it.is_valid(); ++fv_it) {
            if (nn==0)
               _v0 = mesh_final.point(*fv_it);
            if (nn==1)
                _v1 = mesh_final.point(*fv_it);
            if (nn==2)
                _v2 = mesh_final.point(*fv_it);
            nn++;
        }
        assert(_v0 == mesh_final.point(face_vhandles_final[0]));
        assert(_v1 == mesh_final.point(face_vhandles_final[1]));
        assert(_v2 == mesh_final.point(face_vhandles_final[2]));
    }

    deletedTriangles.clear();
    for (unsigned i = 0; i < cleaned_deletedTriangles.size(); i++) {
        deletedTriangles.push_back(cleaned_deletedTriangles[i]);
    }

}

/*
    Add them to mesh_detached to keep in memory what has been deleted
 */
void Lod::addDetachedFaces(Mesh &mesh_detached, const std::vector<DeletedTriangle> &deletedTriangles) {

    // add a fake triangle so that it can be voxelized
    if (mesh_detached.n_faces() == 0) {
        std::vector<Mesh::VertexHandle>  face_vhandles;
        Mesh::VertexHandle vhandle[3];
        // add separated faces in mesh_detached
        vhandle[0] = mesh_detached.add_vertex(Mesh::Point(0,0,0));
        vhandle[1] = mesh_detached.add_vertex(Mesh::Point(0,0,0));
        vhandle[2] = mesh_detached.add_vertex(Mesh::Point(0,0,0));
        face_vhandles.clear();
        face_vhandles.push_back(vhandle[0]);
        face_vhandles.push_back(vhandle[1]);
        face_vhandles.push_back(vhandle[2]);
        mesh_detached.add_face(face_vhandles);
    }

    int tex_id = mesh_detached.n_faces();
    int new_nb_faces = mesh_detached.n_faces() + deletedTriangles.size();
    int old_height = mesh_detached.n_faces()/4096 + 1;
    int height = new_nb_faces/4096+1;

    if(_texturesList.has_diffuse_front) {
        _texturesList.diffuse_front.tex_detached_png.resize(4096, height);
        //_texturesList.diffuse_front.tex_detached_png.clear();
        _texturesList.diffuse_front.copySourceToDetached();
        _texturesList.diffuse_front.tex_detached_png.pngwriter_rename(_texturesList.diffuse_front.tex_detached_path.c_str());

    }

    if(_texturesList.has_specular_front) {
        _texturesList.specular_front.tex_detached_png.resize(4096, height);
        _texturesList.specular_front.copySourceToDetached();
        _texturesList.specular_front.tex_detached_png.pngwriter_rename(_texturesList.specular_front.tex_detached_path.c_str());
    }

    if(_texturesList.has_sggx_front) {
        _texturesList.sggx_front.sggx_sigma.tex_detached_png.resize(4096, height);
        _texturesList.sggx_front.sggx_sigma.copySourceToDetached();
        _texturesList.sggx_front.sggx_r.tex_detached_png.resize(4096, height);
        _texturesList.sggx_front.sggx_r.copySourceToDetached();
        _texturesList.sggx_front.sggx_sigma.tex_detached_png.pngwriter_rename(_texturesList.sggx_front.sggx_sigma.tex_detached_path.c_str());
        _texturesList.sggx_front.sggx_r.tex_detached_png.pngwriter_rename(_texturesList.sggx_front.sggx_r.tex_detached_path.c_str());

    }

    // copy from the texture of detach
    for (Mesh::FaceIter f_it=mesh_detached.faces_begin(); f_it!=mesh_detached.faces_end(); ++f_it) {
        Mesh::FaceHalfedgeIter fhe_it=mesh_detached.fh_iter(*f_it);
        Mesh::TexCoord2D old_tex_coord = mesh_detached.texcoord2D(*fhe_it);
        // chenge y coordinate: * old height / new one
        old_tex_coord[1] *= float(old_height) / float(height);
        mesh_detached.set_texcoord2D(*fhe_it, old_tex_coord);
        fhe_it++;
        mesh_detached.set_texcoord2D(*fhe_it, old_tex_coord);
        fhe_it++;
        mesh_detached.set_texcoord2D(*fhe_it, old_tex_coord);
    }

    for (unsigned i = 0; i < deletedTriangles.size(); i++) {
        DeletedTriangle dt = deletedTriangles[i];
        std::vector<Mesh::VertexHandle>  face_vhandles;
        Mesh::VertexHandle vhandle[3];
        // add separated faces in mesh_detached
        vhandle[0] = mesh_detached.add_vertex(dt.p0_origin);
        vhandle[1] = mesh_detached.add_vertex(dt.p1_origin);
        vhandle[2] = mesh_detached.add_vertex(dt.p2_origin);
        face_vhandles.clear();
        face_vhandles.push_back(vhandle[0]);
        face_vhandles.push_back(vhandle[1]);
        face_vhandles.push_back(vhandle[2]);
        Mesh::FaceHandle fh = mesh_detached.add_face(face_vhandles);

        unsigned x = tex_id % 4096;
        unsigned y = tex_id / 4096;
        Mesh::TexCoord2D tex_coord((float(x)+0.5)/4096.0, (float(y)+0.5)/float(height));
        Mesh::FaceHalfedgeIter fhe_it=mesh_detached.fh_iter(fh);
        mesh_detached.set_texcoord2D(*fhe_it, tex_coord);
        fhe_it++;
        mesh_detached.set_texcoord2D(*fhe_it, tex_coord);
        fhe_it++;
        mesh_detached.set_texcoord2D(*fhe_it, tex_coord);

        int R, G, B;
        // copy from tex_source_png to tex_detached_png
        if(_texturesList.has_diffuse_front) {
            int x_source = int(dt.texCoord2D[0] * 4096 + 1);
            int y_source = int(dt.texCoord2D[1] * _texturesList.diffuse_front.tex_source_png.getheight() + 1);
            R = _texturesList.diffuse_front.tex_source_png.read(x_source, y_source, 1);
            G = _texturesList.diffuse_front.tex_source_png.read(x_source, y_source, 2);
            B = _texturesList.diffuse_front.tex_source_png.read(x_source, y_source, 3);
            _texturesList.diffuse_front.tex_detached_png.plot(x+1, y+1, R, G, B);
        }

        if(_texturesList.has_specular_front) {
            int x_source = int(dt.texCoord2D[0] * 4096 + 1);
            int y_source = int(dt.texCoord2D[1] * _texturesList.specular_front.tex_source_png.getheight() + 1);
            R = _texturesList.specular_front.tex_source_png.read(x_source, y_source, 1);
            G = _texturesList.specular_front.tex_source_png.read(x_source, y_source, 2);
            B = _texturesList.specular_front.tex_source_png.read(x_source, y_source, 3);
             _texturesList.specular_front.tex_detached_png.plot(x+1, y+1, R, G, B);
        }

        if(_texturesList.has_sggx_front) {
            int x_source = int(dt.texCoord2D[0] * 4096 + 1);
            int y_source = int(dt.texCoord2D[1] * _texturesList.sggx_front.sggx_sigma.tex_source_png.getheight() + 1);
            R = _texturesList.sggx_front.sggx_sigma.tex_source_png.read(x_source, y_source, 1);
            G = _texturesList.sggx_front.sggx_sigma.tex_source_png.read(x_source, y_source, 2);
            B = _texturesList.sggx_front.sggx_sigma.tex_source_png.read(x_source, y_source, 3);
            _texturesList.sggx_front.sggx_sigma.tex_detached_png.plot(x+1, y+1, R, G, B);
            R = _texturesList.sggx_front.sggx_r.tex_source_png.read(x_source, y_source, 1);
            G = _texturesList.sggx_front.sggx_r.tex_source_png.read(x_source, y_source, 2);
            B = _texturesList.sggx_front.sggx_r.tex_source_png.read(x_source, y_source, 3);
            _texturesList.sggx_front.sggx_r.tex_detached_png.plot(x+1, y+1, R, G, B);
        }

        tex_id++;

    }

}

// TODO : find the BEST face, not a random one
int Lod::findFace_vertexCase(Mesh &mesh, int v0, Mesh::Point &mean) {
    for (Mesh::VertexOHalfedgeIter vhe_it = mesh.voh_iter(Mesh::VertexHandle(v0)); vhe_it.is_valid(); ++vhe_it) {
        if (mesh.face_handle(*vhe_it).idx() >= 0)
            return mesh.face_handle(*vhe_it).idx();
    }
    return -3;
}

// TODO : find the BEST face, not a random one
int Lod::findFace_edgeCase(Mesh &mesh, int v0, int v1, Mesh::Point &mean) {
    assert(v0 != v1);

    for (Mesh::VertexOHalfedgeIter vhe_it = mesh.voh_iter(Mesh::VertexHandle(v0)); vhe_it.is_valid(); ++vhe_it) {
        if (mesh.to_vertex_handle(*vhe_it).idx() == v1) {
            if (mesh.face_handle(*vhe_it).idx() >= 0)
                return mesh.face_handle(*vhe_it).idx();
        }
    }

    for (Mesh::VertexOHalfedgeIter vhe_it = mesh.voh_iter(Mesh::VertexHandle(v1)); vhe_it.is_valid(); ++vhe_it) {
        if (mesh.to_vertex_handle(*vhe_it).idx() == v0) {
            if (mesh.face_handle(*vhe_it).idx() >= 0)
                return mesh.face_handle(*vhe_it).idx();
        }
    }
    return -3;

}

int Lod::findFace_faceCase(Mesh &mesh, int v0, int v1, int v2, Mesh::Point &mean) {

    // find among faces of v0 one that contains v1 and v2
    for (Mesh::VertexOHalfedgeIter vhe_it = mesh.voh_iter(Mesh::VertexHandle(v0)); vhe_it.is_valid(); ++vhe_it) {

        // if (mesh.status(*vhe_it).deleted() || mesh.status(mesh.to_vertex_handle(*vhe_it)).deleted())
        //     std::cout << "Arggggggggggg" << std::endl;

        assert(mesh.from_vertex_handle(*vhe_it).idx() == v0);

        if (mesh.to_vertex_handle(*vhe_it).idx() == v1) {
            if (mesh.opposite_vh(*vhe_it).idx() == v2) {
                assert(mesh.face_handle(*vhe_it).idx() >= 0);
                return mesh.face_handle(*vhe_it).idx();
            }
        } else if (mesh.to_vertex_handle(*vhe_it).idx() == v2) {
            if (mesh.opposite_vh(*vhe_it).idx() == v1) {
                assert(mesh.face_handle(*vhe_it).idx() >= 0);
                return mesh.face_handle(*vhe_it).idx();
            }
        }
    }
    return -3;
}

/*
    The filtered material is in the textures, at the face coordinate.
    The coordinates can be obtained in mesh_final since edge collapses probably break
    half edges and the uvs are not at the right place anymore.
    Macrotriangles detached hav kept their materials.
    Triangles that have been detached later (after the collapse) have been filtered
    must given the input material again (from source or tex_0).

    Here we just copy materials of faces of mesh (ie simplified and detached)
*/
void Lod::filterMaterials2(Mesh &mesh_final, Mesh &mesh, std::string root, std::string mesh_name, int resolution) {


    int tex_height = -1;
    if(_texturesList.has_diffuse_front) {
        tex_height = _texturesList.diffuse_front.tex_source_png.getheight();
    } else if (_texturesList.has_specular_front) {
        tex_height = _texturesList.specular_front.tex_source_png.getheight();
    } else if (_texturesList.has_sggx_front) {
        tex_height = _texturesList.sggx_front.sggx_sigma.tex_source_png.getheight();
    } else {
        return; // no textures!
    }

    // need the number of elements of textures for mesh, = number of faces not deleted
    int nb_faces_mesh = 0;
    for (Mesh::FaceIter f_it=mesh.faces_sbegin(); f_it!=mesh.faces_end(); ++f_it) {
        if (mesh.status(*f_it).deleted())
            continue;

        nb_faces_mesh++;
    }

    //initTextures(_texturesList, nb_faces_mesh, root, mesh_name, resolution);
    // init only the mesh textures, need nb_faces_mesh (after collapse)
    initTexturesMesh(_texturesList, nb_faces_mesh);


    int height = nb_faces_mesh / 4096 + 1;

    int num_face_mesh = 0;

    // for all the faces not deleted of mesh, copy texture from tex_1 input the new one
    // and update the uvs.
    for (Mesh::FaceIter f_it=mesh.faces_begin(); f_it!=mesh.faces_end(); ++f_it) {

        if (mesh.status(*f_it).deleted())
            continue;

        // coordinates of the face in tex_1
        // in mesh final, otherwise problems because half edges have changed
        Mesh::FaceHalfedgeIter fhe_it=mesh_final.fh_iter(*f_it);
        Mesh::TexCoord2D coord = mesh_final.texcoord2D(*fhe_it);
        int x_coord = int(coord[0] * 4096) + 1;
        int y_coord = int(coord[1] * tex_height) + 1;
        unsigned x = num_face_mesh % 4096;
        unsigned y = num_face_mesh / 4096;

        // DIFFUSE
        if (_texturesList.has_diffuse_front) {
            double R = double(_texturesList.diffuse_front.tex_1_png.read(x_coord, y_coord, 1))/65535.0;
            double G = double(_texturesList.diffuse_front.tex_1_png.read(x_coord, y_coord, 2))/65535.0;
            double B = double(_texturesList.diffuse_front.tex_1_png.read(x_coord, y_coord, 3))/65535.0;
            //std::cout << "Color diffuse: "  << R << " " << G << " " << B << std::endl;
            _texturesList.diffuse_front.tex_mesh_png.plot(x+1, y+1, R, G, B);
        }

        // SPECULAR
        if (_texturesList.has_specular_front) {
            double R = double(_texturesList.specular_front.tex_1_png.read(x_coord, y_coord, 1))/65535.0;
            double G = double(_texturesList.specular_front.tex_1_png.read(x_coord, y_coord, 2))/65535.0;
            double B = double(_texturesList.specular_front.tex_1_png.read(x_coord, y_coord, 3))/65535.0;
            //std::cout << "Color specular: "  << R << " " << G << " " << B << std::endl;
            _texturesList.specular_front.tex_mesh_png.plot(x+1, y+1, R, G, B);
        }

        // SGGX
        if (_texturesList.has_sggx_front) {
            double R = double(_texturesList.sggx_front.sggx_sigma.tex_1_png.read(x_coord, y_coord, 1))/65535.0;
            double G = double(_texturesList.sggx_front.sggx_sigma.tex_1_png.read(x_coord, y_coord, 2))/65535.0;
            double B = double(_texturesList.sggx_front.sggx_sigma.tex_1_png.read(x_coord, y_coord, 3))/65535.0;
            _texturesList.sggx_front.sggx_sigma.tex_mesh_png.plot(x+1, y+1, R, G, B);
            R = double(_texturesList.sggx_front.sggx_r.tex_1_png.read(x_coord, y_coord, 1))/65535.0;
            G = double(_texturesList.sggx_front.sggx_r.tex_1_png.read(x_coord, y_coord, 2))/65535.0;
            B = double(_texturesList.sggx_front.sggx_r.tex_1_png.read(x_coord, y_coord, 3))/65535.0;
            _texturesList.sggx_front.sggx_r.tex_mesh_png.plot(x+1, y+1, R, G, B);
        }


        Mesh::TexCoord2D texcoord((float(x)+0.5)/4096.0, (float(y)+0.5)/float(height));
        // new tex coord
        Mesh::FaceHalfedgeIter fhe_it2=mesh.fh_iter(*f_it);
        mesh.set_texcoord2D(*fhe_it2, texcoord);
        fhe_it2++;
        mesh.set_texcoord2D(*fhe_it2, texcoord);
        fhe_it2++;
        mesh.set_texcoord2D(*fhe_it2, texcoord);
        num_face_mesh++;
    }

    mesh.garbage_collection();
    closeTextures(_texturesList);

    // write mask
    std::string tex_mask_path = root + "maps/lod" + std::to_string(resolution) + "_mask.png";
    pngwriter tex_mask_png;
    tex_mask_png.readfromfile(_texturesList.diffuse_front.tex_1_path.c_str()); // taille: même que les 0 et 1
    tex_mask_png.pngwriter_rename(tex_mask_path.c_str());
    std::cout << "Writing " << tex_mask_path << std::endl;
    for (Mesh::FaceIter f_it=mesh_final.faces_begin(); f_it!=mesh_final.faces_end(); ++f_it) {
        Mesh::FaceHalfedgeIter fhe_it=mesh_final.fh_iter(*f_it);
        Mesh::TexCoord2D tex_coord = mesh_final.texcoord2D(*fhe_it);

        int x_coord = int(tex_coord[0] * 4096) + 1;
        int y_coord = int(tex_coord[1] * tex_height) + 1;

        // need one vertex of the face
        Mesh::HalfedgeHandle he = mesh_final.halfedge_handle(*f_it);
        Mesh::VertexHandle vh = mesh_final.from_vertex_handle(he);
        if (mesh_final.property(vTargetId,vh) == -2) {
            // if face must disapear
            tex_mask_png.plot(x_coord, y_coord, 0.0, 0.0, 0.0);
        } else {
            tex_mask_png.plot(x_coord, y_coord, 1.0, 1.0, 1.0);
        }
    }
    tex_mask_png.close();

}

void Lod::computeMacro(Mesh &mesh, float resolution) {

    int n = 0;
    std::cout << "Compute macro..." << std::endl;
    for (Mesh::FaceIter f_it=mesh.faces_sbegin(); f_it!=mesh.faces_end(); ++f_it) {
        Mesh::VertexHandle v0, v1, v2;
        getVertexHandles(mesh, *f_it, v0, v1, v2);
        Mesh::Point p0, p1, p2;

        getVertexPoints(mesh, *f_it, p0, p1, p2);

        float distv0v1 = (p0-p1).norm();
        float distv1v2 = (p1-p2).norm();
        float distv2v0 = (p2-p0).norm();
        Mesh::Point p;
        if (distv0v1 >= distv1v2 && distv0v1 >= distv2v0) {
            p = (p0+p1)*0.5;
        } else if (distv1v2 >= distv0v1 && distv1v2 >= distv2v0) {
            p = (p1+p2)*0.5;
        } else {
            p = (p2+p0)*0.5;
        }

        bool isMacro = isOnMacroSurface(mesh, *f_it,
                                        p,
                                        resolution);
        mesh.property(fIsMacro,*f_it) = int(isMacro);

        if (mesh.n_faces()/20 > 0) {
            if (n == mesh.n_faces() - 1) {
                std::cout << "\r100%  " << std::endl;
            } else if (n % (mesh.n_faces()/20) == 0) {
                std::cout << "\r"<< int(float(n) / float(mesh.n_faces()) * 100 + 0.5) << "% " << std::flush;
            }
        }
        n++;

        // prepare for the last check
        mesh.status(*f_it).set_selected(false);

    }

    std::cout << "Remove non-macro endings..." << std::endl;

    for (Mesh::FaceIter f_it=mesh.faces_sbegin(); f_it!=mesh.faces_end(); ++f_it) {
        if (!mesh.status(*f_it).selected() && mesh.property(fIsMacro,*f_it)) {
            std::set<int> s_visited_faces;
            std::set<int> s_to_explore;
            s_to_explore.insert(f_it->idx());
            Mesh::Point bbmin, bbmax;
            bool initBBox = false;

            while (s_to_explore.size() > 0) {
                int current = *(s_to_explore.begin());
                s_to_explore.erase(s_to_explore.begin());
                mesh.status(Mesh::FaceHandle(current)).set_selected(true);
                s_visited_faces.insert(current);
                Mesh::Point p0, p1, p2;
                getVertexPoints(mesh, Mesh::FaceHandle(current), p0, p1, p2);
                if (!initBBox) {
                    bbmin = p0;
                    bbmax = p0;
                    initBBox = true;
                }
                addPointBBox(bbmin, bbmax, p0);
                addPointBBox(bbmin, bbmax, p1);
                addPointBBox(bbmin, bbmax, p2);
                // get all connected
                // add faces for visiting
                // (linearly) iterate over all vertices
                for (Mesh::FaceFaceIter ff_it=mesh.ff_iter(Mesh::FaceHandle(current)); ff_it.is_valid(); ++ff_it) {
                    if (mesh.property(fIsMacro,*ff_it)) {
                        if (!hasFace(s_visited_faces, ff_it->idx()) && !hasFace(s_to_explore, ff_it->idx())) {
                            s_to_explore.insert(ff_it->idx());
                        }
                    }
                }
            }

            // check bbox
            if (bbmax[0] - bbmin[0] < 2*resolution
                    && bbmax[1] - bbmin[1] < 2*resolution
                    && bbmax[2] - bbmin[2] < 2*resolution) {
                // std::cout << "found a non macro ending!" << std::endl;
                // std::cout << bbmin << " " << bbmax << std::endl;
                // std::cout << "set size " << s_visited_faces.size() << std::endl;

                // flag all as not macro
                std::set<int>::iterator it;
                for (it = s_visited_faces.begin(); it != s_visited_faces.end(); it++) {
                    mesh.property(fIsMacro,Mesh::FaceHandle(*it)) = int(false);
                }
            } else {
                // std::cout << "found a macro surface" << std::endl;
                // std::cout << bbmin << " " << bbmax << std::endl;
                // std::cout << "set size " << s_visited_faces.size() << std::endl;

            }
        }
    }
}

unsigned Lod::detachMacro(Mesh &mesh, Mesh &mesh2, float resolution) {

    std::cout << "Detach non macro..." << std::endl;
    assert(mesh.n_vertices() == mesh2.n_vertices());
    assert(mesh.n_faces() == mesh2.n_faces());

    unsigned nb_detached = 0;

    Mesh::VertexHandle v0, v1, v2;
    for (Mesh::FaceIter f_it=mesh.faces_sbegin(); f_it!=mesh.faces_end(); ++f_it) {
        getVertexHandles(mesh, *f_it, v0, v1, v2);

        // just in face middle
        if (!mesh.property(fIsMacro,*f_it)) {

            std::vector<Mesh::VertexHandle> new_v(3);


            new_v[0] = mesh.new_vertex();
            mesh.set_point(new_v[0], mesh.point(v0));

            new_v[1] = mesh.new_vertex();
            mesh.set_point(new_v[1], mesh.point(v1));

            new_v[2] = mesh.new_vertex();
            mesh.set_point(new_v[2], mesh.point(v2));

            new_v[0] = mesh2.add_vertex(mesh.point(v0));
            new_v[1] = mesh2.add_vertex(mesh.point(v1));
            new_v[2] = mesh2.add_vertex(mesh.point(v2));
            assert(mesh.n_vertices() == mesh2.n_vertices());

            // set origin
            mesh.property(vOrigin,new_v[0]) = mesh.point(v0);
            mesh.property(vOrigin,new_v[1]) = mesh.point(v1);
            mesh.property(vOrigin,new_v[2]) = mesh.point(v2);
            mesh2.property(vTarget,new_v[0]) = Mesh::Point(-1,-1,-1);
            mesh2.property(vTargetId,new_v[0]) = -1;
            mesh2.property(vTarget,new_v[1]) = Mesh::Point(-1,-1,-1);
            mesh2.property(vTargetId,new_v[1]) = -1;
            mesh2.property(vTarget,new_v[2]) = Mesh::Point(-1,-1,-1);
            mesh2.property(vTargetId,new_v[2]) = -1;

            // get tex coord
            Mesh::FaceHalfedgeIter fhe_it=mesh.fh_iter(*f_it);
            Mesh::TexCoord2D tex = mesh.texcoord2D(*fhe_it);

            // add faces / remove faces
            mesh.delete_face(*f_it);
            mesh2.delete_face(*f_it);
            Mesh::FaceHandle fh = mesh.add_face(new_v);
            mesh2.add_face(new_v);
            mesh.property(fIsMacro,fh) = 1; // avoid infinite loop

            // set tex coord
            Mesh::FaceHalfedgeIter fhe_it2=mesh.fh_iter(fh);
            mesh.set_texcoord2D(*fhe_it2, tex);
            mesh2.set_texcoord2D(*fhe_it2, tex);

            fhe_it2++;
            mesh.set_texcoord2D(*fhe_it2, tex);
            mesh2.set_texcoord2D(*fhe_it2, tex);
            fhe_it2++;
            mesh.set_texcoord2D(*fhe_it2, tex);
            mesh2.set_texcoord2D(*fhe_it2, tex);

            nb_detached++;

            mesh.status(v0).set_locked(true);
            mesh.status(v1).set_locked(true);
            mesh.status(v2).set_locked(true);

        }
    }
    std::cout << "Detached non macro : " << nb_detached << std::endl;
    std::cout << "Garbage collection..." << std::endl;
    mesh.garbage_collection();
    mesh2.garbage_collection();
    return nb_detached;
}

void Lod::targeting(Mesh &mesh_final, Mesh &mesh, DecimaterHelper &decimaterHelper) {

    std::cout << "Colapsed: " << decimaterHelper.getInfoListSize() << std::endl;

    for (int i = decimaterHelper.getInfoListSize()-1; i >= 0; i--) {
        Mesh::VertexHandle v0, v1;
        decimaterHelper.getInfoListV0V1(i, v0, v1);

        // vo is going to v1
        if (mesh_final.property(vTarget,v1) == Mesh::Point(-1,-1,-1)) {
            mesh_final.property(vTarget,v1) = mesh.point(v1);
            mesh_final.property(vTarget,v0) = mesh.point(v1);
            mesh_final.property(vTargetId,v1) = v1.idx();
            mesh_final.property(vTargetId,v0) = v1.idx();

        } else {
            mesh_final.property(vTarget,v0) = mesh_final.property(vTarget,v1);
            mesh_final.property(vTargetId,v0) = mesh_final.property(vTargetId,v1);
        }
    }
    for (Mesh::VertexIter v_it=mesh_final.vertices_begin(); v_it!=mesh_final.vertices_end(); ++v_it) {
        if (mesh_final.property(vTarget,*v_it) == Mesh::Point(-1,-1,-1)) {
            mesh_final.property(vTarget,*v_it) = mesh_final.point(*v_it); // TODO CHECK
            mesh_final.property(vTargetId,*v_it) = v_it->idx();             // TODO CHECK
        }
        mesh_final.property(vSource,*v_it) = mesh_final.point(*v_it);
    }
}
