#ifndef LOD_H
#define LOD_H

#include "../mesh/mesh.h"
#include "../sggx/sggx.h"
#include "../utils/gridInfo.h"
#include "../textures/textures.h"

#include "../decimation/decimaterHelper.h"

#include <sys/stat.h>
#include <unistd.h>



struct DeletedTriangle {
    Mesh::Point p0_origin, p1_origin, p2_origin;
    Mesh::TexCoord2D texCoord2D;
    Mesh::FaceHandle fh;
};

class Lod {

public:



    Lod(std::string root, int res);
    Lod(Lod &previous);

    void process();
    GridInfo _gridInfo;


private:

    OpenMesh::VPropHandleT<Mesh::Point> vTarget;
    OpenMesh::VPropHandleT<int> vTargetId;
    OpenMesh::VPropHandleT<Mesh::Point> vSource;
    OpenMesh::VPropHandleT<Mesh::Point> vOrigin;
    OpenMesh::FPropHandleT<int> fTargetId;
    //OpenMesh::VPropHandleT<int> vIsMacro;
    OpenMesh::FPropHandleT<int> fIsMacro;

    int _targetResolution;
    std::string _mesh_path;
    std::string _locked_path;
    std::string _mesh_name;
    std::string _root;

    TexturesList _texturesList;

    Mesh _mesh, _mesh_final, _mesh_detached;

    void request_attributes(Mesh &mesh, Mesh &mesh_final, Mesh &mesh_detached);
    void request_properties(Mesh &mesh, Mesh &mesh_final, Mesh &mesh_detached);

    bool isGood(Mesh &mesh, Mesh::FaceHandle fh, float size);

    unsigned findFacesToDetach(Mesh &mesh, std::vector<DeletedTriangle> &deletedTriangles);

    void findNeighboringFacesOfDetached(Mesh &mesh_final, std::vector<DeletedTriangle> &deletedTriangles);

    void detachFaces(Mesh &mesh_final, std::vector<DeletedTriangle> &deletedTriangles);

    void addDetachedFaces(Mesh &mesh_detached, const std::vector<DeletedTriangle> &deletedTriangles);

    int findFace_vertexCase(Mesh &mesh, int v0, Mesh::Point &mean);

    int findFace_edgeCase(Mesh &mesh, int v0, int v1, Mesh::Point &mean);

    int findFace_faceCase(Mesh &mesh, int v0, int v1, int v2, Mesh::Point &mean);

    //void matchTriangles(Mesh &mesh_final, Mesh &mesh);

    //void filterMaterials(Mesh &mesh_final, Mesh &mesh, std::string root, std::string mesh_name, int resolution);

    void filterMaterials2(Mesh &mesh_final, Mesh &mesh, std::string root, std::string mesh_name, int resolution);

    void computeMacro(Mesh &mesh, float resolution);

    unsigned detachMacro(Mesh &mesh, Mesh &mesh2, float resolution);

    void targeting(Mesh &mesh_final, Mesh &mesh, DecimaterHelper &decimaterHelper);

};



#endif // LOD_H
