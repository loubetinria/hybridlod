#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <assert.h>
#include <limits>
#include <string>     // std::string, std::to_string

#include <pngwriter.h>



// -------------------- OpenMesh
#include <OpenMesh/Core/IO/MeshIO.hh>
#include <OpenMesh/Core/Mesh/PolyMesh_ArrayKernelT.hh>
#include <OpenMesh/Core/Mesh/TriMesh_ArrayKernelT.hh>
#include <OpenMesh/Core/Utils/RandomNumberGenerator.hh>
#include <OpenMesh/Core/Geometry/VectorT.hh>
//#include <OpenMesh/Tools/Subdivider/Uniform/LongestEdgeUVT.hh>
#include "../openmesh/ModMaxEdgeLengthT.hh"



#include <deque>
#include <list>
#include <sys/stat.h>
#include <unistd.h>

#include <boost/filesystem.hpp>

// ----------------------------------------------------------------------------
typedef OpenMesh::PolyMesh_ArrayKernelT<>  MyMesh;

float norm(const MyMesh::Point &p1) {
    return std::sqrt(p1[0]*p1[0]
                     + p1[1]*p1[1]
                     + p1[2]*p1[2]);
}

inline bool exists(const std::string& name) {
  struct stat buffer;
  return (stat (name.c_str(), &buffer) == 0);
}

void read(MyMesh &mesh, std::string &input_file) {
    bool statusOK = true;
    try
    {
        std::cout << "Reading mesh... " << input_file << std::endl;
        OpenMesh::IO::Options opt(OpenMesh::IO::Options::FaceTexCoord);

        if ( !OpenMesh::IO::read_mesh(mesh, input_file.c_str(), opt) )
        {
            std::cerr << "Error: cannot find mesh" << input_file << std::endl;
            statusOK = false;
        } else {
            std::cout << "Triangulate faces (hybridlod only supports triangles currently)..." << std::endl;
            mesh.triangulate();

        }
    }
    catch( std::exception& x )
    {
        std::cerr << x.what() << std::endl;
        statusOK = false;
    }
    assert(statusOK);
}

bool readTexture(std::string path, pngwriter &image) {
    if (!exists(path)) {
        std::cout << path << " : not found" << std::endl;
        return false;
    }
    image.readfromfile(path.c_str());
    if (image.getheight()*image.getwidth() == 1) {
        std::cout << path << " : problem or too small" << std::endl;
        return false;
    }
    std::cout << path << " : " << image.getheight()<< "x" << image.getwidth() << std::endl;
    return true;
}

MyMesh::Point sampleTriangle(pngwriter &image, MyMesh::TexCoord2D &uvs_v0, MyMesh::TexCoord2D &uvs_v1, MyMesh::TexCoord2D &uvs_v2,
                             OpenMesh::RandomNumberGenerator &gen) {
    // get 2D sample
    float A = gen.getRand(); // between 0 et 1
    float B = gen.getRand();
    if (A + B > 1.0) {
        A = 1.0 - A;
        B = 1.0 - B;
    }


    MyMesh::TexCoord2D sample = uvs_v0 + A*(uvs_v1 - uvs_v0) + B*(uvs_v2 - uvs_v0);

    MyMesh::Point res;
    // todo : check if u = width or height
    res[0] = image.bilinear_interpolation_dread(sample[0]*image.getwidth(), sample[1]*image.getheight(), 1);
    res[1] = image.bilinear_interpolation_dread(sample[0]*image.getwidth(), sample[1]*image.getheight(), 2);
    res[2] = image.bilinear_interpolation_dread(sample[0]*image.getwidth(), sample[1]*image.getheight(), 3);
    return res;
}


MyMesh::Color sample(pngwriter &image, MyMesh::TexCoord2D &uvs_v0, MyMesh::TexCoord2D &uvs_v1, MyMesh::TexCoord2D &uvs_v2,
                     OpenMesh::RandomNumberGenerator &gen) {

    MyMesh::Point res(0.0,0.0,0.0);
    int NB_SAMPLES = 20;
    for (int i = 0; i < NB_SAMPLES; i++) {
        res += sampleTriangle(image, uvs_v0, uvs_v1, uvs_v2, gen);
    }
    res /= float(NB_SAMPLES);

    MyMesh::Color color;
    color[0] = char(res[0]*255);
    color[1] = char(res[1]*255);
    color[2] = char(res[2]*255);

    return color;
}

void writeTexture(MyMesh::Color *texture, std::string &fileName, unsigned nbElements, unsigned height) {
    std::remove(fileName.c_str()); // delete file

    std::cout << "Writing " << fileName << std::endl;

    pngwriter toWrite(4096, height, 0, fileName.c_str());
    toWrite.setgamma(1.0); // works ?
    for (unsigned i = 0; i < nbElements; i++) {
        unsigned x = i % 4096;
        unsigned y = i / 4096;
        toWrite.plot(x+1,y+1, double(texture[i][0])/255.0, double(texture[i][1])/255.0, double(texture[i][2])/255.0);
    }
    toWrite.close();
}

void writeNewObj(MyMesh &mesh, std::string path, int height) {
    std::remove(path.c_str()); // delete file

    std::cout << "Writing " << path << std::endl;

    std::ofstream myfile;
    myfile.open(path.c_str());
    for (MyMesh::VertexIter v_it=mesh.vertices_begin(); v_it!=mesh.vertices_end(); ++v_it) {
        MyMesh::Point p = mesh.point(*v_it);
        myfile << "v " << p[0] << " " << p[1] << " " << p[2] << std::endl;
    }

    for (unsigned i = 0; i < mesh.n_faces(); i++) {

        // x et y sont les pixels, de 0 à taille moins 1;
        // il faut écrire entre 0 et 1, au centre des pixels
        unsigned x = i % 4096;
        unsigned y = i / 4096;
        myfile << "vt " << (float(x)+0.5)/4096.0 << " " << (float(y)+0.5)/float(height) << std::endl;
    }

    for (MyMesh::FaceIter f_it=mesh.faces_begin(); f_it!=mesh.faces_end(); ++f_it) {
        MyMesh::FaceVertexIter fv_it=mesh.fv_iter(*f_it);
        int v0 = fv_it->idx(); fv_it++;
        int v1 = fv_it->idx(); fv_it++;
        int v2 = fv_it->idx();
        myfile << "f " << v0+1 << "/" << f_it->idx()+1 << " "
                       << v1+1 << "/" << f_it->idx()+1 << " "
                       << v2+1 << "/" << f_it->idx()+1 << " " << std::endl;
    }

    myfile.close();
}

int main(int argc, char *argv[])
{

    if (argc != 3) {
        std::cout << "Usage: textureFiltering path/to/data/ roughness" << std::endl;
        std::cout << "Example: textureFiltering data/lod/cube/ 0.005" << std::endl;
        return 0;
    }

    assert(argc == 3);

    MyMesh input;
    std::string input_file = std::string(argv[1]) + "src/src.obj";
    std::string diffuse_front_path = std::string(argv[1]) + "src/src_diffuse_front.png";
    std::string specular_front_path = std::string(argv[1]) + "src/src_specular_front.png";

    boost::filesystem::create_directory(std::string(argv[1]) + "maps");
    boost::filesystem::create_directory(std::string(argv[1]) + "geometry");

    float roughness = std::atof(argv[2]);

    if (roughness < 0.001f) {
        std::cout << "[Warning] Roughness should be > 0.001, otherwise numerical errors will appear (SGGX)" << std::endl;
        std::cout << "[Warning] Setting roughness to 0.001 instead" << std::endl;
        roughness = 0.001f;
    }


    input.request_halfedge_texcoords2D();
    input.request_face_normals();

    read(input, input_file);

    input.update_normals();

    bool got_diffuse_front = false;
    bool got_specular_front = false;


    // look for textures
    pngwriter diffuse_front(1, 1, 0, "diffuse_front.png");
    got_diffuse_front = readTexture(diffuse_front_path, diffuse_front);

    MyMesh::Color *diffuse_front_list;
    if (got_diffuse_front)
        diffuse_front_list = new MyMesh::Color[input.n_faces()];

    pngwriter specular_front(1, 1, 0, "specular_front.png");
    got_specular_front = readTexture(specular_front_path, specular_front);
    MyMesh::Color *specular_front_list;
    if (got_specular_front)
        specular_front_list = new MyMesh::Color[input.n_faces()];

    MyMesh::Color *sggx_sigma_front_list = new MyMesh::Color[input.n_faces()];
    MyMesh::Color *sggx_r_front_list = new MyMesh::Color[input.n_faces()];

    OpenMesh::RandomNumberGenerator gen(100000);

    int n = 0;
    for (MyMesh::FaceIter f_it=input.faces_begin(); f_it!=input.faces_end(); ++f_it) {

        if (input.n_faces()/20 > 0) {
            if (n  == input.n_faces() - 1) {
                std::cout << "\r100% "<< std::endl;
            } else if (n % (input.n_faces()/20) == 0) {
                std::cout << "\r" << int(float(n) / float(input.n_faces()) * 100 + 0.5) << "% " << std::flush;
            }
        }
        n++;

        MyMesh::FaceHalfedgeIter fhe_it=input.fh_iter(*f_it);
        MyMesh::TexCoord2D uvs_v0 = input.texcoord2D(*fhe_it);
        fhe_it++;
        MyMesh::TexCoord2D uvs_v1 = input.texcoord2D(*fhe_it);
        fhe_it++;
        MyMesh::TexCoord2D uvs_v2 = input.texcoord2D(*fhe_it);

        if (got_diffuse_front) {
            diffuse_front_list[f_it->idx()] = sample(diffuse_front, uvs_v0, uvs_v1, uvs_v2, gen);
            //std::cout << "sampled diffuse : " << filtered_diffuse_front << std::endl;
        }
        if (got_specular_front) {
            specular_front_list[f_it->idx()] = sample(specular_front, uvs_v0, uvs_v1, uvs_v2, gen);
        }

        // create sggx from roughness and triangle normal
        MyMesh::Normal face_normal = input.normal(*f_it);
        OpenMesh::Vec3f diag, tri, sigma, r, sigma_back, r_back;

        //std::cout << "facenormal " << face_normal << std::endl;

        // from sggx paper
        diag[0] = face_normal[0]*face_normal[0] + roughness * (face_normal[1]*face_normal[1] + face_normal[2]*face_normal[2]);
        diag[1] = face_normal[1]*face_normal[1] + roughness * (face_normal[0]*face_normal[0] + face_normal[2]*face_normal[2]);
        diag[2] = face_normal[2]*face_normal[2] + roughness * (face_normal[0]*face_normal[0] + face_normal[1]*face_normal[1]);

        tri[0] = face_normal[0]*face_normal[1] * (1.0 - roughness);
        tri[1] = face_normal[0]*face_normal[2] * (1.0 - roughness);
        tri[2] = face_normal[1]*face_normal[2] * (1.0 - roughness);

        // conversion sigma et r pour la quantification
        sigma[0] = std::sqrt(diag[0]);
        sigma[1] = std::sqrt(diag[1]);
        sigma[2] = std::sqrt(diag[2]);
        r[0] = tri[0] / (sigma[0]*sigma[1]);
        r[1] = tri[1] / (sigma[0]*sigma[2]);
        r[2] = tri[2] / (sigma[1]*sigma[2]);

        // d'après le papier, entre -1 et 1
        // puis [-1, 1] to uc (= de 0 à 255)

        sggx_sigma_front_list[f_it->idx()][0] = (unsigned char)(sigma[0]*255);
        sggx_sigma_front_list[f_it->idx()][1] = (unsigned char)(sigma[1]*255);
        sggx_sigma_front_list[f_it->idx()][2] = (unsigned char)(sigma[2]*255);
        sggx_r_front_list[f_it->idx()][0] = (unsigned char)((r[0] + 1.0)*0.5*255);
        sggx_r_front_list[f_it->idx()][1] = (unsigned char)((r[1] + 1.0)*0.5*255);
        sggx_r_front_list[f_it->idx()][2] = (unsigned char)((r[2] + 1.0)*0.5*255);

        // test : transform back
        /*
        sigma_back[0] = float(sggx_sigma_front_list[f_it->idx()][0]) / 255.0;
        sigma_back[1] = float(sggx_sigma_front_list[f_it->idx()][1]) / 255.0;
        sigma_back[2] = float(sggx_sigma_front_list[f_it->idx()][2]) / 255.0;
        r_back[0] = float(sggx_r_front_list[f_it->idx()][0]) / 255.0 * 2.0 - 1.0;
        r_back[1] = float(sggx_r_front_list[f_it->idx()][1]) / 255.0 * 2.0 - 1.0;
        r_back[2] = float(sggx_r_front_list[f_it->idx()][2]) / 255.0 * 2.0 - 1.0;
        std::cout << "avant : " << sigma << " " << r << std::endl;
        std::cout << "apres : " << sigma_back << " " << r_back << std::endl;
        */


    }

    // write obj à la mano
    // texture ? sqrt(nombre de face) ? ou 4048 * ce qui reste. 1024, 2048, 4096
    int img_width = input.n_faces() / 4096 + 1;
    if (got_diffuse_front) {
        std::string filtered_diffuse_front_path = std::string(argv[1]) + "maps/input_diffuse_front.png";
        writeTexture(diffuse_front_list, filtered_diffuse_front_path, input.n_faces(), img_width);
    }

    if (got_specular_front) {
        std::string filtered_specular_front_path = std::string(argv[1]) + "maps/input_specular_front.png";
        writeTexture(specular_front_list, filtered_specular_front_path, input.n_faces(), img_width);
    }

    std::string sggx_sigma_front_path = std::string(argv[1]) + "maps/input_sggx_sigma_front.png";
    writeTexture(sggx_sigma_front_list, sggx_sigma_front_path, input.n_faces(), img_width);

    std::string sggx_r_front_path = std::string(argv[1]) + "maps/input_sggx_r_front.png";
    writeTexture(sggx_r_front_list, sggx_r_front_path, input.n_faces(), img_width);

    std::string new_obj_path = std::string(argv[1]) + "geometry/input.obj";
    writeNewObj(input,new_obj_path, img_width);

    return 0;


}
