#include <assert.h>
#include <stdlib.h>
#include <cstdio>
#include <fstream>
#include <iostream>
#include <vector>
#include <pngwriter.h>
using namespace std;

#define VOXEL_DATA_LENGTH 16
#define OUTPUT_FILE_DENS "density"
#define OUTPUT_FILE_DIFFUSE "diffuse"
#define OUTPUT_FILE_SPECULAR "specular"
#define OUTPUT_FILE_SGGX_SIGMA "sggx_sigma"
#define OUTPUT_FILE_SGGX_R "sggx_r"

void writeMtsHeader(FILE* pFile, int chanels,
                    int grid_size_X,  int grid_size_Y, int grid_size_Z,
                    float minX, float minY, float minZ,
                    float maxX, float maxY, float maxZ) {
    // Bytes 1-3 ASCII Bytes ’V’, ’O’, and ’L’
    int V = 86, O = 79, L = 76;
    fwrite(reinterpret_cast<const char*>(&V), 1, sizeof(char), pFile);
    fwrite(reinterpret_cast<const char*>(&O), 1, sizeof(char), pFile);
    fwrite(reinterpret_cast<const char*>(&L), 1, sizeof(char), pFile);

    // Byte 4 File format version number (currently 3)
    int version = 3;
    fwrite(reinterpret_cast<const char*>(&version), 1, sizeof(char), pFile);

    // Bytes 5-8 Encoding identifier (32-bit integer). The following choices are available:
    //  1. Dense float32-based representation
    //  2. Dense float16-based representation (currently not supported by this implementation)
    //  3. Dense uint8-based representation (The range 0..255 will be mapped to 0..1)
    //  4. Dense quantized directions. The directions are stored in spherical coordinates with a total storage cost of 16 bit per entry.
    int type = (chanels == 1) ? 1 : 3; //float32 pour density et char pour le reste
    fwrite(reinterpret_cast<const char*>(&type), 1, sizeof(int), pFile);

    // Bytes 9-12 Number of cells along the X axis (32 bit integer)
    // Bytes 13-16 Number of cells along the Y axis (32 bit integer)
    // Bytes 17-20 Number of cells along the Z axis (32 bit integer)
    fwrite(reinterpret_cast<const char*>(&grid_size_X), 1, sizeof(int), pFile);
    fwrite(reinterpret_cast<const char*>(&grid_size_Y), 1, sizeof(int), pFile);
    fwrite(reinterpret_cast<const char*>(&grid_size_Z), 1, sizeof(int), pFile);

    // Bytes 21-24 Number of channels (32 bit integer, supported values: 1 or 3)
    fwrite(reinterpret_cast<const char*>(&chanels), 1, sizeof(int), pFile);

    // Bytes 25-48 Axis-aligned bounding box of the data stored in single precision (order: xmin, ymin, zmin, xmax, ymax, zmax)
    // bbox
    fwrite(reinterpret_cast<const char*>(&minX), 1, sizeof(int), pFile);
    fwrite(reinterpret_cast<const char*>(&minY), 1, sizeof(int), pFile);
    fwrite(reinterpret_cast<const char*>(&minZ), 1, sizeof(int), pFile);
    fwrite(reinterpret_cast<const char*>(&maxX), 1, sizeof(int), pFile);
    fwrite(reinterpret_cast<const char*>(&maxY), 1, sizeof(int), pFile);
    fwrite(reinterpret_cast<const char*>(&maxZ), 1, sizeof(int), pFile);
}

int main(int argc, char *argv[])
{

    assert(argc == 13);

    std::string inputFile(argv[1]);
    std::cout << "Converting from voxels data in " << inputFile << " to mstvol" << std::endl;

    std::string outputDir(argv[2]);
    std::cout << "OutputDir is " << outputDir << std::endl;

    int grid_size_X = std::atoi(argv[3]);
    int grid_size_Y = std::atoi(argv[4]);
    int grid_size_Z = std::atoi(argv[5]);
    int grid_size_3 = grid_size_X*grid_size_Y*grid_size_Z;
    int resolution = std::max(grid_size_X, std::max(grid_size_Y,grid_size_Z));

    float MIN_X = std::atof(argv[6]);
    float MIN_Y = std::atof(argv[7]);
    float MIN_Z = std::atof(argv[8]);

    float MAX_X = std::atof(argv[9]);
    float MAX_Y = std::atof(argv[10]);
    float MAX_Z = std::atof(argv[11]);

    int isRef = std::atoi(argv[12]);

    std::cout << "grid_size=" << grid_size_X << " " << grid_size_Y << " "  << grid_size_Z << std::endl;

    // allouer la grille
    float* density = NULL;
    unsigned char* diffuse_r = NULL;
    unsigned char* diffuse_g = NULL;
    unsigned char* diffuse_b = NULL;
    unsigned char* specular_r = NULL;
    unsigned char* specular_g = NULL;
    unsigned char* specular_b = NULL;
    unsigned char* sggx_sigma_1 = NULL;
    unsigned char* sggx_sigma_2 = NULL;
    unsigned char* sggx_sigma_3 = NULL;
    unsigned char* sggx_r_1 = NULL;
    unsigned char* sggx_r_2 = NULL;
    unsigned char* sggx_r_3 = NULL;

    density = new float[grid_size_3];
    diffuse_r = new unsigned char[grid_size_3];
    diffuse_g = new unsigned char[grid_size_3];
    diffuse_b = new unsigned char[grid_size_3];
    specular_r = new unsigned char[grid_size_3];
    specular_g = new unsigned char[grid_size_3];
    specular_b = new unsigned char[grid_size_3];
    sggx_sigma_1 = new unsigned char[grid_size_3];
    sggx_sigma_2 = new unsigned char[grid_size_3];
    sggx_sigma_3 = new unsigned char[grid_size_3];
    sggx_r_1 = new unsigned char[grid_size_3];
    sggx_r_2 = new unsigned char[grid_size_3];
    sggx_r_3 = new unsigned char[grid_size_3];

    assert(density);
    assert(diffuse_r);
    assert(diffuse_g);
    assert(diffuse_b);
    assert(specular_r);
    assert(specular_g);
    assert(specular_b);
    assert(sggx_sigma_1);
    assert(sggx_sigma_2);
    assert(sggx_sigma_3);
    assert(sggx_r_1);
    assert(sggx_r_2);
    assert(sggx_r_3);


    //init
    for (int i=0; i < grid_size_3;i++) {
        density[i] = 0;
        diffuse_r[i] = 0;
        diffuse_g[i] = 0;
        diffuse_b[i] = 0;
        specular_r[i] = 0;
        specular_g[i] = 0;
        specular_b[i] = 0;
        sggx_sigma_1[i] = 1;
        sggx_sigma_2[i] = 1;
        sggx_sigma_3[i]= 1;
        sggx_r_1[i] = 0;
        sggx_r_2[i] = 0;
        sggx_r_3[i] = 0;
    }

    // lire le fichier des voxels
    FILE *ptr_myfile;
    float voxelData[VOXEL_DATA_LENGTH];

    int count = 0;

    //pngwriter diffuse_png(2048, 2048, 0, "diffuse.png");

    ptr_myfile=fopen(inputFile.c_str(),"rb");
    if (ptr_myfile) {
        while(fread(&voxelData, VOXEL_DATA_LENGTH*sizeof(float),1,ptr_myfile)) {
            int x = voxelData[0];
            int y = voxelData[1];
            int z = voxelData[2];
            int id = x + y * grid_size_X + z * grid_size_X * grid_size_Y;
            assert(id < grid_size_3);

            density[id] = voxelData[3];

            diffuse_r[id] = (unsigned char)(voxelData[4]*255.0);
            diffuse_g[id] = (unsigned char)(voxelData[5]*255.0);
            diffuse_b[id] = (unsigned char)(voxelData[6]*255.0);

            specular_r[id] = (unsigned char)(voxelData[7]*255.0);
            specular_g[id] = (unsigned char)(voxelData[8]*255.0);
            specular_b[id] = (unsigned char)(voxelData[9]*255.0);

            /*
            sigma[0] = std::sqrt(diag[0]);
            sigma[1] = std::sqrt(diag[1]);
            sigma[2] = std::sqrt(diag[2]);
            r[0] = tri[0] / (sigma[0]*sigma[1]);
            r[1] = tri[1] / (sigma[0]*sigma[2]);
            r[2] = tri[2] / (sigma[1]*sigma[2]);
            */
            float sigma[3];
            float r[3];
            sigma[0] = std::sqrt(voxelData[10]);
            sigma[1] = std::sqrt(voxelData[11]);
            sigma[2] = std::sqrt(voxelData[12]);
            r[0] = voxelData[13] / (sigma[0]*sigma[1]);
            r[1] = voxelData[14] / (sigma[0]*sigma[2]);
            r[2] = voxelData[15] / (sigma[1]*sigma[2]);

            sggx_sigma_1[id] = (unsigned char)(sigma[0]*255);
            sggx_sigma_2[id] = (unsigned char)(sigma[1]*255);
            sggx_sigma_3[id] = (unsigned char)(sigma[2]*255);

            sggx_r_1[id] = (unsigned char)((r[0] + 1.0)*0.5*255);
            sggx_r_2[id] = (unsigned char)((r[1] + 1.0)*0.5*255);
            sggx_r_3[id] = (unsigned char)((r[2] + 1.0)*0.5*255);

            count++;
        }
        fclose(ptr_myfile);
    }

    cout << "Count: " << count << endl;
    cout << "Sparsity: " << float(count)/float(grid_size_3)*100 <<"%" << endl;

    assert(count <= grid_size_3);
	
    // ecrire le fichier de mitsuba

    FILE* pFile_dens;
    FILE* pFile_diffuse;
    FILE* pFile_specular;
    FILE* pFile_sggx_sigma;
    FILE* pFile_sggx_r;

    string prefixe = (isRef) ? "volume/ref_" : "volume/lod_";

    string _file_dens = outputDir + prefixe + OUTPUT_FILE_DENS + to_string(resolution) + ".mtsvol";
    string _file_diffuse = outputDir + prefixe + OUTPUT_FILE_DIFFUSE + to_string(resolution) + ".mtsvol";
    string _file_specular = outputDir + prefixe + OUTPUT_FILE_SPECULAR + to_string(resolution) + ".mtsvol";
    string _file_sggx_sigma = outputDir + prefixe + OUTPUT_FILE_SGGX_SIGMA + to_string(resolution) + ".mtsvol";
    string _file_sggx_r = outputDir + prefixe + OUTPUT_FILE_SGGX_R + to_string(resolution) + ".mtsvol";

    std::remove(_file_dens.c_str()); // delete file
    std::remove(_file_diffuse.c_str()); // delete file
    std::remove(_file_specular.c_str()); // delete file
    std::remove(_file_sggx_sigma.c_str()); // delete file
    std::remove(_file_sggx_r.c_str()); // delete file


    pFile_dens = fopen(_file_dens.c_str(), "a");
    pFile_diffuse = fopen(_file_diffuse.c_str(), "a");
    pFile_specular = fopen(_file_specular.c_str(), "a");
    pFile_sggx_sigma = fopen(_file_sggx_sigma.c_str(), "a");
    pFile_sggx_r = fopen(_file_sggx_r.c_str(), "a");

    assert(pFile_dens);
    assert(pFile_diffuse);
    assert(pFile_specular);
    assert(pFile_sggx_sigma);
    assert(pFile_sggx_r);

    cout << "Writing " << _file_dens << endl;
    cout << "Writing " << _file_diffuse << endl;
    cout << "Writing " << _file_specular << endl;
    cout << "Writing " << _file_sggx_sigma << endl;
    cout << "Writing " << _file_sggx_r << endl;

    writeMtsHeader(pFile_dens, 1,       grid_size_X, grid_size_Y, grid_size_Z, MIN_X, MIN_Y, MIN_Z, MAX_X, MAX_Y, MAX_Z);
    writeMtsHeader(pFile_diffuse, 3,    grid_size_X, grid_size_Y, grid_size_Z, MIN_X, MIN_Y, MIN_Z, MAX_X, MAX_Y, MAX_Z);
    writeMtsHeader(pFile_specular, 3,    grid_size_X, grid_size_Y, grid_size_Z, MIN_X, MIN_Y, MIN_Z, MAX_X, MAX_Y, MAX_Z);
    writeMtsHeader(pFile_sggx_sigma, 3,     grid_size_X, grid_size_Y, grid_size_Z, MIN_X, MIN_Y, MIN_Z, MAX_X, MAX_Y, MAX_Z);
    writeMtsHeader(pFile_sggx_r, 3,   grid_size_X, grid_size_Y, grid_size_Z, MIN_X, MIN_Y, MIN_Z, MAX_X, MAX_Y, MAX_Z);

    // Bytes 49-*
    // données
    for (int z = 0; z < grid_size_Z; z++)
        for (int y = 0; y < grid_size_Y; y++) {
            for (int x = 0; x < grid_size_X; x++) {
                float data_dens = density[x + y * grid_size_X + z * grid_size_X * grid_size_Y];
                char data_diffuse_r = diffuse_r[x + y * grid_size_X + z * grid_size_X * grid_size_Y];
                char data_diffuse_g = diffuse_g[x + y * grid_size_X + z * grid_size_X * grid_size_Y];
                char data_diffuse_b = diffuse_b[x + y * grid_size_X + z * grid_size_X * grid_size_Y];
                char data_specular_r = specular_r[x + y * grid_size_X + z * grid_size_X * grid_size_Y];
                char data_specular_g = specular_g[x + y * grid_size_X + z * grid_size_X * grid_size_Y];
                char data_specular_b = specular_b[x + y * grid_size_X + z * grid_size_X * grid_size_Y];

                char data_sggx_sigma_1 = sggx_sigma_1[x + y * grid_size_X + z * grid_size_X * grid_size_Y];
                char data_sggx_sigma_2 = sggx_sigma_2[x + y * grid_size_X + z * grid_size_X * grid_size_Y];
                char data_sggx_sigma_3 = sggx_sigma_3[x + y * grid_size_X + z * grid_size_X * grid_size_Y];
                char data_sggx_r_1 = sggx_r_1[x + y * grid_size_X + z * grid_size_X * grid_size_Y];
                char data_sggx_r_2 = sggx_r_2[x + y * grid_size_X + z * grid_size_X * grid_size_Y];
                char data_sggx_r_3 = sggx_r_3[x + y * grid_size_X + z * grid_size_X * grid_size_Y];

                fwrite(reinterpret_cast<const char*>(&data_dens),    1, sizeof(float), pFile_dens);

                fwrite(reinterpret_cast<const char*>(&data_diffuse_r), 1, sizeof(char), pFile_diffuse);
                fwrite(reinterpret_cast<const char*>(&data_diffuse_g), 1, sizeof(char), pFile_diffuse);
                fwrite(reinterpret_cast<const char*>(&data_diffuse_b), 1, sizeof(char), pFile_diffuse);

                fwrite(reinterpret_cast<const char*>(&data_specular_r), 1, sizeof(char), pFile_specular);
                fwrite(reinterpret_cast<const char*>(&data_specular_g), 1, sizeof(char), pFile_specular);
                fwrite(reinterpret_cast<const char*>(&data_specular_b), 1, sizeof(char), pFile_specular);

                fwrite(reinterpret_cast<const char*>(&data_sggx_sigma_1), 1, sizeof(char), pFile_sggx_sigma);
                fwrite(reinterpret_cast<const char*>(&data_sggx_sigma_2), 1, sizeof(char), pFile_sggx_sigma);
                fwrite(reinterpret_cast<const char*>(&data_sggx_sigma_3), 1, sizeof(char), pFile_sggx_sigma);

                fwrite(reinterpret_cast<const char*>(&data_sggx_r_1), 1, sizeof(char), pFile_sggx_r);
                fwrite(reinterpret_cast<const char*>(&data_sggx_r_2), 1, sizeof(char), pFile_sggx_r);
                fwrite(reinterpret_cast<const char*>(&data_sggx_r_3), 1, sizeof(char), pFile_sggx_r);
            }
        }
    fclose(pFile_dens);
    fclose(pFile_diffuse);
    fclose(pFile_specular);
    fclose(pFile_sggx_sigma);
    fclose(pFile_sggx_r);

    std::cout << "voxels2vol: done." << std::endl;

	return 0;
}
