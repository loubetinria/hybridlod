/* ========================================================================= *
 *                                                                           *
 *                               OpenMesh                                    *
 *           Copyright (c) 2001-2015, RWTH-Aachen University                 *
 *           Department of Computer Graphics and Multimedia                  *
 *                          All rights reserved.                             *
 *                            www.openmesh.org                               *
 *                                                                           *
 *---------------------------------------------------------------------------*
 * This file is part of OpenMesh.                                            *
 *---------------------------------------------------------------------------*
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 * 1. Redistributions of source code must retain the above copyright notice, *
 *    this list of conditions and the following disclaimer.                  *
 *                                                                           *
 * 2. Redistributions in binary form must reproduce the above copyright      *
 *    notice, this list of conditions and the following disclaimer in the    *
 *    documentation and/or other materials provided with the distribution.   *
 *                                                                           *
 * 3. Neither the name of the copyright holder nor the names of its          *
 *    contributors may be used to endorse or promote products derived from   *
 *    this software without specific prior written permission.               *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER *
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,  *
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,       *
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR        *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
 *                                                                           *
 * ========================================================================= */

/*===========================================================================*\
 *                                                                           *
 *   $Revision$                                                        *
 *   $Date$                   *
 *                                                                           *
 \*===========================================================================*/

/** \file ModEdgeLengthT.cc
 */

//=============================================================================
//
//  CLASS ModEdgeLengthT - IMPLEMENTATION
//
//=============================================================================
#define OPENMESH_DECIMATER_MODMAXEDGELENGTHT_C

//== INCLUDES =================================================================

#include "ModMaxEdgeLengthT.hh"
#include <cfloat>

//== NAMESPACES ===============================================================

namespace OpenMesh {
namespace Decimater {

//== IMPLEMENTATION ==========================================================

template<class MeshT>
ModMaxEdgeLengthT<MeshT>::ModMaxEdgeLengthT(MeshT &_mesh, float _max_edge_length,
    bool _is_binary) :
    Base(_mesh, _is_binary), mesh_(Base::mesh()) {
  set_max_edge_length(_max_edge_length);
}

//-----------------------------------------------------------------------------

template<class MeshT>
float ModMaxEdgeLengthT<MeshT>::collapse_priority(const CollapseInfo& _ci) {

    // assurer qu'on ne crée pas d'edge trop long
    // si il y avait déjà un edge trop long, on veut pas le faire grandir


    float dist_max_after = 0.0;
    float dist_max_before = 0.0;

    typename Mesh::ConstVertexVertexIter vv_it(mesh_, _ci.v0);
    while (vv_it.is_valid()) {
        float dist_after = (_ci.p1 - mesh_.point(*vv_it)).norm();
        float dist_before = (_ci.p0 - mesh_.point(*vv_it)).norm();

        dist_max_after = (dist_max_after < dist_after) ? dist_after : dist_max_after;
        dist_max_before = (dist_max_before < dist_before) ? dist_before : dist_max_before;
        ++vv_it;
    }

    // TODO : trouver edge le plus long après collapse
    // vérifié que < max_edge_length

  //typename Mesh::Scalar sqr_length = (_ci.p0 - _ci.p1).sqrnorm();

  // legal si dist_max_after <= max_edge_length_
    // ou si

    return ( ((dist_max_after <= max_edge_length_) || (dist_max_after <= dist_max_before)) ? float(Base::LEGAL_COLLAPSE) : float(Base::ILLEGAL_COLLAPSE));
}

//-----------------------------------------------------------------------------

template<class MeshT>
void ModMaxEdgeLengthT<MeshT>::set_error_tolerance_factor(double _factor) {
  if (_factor >= 0.0 && _factor <= 1.0) {
    // the smaller the factor, the smaller edge_length_ gets
    // thus creating a stricter constraint
    // division by error_tolerance_factor_ is for normalization
    float max_edge_length = max_edge_length_ * _factor / this->error_tolerance_factor_;
    set_max_edge_length(max_edge_length);
    this->error_tolerance_factor_ = _factor;
  }
}

//=============================================================================
}
}
//=============================================================================
