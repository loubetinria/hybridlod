/* ========================================================================= *
 *                                                                           *
 *                               OpenMesh                                    *
 *           Copyright (c) 2001-2015, RWTH-Aachen University                 *
 *           Department of Computer Graphics and Multimedia                  *
 *                          All rights reserved.                             *
 *                            www.openmesh.org                               *
 *                                                                           *
 *---------------------------------------------------------------------------*
 * This file is part of OpenMesh.                                            *
 *---------------------------------------------------------------------------*
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 * 1. Redistributions of source code must retain the above copyright notice, *
 *    this list of conditions and the following disclaimer.                  *
 *                                                                           *
 * 2. Redistributions in binary form must reproduce the above copyright      *
 *    notice, this list of conditions and the following disclaimer in the    *
 *    documentation and/or other materials provided with the distribution.   *
 *                                                                           *
 * 3. Neither the name of the copyright holder nor the names of its          *
 *    contributors may be used to endorse or promote products derived from   *
 *    this software without specific prior written permission.               *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER *
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,  *
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,       *
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR        *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
 *                                                                           *
 * ========================================================================= */

/*===========================================================================*\
 *                                                                           *
 *   $Revision$                                                        *
 *   $Date$                   *
 *                                                                           *
 \*===========================================================================*/

/** \file ModQuadraticT.hh
 */

//=============================================================================
//
//  CLASS ModQuadraticT
//
//=============================================================================
#ifndef OPENMESH_DECIMATER_MODQUADRATICT_HH
#define OPENMESH_DECIMATER_MODQUADRATICT_HH

//== INCLUDES =================================================================

#include <OpenMesh/Tools/Decimater/ModBaseT.hh>
#include <cfloat>

//== INCLUDES FOR TEXTURES ====================================================
#include "../collapser/mesh/mesh.h"
#include "../collapser/textures/textures.h"



//== NAMESPACES ===============================================================

namespace OpenMesh {
namespace Decimater {

//== CLASS DEFINITION =========================================================

/** \brief Use edge length to control decimation
 *
 * This module computes the edge length.
 *
 * In binary and continuous mode, the collapse is legal if:
 *  - The length after the collapse is lower than the given tolerance
 *
 */
template<class MeshT>
class ModQuadraticT: public ModBaseT<MeshT> {
  public:

    DECIMATING_MODULE( ModQuadraticT, MeshT, Quadratic )
    ;


    ModQuadraticT(MeshT& _mesh) :
        Base(_mesh, true), mesh_(Base::mesh()) {

        //testAll();

        mesh_.add_property(fV0orV1);
        //mesh_.add_property(normal_before_);
        nb_max_loop = 0;
        loops_nb_max_loop= 0.0;
        nb_optimum = 0;
        loops_optimum = 0.0;
        nb_no_error = 0;
        loops_no_error = 0.0;
        count = 0;
        voxelSize_ = 0.f;
        texturesList = NULL;
        mesh_final_ = NULL;
    }

    void printStats() {

        if (count == 2000) {
            count = 0;
            int nb_collapses = nb_max_loop + nb_optimum + nb_no_error;
            std::cout << " " << std::endl;

            std::cout << "Stat quadratic collapse" << std::endl;
            std::cout << float(nb_max_loop)/float(nb_collapses)*100.0 << "% of max loops" << std::endl;
            if (nb_max_loop > 0)
                std::cout << int(float(loops_nb_max_loop)/float(nb_max_loop))<< " of mean loops for max loops" << std::endl;

            std::cout << float(nb_optimum)/float(nb_collapses)*100.0 << "% of optimum" << std::endl;
            if (nb_optimum > 0)
                std::cout << int(float(loops_optimum)/float(nb_optimum)) << " of mean loops for optimum" << std::endl;

            std::cout << float(nb_no_error)/float(nb_collapses)*100.0 << "% of no error" << std::endl;
            if (nb_no_error > 0)
                std::cout << int(float(loops_no_error)/float(nb_no_error)) << " of mean loops for no error" << std::endl;

            std::cout << " " << std::endl;

        } else {
            count++;
        }


    }


    struct Triangle {
        typename Mesh::Point p0;
        typename Mesh::Point p1;
        typename Mesh::Point p2;
        typename Mesh::FaceHandle fh;

        Triangle(typename Mesh::Point p0,
                 typename Mesh::Point p1,
                 typename Mesh::Point p2,
                 typename Mesh::FaceHandle fh) : p0(p0), p1(p1), p2(p2),fh(fh) {}


        typename Mesh::Point center() {
            return (p0 + p1 + p2) / 3.f;
        }

        double area() const {
            return 0.5 * double((cross(p1 - p0, p2 - p0)).norm());
        }

        void print() {
            std::cout << "Triangle: (" << p0 << "), ("
                      << p1 << "), ("
                      << p2 << ")" << std::endl;
        }

    };

    void printTriangleMatlab(std::vector<Triangle> &list) {
        if (list.size() == 0)
            return;

        std::cout << "X = [";
        for (unsigned i = 0; i < list.size(); i++)
            std::cout << list[i].p0[0] << " ";
        std::cout << "; ";
        for (unsigned i = 0; i < list.size(); i++)
            std::cout << list[i].p1[0] << " ";
        std::cout << "; ";
        for (unsigned i = 0; i < list.size(); i++)
            std::cout << list[i].p2[0] << " ";
        std::cout << "]" << std::endl;

        std::cout << "Y = [";
        for (unsigned i = 0; i < list.size(); i++)
            std::cout << list[i].p0[1] << " ";
        std::cout << "; ";
        for (unsigned i = 0; i < list.size(); i++)
            std::cout << list[i].p1[1] << " ";
        std::cout << "; ";
        for (unsigned i = 0; i < list.size(); i++)
            std::cout << list[i].p2[1] << " ";
        std::cout << "]" << std::endl;

        std::cout << "Z = [";
        for (unsigned i = 0; i < list.size(); i++)
            std::cout << list[i].p0[2] << " ";
        std::cout << "; ";
        for (unsigned i = 0; i < list.size(); i++)
            std::cout << list[i].p1[2] << " ";
        std::cout << "; ";
        for (unsigned i = 0; i < list.size(); i++)
            std::cout << list[i].p2[2] << " ";
        std::cout << "]" << std::endl;

    }

    struct Edge {
        typename Mesh::Point pa;
        typename Mesh::Point pb;
        typename Mesh::VertexHandle va;
        typename Mesh::VertexHandle vb;

        Edge(typename Mesh::Point pa,
             typename Mesh::Point pb,
             typename Mesh::VertexHandle va,
             typename Mesh::VertexHandle vb) : pa(pa), pb(pb), va(va), vb(vb) {}

        typename Mesh::Point center() {
            return (pa + pb) / 2.f;
        }


    };

    struct Eq {
        bool used;
        float A_i;
        typename Mesh::Point b_i;
        float dist_i;
        Eq(float A_i, typename Mesh::Point b_i, float dist_i)
            : A_i(A_i), b_i(b_i), dist_i(dist_i) {
            used = false;
        }
    };


    /** Compute priority:
     Binary mode: Don't collapse edges longer then edge_length_
     Cont. mode:  Collapse smallest edge first, but
     don't collapse edges longer as edge_length_
     */
    float collapse_priority(const CollapseInfo& _ci);
    void preprocess_collapse(const CollapseInfo&  _ci );

    void postprocess_collapse(const CollapseInfo&  _ci );

    void set_voxelSize(float vs) {
        assert(vs > 0.0f && "voxelside must be positive");
        voxelSize_ = vs;
    }

    void set_texturesList(TexturesList *tl) {
        texturesList = tl;
    }

    void set_meshFinal(Mesh *mesh_final) {
        mesh_final_ = mesh_final;
    }

  private:


    void testAll();
    void test_distPointTriangle();
    void test_distPointTriangle_bad_cases();
    void test_findNearestInOld();
    void test_optimize_1();
    void test_optimize_2();
    void test_buildOldLists_1();
    void test_buildOldLists_2();

    void tag_v0v1(Mesh &mesh, const CollapseInfo&  _ci );
    void optimize(Mesh &mesh, const CollapseInfo& _ci, std::vector<Triangle> &triangles_old);
    void filter(Mesh &mesh, const CollapseInfo& _ci, const std::vector<Triangle> &triangles_old);

    void buildOldLists(Mesh &mesh, const CollapseInfo&  _ci,
                       std::vector<Triangle> &triangles_old,
                       std::vector<Edge> &edges_old);

    void assertIsAlmost(float test, float expected, float margin);

    float clamp(float val) const;

    float distPointTriangle_(typename Mesh::Point p,
                            typename Mesh::Point p0,
                            typename Mesh::Point p1,
                            typename Mesh::Point p2,
                            float &s,
                            float &t) const;

    typename Mesh::Point findNearestInOld(typename Mesh::Point p,
                                          const std::vector<Triangle> &triangles_old);

    void findNearestInNew(Mesh &mesh,
                          typename Mesh::Point p,
                          const CollapseInfo& _ci,
                          float &a, typename Mesh::Point &b);

    // important, même ordre/signe que getNormal
    typename Mesh::Point getAreaVector(const typename Mesh::Point &p_1,
                                       const typename Mesh::Point &p_2,
                                       const typename Mesh::Point &p_3);

    void getVertices(Mesh &mesh,
                     typename Mesh::FaceHandle fh,
                     typename Mesh::VertexHandle &v_1,
                     typename Mesh::VertexHandle &v_2,
                     typename Mesh::VertexHandle &v_3);

    Mesh& mesh_;
    Mesh *mesh_final_;

    //VPropHandleT<float> area_before_;
    FPropHandleT<int> fV0orV1;

    int nb_max_loop;
    float loops_nb_max_loop;
    int nb_optimum;
    float loops_optimum;
    int nb_no_error;
    float loops_no_error;
    int count;
    TexturesList *texturesList;
    float voxelSize_;

};

//=============================================================================
}// END_NS_DECIMATER
} // END_NS_OPENMESH
//=============================================================================
#if defined(OM_INCLUDE_TEMPLATES) && !defined(OPENMESH_DECIMATER_MODQUADRATICT_C)
#define MODQUADRATICT_TEMPLATES
#include "ModQuadraticT_impl.hh"
#endif
//=============================================================================
#endif // OPENMESH_DECIMATER_MODQUADRATICT_HH defined
//=============================================================================

