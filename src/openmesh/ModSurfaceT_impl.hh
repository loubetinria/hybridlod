/* ========================================================================= *
 *                                                                           *
 *                               OpenMesh                                    *
 *           Copyright (c) 2001-2015, RWTH-Aachen University                 *
 *           Department of Computer Graphics and Multimedia                  *
 *                          All rights reserved.                             *
 *                            www.openmesh.org                               *
 *                                                                           *
 *---------------------------------------------------------------------------*
 * This file is part of OpenMesh.                                            *
 *---------------------------------------------------------------------------*
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 * 1. Redistributions of source code must retain the above copyright notice, *
 *    this list of conditions and the following disclaimer.                  *
 *                                                                           *
 * 2. Redistributions in binary form must reproduce the above copyright      *
 *    notice, this list of conditions and the following disclaimer in the    *
 *    documentation and/or other materials provided with the distribution.   *
 *                                                                           *
 * 3. Neither the name of the copyright holder nor the names of its          *
 *    contributors may be used to endorse or promote products derived from   *
 *    this software without specific prior written permission.               *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER *
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,  *
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,       *
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR        *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
 *                                                                           *
 * ========================================================================= */

/*===========================================================================*\
 *                                                                           *
 *   $Revision$                                                        *
 *   $Date$                   *
 *                                                                           *
 \*===========================================================================*/

/** \file ModSurfaceT.cc
 */

//=============================================================================
//
//  CLASS ModSurfaceT - IMPLEMENTATION
//
//=============================================================================
#define OPENMESH_DECIMATER_MODSURFACET_C

//== INCLUDES =================================================================

#include "ModSurfaceT.hh"
#include <cfloat>
#include <random>

//== NAMESPACES ===============================================================

namespace OpenMesh {
namespace Decimater {

//== UTILS ===============================================================


template<class MeshT>
float ModSurfaceT<MeshT>::getProjArea(const typename Mesh::Point &p_1,
                                    const typename Mesh::Point &p_2,
                                    const typename Mesh::Point &p_3) {
    return 0.5f * (cross(p_2 - p_1, p_3 - p_1)).norm();
}

template<class MeshT>
void ModSurfaceT<MeshT>::getPoints(typename Mesh::FaceHandle fh,
                                 typename Mesh::Point &p_1,
                                 typename Mesh::Point &p_2,
                                 typename Mesh::Point &p_3) {
    typename Mesh::ConstFaceVertexIter fv_it(mesh_, fh);
    p_1 = mesh_.point(*fv_it);
    fv_it++;
    p_2 = mesh_.point(*fv_it);
    fv_it++;
    p_3 = mesh_.point(*fv_it);
}

template<class MeshT>
typename ModSurfaceT<MeshT>::Mesh::Point ModSurfaceT<MeshT>::ortho(typename Mesh::Point normal) {
    if (normal[0] != 0.0 || normal[1] != 0) {
        return typename Mesh::Point(-normal[1], normal[0], 0.0) / (typename Mesh::Point(-normal[1], normal[0], 0.0)).norm();
    }
    return typename Mesh::Point(0.0, -normal[2], normal[1]) / (typename Mesh::Point(0.0, -normal[2], normal[1])).norm();
}

//== IMPLEMENTATION ==========================================================

//-----------------------------------------------------------------------------

template<class MeshT>
float ModSurfaceT<MeshT>::collapse_priority(const CollapseInfo& _ci) {
    return float(Base::LEGAL_COLLAPSE);
}

//-----------------------------------------------------------------------------

template<class MeshT>
void ModSurfaceT<MeshT>::preprocess_collapse(const CollapseInfo& _ci) {


    int nbface = 0;

    // calculer les directions et les aires initiales dans ces directions
    float area_before = 0.f;

    typename Mesh::ConstVertexFaceIter vf0_it(mesh_, _ci.v0);
    while (vf0_it.is_valid()) {
        if ((*vf0_it != _ci.fl) && (*vf0_it != _ci.fr)) {
            typename Mesh::Point p0, p1, p2;
            getPoints(*vf0_it, p0, p1, p2);
            area_before += getProjArea(p0, p1, p2);
            nbface++;
        }
        ++vf0_it;
    }

    typename Mesh::ConstVertexFaceIter vf1_it(mesh_, _ci.v1);
    while (vf1_it.is_valid()) {
        if ((*vf1_it != _ci.fl) && (*vf1_it != _ci.fr)) {
            typename Mesh::Point p0, p1, p2;
            getPoints(*vf1_it, p0, p1, p2);
            area_before += getProjArea(p0, p1, p2);
            nbface++;
        }
        ++vf1_it;
    }

    if (_ci.fl.is_valid()) {
        area_before += getProjArea(_ci.p0, _ci.p1, mesh_.point(_ci.vl));
        nbface++;
    }

    if (_ci.fr.is_valid()) {
        area_before += getProjArea(_ci.p0, _ci.p1, mesh_.point(_ci.vr));
        nbface++;
    }

    mesh_.property(area_before_, _ci.v1) = area_before;

    /*
    std::cout << "preprocess OK, nb faces " << nbface << " areas "
              << mesh_.property(area_before_1_, _ci.v1) << " "<<  mesh_.property(area_before_2_, _ci.v1) << " " << mesh_.property(area_before_3_, _ci.v1)
              << " ns "
              << mesh_.property(n1_, _ci.v1) << " "<<  mesh_.property(n2_, _ci.v1) << " " << mesh_.property(n3_, _ci.v1) << std::endl;
    */

}


template<class MeshT>
void ModSurfaceT<MeshT>::postprocess_collapse(const CollapseInfo& _ci) {
    // optimiser pas position de v1 pour conserver les aires projetées.

    float area_before = mesh_.property(area_before_, _ci.v1);


    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> dis(-1.f, 1.f);

    float dist01 = (_ci.p0 - _ci.p1).norm();

    float error_init;

    // partir de (p0 + p1)/2, et bonger au pif pour minimiser la différence d'aire.

    typename Mesh::Point init_p = (_ci.p0 + _ci.p1)*0.5;
    mesh_.set_point(_ci.v1, init_p);

    float current_error = 0;
    int nbface = 0;

    int nb_without = 0;
    int i = 0;

    while (nb_without < 20) {


        typename Mesh::Point r(dis(gen),
                               dis(gen),
                               dis(gen));

        // calculer l'erreur du point next_p
        typename Mesh::Point next_p = mesh_.point(_ci.v1) + r * dist01*0.1;
        typename Mesh::Point current_p = mesh_.point(_ci.v1);

        //std::cout << r * dist01*0.05 << std::endl;

        // test: remove this constraint
        if (((init_p - next_p).norm() > dist01*0.5)) {
           continue;
        }

        // simulate
        mesh_.set_point(_ci.v1, next_p);


        // compute new areas
        float area_after = 0.f;

        // ici, juste les faces voisines de v1, j'espère que la connection est la bonne,
        // ie que le collapse a vraiment été fait

        nbface = 0;
        typename Mesh::ConstVertexFaceIter vf1_it(mesh_, _ci.v1);
        while (vf1_it.is_valid()) {
            if (!mesh_.status(*vf1_it).deleted()) {
                typename Mesh::Point p0, p1, p2;
                getPoints(*vf1_it, p0, p1, p2);
                area_after += getProjArea(p0, p1, p2);
                nbface++;
            }
            ++vf1_it;
        }

        //std::cout << "error : " << area_after_1 << " " << area_after_2 << " " << area_after_3 << std::endl;
        //std::cout << "n1 : " << n1 << " " << n2 << " " << n3 << std::endl;

        float error = std::abs(area_after - area_before);

        if ((i == 0) || ((error < current_error) )) {
            current_error = error;
            if (i == 0)
                error_init = current_error;
            nb_without = 0;
        } else {
            // unset
            mesh_.set_point(_ci.v1, current_p);
        }

        nb_without++;
        i++;

    }

    // test si erreur assez faible, sinon milieu ?


    //std::cout << "nbface : " << nbface << std::endl;

    //std::cout << "collapse postprocessed : errors " << error_init << " " << current_error << std::endl;
}

//=============================================================================
}
}
//=============================================================================
