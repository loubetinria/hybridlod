/* ========================================================================= *
 *                                                                           *
 *                               OpenMesh                                    *
 *           Copyright (c) 2001-2015, RWTH-Aachen University                 *
 *           Department of Computer Graphics and Multimedia                  *
 *                          All rights reserved.                             *
 *                            www.openmesh.org                               *
 *                                                                           *
 *---------------------------------------------------------------------------*
 * This file is part of OpenMesh.                                            *
 *---------------------------------------------------------------------------*
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 * 1. Redistributions of source code must retain the above copyright notice, *
 *    this list of conditions and the following disclaimer.                  *
 *                                                                           *
 * 2. Redistributions in binary form must reproduce the above copyright      *
 *    notice, this list of conditions and the following disclaimer in the    *
 *    documentation and/or other materials provided with the distribution.   *
 *                                                                           *
 * 3. Neither the name of the copyright holder nor the names of its          *
 *    contributors may be used to endorse or promote products derived from   *
 *    this software without specific prior written permission.               *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER *
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,  *
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,       *
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR        *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
 *                                                                           *
 * ========================================================================= */

/*===========================================================================*\
 *                                                                           *
 *   $Revision$                                                        *
 *   $Date$                   *
 *                                                                           *
 \*===========================================================================*/

/** \file ModQuadraticT.cc
 */

//=============================================================================
//
//  CLASS ModSurfaceT - IMPLEMENTATION
//
//=============================================================================
#define OPENMESH_DECIMATER_MODQUADRATICT_C

//== INCLUDES =================================================================

#include "ModQuadraticT.hh"
#include <cfloat>
#include <random>

#define THRESHOLD 10e-3
#define DEBUG_OPT_PRINT 0


//== NAMESPACES ===============================================================

namespace OpenMesh {
namespace Decimater {

//== UTILS ===============================================================

template<class MeshT>
void ModQuadraticT<MeshT>::testAll() {

    std::cout << "Begin tests ModQuadraticT" << std::endl;
    test_distPointTriangle();
    test_findNearestInOld();
    test_distPointTriangle_bad_cases();
    test_optimize_1();
    test_optimize_2();
    test_buildOldLists_1();
    test_buildOldLists_2();

    std::cout << "Done!" << std::endl;
    assert(0 && "end of tests");
}

template<class MeshT>
void ModQuadraticT<MeshT>::assertIsAlmost(float test, float expected, float margin) {
    if (margin < 0)
        margin = -margin;

    if (test >= expected + margin || test <= expected - margin) {
        std::cout << "Expected " << expected << ", found " << test << std::endl;
    }

    assert(test < expected + margin);
    assert(test > expected - margin);

}

template<class MeshT>
float ModQuadraticT<MeshT>::clamp(float val) const {
    if (val < 0.f)
        return 0.f;
    if (val > 1.f)
        return 1.f;
    return val;
}

template<class MeshT>
float ModQuadraticT<MeshT>::distPointTriangle_(typename Mesh::Point p,
                                              typename Mesh::Point p0,
                                              typename Mesh::Point p1,
                                              typename Mesh::Point p2,
                                              float &s,
                                              float &t) const {

    // from David Eberly
    // triangle T(s,t) = B +sE0 +tE1
    typename Mesh::Point B = p0;
    typename Mesh::Point E0 = p1 - p0;
    typename Mesh::Point E1 = p2 - p0;
    typename Mesh::Point D = B - p;     //D = B − P
    float a = dot(E0, E0);              //a = E0 · E0
    float b = dot(E0, E1);              //b = E0 · E1
    float c = dot(E1, E1);              //c = E1 · E1
    float d = dot(E0, D);               //d = E0 · D
    float e = dot(E1, D);               //e = E1 · D
    //float f = dot(D, D);                //f = D · D

    // check degenerated triangles
    if ((p0-p1).norm() < voxelSize_ * THRESHOLD) {
        if ((p1-p2).norm() < voxelSize_ * THRESHOLD) {
            s = 0;
            t = 0;
            return (p - p0).norm();
        } else {
            // distance au segment p2 --- p1/p0
            s = 0;
            t = clamp(dot((p-p0),(p2-p0)/(p2-p0).sqrnorm()));
            return (B + s*E0 + t*E1 - p).norm();
        }
    } else if ((p1-p2).norm() < voxelSize_ * THRESHOLD) {
        if ((p0-p2).norm() < voxelSize_ * THRESHOLD) {
            // should not happen
            s = 0;
            t = 0;
            return (p - p0).norm();
        } else {
            // distance au segment p0 --- p1/p2
            s = 0;
            t = clamp(dot((p-p0),(p1-p0)/(p1-p0).sqrnorm()));
            return (B + s*E0 + t*E1 - p).norm();
        }
    } else if ((p0-p2).norm() < voxelSize_ * THRESHOLD) {
        if ((p1-p2).norm() < voxelSize_ * THRESHOLD) {
            // should not happen
            s = 0;
            t = 0;
            return (p - p0).norm();
        } else {
            // distance au segment p0/p2 --- p1
            t = 0;
            s = clamp(dot((p-p0),(p1-p0)/(p1-p0).sqrnorm()));
            return (B + s*E0 + t*E1 - p).norm();
        }
    }

    float det = a*c-b*b;
    if (det < 0.f) {
        //std::cout << "Det should be >= 0 ?? got " << det << std::endl;
        det = -det;
    }
    s = b*e-c*d;
    t = b*d-a*e ;

    if (s+t <= det) {
        if (s < 0.f) {
            if (t < 0.f) {
                // region4
                if (d < 0.f) {
                    t = 0.f;
                    s = (-d >= a) ? 1.f : -d/a;
                } else {
                    s = 0;
                    t = (e >= 0.f) ? 0.f : ((-e >= c) ? 1.f : -e/c);
                }
                // end of region 4
            } else {
                // region 3
                s = 0.f;
                t = (e >= 0.f) ? 0.f : ((-e >= c) ? 1.f : -e/c);
                // end of region 3
            }
        } else if (t < 0.f) {
                // region 5
                t = 0.f;
                s = (d >= 0.f) ? 0.f : ((-d >= a) ? 1.f : -d/a);
                // end of region 5
            } else {
                // region 0
                if (det <= 0.f) {
                    //std::cout << "problem with null det" << std::endl;
                    //std::cout << (p0-p1).norm() << std::endl;
                    //std::cout << (p1-p2).norm() << std::endl;
                    //std::cout << (p2-p0).norm() << std::endl;
                    s = 0;
                    t = 0;

                    return std::min((p-p0).norm() , std::min((p-p1).norm(),(p-p2).norm()));
                    //assert(0);
                }
                float invDet = 1.f/det;
                s = s*invDet;
                t = t*invDet;
                // end of region 0
            }
        } else {
        if (s < 0.f) {
            // region 2
            float tmp0 = b + d;
            float tmp1 = c + e;
            if (tmp1 > tmp0 ) { // minimum on edge s+t=1
                float numer = tmp1 - tmp0;
                float denom = a - 2.f*b + c;
                s = (numer >= denom) ? 1.f : numer/denom;
                t = 1.f-s;
            } else {          // minimum on edge s=0
                s = 0.f;
                t = (tmp1 <= 0.f) ? 1.f : ((e >= 0.f) ? 0.f : -e/c);
            }
            // end of region 2
        } else if (t < 0.f) {
            // region6
            float tmp0 = b + e;
            float tmp1 = a + d;
            if (tmp1 > tmp0) {
                float numer = tmp1 - tmp0;
                float denom = a-2.f*b+c;
                t = (numer >= denom) ? 1.f : numer/denom;
                s = 1.f - t;
            } else {
                t = 0.f;
                s = (tmp1 <= 0.f) ? 1.f : ((d >= 0) ? 0.f : -d/a);
            }
            // end region 6
        } else {
            // region 1
            float numer = c + e - b - d;
            if (numer <= 0.f) {
                s = 0.f;
            } else {
                float denom = a - 2.f*b + c;
                s = (numer >= denom) ? 1.f : numer/denom;
            }
            t = 1.f-s;
            // end of region 1
        }
    }

    assert(t >= 0.0 && t <= 1.0);
    assert(s >= 0.0 && s <= 1.0);

    //if (s+t > 1.f) {
        //std::cout << "problem with s+t > 1.f : " << s+t << std::endl;
        //assert(s+t <= 1.0);
    //}
    return (B + s*E0 + t*E1 - p).norm();

}


template<class MeshT>
typename ModQuadraticT<MeshT>::Mesh::Point ModQuadraticT<MeshT>::findNearestInOld(typename Mesh::Point p,
                                                                                  const std::vector<Triangle> &triangles_old) {


    float minDist = -1;
    assert(triangles_old.size() > 0 && "void list!");
    float s, t;
    typename Mesh::Point nearest;

    for (unsigned i = 0; i < triangles_old.size(); i++) {
        float dist = distPointTriangle_(p,
                                       triangles_old[i].p0,
                                       triangles_old[i].p1,
                                       triangles_old[i].p2, s, t);
        if (dist < minDist || minDist == -1) {
            nearest = triangles_old[i].p0 * (1.0 - s - t) + s * triangles_old[i].p1 + t*triangles_old[i].p2;
            minDist = dist;
        }
    }
    return nearest;
}

// donne a et b tels que le point le plus proche soit a*p1 + b
template<class MeshT>
void ModQuadraticT<MeshT>::findNearestInNew(Mesh &mesh,
                                            typename Mesh::Point p,
                                            const CollapseInfo& _ci,
                                            float &a, typename Mesh::Point &b) {

    // parcourir les nouveaux triangles, trouver le point le plus proche,
    // sauvegarder ses coordonnées en fonction de p1

    float minDist = -1;

    // on boucle sur les faces de v1 : tout sauf les deux supprimées
    typename Mesh::ConstVertexFaceIter vf1_it(mesh, _ci.v1);
    while (vf1_it.is_valid()) {
        if (!mesh.status(*vf1_it).deleted()) {

            float dist;
            float s, t;

            typename Mesh::VertexHandle va, vb, vc;
            getVertices(mesh, *vf1_it, va, vb, vc);
            if (va == _ci.v1) {
                dist = distPointTriangle_(p,mesh.point(va),mesh.point(vb),mesh.point(vc), s, t);
                // nearest = p1 + s*(pa - p1) + t * (pb - p1)
                //         = p1 * (1 - s - t) + s*pa + t*pb
                if (dist < minDist || minDist == -1) { // save point
                    a = 1.f - s - t;
                    b = s*mesh.point(vb) + t*mesh.point(vc);
                    minDist = dist;
                }
            } else if (vb == _ci.v1) {
                dist = distPointTriangle_(p,mesh.point(vb),mesh.point(va),mesh.point(vc), s, t);
                if (dist < minDist || minDist == -1) { // save point
                    a = 1.f - s - t;
                    b = s*mesh.point(va) + t*mesh.point(vc);
                    minDist = dist;
                }
            } else if (vc == _ci.v1) {
                dist = distPointTriangle_(p,mesh.point(vc),mesh.point(va),mesh.point(vb), s, t);
                if (dist < minDist || minDist == -1) { // save point
                    a = 1.f - s - t;
                    b = s*mesh.point(va) + t*mesh.point(vb);
                    minDist = dist;
                }
            } else {
                assert(0 && "v1 should be one of the vertices");
            }
        }
        ++vf1_it;
    }
    // c'est tout, le point est a*p1 + b
}




template<class MeshT>
void ModQuadraticT<MeshT>::getVertices(Mesh &mesh,
                                       typename Mesh::FaceHandle fh,
                                       typename Mesh::VertexHandle &v_1,
                                       typename Mesh::VertexHandle &v_2,
                                       typename Mesh::VertexHandle &v_3) {
    typename Mesh::ConstFaceVertexIter fv_it(mesh, fh);
    v_1 = *fv_it;
    fv_it++;
    v_2 = *fv_it;
    fv_it++;
    v_3 = *fv_it;
}


//== IMPLEMENTATION ==========================================================

//-----------------------------------------------------------------------------

template<class MeshT>
float ModQuadraticT<MeshT>::collapse_priority(const CollapseInfo& _ci) {

    if (mesh_.status(_ci.v0).locked() || mesh_.status(_ci.v1).locked())
        return float(Base::ILLEGAL_COLLAPSE);

    return float(Base::LEGAL_COLLAPSE);
}

//-----------------------------------------------------------------------------

template<class MeshT>
void ModQuadraticT<MeshT>::tag_v0v1(Mesh &mesh, const CollapseInfo& _ci) {
    typename Mesh::ConstVertexFaceIter vf1_it(mesh, _ci.v1);
    while (vf1_it.is_valid()) {
        if (!mesh.status(*vf1_it).deleted()) {
            mesh.property(fV0orV1,*vf1_it) = 1; // set as neighour of v1
        }
        ++vf1_it;
    }

    typename Mesh::ConstVertexFaceIter vf0_it(mesh, _ci.v0);
    while (vf0_it.is_valid()) {
        if (!mesh.status(*vf0_it).deleted()) {
           mesh.property(fV0orV1,*vf0_it) = 0; // set as neighour of v1
        }
        ++vf0_it;
    }

    //std::cout << "Pre process completed" << std::endl;


}

template<class MeshT>
void ModQuadraticT<MeshT>::preprocess_collapse(const CollapseInfo& _ci) {
    tag_v0v1(mesh_, _ci);
    assert(voxelSize_ > 0.f && "voxelSize must be set");

    //std::cout << "Pre process completed" << std::endl;
}

template<class MeshT>
void ModQuadraticT<MeshT>::buildOldLists(Mesh &mesh, const CollapseInfo& _ci,
                                         std::vector<Triangle> &triangles_old,
                                         std::vector<Edge> &edges_old) {

    triangles_old.clear();
    edges_old.clear();
    std::vector<Edge> edges_old_doubles;


    // for all faces adjacent to v1 in the new mesh (two old faces missing)
    typename Mesh::ConstVertexFaceIter vf1_it(mesh, _ci.v1);
    while (vf1_it.is_valid()) {
        if (!mesh.status(*vf1_it).deleted()) { // maybe useless
            typename Mesh::VertexHandle va, vb, vc;
            getVertices(mesh, *vf1_it, va, vb, vc);

            // look if this face was originally adjancent to v0 or v1
            int v0orv1 = mesh.property(fV0orV1,*vf1_it); // 1 means v1
            typename Mesh::Point p0orp1 = (v0orv1) ? _ci.p1 : _ci.p0;

            // add the triangle with the original position
            if (va == _ci.v1) {
                triangles_old.push_back(Triangle(mesh.point(vb), mesh.point(vc), p0orp1,*vf1_it));
                if (v0orv1) {
                    edges_old_doubles.push_back(Edge(_ci.p1,mesh.point(vb), _ci.v1,vb));
                    edges_old_doubles.push_back(Edge(_ci.p1,mesh.point(vc), _ci.v1,vc));
                } else {
                    edges_old_doubles.push_back(Edge(_ci.p0,mesh.point(vb), _ci.v0,vb));
                    edges_old_doubles.push_back(Edge(_ci.p0,mesh.point(vc), _ci.v0,vc));
                }
            } else if (vb == _ci.v1) {
                triangles_old.push_back(Triangle(mesh.point(va), mesh.point(vc), p0orp1,*vf1_it));
                if (v0orv1) {
                    edges_old_doubles.push_back(Edge(_ci.p1,mesh.point(va), _ci.v1,va));
                    edges_old_doubles.push_back(Edge(_ci.p1,mesh.point(vc), _ci.v1,vc));
                } else {
                    edges_old_doubles.push_back(Edge(_ci.p0,mesh.point(va), _ci.v0,va));
                    edges_old_doubles.push_back(Edge(_ci.p0,mesh.point(vc), _ci.v0,vc));
                }
            } else if (vc == _ci.v1) {
                triangles_old.push_back(Triangle(mesh.point(va), mesh.point(vb), p0orp1,*vf1_it));
                if (v0orv1) {
                    edges_old_doubles.push_back(Edge(_ci.p1,mesh.point(va), _ci.v1,va));
                    edges_old_doubles.push_back(Edge(_ci.p1,mesh.point(vb), _ci.v1,vb));
                } else {
                    edges_old_doubles.push_back(Edge(_ci.p0,mesh.point(va), _ci.v0,va));
                    edges_old_doubles.push_back(Edge(_ci.p0,mesh.point(vb), _ci.v0,vb));
                }
            } else {
                assert(0 && "v1 should be one of the vertices");
            }
        }
        ++vf1_it;
    }

    // add old deleted faces
    if (_ci.vl.is_valid()) {
        triangles_old.push_back(Triangle(mesh.point(_ci.vl), _ci.p0, _ci.p1,_ci.fl));
        edges_old_doubles.push_back(Edge(_ci.p0,mesh.point(_ci.vl), _ci.v0,_ci.vl));
        edges_old_doubles.push_back(Edge(_ci.p1,mesh.point(_ci.vl), _ci.v1,_ci.vl));
    }
    if (_ci.vr.is_valid()) {
        triangles_old.push_back(Triangle(mesh.point(_ci.vr), _ci.p0, _ci.p1,_ci.fr));
        edges_old_doubles.push_back(Edge(_ci.p0,mesh.point(_ci.vr), _ci.v0,_ci.vr));
        edges_old_doubles.push_back(Edge(_ci.p1,mesh.point(_ci.vr), _ci.v1,_ci.vr));
    }

    // add v0v1 old edge
    edges_old_doubles.push_back(Edge(_ci.p0,_ci.p1, _ci.v0,_ci.v1));

/*
    for (unsigned i = 0; i < triangles_old.size(); i++) {
        std::cout << "Old triangle : " << triangles_old[i].p0 << " "
                  << triangles_old[i].p1 << " "
                  << triangles_old[i].p2 << std::endl;
    }
*/
    // remove doublons in edges_old_doubles
    for (unsigned i = 0; i < edges_old_doubles.size(); i++) {
        bool found = false;
        for (unsigned j = 0; j < edges_old.size(); j++) {
            if (edges_old[j].va == edges_old_doubles[i].va
                    && edges_old[j].vb == edges_old_doubles[i].vb) {
                found = true;
            }
        }
        if (!found) {
            edges_old.push_back(edges_old_doubles[i]);
        }
    }
/*
    for (unsigned i = 0; i < edges_old.size(); i++) {
        std::cout << "Old edge : " << edges_old[i].pa << " "
                  << edges_old[i].pb << std::endl;

    }

    std::cout << "Old triangles " << triangles_old.size() << std::endl;
    std::cout << "Old edges doubles " << edges_old_doubles.size() << std::endl;
    std::cout << "Old edges " << edges_old.size() << std::endl;
*/
}


template<class MeshT>
void ModQuadraticT<MeshT>::optimize(Mesh &mesh, const CollapseInfo& _ci, std::vector<Triangle> &triangles_old) {

    //mesh.set_point(_ci.v1, _ci.p1*0.5 + _ci.p0*0.5);

    // Create a the list of old triangles, to be used in the loop
    triangles_old.clear();
    std::vector<Edge> edges_old;

    buildOldLists(mesh, _ci, triangles_old, edges_old);

    // loop : look for a better position for v1
    // todo : adaptative, stop when converged
    int loop = 0;
    float currentError;

    int max_loop = 200;
    for (int i = 0; i < max_loop; i++) {
    //while (true) {
        loop++;
        // here X = the wanted position for p1
        // we want to minimize a sqared distance between a point aX+b and a target
        // equation : (aX + b - target).(aX + b - target)
        //          = a^2 XX + 2a(b - target)X
        // A = a^2
        // B = 2a(b-target)

        // adding contributions from points of the new surface
        float A_from_new = 0;
        typename Mesh::Point B_from_new(0.f,0.f,0.f);
        float C_from_new = 0;
        int nb_points_new = 0;

        /*

        // from middle of faces
        typename Mesh::ConstVertexFaceIter vf2_it(mesh, _ci.v1);
        while (vf2_it.is_valid()) {
            if (!mesh.status(*vf2_it).deleted()) {
                typename Mesh::VertexHandle va, vb, vc;
                getVertices(mesh, *vf2_it, va, vb, vc);
                typename Mesh::Point b;
                // the middle of the face is 0.333333 * X + 0.3333333*(sum other vertices)
                if (va == _ci.v1) {
                    b = (mesh.point(vb) + mesh.point(vc))/3.0;
                } else if (vb == _ci.v1) {
                    b = (mesh.point(va) + mesh.point(vc))/3.0;
                } else if (vc == _ci.v1) {
                    b = (mesh.point(va) + mesh.point(vb))/3.0;
                } else {
                    assert(0 && "v1 should be one of the vertices");
                }
                typename Mesh::Point current_face_middle = b + mesh.point(_ci.v1)/3.0;
                typename Mesh::Point nearest_in_old = findNearestInOld(current_face_middle,triangles_old);
                A_from_new += 1.0/9.0;
                B_from_new += 2.0/3.0*(b - nearest_in_old);
                nb_points_new++;
            }
            ++vf2_it;
        }

        */

        // from middle of edges

        typename Mesh::ConstVertexEdgeIter ve0_it(mesh, _ci.v1);
        while (ve0_it.is_valid()) {
            if (!mesh.status(*ve0_it).deleted()) {
                typename Mesh::VertexHandle va, vb;
                typename Mesh::HalfedgeHandle he = mesh.halfedge_handle(*ve0_it,0);
                va = mesh.from_vertex_handle(he);
                vb = mesh.to_vertex_handle(he);

                typename Mesh::Point b;
                // the middle of the edge is 0.5 * X + 0.5*(l'autre)
                if (va == _ci.v1) {
                    b = 0.5*mesh.point(vb);
                } else {
                    b = 0.5*mesh.point(va);
                }

                typename Mesh::Point current_face_middle = b + 0.5*mesh.point(_ci.v1);
                typename Mesh::Point nearest_in_old = findNearestInOld(current_face_middle,triangles_old);
                A_from_new += 0.25;
                B_from_new += (b - nearest_in_old);
                C_from_new += (b - nearest_in_old).sqrnorm();
                nb_points_new++;
            }
            ++ve0_it;
        }

        // + p1 courant

        typename Mesh::Point nearest_in_old = findNearestInOld(mesh.point(_ci.v1),triangles_old);
        // (X - target)(X-target) = XX - 2 target X + target^target
        A_from_new += 1.0;
        B_from_new += 2.0*( - nearest_in_old);
        C_from_new += nearest_in_old.sqrnorm();
        nb_points_new++;

        A_from_new /= nb_points_new;
        B_from_new /= nb_points_new;
        C_from_new /= nb_points_new;
        A_from_new *= 0.3;
        B_from_new *= 0.3;
        C_from_new *= 0.3;

        // add points from old surface
        float A_from_old = 0;
        typename Mesh::Point B_from_old(0.f,0.f,0.f);
        float C_from_old = 0;

        int nb_points_old = 0;

        // middle of faces
        /*
        for (unsigned i = 0; i < triangles_old.size(); i++) {
            // for the moment, using only middle of old faces
            typename Mesh::Point target = triangles_old[i].center();

            float a;
            typename Mesh::Point b;
            // get a and b so that the nearest point in the new surface is
            // a * current_p1 + b
            findNearestInNew(mesh, target, _ci, a, b);
            // A = a^2
            // B = 2 a (b - target)
            A_from_old += a*a;
            B_from_old += 2.f*a*(b - target);
            nb_points_old++;
        }
        */

        // milieu edges

        for (unsigned i = 0; i < edges_old.size(); i++) {
            // for the moment, using only middle of old faces
            typename Mesh::Point target = edges_old[i].center();

            float a;
            typename Mesh::Point b;
            // get a and b so that the nearest point in the new surface is
            // a * current_p1 + b
            findNearestInNew(mesh, target, _ci, a, b);
            //std::cout << "use center in eq : " << target << std::endl;
            //std::cout << "distance : " << (a*mesh.point(_ci.v1) + b - target).norm() << std::endl;
            //std::cout << "distance^2 : " << (a*mesh.point(_ci.v1) + b - target).sqrnorm() << std::endl;

            // A = a^2
            // B = 2 a (b - target)
            // C = (b - target)*(b - target)

            A_from_old += a*a;
            B_from_old += 2.f*a*(b - target);
            C_from_old += (b - target).sqrnorm();
            nb_points_old++;
        }


        // sommets
        float a;
        typename Mesh::Point b;
        findNearestInNew(mesh, _ci.p0, _ci, a, b);
        A_from_old += a*a;
        B_from_old += 2.f*a*(b - _ci.p0);
        C_from_old += (b - _ci.p0).sqrnorm();

        nb_points_old++;
        findNearestInNew(mesh, _ci.p1, _ci, a, b);
        A_from_old += a*a;
        B_from_old += 2.f*a*(b - _ci.p1);
        C_from_old += (b - _ci.p1).sqrnorm();

        nb_points_old++;


        A_from_old /= nb_points_old;
        B_from_old /= nb_points_old;
        C_from_old /= nb_points_old;

        // add quadratic equations from old and new surface
        float A = A_from_new + A_from_old;
        typename Mesh::Point B = B_from_new + B_from_old;
        float C = C_from_new + C_from_old;

        // maybe add a constraint between X and (p0 + p1)*0.5
        // ((p0 + p1)*0.5 - X) ^2 = XX - 2.0 * (p0 + p1)*0.5 * X

        //std::cout << "A = " << A << "     B = " << B << std::endl;

        //A += 1.0 * 0.001;
        //B += -2.0*(_ci.p1)*0.001;


        if (A < voxelSize_ * THRESHOLD) {
            assert(0 && "problem, A = 0, cannot solve");
            return;
        }

        // Erreur : AXX + BX + C
        currentError = A * mesh.point(_ci.v1).sqrnorm() + dot(mesh.point(_ci.v1),B) + C;
        //std::cout << "Current error : " << currentError << std::endl;

        if (currentError < voxelSize_*voxelSize_*0.1) { // error est une somme de distance au carré,
#if DEBUG_OPT_PRINT
            std::cout << "Stop cause zero error with " <<  loop  << " loops and p=" << mesh.point(_ci.v1) << std::endl;
#endif
            //nb_no_error++;
            //loops_no_error += loop;
            //printStats();
            //return;
        }

        // ici, on doit résoudre min_X [ AXX + BX ]
        // Gradient = 0 donne 2A Id*X + B = 0
        // donc X = -B / (2*A)
        //std::cout << "Set point from " <<  mesh.point(_ci.v1) << " to " <<  -B / (2*A) << std::endl;
        //mesh.set_point(_ci.v1, mesh.point(_ci.v1)*0.5 + -B / (2.f*A)*0.5);
        if ((mesh.point(_ci.v1)  + B / (2.f*A)).norm() < voxelSize_ * 0.01) {
#if DEBUG_OPT_PRINT
            std::cout << "Stop cause local optimum : " << currentError
                      << " error at " << loop << " loops" << std::endl;
            //std::cout << "current pos: " << mesh.point(_ci.v1) << std::endl;
#endif
            nb_optimum++;
            loops_optimum += loop;
            //if (loop > 10)
            //    std::cout << "(optimum loops " << loop << ")" << std::endl;
            //printStats();

            break; // leave the for loop

        }

        //mesh.set_point(_ci.v1, mesh.point(_ci.v1)*0.5 -B / (2.f*A)*0.5);
        mesh.set_point(_ci.v1,  -B / (2.f*A));



#if DEBUG_OPT_PRINT
        if (i+1==max_loop) {
           // std::cout << "Stop cause max loops: " << std::endl;
            //std::cout << "B: " << B << std::endl;
            //std::cout << "A: " << A << std::endl;
            //std::cout << "current pos: " << mesh.point(_ci.v1) << std::endl;
        }
#endif

        if (loop > 1000) {
           // std::cout << mesh.point(_ci.v1) << std::endl;
            //printTriangleMatlab(triangles_old);
         }

    }


#if DEBUG_OPT_PRINT
    //std::cout << "Current error : " << currentError << std::endl;
    std::cout << "Iterations " <<  loop << std::endl;
#endif
    nb_max_loop++;
    loops_nb_max_loop+= loop;
    //printStats();
    //printTriangleMatlab(triangles_old);

    //if (loop >= 20)
    //    mesh.set_point(_ci.v1, _ci.p1);

        //std::cout << "Iterations : " <<  loop << std::endl;

}


template<class MeshT>
void ModQuadraticT<MeshT>::filter(Mesh &mesh, const CollapseInfo& _ci, const std::vector<Triangle> &triangles_old) {
    //FILTERING

    int tex_height = -1;
    if(texturesList->has_diffuse_front) {
        tex_height = texturesList->diffuse_front.tex_source_png.getheight();
    } else if (texturesList->has_specular_front) {
        tex_height = texturesList->specular_front.tex_source_png.getheight();
    } else if (texturesList->has_sggx_front) {
        tex_height = texturesList->sggx_front.sggx_sigma.tex_source_png.getheight();
    } else {
        return; // no textures!
    }


    std::vector<double> old_proportions;
    std::vector<double> new_proportions;
    assert(texturesList != NULL);
    double sum_old_area = 0;
    double sum_new_area = 0;

    // compute proportions
    for (unsigned i = 0; i < triangles_old.size(); i++) {
        old_proportions.push_back(triangles_old[i].area());
        sum_old_area += old_proportions[i];
        if (!mesh_.status(triangles_old[i].fh).deleted()) {
            typename Mesh::VertexHandle va, vb, vc;
            typename Mesh::Point pa, pb, pc;
            getVertices(mesh_, triangles_old[i].fh, va, vb, vc);
            pa = mesh_.point(va);
            pb = mesh_.point(vb);
            pc = mesh_.point(vc);
            new_proportions.push_back(0.5 * double((cross(pb - pa, pc - pa)).norm()));
            sum_new_area += new_proportions[i];
        } else {
            new_proportions.push_back(0.0);
        }
    }
    assert(triangles_old.size() == old_proportions.size());
    assert(triangles_old.size() == new_proportions.size());
    assert(sum_old_area > 0.0 && sum_new_area > 0.0);
    for (unsigned i = 0; i < old_proportions.size(); i++) {
        old_proportions[i] /= sum_old_area;
        new_proportions[i] /= sum_new_area;
    }


    // filtering ?

    OpenMesh::Vec3d mean_diffuse_front(0,0,0);
    OpenMesh::Vec3d mean_specular_front(0,0,0);
    OpenMesh::Vec6d mean_sggx_front(0,0,0,0,0,0);

    std::vector<OpenMesh::Vec3d> old_diffuse_front;
    std::vector<OpenMesh::Vec3d> old_specular_front;
    std::vector<OpenMesh::Vec6d> old_sggx_front;

    double mean_area = 0;

    assert(mesh_final_ != NULL);

    for (unsigned i = 0; i < triangles_old.size(); i++) {
        typename Mesh::FaceHalfedgeIter fhe_it=mesh_final_->fh_iter(triangles_old[i].fh);
        typename Mesh::TexCoord2D coord = mesh_final_->texcoord2D(*fhe_it);
        int x_coord = int(coord[0] * 4096) + 1;
        int y_coord = int(coord[1] * tex_height) + 1;


        // DIFFUSE
        if(texturesList->has_diffuse_front) {
            double r= double(texturesList->diffuse_front.tex_1_png.read(x_coord, y_coord, 1))/65535.0;
            double g= double(texturesList->diffuse_front.tex_1_png.read(x_coord, y_coord, 2))/65535.0;
            double b= double(texturesList->diffuse_front.tex_1_png.read(x_coord, y_coord, 3))/65535.0;
            old_diffuse_front.push_back(OpenMesh::Vec3d(r,g,b));
        }

        // SPECULAR
        if(texturesList->has_specular_front) {
            double r= double(texturesList->specular_front.tex_1_png.read(x_coord, y_coord, 1))/65535.0;
            double g= double(texturesList->specular_front.tex_1_png.read(x_coord, y_coord, 2))/65535.0;
            double b= double(texturesList->specular_front.tex_1_png.read(x_coord, y_coord, 3))/65535.0;
            old_specular_front.push_back(OpenMesh::Vec3d(r,g,b));
        }

        // SGGX
        if(texturesList->has_sggx_front) {
            // sigma déja entre 0 et 1
            double sigma_x = double(texturesList->sggx_front.sggx_sigma.tex_1_png.read(x_coord, y_coord, 1))/65535.0;
            double sigma_y = double(texturesList->sggx_front.sggx_sigma.tex_1_png.read(x_coord, y_coord, 2))/65535.0;
            double sigma_z = double(texturesList->sggx_front.sggx_sigma.tex_1_png.read(x_coord, y_coord, 3))/65535.0;
            // from 0 .. 1 to -1 .. 1
            double r_xy = double(texturesList->sggx_front.sggx_r.tex_1_png.read(x_coord, y_coord, 1))/65535.0*2.0 - 1.0;
            double r_xz = double(texturesList->sggx_front.sggx_r.tex_1_png.read(x_coord, y_coord, 2))/65535.0*2.0 - 1.0;
            double r_yz = double(texturesList->sggx_front.sggx_r.tex_1_png.read(x_coord, y_coord, 3))/65535.0*2.0 - 1.0;

            // push back SGGX matrix, can be filtered linearly
            old_sggx_front.push_back(OpenMesh::Vec6d(sigma_x * sigma_x,
                                                     sigma_y * sigma_y,
                                                     sigma_z * sigma_z,
                                                     r_xy * sigma_x * sigma_y,
                                                     r_xz * sigma_x * sigma_z,
                                                     r_yz * sigma_y * sigma_z));
        }


        if (old_proportions[i] > new_proportions[i]) {
            if(texturesList->has_diffuse_front)
                mean_diffuse_front += old_diffuse_front[i] * (old_proportions[i] - new_proportions[i]);

            if(texturesList->has_specular_front)
                mean_specular_front += old_specular_front[i] * (old_proportions[i] - new_proportions[i]);

            if(texturesList->has_sggx_front)
                mean_sggx_front += old_sggx_front[i] * (old_proportions[i] - new_proportions[i]);

            mean_area += old_proportions[i] - new_proportions[i];
        }
    }

    // normalize
    if (mean_area > 0) { // should be, unless degenerated triangles
        if(texturesList->has_diffuse_front)
            mean_diffuse_front /= mean_area;

        if(texturesList->has_specular_front)
            mean_specular_front /= mean_area;

        if(texturesList->has_sggx_front)
            mean_sggx_front /= mean_area;
    }

    // now udate material form triangles that have increasing proportion
    for (unsigned i = 0; i < triangles_old.size(); i++) {
        if (old_proportions[i] < new_proportions[i]) {

            OpenMesh::Vec3d filtered_diffuse_front(0,0,0);
            OpenMesh::Vec3d filtered_specular_front(0,0,0);
            OpenMesh::Vec6d filtered_sggx_front(0,0,0,0,0,0);

            typename Mesh::FaceHalfedgeIter fhe_it=mesh_final_->fh_iter(triangles_old[i].fh);
            typename Mesh::TexCoord2D coord = mesh_final_->texcoord2D(*fhe_it);
            int x_coord = int(coord[0] * 4096) + 1;
            int y_coord = int(coord[1] * tex_height) + 1;

            if(texturesList->has_diffuse_front) {
                filtered_diffuse_front = old_proportions[i] * old_diffuse_front[i] + (new_proportions[i] - old_proportions[i]) * mean_diffuse_front;
                filtered_diffuse_front /= new_proportions[i];
                texturesList->diffuse_front.tex_1_png.plot(x_coord, y_coord, filtered_diffuse_front[0], filtered_diffuse_front[1], filtered_diffuse_front[2]);
            }

            if(texturesList->has_specular_front) {
                filtered_specular_front = old_proportions[i] * old_specular_front[i] + (new_proportions[i] - old_proportions[i]) * mean_specular_front;
                filtered_specular_front /= new_proportions[i];
                texturesList->specular_front.tex_1_png.plot(x_coord, y_coord, filtered_specular_front[0], filtered_specular_front[1], filtered_specular_front[2]);
            }

            if(texturesList->has_sggx_front) {
                filtered_sggx_front = old_proportions[i] * old_sggx_front[i] + (new_proportions[i] - old_proportions[i]) * mean_sggx_front;
                filtered_sggx_front /= new_proportions[i];
                // get back to sigma / r
                double sigma_x = std::sqrt(filtered_sggx_front[0]);
                double sigma_y = std::sqrt(filtered_sggx_front[1]);
                double sigma_z = std::sqrt(filtered_sggx_front[2]);
                double r_xy = filtered_sggx_front[3] / (sigma_x * sigma_y) * 0.5 + 0.5;
                double r_xz = filtered_sggx_front[4] / (sigma_x * sigma_z) * 0.5 + 0.5;
                double r_yz = filtered_sggx_front[5] / (sigma_y * sigma_z) * 0.5 + 0.5;
                texturesList->sggx_front.sggx_sigma.tex_1_png.plot(x_coord, y_coord, sigma_x, sigma_y, sigma_z);
                texturesList->sggx_front.sggx_r.tex_1_png.plot(x_coord, y_coord, r_xy, r_xz, r_yz);
            }
        }
    }
}

template<class MeshT>
void ModQuadraticT<MeshT>::postprocess_collapse(const CollapseInfo& _ci) {


    //mesh_.set_point(_ci.v1, _ci.p1*0.5 + _ci.p0*0.5);
    std::vector<Triangle> old_triangles;
    optimize(mesh_,_ci, old_triangles);
    //std::cout << "Post process completed" << std::endl;
    filter(mesh_,_ci,old_triangles);
}

template<class MeshT>
void ModQuadraticT<MeshT>::test_distPointTriangle() {

    typename Mesh::Point p, pa, pb, pc;
    pa = typename Mesh::Point(0.2,3.0,-9.0);
    pb = typename Mesh::Point(-0.2,0.0,1.0);
    pc = typename Mesh::Point(2.0,3.0,0.1);

    typename Mesh::Point normal = cross(pb - pa, pc - pa);
    normal /= normal.norm();

    float s, t;
    float dist;

    // test 1 : p in the middle
    p = (pa + pb + pc) / 3.0;
    dist = distPointTriangle_(p, pa, pb, pc, s, t);
    assert(dist < 10e-5 && "should be 0");
    assertIsAlmost(s,1.0/3.0,0.0001);
    assertIsAlmost(t,1.0/3.0,0.0001);

    // test 2 : p somewhere inside
    p = 0.4 * pa + 0.5 * pb + 0.1*pc;
    dist = distPointTriangle_(p, pa, pb, pc, s, t);
    assert(dist < 10e-5 && "should be 0");
    assertIsAlmost(s,0.5,0.0001);
    assertIsAlmost(t,0.1,0.0001);

    // test 3 : p on a corner
    p = pa;
    dist = distPointTriangle_(p, pa, pb, pc, s, t);
    assert(dist < 10e-5 && "should be 0");
    assertIsAlmost(s,0.0,0.0001);
    assertIsAlmost(t,0.0,0.0001);

    // test 4 : p on another corner
    p = pb;
    dist = distPointTriangle_(p, pa, pb, pc, s, t);
    assert(dist < 10e-5 && "should be 0");
    assertIsAlmost(s,1.0,0.0001);
    assertIsAlmost(t,0.0,0.0001);

    // test 5 : p on another corner
    p = pc;
    dist = distPointTriangle_(p, pa, pb, pc, s, t);
    assert(dist < 10e-5 && "should be 0");
    assertIsAlmost(s,0.0,0.0001);
    assertIsAlmost(t,1.0,0.0001);

    // test 6 : p on an edge
    p = 0.5*pc + 0.5*pa;
    dist = distPointTriangle_(p, pa, pb, pc, s, t);
    assert(dist < 10e-5 && "should be 0");
    assertIsAlmost(s,0.0,0.0001);
    assertIsAlmost(t,0.5,0.0001);

    // test 7 : p above, inside
    p = 0.4 * pa + 0.5 * pb + 0.1*pc;
    p += normal * 0.3;
    dist = distPointTriangle_(p, pa, pb, pc, s, t);
    assertIsAlmost(dist, 0.3, 0.0001);
    assertIsAlmost(s,0.5,0.0001);
    assertIsAlmost(t,0.1,0.0001);

    // test 8 : p on one side
    p = 0.2 * pa + 0.8 * pc;
    typename Mesh::Point ext = cross(normal, pc-pa);
    ext /= ext.norm();
    p += ext * 0.26;
    dist = distPointTriangle_(p, pa, pb, pc, s, t);
    assertIsAlmost(dist, 0.26, 0.0001);
    assertIsAlmost(s,0.0,0.0001);
    assertIsAlmost(t,0.8,0.0001);

    std::cout << "test_distPointTriangle: OK!" << std::endl;

}

template<class MeshT>
void ModQuadraticT<MeshT>::test_distPointTriangle_bad_cases() {

    typename Mesh::Point p, pa, pb, pc;
    pa = typename Mesh::Point(0.2,3.0,-9.0);
    pb = typename Mesh::Point(-0.2,0.0,1.0);
    pc = typename Mesh::Point(2.0,3.0,0.1);

    typename Mesh::Point normal = cross(pb - pa, pc - pa);
    normal /= normal.norm();

    float s, t;
    float dist;

    // test 1a : pa == pb
    p = pa*0.5 + pc*0.5;
    dist = distPointTriangle_(pa, pa, pa, pc, s, t);
    assertIsAlmost(dist,0.0,0.0001);

    // test 1b : pa == pb
    p = (pa + pa + pc) / 3.0f;
    dist = distPointTriangle_(p, pa, pa, pc, s, t);
    assertIsAlmost(dist,0.0,0.0001);
    assertIsAlmost(s,0.0,0.0001);
    assertIsAlmost(t,1.0/3.0,0.0001);

    // test 2 : pb == pc
    p = (pa + pc + pc) / 3.0;
    dist = distPointTriangle_(p, pa, pc, pc, s, t);
    assertIsAlmost(dist,0.0,0.0001);


    // test 3 : pa == pc
    p = (pa + pb + pa) / 3.0;
    dist = distPointTriangle_(p, pa, pb, pa, s, t);
    assertIsAlmost(dist,0.0,0.0001);


    // test 4 : pa == pc == pb
    p = (pa + pa + pa) / 3.0;
    dist = distPointTriangle_(p, pa, pa, pa, s, t);
    assertIsAlmost(dist,0.0,0.0001);

    // test 5 : p ailleurs
    p = pa*3.0;
    dist = distPointTriangle_(p, pa, pa, pa, s, t);
    assertIsAlmost(dist,(p-pa).norm(),0.0001);

    std::cout << "test_distPointTriangle_bad_cases: OK!" << std::endl;

}

template<class MeshT>
void ModQuadraticT<MeshT>::test_findNearestInOld() {


    typename Mesh::Point p, pa, pb, pc, nearest;
    pa = typename Mesh::Point(0.2,3.0,-9.0);
    pb = typename Mesh::Point(-0.2,0.0,1.0);
    pc = typename Mesh::Point(2.0,3.0,0.1);
    Triangle tri1(pa, pb, pc);
    std::vector<Triangle> tri_list;
    tri_list.push_back(tri1);

    // test 1 : p in the middle, one triangle
    p = (pa + pb + pc) / 3.0;
    nearest = findNearestInOld(p, tri_list);
    assertIsAlmost(nearest[0],p[0],0.0001);
    assertIsAlmost(nearest[1],p[1],0.0001);
    assertIsAlmost(nearest[2],p[2],0.0001);


    // test 2 : p in the middle, two triangles
    pa = typename Mesh::Point(6.5,3.6,5.2);
    pb = typename Mesh::Point(6.5,8.0,9.2);
    pc = typename Mesh::Point(7.5,5.6,4.6);
    tri_list.push_back(Triangle(pa, pb, pc));
    nearest = findNearestInOld(p, tri_list);
    assertIsAlmost(nearest[0],p[0],0.0001);
    assertIsAlmost(nearest[1],p[1],0.0001);
    assertIsAlmost(nearest[2],p[2],0.0001);


    // test 3 : p in the middle, two triangles, second
    nearest = findNearestInOld(pa, tri_list);
    assertIsAlmost(nearest[0],pa[0],0.0001);
    assertIsAlmost(nearest[1],pa[1],0.0001);
    assertIsAlmost(nearest[2],pa[2],0.0001);


    std::cout << "test_findNearestInOld: OK!" << std::endl;

}


template<class MeshT>
void ModQuadraticT<MeshT>::test_optimize_1() {


    Mesh mesh;
    mesh.add_property(fV0orV1);
    mesh.request_vertex_status();
    mesh.request_edge_status();
    mesh.request_face_status();

    /*
     *
     *
     *
     *                v0(-1,0)-----v1(1,0)
     *                /    .        \
     *               /         .     \
     *              /              .  \
     *            v2(-2,-1)----------v3(2,-1)
     *
     */

    typename Mesh::VertexHandle vhandle[4];
    vhandle[0] = mesh.add_vertex(typename Mesh::Point(-1,  0,  0));
    vhandle[1] = mesh.add_vertex(typename Mesh::Point( 1,  0,  0));
    vhandle[2] = mesh.add_vertex(typename Mesh::Point(-2, -1,  0));
    vhandle[3] = mesh.add_vertex(typename Mesh::Point( 2, -1,  0));

    std::vector<typename Mesh::VertexHandle>  face_vhandles;
    face_vhandles.clear();
    face_vhandles.push_back(vhandle[0]);
    face_vhandles.push_back(vhandle[1]);
    face_vhandles.push_back(vhandle[3]);
    mesh.add_face(face_vhandles);
    face_vhandles.clear();
    face_vhandles.push_back(vhandle[0]);
    face_vhandles.push_back(vhandle[3]);
    face_vhandles.push_back(vhandle[2]);
    mesh.add_face(face_vhandles);

    typename Mesh::HalfedgeHandle heh = mesh.halfedge_handle(0);
    assert(mesh.from_vertex_handle(heh) == vhandle[0]);
    assert(mesh.to_vertex_handle(heh) == vhandle[1]);
    CollapseInfo ci(mesh,heh);

    tag_v0v1(mesh,ci);
    mesh.collapse(heh);

    float a;
    typename Mesh::Point b;
    typename Mesh::Point p;
    p = ci.p1;
    findNearestInNew(mesh, p, ci, a, b);
    assertIsAlmost(a,1.f,0.000001);
    assertIsAlmost(b[0],0.f,0.000001);
    assertIsAlmost(b[1],0.f,0.000001);
    assertIsAlmost(b[2],0.f,0.000001);

    p = ci.p1*0.1 + mesh.point(vhandle[2])*0.6 + mesh.point(vhandle[3])*0.3;
    findNearestInNew(mesh, p, ci, a, b);
    assertIsAlmost((a*ci.p1 + b - p).norm(),0.f,0.000001);

    p = ci.p1*0.6 + mesh.point(vhandle[2])*0.3 + mesh.point(vhandle[3])*0.2;
    findNearestInNew(mesh, p, ci, a, b);
    assertIsAlmost((a*ci.p1 + b - p).norm(),0.f,0.000001);

    optimize(mesh,ci);

    p = mesh.point(vhandle[2]);
    findNearestInNew(mesh, p, ci, a, b);
    assertIsAlmost(a,0.f,0.000001);
    assertIsAlmost(b[0],p[0],0.000001);
    assertIsAlmost(b[1],p[1],0.000001);
    assertIsAlmost(b[2],p[2],0.000001);

    std::cout << "test_optimize_1: p = " << mesh.point(vhandle[1]) << std::endl;

    std::cout << "test_optimize_1: OK!" << std::endl;

}

template<class MeshT>
void ModQuadraticT<MeshT>::test_optimize_2() {


    Mesh mesh;
    mesh.add_property(fV0orV1);
    mesh.request_vertex_status();
    mesh.request_edge_status();
    mesh.request_face_status();

    /*
     *
     *
     *
     *                v0(-1,0)-----v1(1,0)-----v4(2,-0)
     *
     *
     *
     *
     *                v2(-1,-1)-----v3(1,-1)
     *
     */

    typename Mesh::VertexHandle vhandle[5];
    vhandle[0] = mesh.add_vertex(typename Mesh::Point(-1,  0,  0));
    vhandle[1] = mesh.add_vertex(typename Mesh::Point( 1,  0,  0));
    vhandle[2] = mesh.add_vertex(typename Mesh::Point(-1, -1,  0));
    vhandle[3] = mesh.add_vertex(typename Mesh::Point( 1, -1,  0));
    vhandle[4] = mesh.add_vertex(typename Mesh::Point( 2, 0,  0));

    std::vector<typename Mesh::VertexHandle>  face_vhandles;
    face_vhandles.clear();
    face_vhandles.push_back(vhandle[0]);
    face_vhandles.push_back(vhandle[1]);
    face_vhandles.push_back(vhandle[3]);
    mesh.add_face(face_vhandles);
    face_vhandles.clear();
    face_vhandles.push_back(vhandle[0]);
    face_vhandles.push_back(vhandle[3]);
    face_vhandles.push_back(vhandle[2]);
    mesh.add_face(face_vhandles);
    face_vhandles.clear();
    face_vhandles.push_back(vhandle[1]);
    face_vhandles.push_back(vhandle[4]);
    face_vhandles.push_back(vhandle[3]);
    mesh.add_face(face_vhandles);

    typename Mesh::HalfedgeHandle heh = mesh.halfedge_handle(0);
    assert(mesh.from_vertex_handle(heh) == vhandle[0]);
    assert(mesh.to_vertex_handle(heh) == vhandle[1]);
    CollapseInfo ci(mesh,heh);

    tag_v0v1(mesh,ci);
    mesh.collapse(heh);

    float a;
    typename Mesh::Point b;
    typename Mesh::Point p;
    p = ci.p1;
    findNearestInNew(mesh, p, ci, a, b);
    assertIsAlmost(a,1.f,0.000001);
    assertIsAlmost(b[0],0.f,0.000001);
    assertIsAlmost(b[1],0.f,0.000001);
    assertIsAlmost(b[2],0.f,0.000001);

    p = ci.p1*0.1 + mesh.point(vhandle[2])*0.6 + mesh.point(vhandle[3])*0.3;
    findNearestInNew(mesh, p, ci, a, b);
    assertIsAlmost((a*ci.p1 + b - p).norm(),0.f,0.000001);

    p = ci.p1*0.6 + mesh.point(vhandle[2])*0.3 + mesh.point(vhandle[3])*0.2;
    findNearestInNew(mesh, p, ci, a, b);
    assertIsAlmost((a*ci.p1 + b - p).norm(),0.f,0.000001);

    optimize(mesh,ci);

    p = mesh.point(vhandle[2]);
    findNearestInNew(mesh, p, ci, a, b);
    assertIsAlmost(a,0.f,0.000001);
    assertIsAlmost(b[0],p[0],0.000001);
    assertIsAlmost(b[1],p[1],0.000001);
    assertIsAlmost(b[2],p[2],0.000001);

    std::cout << "test_optimize_2: p = " << mesh.point(vhandle[1]) << std::endl;

    std::cout << "test_optimize_2: OK!" << std::endl;
    /*
     *
     *
     *                                 v5(1.5,1)
     *                                / \
     *                               /   \
     *                              /     \
     *                v0(0,0)-----v1(1,0)-v4(2,0)
     *                /    .        \     /
     *               /         .     \   /
     *              /              .  \ /
     *            v2(-0.5,-1)----------v3(1.5,-1)
     *
     */


}

template<class MeshT>
void ModQuadraticT<MeshT>::test_buildOldLists_1() {
    Mesh mesh;
    mesh.add_property(fV0orV1);
    mesh.request_vertex_status();
    mesh.request_edge_status();
    mesh.request_face_status();

    /*
     *
     *
     *
     *                v0(-1,0)-----v1(1,0)
     *                /    .        \
     *               /         .     \
     *              /              .  \
     *            v2(-2,-1)----------v3(2,-1)
     *
     */

    typename Mesh::VertexHandle vhandle[4];
    vhandle[0] = mesh.add_vertex(typename Mesh::Point(-1,  0,  0));
    vhandle[1] = mesh.add_vertex(typename Mesh::Point( 1,  0,  0));
    vhandle[2] = mesh.add_vertex(typename Mesh::Point(-2, -1,  0));
    vhandle[3] = mesh.add_vertex(typename Mesh::Point( 2, -1,  0));

    std::vector<typename Mesh::VertexHandle>  face_vhandles;
    face_vhandles.clear();
    face_vhandles.push_back(vhandle[0]);
    face_vhandles.push_back(vhandle[1]);
    face_vhandles.push_back(vhandle[3]);
    mesh.add_face(face_vhandles);
    face_vhandles.clear();
    face_vhandles.push_back(vhandle[0]);
    face_vhandles.push_back(vhandle[3]);
    face_vhandles.push_back(vhandle[2]);
    mesh.add_face(face_vhandles);

    typename Mesh::HalfedgeHandle heh = mesh.halfedge_handle(0);
    assert(mesh.from_vertex_handle(heh) == vhandle[0]);
    assert(mesh.to_vertex_handle(heh) == vhandle[1]);
    CollapseInfo ci(mesh,heh);

    tag_v0v1(mesh,ci);
    mesh.collapse(heh);

    std::vector<Triangle> triangles_old;
    std::vector<Edge> edges_old;
    buildOldLists(mesh,ci,triangles_old, edges_old);
    assert(triangles_old.size() == 2);
    assert(edges_old.size() == 4);

    std::cout << "test_buildOldLists_1: OK!" << std::endl;

}

template<class MeshT>
void ModQuadraticT<MeshT>::test_buildOldLists_2() {
    Mesh mesh;
    mesh.add_property(fV0orV1);
    mesh.request_vertex_status();
    mesh.request_edge_status();
    mesh.request_face_status();

    /*
     *
     *            v6(-2,1)----------v5(2,1)
     *
     *
     *   v7(-3,0)     v0(-1,0)-----v1(1,0)       v4(3,0)
     *                /    .        \
     *               /         .     \
     *              /              .  \
     *            v2(-2,-1)----------v3(2,-1)
     *
     */

    typename Mesh::VertexHandle vhandle[8];
    vhandle[0] = mesh.add_vertex(typename Mesh::Point(-1,  0,  0));
    vhandle[1] = mesh.add_vertex(typename Mesh::Point( 1,  0,  0));
    vhandle[2] = mesh.add_vertex(typename Mesh::Point(-2, -1,  0));
    vhandle[3] = mesh.add_vertex(typename Mesh::Point( 2, -1,  0));

    vhandle[4] = mesh.add_vertex(typename Mesh::Point( 3,  0,  0));
    vhandle[5] = mesh.add_vertex(typename Mesh::Point( 2,  1,  0));
    vhandle[6] = mesh.add_vertex(typename Mesh::Point(-2,  1,  0));
    vhandle[7] = mesh.add_vertex(typename Mesh::Point(-3,  0,  0));

    std::vector<typename Mesh::VertexHandle>  face_vhandles;
    face_vhandles.clear();
    face_vhandles.push_back(vhandle[0]);
    face_vhandles.push_back(vhandle[1]);
    face_vhandles.push_back(vhandle[3]);
    mesh.add_face(face_vhandles);
    face_vhandles.clear();
    face_vhandles.push_back(vhandle[0]);
    face_vhandles.push_back(vhandle[3]);
    face_vhandles.push_back(vhandle[2]);
    mesh.add_face(face_vhandles);
    face_vhandles.clear();
    face_vhandles.push_back(vhandle[1]);
    face_vhandles.push_back(vhandle[4]);
    face_vhandles.push_back(vhandle[3]);
    mesh.add_face(face_vhandles);
    face_vhandles.clear();
    face_vhandles.push_back(vhandle[1]);
    face_vhandles.push_back(vhandle[5]);
    face_vhandles.push_back(vhandle[4]);
    mesh.add_face(face_vhandles);
    face_vhandles.clear();
    face_vhandles.push_back(vhandle[1]);
    face_vhandles.push_back(vhandle[6]);
    face_vhandles.push_back(vhandle[5]);
    mesh.add_face(face_vhandles);
    face_vhandles.clear();
    face_vhandles.push_back(vhandle[1]);
    face_vhandles.push_back(vhandle[0]);
    face_vhandles.push_back(vhandle[6]);
    mesh.add_face(face_vhandles);
    face_vhandles.clear();
    face_vhandles.push_back(vhandle[0]);
    face_vhandles.push_back(vhandle[7]);
    face_vhandles.push_back(vhandle[6]);
    mesh.add_face(face_vhandles);
    face_vhandles.clear();
    face_vhandles.push_back(vhandle[0]);
    face_vhandles.push_back(vhandle[2]);
    face_vhandles.push_back(vhandle[7]);
    mesh.add_face(face_vhandles);

    typename Mesh::HalfedgeHandle heh = mesh.halfedge_handle(0);
    assert(mesh.from_vertex_handle(heh) == vhandle[0]);
    assert(mesh.to_vertex_handle(heh) == vhandle[1]);
    CollapseInfo ci(mesh,heh);

    tag_v0v1(mesh,ci);
    mesh.collapse(heh);

    std::vector<Triangle> triangles_old;
    std::vector<Edge> edges_old;
    buildOldLists(mesh,ci,triangles_old, edges_old);
    assert(triangles_old.size() == 8);
    assert(edges_old.size() == 9);

    std::cout << "test_buildOldLists_2: OK!" << std::endl;

}


//=============================================================================
}
}
//=============================================================================
