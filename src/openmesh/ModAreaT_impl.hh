/* ========================================================================= *
 *                                                                           *
 *                               OpenMesh                                    *
 *           Copyright (c) 2001-2015, RWTH-Aachen University                 *
 *           Department of Computer Graphics and Multimedia                  *
 *                          All rights reserved.                             *
 *                            www.openmesh.org                               *
 *                                                                           *
 *---------------------------------------------------------------------------*
 * This file is part of OpenMesh.                                            *
 *---------------------------------------------------------------------------*
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 * 1. Redistributions of source code must retain the above copyright notice, *
 *    this list of conditions and the following disclaimer.                  *
 *                                                                           *
 * 2. Redistributions in binary form must reproduce the above copyright      *
 *    notice, this list of conditions and the following disclaimer in the    *
 *    documentation and/or other materials provided with the distribution.   *
 *                                                                           *
 * 3. Neither the name of the copyright holder nor the names of its          *
 *    contributors may be used to endorse or promote products derived from   *
 *    this software without specific prior written permission.               *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER *
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,  *
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,       *
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR        *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
 *                                                                           *
 * ========================================================================= */

/*===========================================================================*\
 *                                                                           *
 *   $Revision$                                                        *
 *   $Date$                   *
 *                                                                           *
 \*===========================================================================*/

/** \file ModAreaT.cc
 */

//=============================================================================
//
//  CLASS ModAreaT - IMPLEMENTATION
//
//=============================================================================
#define OPENMESH_DECIMATER_MODAREAT_C

//== INCLUDES =================================================================

#include "ModAreaT.hh"

//== NAMESPACES ===============================================================

namespace OpenMesh {
namespace Decimater {

//== IMPLEMENTATION ==========================================================

template<class MeshT>
typename ModAreaT<MeshT>::Scalar ModAreaT<MeshT>::area(
    const Point& _v0, const Point& _v1, const Point& _v2) {
  Point d0 = _v0 - _v1;
  Point d1 = _v1 - _v2;

  // finds the max squared edge length
  Scalar l2, maxl2 = d0.sqrnorm();
  if ((l2 = d1.sqrnorm()) > maxl2)
    maxl2 = l2;
  // keep searching for the max squared edge length
  d1 = _v2 - _v0;
  if ((l2 = d1.sqrnorm()) > maxl2)
    maxl2 = l2;

  // squared area of the parallelogram spanned by d0 and d1
  return (d0 % d1).norm()*0.5;
  //Scalar a2 = (d0 % d1).sqrnorm();

  // the area of the triangle would be
  // sqrt(a2)/2 or length * height / 2
  // aspect ratio = length / height
  //              = length * length / (2*area)
  //              = length * length / sqrt(a2)

  // returns the length of the longest edge
  //         divided by its corresponding height
  //return sqrt((maxl2 * maxl2) / a2);
}

//-----------------------------------------------------------------------------

template<class MeshT>
void ModAreaT<MeshT>::initialize() {
  typename Mesh::FaceIter f_it, f_end(mesh_.faces_end());
  typename Mesh::FVIter fv_it;

  for (f_it = mesh_.faces_begin(); f_it != f_end; ++f_it) {
    fv_it = mesh_.fv_iter(*f_it);
    typename Mesh::Point& p0 = mesh_.point(*fv_it);
    typename Mesh::Point& p1 = mesh_.point(*(++fv_it));
    typename Mesh::Point& p2 = mesh_.point(*(++fv_it));

    mesh_.property(area_, *f_it) = static_cast<typename Mesh::Scalar>(area(p0, p1, p2));
  }
}

//-----------------------------------------------------------------------------

template<class MeshT>
void ModAreaT<MeshT>::preprocess_collapse(const CollapseInfo& _ci) {
  typename Mesh::FaceHandle fh;
  typename Mesh::FVIter fv_it;

  for (typename Mesh::VFIter vf_it = mesh_.vf_iter(_ci.v0); vf_it.is_valid(); ++vf_it) {
    fh = *vf_it;
    if (fh != _ci.fl && fh != _ci.fr) {
      fv_it = mesh_.fv_iter(fh);
      typename Mesh::Point& p0 = mesh_.point(*fv_it);
      typename Mesh::Point& p1 = mesh_.point(*(++fv_it));
      typename Mesh::Point& p2 = mesh_.point(*(++fv_it));

      mesh_.property(area_, fh) = static_cast<typename Mesh::Scalar>(area(p0, p1, p2));
    }
  }
}

//-----------------------------------------------------------------------------

template<class MeshT>
float ModAreaT<MeshT>::collapse_priority(const CollapseInfo& _ci) {
  typename Mesh::VertexHandle v2, v3;
  typename Mesh::FaceHandle fh;
  const typename Mesh::Point *p1(&_ci.p1), *p2, *p3;
  typename Mesh::Scalar r0, r1, r0_min(100000.0), r1_min(100000.0), r0_max(0.0), r1_max(0.0), r0_sum(0.0), r1_sum(0);
  typename Mesh::ConstVertexOHalfedgeIter voh_it(mesh_, _ci.v0); // tous les halfedge sortant = un par face voisine de v0, le vertex qui va disparaitre

  v3 = mesh_.to_vertex_handle(*voh_it);
  p3 = &mesh_.point(v3);

  while (voh_it.is_valid()) {
    v2 = v3;
    p2 = p3;

    ++voh_it;
    v3 = mesh_.to_vertex_handle(*voh_it);
    p3 = &mesh_.point(v3);

    fh = mesh_.face_handle(*voh_it);

    // if not boundary
    if (fh.is_valid()) {
      // area before

      r0 = mesh_.property(area_, fh);
      if (r0 < r0_min)
        r0_min = r0;

      // area before
      if (r0 > r0_max)
        r0_max = r0;

      r0_sum += r0;

      // area after
      r1 = static_cast<typename Mesh::Scalar>(area(*p1, *p2, *p3));
      if (!(v2 == _ci.v1 || v3 == _ci.v1))
        if (r1 < r1_min)
          r1_min = r1;

      // area after
      if (!(v2 == _ci.v1 || v3 == _ci.v1))
        if (r1 > r1_max)
          r1_max = r1;
    }
  }

  if (Base::is_binary()) {
      //std::cout << 1.0/r0_min << " " << 1.0/min_area_ << std::endl;
    return
        // r1_min = l'aire la plus petite après, r0_min = aire la plus petite avant
        // il faut que au moins un des triangles ait été trop petit : r0_min > min_area_
        // comme inverse, si rmin après est plus grande que avant, OK, et si rmin après plus peti
            ((r1_max <= max_area_) /*&& (r1_min > r0_min) && (r1_sum <= r0_sum)*/) ? float(Base::LEGAL_COLLAPSE) :
            float(Base::ILLEGAL_COLLAPSE);

  } else {
      std::cerr << "Not supported" << std::endl;
  }
}

//-----------------------------------------------------------------------------

template<class MeshT>
void ModAreaT<MeshT>::set_error_tolerance_factor(double _factor) {
  if (_factor >= 0.0 && _factor <= 1.0) {
    // the smaller the factor, the larger min_area_ gets
    // thus creating a stricter constraint
    // division by (2.0 - error_tolerance_factor_) is for normalization
    float min_area = min_area_ * (2.f - float(_factor)) / (2.f - float(this->error_tolerance_factor_));
    set_area_min(min_area);
    this->error_tolerance_factor_ = _factor;
  }
}

//=============================================================================
}
}
//=============================================================================
