/* ========================================================================= *
 *                                                                           *
 *                               OpenMesh                                    *
 *           Copyright (c) 2001-2015, RWTH-Aachen University                 *
 *           Department of Computer Graphics and Multimedia                  *
 *                          All rights reserved.                             *
 *                            www.openmesh.org                               *
 *                                                                           *
 *---------------------------------------------------------------------------*
 * This file is part of OpenMesh.                                            *
 *---------------------------------------------------------------------------*
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 * 1. Redistributions of source code must retain the above copyright notice, *
 *    this list of conditions and the following disclaimer.                  *
 *                                                                           *
 * 2. Redistributions in binary form must reproduce the above copyright      *
 *    notice, this list of conditions and the following disclaimer in the    *
 *    documentation and/or other materials provided with the distribution.   *
 *                                                                           *
 * 3. Neither the name of the copyright holder nor the names of its          *
 *    contributors may be used to endorse or promote products derived from   *
 *    this software without specific prior written permission.               *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER *
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,  *
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,       *
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR        *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
 *                                                                           *
 * ========================================================================= */

/*===========================================================================*\
 *                                                                           *
 *   $Revision$                                                        *
 *   $Date$                   *
 *                                                                           *
 \*===========================================================================*/

/** \file ModOptimT.cc
 */

//=============================================================================
//
//  CLASS ModOptimT - IMPLEMENTATION
//
//=============================================================================
#define OPENMESH_DECIMATER_MODOPTIMT_C

//== INCLUDES =================================================================

#include "ModOptimT.hh"
#include <cfloat>
#include <random>

//== NAMESPACES ===============================================================

namespace OpenMesh {
namespace Decimater {

//== UTILS ===============================================================

template<class MeshT>
float ModOptimT<MeshT>::getProjArea(const typename Mesh::Point &p_1,
                                    const typename Mesh::Point &p_2,
                                    const typename Mesh::Point &p_3,
                                    const typename Mesh::Point &n) {
    float area = dot(n, cross(p_2 - p_1, p_3 - p_1));
    area = std::abs(area) * 0.5;
    /*
    std::cout << "area " << area  << std::endl;
    std::cout << "p_1 " << p_1  << std::endl;
    std::cout << "p_2 " << p_2  << std::endl;
    std::cout << "p_3 " << p_3  << std::endl;
    std::cout << "n " << n  << std::endl;
    */
    return area;
}

template<class MeshT>
void ModOptimT<MeshT>::getPoints(typename Mesh::FaceHandle fh,
                                 typename Mesh::Point &p_1,
                                 typename Mesh::Point &p_2,
                                 typename Mesh::Point &p_3) {
    typename Mesh::ConstFaceVertexIter fv_it(mesh_, fh);
    p_1 = mesh_.point(*fv_it);
    fv_it++;
    p_2 = mesh_.point(*fv_it);
    fv_it++;
    p_3 = mesh_.point(*fv_it);
}

template<class MeshT>
typename ModOptimT<MeshT>::Mesh::Point ModOptimT<MeshT>::ortho(typename Mesh::Point normal) {
    if (normal[0] != 0.0 || normal[1] != 0) {
        return typename Mesh::Point(-normal[1], normal[0], 0.0) / (typename Mesh::Point(-normal[1], normal[0], 0.0)).norm();
    }
    return typename Mesh::Point(0.0, -normal[2], normal[1]) / (typename Mesh::Point(0.0, -normal[2], normal[1])).norm();
}

//== IMPLEMENTATION ==========================================================

//-----------------------------------------------------------------------------

template<class MeshT>
float ModOptimT<MeshT>::collapse_priority(const CollapseInfo& _ci) {
    return float(Base::LEGAL_COLLAPSE);
}

//-----------------------------------------------------------------------------

template<class MeshT>
void ModOptimT<MeshT>::preprocess_collapse(const CollapseInfo& _ci) {


    int nbface = 0;

    // calculer les directions et les aires initiales dans ces directions
    float area_before_1 = 0.f;
    float area_before_2 = 0.f;
    float area_before_3 = 0.f;
    typename Mesh::Point n1, n2, n3;


    // calculer les directions
    if (_ci.fl.is_valid() && _ci.fr.is_valid()) {

        // faces voisines
        typename Mesh::Point normal_l = cross(_ci.p1 - _ci.p0, mesh_.point(_ci.vl) - _ci.p0);
        normal_l /= (normal_l).norm();

        typename Mesh::Point normal_r = -cross(_ci.p1 - _ci.p0, mesh_.point(_ci.vr) - _ci.p0);
        normal_r /= (normal_r).norm();

        if (dot(normal_l,normal_r) > -0.95) {
            n1 = cross(_ci.p1 - _ci.p0, normal_l + normal_r);
        } else {
            // avoid insconsistencies?
            n1 = normal_r;
        }
    } else if (_ci.fl.is_valid()) {
        n1 = cross(_ci.p1 - _ci.p0, mesh_.point(_ci.vl) - _ci.p0);
    } else {
        n1 = cross(_ci.p1 - _ci.p0, mesh_.point(_ci.vr) - _ci.p0);
    }

    n1 /= (n1).norm();
    n2 = ortho(n1);
    n3 = cross(n1,n2);
    n3 /= (n3).norm();

    typename Mesh::ConstVertexFaceIter vf0_it(mesh_, _ci.v0);
    while (vf0_it.is_valid()) {
        if ((*vf0_it != _ci.fl) && (*vf0_it != _ci.fr)) {
            typename Mesh::Point p0, p1, p2;
            getPoints(*vf0_it, p0, p1, p2);
            area_before_1 += getProjArea(p0, p1, p2, n1);
            area_before_2 += getProjArea(p0, p1, p2, n2);
            area_before_3 += getProjArea(p0, p1, p2, n3);
            nbface++;
        }
        ++vf0_it;
    }

    typename Mesh::ConstVertexFaceIter vf1_it(mesh_, _ci.v1);
    while (vf1_it.is_valid()) {
        if ((*vf1_it != _ci.fl) && (*vf1_it != _ci.fr)) {
            typename Mesh::Point p0, p1, p2;
            getPoints(*vf1_it, p0, p1, p2);
            area_before_1 += getProjArea(p0, p1, p2, n1);
            area_before_2 += getProjArea(p0, p1, p2, n2);
            area_before_3 += getProjArea(p0, p1, p2, n3);
            nbface++;
        }
        ++vf1_it;
    }

    if (_ci.fl.is_valid()) {
        area_before_1 += getProjArea(_ci.p0, _ci.p1, mesh_.point(_ci.vl), n1);
        area_before_2 += getProjArea(_ci.p0, _ci.p1, mesh_.point(_ci.vl), n2);
        area_before_3 += getProjArea(_ci.p0, _ci.p1, mesh_.point(_ci.vl), n3);
        nbface++;
    }

    if (_ci.fr.is_valid()) {
        area_before_1 += getProjArea(_ci.p0, _ci.p1, mesh_.point(_ci.vr), n1);
        area_before_2 += getProjArea(_ci.p0, _ci.p1, mesh_.point(_ci.vr), n2);
        area_before_3 += getProjArea(_ci.p0, _ci.p1, mesh_.point(_ci.vr), n3);
        nbface++;
    }
    mesh_.property(n1_, _ci.v1) = n1;
    mesh_.property(n2_, _ci.v1) = n2;
    mesh_.property(n3_, _ci.v1) = n3;
    mesh_.property(area_before_1_, _ci.v1) = area_before_1;
    mesh_.property(area_before_2_, _ci.v1) = area_before_2;
    mesh_.property(area_before_3_, _ci.v1) = area_before_3;

    /*
    std::cout << "preprocess OK, nb faces " << nbface << " areas "
              << mesh_.property(area_before_1_, _ci.v1) << " "<<  mesh_.property(area_before_2_, _ci.v1) << " " << mesh_.property(area_before_3_, _ci.v1)
              << " ns "
              << mesh_.property(n1_, _ci.v1) << " "<<  mesh_.property(n2_, _ci.v1) << " " << mesh_.property(n3_, _ci.v1) << std::endl;
    */

}


template<class MeshT>
void ModOptimT<MeshT>::postprocess_collapse(const CollapseInfo& _ci) {
    // optimiser pas position de v1 pour conserver les aires projetées.

    typename Mesh::Point n1 = mesh_.property(n1_, _ci.v1);
    typename Mesh::Point n2 = mesh_.property(n2_, _ci.v1);
    typename Mesh::Point n3 = mesh_.property(n3_, _ci.v1);

    float area_before_1 = mesh_.property(area_before_1_, _ci.v1);
    float area_before_2 = mesh_.property(area_before_2_, _ci.v1);
    float area_before_3 = mesh_.property(area_before_3_, _ci.v1);

    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> dis(-1.f, 1.f);

    float dist01 = (_ci.p0 - _ci.p1).norm();

    float error_init;

    // partir de (p0 + p1)/2, et bonger au pif pour minimiser la différence d'aire.

    typename Mesh::Point init_p = (_ci.p0 + _ci.p1)*0.5;
    mesh_.set_point(_ci.v1, init_p);

    float current_error = 0;
    int nbface = 0;

    int nb_without = 0;
    int i = 0;

    while (nb_without < 20) {


        typename Mesh::Point r(dis(gen),
                               dis(gen),
                               dis(gen));

        // calculer l'erreur du point next_p
        typename Mesh::Point next_p = mesh_.point(_ci.v1) + r * dist01*0.1;
        typename Mesh::Point current_p = mesh_.point(_ci.v1);

        //std::cout << r * dist01*0.05 << std::endl;

        if (((init_p - next_p).norm() > dist01*0.5)) {
            continue;
        }

        // simulate
        mesh_.set_point(_ci.v1, next_p);


        // compute new areas
        float area_after_1 = 0.f;
        float area_after_2 = 0.f;
        float area_after_3 = 0.f;

        // ici, juste les faces voisines de v1, j'espère que la connection est la bonne,
        // ie que le collapse a vraiment été fait

        nbface = 0;
        typename Mesh::ConstVertexFaceIter vf1_it(mesh_, _ci.v1);
        while (vf1_it.is_valid()) {
            if (!mesh_.status(*vf1_it).deleted()) {
                typename Mesh::Point p0, p1, p2;
                getPoints(*vf1_it, p0, p1, p2);
                area_after_1 += getProjArea(p0, p1, p2, n1);
                area_after_2 += getProjArea(p0, p1, p2, n2);
                area_after_3 += getProjArea(p0, p1, p2, n3);
                nbface++;
            }
            ++vf1_it;
        }

        //std::cout << "error : " << area_after_1 << " " << area_after_2 << " " << area_after_3 << std::endl;
        //std::cout << "n1 : " << n1 << " " << n2 << " " << n3 << std::endl;

        float error = std::abs(area_after_1 - area_before_1)
                + std::abs(area_after_2 - area_before_2)
                + std::abs(area_after_3 - area_before_3);

        if ((i == 0) || ((error < current_error) )) {
            current_error = error;
            if (i == 0)
                error_init = current_error;
            nb_without = 0;
        } else {
            // unset
            mesh_.set_point(_ci.v1, current_p);
        }

        nb_without++;
        i++;

    }
    //std::cout << "nbface : " << nbface << std::endl;

    //std::cout << "collapse postprocessed : errors " << error_init << " " << current_error << std::endl;
}

//=============================================================================
}
}
//=============================================================================
