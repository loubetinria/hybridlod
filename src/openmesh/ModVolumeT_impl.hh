/* ========================================================================= *
 *                                                                           *
 *                               OpenMesh                                    *
 *           Copyright (c) 2001-2015, RWTH-Aachen University                 *
 *           Department of Computer Graphics and Multimedia                  *
 *                          All rights reserved.                             *
 *                            www.openmesh.org                               *
 *                                                                           *
 *---------------------------------------------------------------------------*
 * This file is part of OpenMesh.                                            *
 *---------------------------------------------------------------------------*
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 * 1. Redistributions of source code must retain the above copyright notice, *
 *    this list of conditions and the following disclaimer.                  *
 *                                                                           *
 * 2. Redistributions in binary form must reproduce the above copyright      *
 *    notice, this list of conditions and the following disclaimer in the    *
 *    documentation and/or other materials provided with the distribution.   *
 *                                                                           *
 * 3. Neither the name of the copyright holder nor the names of its          *
 *    contributors may be used to endorse or promote products derived from   *
 *    this software without specific prior written permission.               *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER *
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,  *
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,       *
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR        *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
 *                                                                           *
 * ========================================================================= */

/*===========================================================================*\
 *                                                                           *
 *   $Revision$                                                        *
 *   $Date$                   *
 *                                                                           *
 \*===========================================================================*/

/** \file ModVolumeT.cc
 */

//=============================================================================
//
//  CLASS ModVolumeT - IMPLEMENTATION
//
//=============================================================================
#define OPENMESH_DECIMATER_MODVOLUMET_C

//== INCLUDES =================================================================

#include "ModVolumeT.hh"
#include <OpenMesh/Core/Geometry/VectorT.hh>
#include <Eigen/Dense>
#include <cfloat>
#include <random>



//== NAMESPACES ===============================================================

namespace OpenMesh {
namespace Decimater {

//== UTILS ===============================================================

template<class MeshT>
float ModVolumeT<MeshT>::getProjArea(const typename Mesh::Point &p_1,
                                    const typename Mesh::Point &p_2,
                                    const typename Mesh::Point &p_3,
                                    const typename Mesh::Point &n) {
    float area = dot(n, cross(p_2 - p_1, p_3 - p_1));
    area = std::abs(area) * 0.5;
    /*
    std::cout << "area " << area  << std::endl;
    std::cout << "p_1 " << p_1  << std::endl;
    std::cout << "p_2 " << p_2  << std::endl;
    std::cout << "p_3 " << p_3  << std::endl;
    std::cout << "n " << n  << std::endl;
    */
    return area;
}

template<class MeshT>
void ModVolumeT<MeshT>::getPoints(typename Mesh::FaceHandle fh,
                                 Eigen::Vector3f &p_1,
                                 Eigen::Vector3f &p_2,
                                 typename Mesh::Point &p_3) {
    typename Mesh::ConstFaceVertexIter fv_it(mesh_, fh);
    typename Mesh::Point p1, p2, p3;
    p1 = mesh_.point(*fv_it);
    fv_it++;
    p2 = mesh_.point(*fv_it);
    fv_it++;
    p3 = mesh_.point(*fv_it);
    // to Eigen points
    p_1 = toEigen(p1);
    p_2 = toEigen(p2);
    p_3 = toEigen(p3);
}

template<class MeshT>
typename ModVolumeT<MeshT>::Mesh::Point ModVolumeT<MeshT>::ortho(typename Mesh::Point normal) {
    if (normal[0] != 0.0 || normal[1] != 0) {
        return typename Mesh::Point(-normal[1], normal[0], 0.0) / (typename Mesh::Point(-normal[1], normal[0], 0.0)).norm();
    }
    return typename Mesh::Point(0.0, -normal[2], normal[1]) / (typename Mesh::Point(0.0, -normal[2], normal[1])).norm();
}

//== STRUCT CONSTRAINTS ==========================================================


//== IMPLEMENTATION ==========================================================

//-----------------------------------------------------------------------------
template<class MeshT>
void ModVolumeT<MeshT>::volumePreservation(const CollapseInfo& _ci,
                                           Eigen::Vector3f &ai, float &b) {

    // loop over the edge neighbouring faces, compute normal and det
    ai = Eigen::Vector3f(0.f, 0.f, 0.f);
    b = 0.f;

    typename Mesh::ConstVertexFaceIter vf0_it(mesh_, _ci.v0);
    while (vf0_it.is_valid()) {
        if ((*vf0_it != _ci.fl) && (*vf0_it != _ci.fr)) {
            Eigen::Vector3f p0, p1, p2;
            getPoints(*vf0_it, p0, p1, p2);
            ai += (p1-p0).cross(p2-p0);
            b += Eigen::Matrix3f(p0, p1, p2);
        }
        ++vf0_it;
    }

    typename Mesh::ConstVertexFaceIter vf1_it(mesh_, _ci.v1);
    while (vf1_it.is_valid()) {
        if ((*vf1_it != _ci.fl) && (*vf1_it != _ci.fr)) {
            Eigen::Vector3f p0, p1, p2;
            getPoints(*vf1_it, p0, p1, p2);
            ai += (p1-p0).cross(p2-p0);
            b += Eigen::Matrix3f(p0, p1, p2);
        }
        ++vf1_it;
    }

    if (_ci.fl.is_valid()) {
        Eigen::Vector3f p0, p1, p2;
        getPoints(_ci.fl, p0, p1, p2);
        ai += (p1-p0).cross(p2-p0);
        b += Eigen::Matrix3f(p0, p1, p2);
    }

    if (_ci.fr.is_valid()) {
        Eigen::Vector3f p0, p1, p2;
        getPoints(_ci.fr, p0, p1, p2);
        ai += (p1-p0).cross(p2-p0);
        b += Eigen::Matrix3f(p0, p1, p2);
    }
}

template<class MeshT>
void ModVolumeT<MeshT>::boundaryPreservation(const CollapseInfo& _ci,
                                             Eigen::Vector3f &ai_0, float &b_0,
                                             Eigen::Vector3f &ai_1, float &b_1) {

    Eigen::Vector3f pa, p0, p1, pb;
    // dans l'ordre : pa, p0, p1, pb
    if (mesh_.is_boundary(_ci.v0v1)) {
        typename Mesh::HalfedgeHandle next_heh = mesh_.next_halfedge_handle(_ci.v0v1);
        typename Mesh::HalfedgeHandle prev_heh = mesh_.prev_halfedge_handle(_ci.v0v1);
        assert(mesh_.to_vertex_handle(prev_heh) == _ci.v0);
        assert(mesh_.from_vertex_handle(next_heh) == _ci.v1);

        p0 = toEigen(_ci.p0);
        p1 = toEigen(_ci.p1);
        pa = toEigen(mesh_.point(mesh_.from_vertex_handle(prev_heh)));
        pb = toEigen(mesh_.point(mesh_.to_vertex_handle(next_heh)));

    } else if (mesh_.is_boundary(_ci.v1v0)) {
        typename Mesh::HalfedgeHandle next_heh = mesh_.next_halfedge_handle(_ci.v1v0);
        typename Mesh::HalfedgeHandle prev_heh = mesh_.prev_halfedge_handle(_ci.v1v0);
        assert(mesh_.to_vertex_handle(prev_heh) == _ci.v1);
        assert(mesh_.from_vertex_handle(next_heh) == _ci.v0);
        p0 = toEigen(_ci.p0);
        p1 = toEigen(_ci.p1);
        pa = toEigen(mesh_.point(mesh_.to_vertex_handle(next_heh)));
        pb = toEigen(mesh_.point(mesh_.from_vertex_handle(prev_heh)));

    } else {
        // problem
        assert(0 && "Should have a boundary vertex here");
    }

    // e1 = p0 - pa + p1 - p0 + pb - p1 = pb - pa
    Eigen::Vector3f e1 = pb - pa;

    // e2 = p0 X pa + p1 X p0 + pb X p1
    Eigen::Vector3f e2 = p0.cross(pa) + p1.cross(p0) + pb.cross(p1);

    // e3 = e1 X e2
    Eigen::Vector3f e3 = e1.cross(e2);

    // equation from (5) and (6)
    ai_0 = e1.dot(e1) * e3;
    b_0 = e3.dot(e3);
    ai_1 = e1.cross(e3);
    b_1 = 0;
}

template<class MeshT>
float ModVolumeT<MeshT>::collapse_priority(const CollapseInfo& _ci) {

    // if edge pas boundary mais les deux vertex sont boundary => illégal

    if (!(mesh_.is_boundary(_ci.v0v1) || mesh_.is_boundary(_ci.v1v0))
            && mesh_.is_boundary(_ci.v0) && mesh_.is_boundary(_ci.v1))
        return float(Base::ILLEGAL_COLLAPSE);

    Constraints constraints;

    if (!mesh_.is_boundary(_ci.v0v1) && !mesh_.is_boundary(_ci.v1v0)) {
        // volume
        Eigen::Vector3f ai;
        float b;
        volumePreservation(_ci, ai, b);
        constraints.add(ai, b);


    }

    // calculer la position optimale



    // puis le coût

    return float(Base::LEGAL_COLLAPSE);
}

//-----------------------------------------------------------------------------

template<class MeshT>
void ModVolumeT<MeshT>::preprocess_collapse(const CollapseInfo& _ci) {

}


template<class MeshT>
void ModVolumeT<MeshT>::postprocess_collapse(const CollapseInfo& _ci) {

    // 1: savoir si bord ou pas bord
    // si aucun des deux vertices est boundary, OK
    // si l'edge est boundary => bord.
    // si un des deux boundary : ? volume ou boudary ?
    // si les deux et pas edge boudary : interdit.


    // equations de conservation

    // equations d'optimisation

    // bouger

}

//=============================================================================
}
}
//=============================================================================
