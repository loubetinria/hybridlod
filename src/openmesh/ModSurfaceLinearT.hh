/* ========================================================================= *
 *                                                                           *
 *                               OpenMesh                                    *
 *           Copyright (c) 2001-2015, RWTH-Aachen University                 *
 *           Department of Computer Graphics and Multimedia                  *
 *                          All rights reserved.                             *
 *                            www.openmesh.org                               *
 *                                                                           *
 *---------------------------------------------------------------------------*
 * This file is part of OpenMesh.                                            *
 *---------------------------------------------------------------------------*
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 * 1. Redistributions of source code must retain the above copyright notice, *
 *    this list of conditions and the following disclaimer.                  *
 *                                                                           *
 * 2. Redistributions in binary form must reproduce the above copyright      *
 *    notice, this list of conditions and the following disclaimer in the    *
 *    documentation and/or other materials provided with the distribution.   *
 *                                                                           *
 * 3. Neither the name of the copyright holder nor the names of its          *
 *    contributors may be used to endorse or promote products derived from   *
 *    this software without specific prior written permission.               *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER *
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,  *
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,       *
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR        *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
 *                                                                           *
 * ========================================================================= */

/*===========================================================================*\
 *                                                                           *
 *   $Revision$                                                        *
 *   $Date$                   *
 *                                                                           *
 \*===========================================================================*/

/** \file ModSurfaceLinearT.hh
 */

//=============================================================================
//
//  CLASS ModSurfaceLinearT
//
//=============================================================================
#ifndef OPENMESH_DECIMATER_MODSURFACELINEART_HH
#define OPENMESH_DECIMATER_MODSURFACELINEART_HH

//== INCLUDES =================================================================

#include <OpenMesh/Tools/Decimater/ModBaseT.hh>
#include <cfloat>

//== NAMESPACES ===============================================================

namespace OpenMesh {
namespace Decimater {

//== CLASS DEFINITION =========================================================

/** \brief Use edge length to control decimation
 *
 * This module computes the edge length.
 *
 * In binary and continuous mode, the collapse is legal if:
 *  - The length after the collapse is lower than the given tolerance
 *
 */
template<class MeshT>
class ModSurfaceLinearT: public ModBaseT<MeshT> {
  public:

    DECIMATING_MODULE( ModSurfaceLinearT, MeshT, SurfaceLinear )
    ;


    ModSurfaceLinearT(MeshT& _mesh) :
        Base(_mesh, true), mesh_(Base::mesh()) {
      mesh_.add_property(area_before_);
      mesh_.add_property(normal_before_);
    }


    /** Compute priority:
     Binary mode: Don't collapse edges longer then edge_length_
     Cont. mode:  Collapse smallest edge first, but
     don't collapse edges longer as edge_length_
     */
    float collapse_priority(const CollapseInfo& _ci);

    void postprocess_collapse(const CollapseInfo&  _ci );
    void preprocess_collapse(const CollapseInfo&  _ci );

  private:


    float getArea(const typename Mesh::Point &p_1,
                      const typename Mesh::Point &p_2,
                      const typename Mesh::Point &p_3);

    float getProjArea(const typename Mesh::Point &p_1,
                      const typename Mesh::Point &p_2,
                      const typename Mesh::Point &p_3,
                      const typename Mesh::Point &n);


    void getPoints(typename Mesh::FaceHandle fh,
                   typename Mesh::Point &p_1,
                   typename Mesh::Point &p_2,
                   typename Mesh::Point &p_3);

    // important, même ordre/signe que getAreaVector
    typename Mesh::Point getNormal(const typename Mesh::Point &p_1,
                                   const typename Mesh::Point &p_2,
                                   const typename Mesh::Point &p_3);

    // important, même ordre/signe que getNormal
    typename Mesh::Point getAreaVector(const typename Mesh::Point &p_1,
                                       const typename Mesh::Point &p_2,
                                       const typename Mesh::Point &p_3);

    void getVertices(typename Mesh::FaceHandle fh,
                     typename Mesh::VertexHandle &v_1,
                     typename Mesh::VertexHandle &v_2,
                     typename Mesh::VertexHandle &v_3);

    typename Mesh::Point ortho(typename Mesh::Point normal);

    Mesh& mesh_;
    VPropHandleT<float> area_before_;
    FPropHandleT<typename Mesh::Point> normal_before_;




};

//=============================================================================
}// END_NS_DECIMATER
} // END_NS_OPENMESH
//=============================================================================
#if defined(OM_INCLUDE_TEMPLATES) && !defined(OPENMESH_DECIMATER_MODSURFACELINEART_C)
#define MODSURFACELINEART_TEMPLATES
#include "ModSurfaceLinearT_impl.hh"
#endif
//=============================================================================
#endif // OPENMESH_DECIMATER_MODSURFACELINEART_HH defined
//=============================================================================

