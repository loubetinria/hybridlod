/* ========================================================================= *
 *                                                                           *
 *                               OpenMesh                                    *
 *           Copyright (c) 2001-2015, RWTH-Aachen University                 *
 *           Department of Computer Graphics and Multimedia                  *
 *                          All rights reserved.                             *
 *                            www.openmesh.org                               *
 *                                                                           *
 *---------------------------------------------------------------------------*
 * This file is part of OpenMesh.                                            *
 *---------------------------------------------------------------------------*
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 * 1. Redistributions of source code must retain the above copyright notice, *
 *    this list of conditions and the following disclaimer.                  *
 *                                                                           *
 * 2. Redistributions in binary form must reproduce the above copyright      *
 *    notice, this list of conditions and the following disclaimer in the    *
 *    documentation and/or other materials provided with the distribution.   *
 *                                                                           *
 * 3. Neither the name of the copyright holder nor the names of its          *
 *    contributors may be used to endorse or promote products derived from   *
 *    this software without specific prior written permission.               *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER *
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,  *
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,       *
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR        *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
 *                                                                           *
 * ========================================================================= */

/*===========================================================================*\
 *                                                                           *
 *   $Revision$                                                        *
 *   $Date$                   *
 *                                                                           *
 \*===========================================================================*/

/** \file ModSurfaceLinearT.cc
 */

//=============================================================================
//
//  CLASS ModSurfaceT - IMPLEMENTATION
//
//=============================================================================
#define OPENMESH_DECIMATER_MODSURFACELINEART_C

//== INCLUDES =================================================================

#include "ModSurfaceLinearT.hh"
#include <cfloat>
#include <random>

//== NAMESPACES ===============================================================

namespace OpenMesh {
namespace Decimater {

//== UTILS ===============================================================

template<class MeshT>
float ModSurfaceLinearT<MeshT>::getArea(const typename Mesh::Point &p_1,
                                    const typename Mesh::Point &p_2,
                                    const typename Mesh::Point &p_3) {
    return 0.5f * (cross(p_2 - p_1, p_3 - p_1)).norm();
}

template<class MeshT>
float ModSurfaceLinearT<MeshT>::getProjArea(const typename Mesh::Point &p_1,
                                    const typename Mesh::Point &p_2,
                                    const typename Mesh::Point &p_3,
                                    const typename Mesh::Point &n) {
    float area = dot(n, cross(p_2 - p_1, p_3 - p_1));
    area = std::abs(area) * 0.5f;
    return area;
}

// important, même ordre/signe que getAreaVector
template<class MeshT>
typename ModSurfaceLinearT<MeshT>::Mesh::Point ModSurfaceLinearT<MeshT>::getNormal(const typename Mesh::Point &p_1,
                                                                                   const typename Mesh::Point &p_2,
                                                                                   const typename Mesh::Point &p_3) {
    typename Mesh::Point res = cross(p_2 - p_1, p_3 - p_1);
    if (res.norm() > 0.f)
        return res / res.norm();
    std::cout << "Warning, no normal : "<< p_1 << " " << p_2 << " " << p_3 << std::endl;
    return typename Mesh::Point(1.0,0.0,0.0);
}

// important, même ordre/signe que getNormal
template<class MeshT>
typename ModSurfaceLinearT<MeshT>::Mesh::Point ModSurfaceLinearT<MeshT>::getAreaVector(const typename Mesh::Point &p_1,
                                                                                       const typename Mesh::Point &p_2,
                                                                                       const typename Mesh::Point &p_3) {
    typename Mesh::Point res = cross(p_2 - p_1, p_3 - p_1);
    return res * 0.5;
}


template<class MeshT>
void ModSurfaceLinearT<MeshT>::getPoints(typename Mesh::FaceHandle fh,
                                 typename Mesh::Point &p_1,
                                 typename Mesh::Point &p_2,
                                 typename Mesh::Point &p_3) {
    typename Mesh::ConstFaceVertexIter fv_it(mesh_, fh);
    p_1 = mesh_.point(*fv_it);
    fv_it++;
    p_2 = mesh_.point(*fv_it);
    fv_it++;
    p_3 = mesh_.point(*fv_it);
}

template<class MeshT>
void ModSurfaceLinearT<MeshT>::getVertices(typename Mesh::FaceHandle fh,
                                 typename Mesh::VertexHandle &v_1,
                                 typename Mesh::VertexHandle &v_2,
                                 typename Mesh::VertexHandle &v_3) {
    typename Mesh::ConstFaceVertexIter fv_it(mesh_, fh);
    v_1 = *fv_it;
    fv_it++;
    v_2 = *fv_it;
    fv_it++;
    v_3 = *fv_it;
}

template<class MeshT>
typename ModSurfaceLinearT<MeshT>::Mesh::Point ModSurfaceLinearT<MeshT>::ortho(typename Mesh::Point normal) {
    if (normal[0] != 0.0 || normal[1] != 0) {
        return typename Mesh::Point(-normal[1], normal[0], 0.0) / (typename Mesh::Point(-normal[1], normal[0], 0.0)).norm();
    }
    return typename Mesh::Point(0.0, -normal[2], normal[1]) / (typename Mesh::Point(0.0, -normal[2], normal[1])).norm();
}

//== IMPLEMENTATION ==========================================================

//-----------------------------------------------------------------------------

template<class MeshT>
float ModSurfaceLinearT<MeshT>::collapse_priority(const CollapseInfo& _ci) {
    return float(Base::LEGAL_COLLAPSE);
}

//-----------------------------------------------------------------------------

template<class MeshT>
void ModSurfaceLinearT<MeshT>::preprocess_collapse(const CollapseInfo& _ci) {

    int nbface = 0;

    // calculer les directions et les aires initiales dans ces directions
    float area_before = 0.f;

    typename Mesh::ConstVertexFaceIter vf0_it(mesh_, _ci.v0);
    while (vf0_it.is_valid()) {
        if ((*vf0_it != _ci.fl) && (*vf0_it != _ci.fr)) {
            typename Mesh::Point p0, p1, p2;
            getPoints(*vf0_it, p0, p1, p2);
            area_before += getArea(p0, p1, p2);
            nbface++;

            mesh_.property(normal_before_, *vf0_it) = getNormal(p0, p1, p2);

        }
        ++vf0_it;
    }

    typename Mesh::ConstVertexFaceIter vf1_it(mesh_, _ci.v1);
    while (vf1_it.is_valid()) {
        if ((*vf1_it != _ci.fl) && (*vf1_it != _ci.fr)) {
            typename Mesh::Point p0, p1, p2;
            getPoints(*vf1_it, p0, p1, p2);
            area_before += getArea(p0, p1, p2);
            nbface++;

            mesh_.property(normal_before_, *vf1_it) = getNormal(p0, p1, p2);
        }
        ++vf1_it;
    }

    if (_ci.fl.is_valid()) {
        typename Mesh::Point p0, p1, p2;
        getPoints(_ci.fl, p0, p1, p2);
        area_before += getArea(p0, p1, p2);
        nbface++;
        mesh_.property(normal_before_, _ci.fl) = getNormal(p0, p1, p2);
    }

    if (_ci.fr.is_valid()) {
        typename Mesh::Point p0, p1, p2;
        getPoints(_ci.fr, p0, p1, p2);
        area_before += getArea(p0, p1, p2);
        nbface++;
        mesh_.property(normal_before_, _ci.fr) = getNormal(p0, p1, p2);
    }

    mesh_.property(area_before_, _ci.v1) = area_before;
    std::cout << "area before before: " << area_before << std::endl;

    /*
    std::cout << "preprocess OK, nb faces " << nbface << " areas "
              << mesh_.property(area_before_1_, _ci.v1) << " "<<  mesh_.property(area_before_2_, _ci.v1) << " " << mesh_.property(area_before_3_, _ci.v1)
              << " ns "
              << mesh_.property(n1_, _ci.v1) << " "<<  mesh_.property(n2_, _ci.v1) << " " << mesh_.property(n3_, _ci.v1) << std::endl;
    */

}


template<class MeshT>
void ModSurfaceLinearT<MeshT>::postprocess_collapse(const CollapseInfo& _ci) {

    // optimiser pas position de v1 pour conserver les aires projetées.

    float area_before = mesh_.property(area_before_, _ci.v1);

    // equation : a.p + b = 0;
    float b = -area_before;
    typename Mesh::Point a(0.f,0.f,0.f);

    // ici, juste les faces voisines de v1, j'espère que la connection est la bonne,
    // ie que le collapse a vraiment été fait

    int nbface = 0;
    typename Mesh::ConstVertexFaceIter vf1_it(mesh_, _ci.v1);
    while (vf1_it.is_valid()) {
        if (!mesh_.status(*vf1_it).deleted()) {

            typename Mesh::Point n = mesh_.property(normal_before_,*vf1_it);
            std::cout << "n: " << n << std::endl;

            typename Mesh::VertexHandle va, vb, vc;
            typename Mesh::Point p0, p1;
            getVertices(*vf1_it, va, vb, vc);
            // (p-p0)X(p1-p0) doit être orienté comme n.
            // => ordre = p0, p, p1
            if (va == _ci.v1) {
                p1 = mesh_.point(vb);
                p0 = mesh_.point(vc);
            } else if (vb == _ci.v1) {
                p1 = mesh_.point(vc);
                p0 = mesh_.point(va);
            } else if (vc == _ci.v1) {
                p1 = mesh_.point(va);
                p0 = mesh_.point(vb);
            } else {
                assert(0 && "v1 should be one of the vertices");
            }

            a += 0.5f * cross(p1-p0,n);
            b += -dot(p0,cross(p1-p0,n))*0.5f;
            // equation : n.[(p-p0)X(p1-p0)] = (p-p0).[(p1-p0)Xn] =  p.[(p1-p0)Xn] - p0.[(p1-p0)Xn]
            nbface++;
        }
        ++vf1_it;
    }
    // solution : projeté ortho de (p0 - p1)*0.5 sur le plan.
    typename Mesh::Point middle = _ci.p0 * 0.5 + _ci.p1 * 0.5;
    // a.(p_middle + h*a) + b = 0
    // h*a.a = -b -a.middle
    // h = -(b + a.middle)/a.a
    if (dot(a,a) <= 0) {
        return;
    }
    float h = -(b + dot(a,middle))/dot(a,a);
    //std::cout << "area before: " << area_before << std::endl;
    //std::cout << " a et b : " << a << " "  << b << std::endl;

    // clamp h à la sphère de diametre p1 p0
/*
    h *= a.norm();
    a /= a.norm();
    if (h < 0)
        a = -a;
    h = std::min((_ci.p1 - _ci.p0).norm()*0.5f, std::fabs(h));
*/
    mesh_.set_point(_ci.v1, middle+h*a);

    // check validity : nouvelle normale . ancienne > 0
    typename Mesh::ConstVertexFaceIter vf2_it(mesh_, _ci.v1);
    while (vf2_it.is_valid()) {
        if (!mesh_.status(*vf2_it).deleted()) {
            typename Mesh::Point normal = mesh_.property(normal_before_,*vf2_it);
            typename Mesh::Point p0, p1, p2;
            getPoints(*vf2_it, p0, p1, p2);
            typename Mesh::Point new_normal = getNormal(p0, p1, p2);
            if (dot(normal,new_normal) <= 0) {
                //std::cout << "SurfaceLinear Bad config" << std::endl;
                mesh_.set_point(_ci.v1, _ci.p1);
                return;
            } else {
                //std::cout << "Good config" << std::endl;
            }
        }
        ++vf2_it;
    }

}

//=============================================================================
}
}
//=============================================================================
