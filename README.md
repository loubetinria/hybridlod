# The hybrid LOD project

This project consists of a set of tools for processing high-resolution textured meshes and generating low-resolution data with low memory footprint, as described in the paper [Hybrid mesh-volume LoDs for all-scale pre-filtering of complex 3D assets](https://hal.archives-ouvertes.fr/hal-01468817). The specificity of this prefiltering pipeline is that it generates hybrid level-of-detail (LOD) data, that is, textured meshes for the large surfaces that cover more than one pixel, and 3D grid containing volumetric scattering parameters representing the prefiltered appearance of the subresolution geometry. The LODs are intended to have the same appearance as the input data when they are distant enough from the camera and when they are renderer using a physically-based path tracer.

The project builds on various projects and publications, including:

* Mesh simplification techniques based on edge collapse (tons of publications).
* The [Mitsuba renderer](https://www.mitsuba-renderer.org/).
* [Microflake-based volume rendering](https://www.cs.cornell.edu/~kb/publications/SIG10Aniso.pdf).
* The [SGGX microflake distribution](https://research.nvidia.com/publication/2015-08_The-SGGX-microflake).
* The [ellipsoid NDF](http://www.cs.cornell.edu/Projects/metalappearance/).

## Compiling the project 

The code is organized in two different components:

* Some C++ tools for texture and mesh prefiltering are located in the folder `src/`. This code can be compiled using [CMake](https://cmake.org/) and the file `CMakeLists.txt` located in the root directory (see details bellow).
* A modified version of Mitsuba, located in `mitsuba`. This version of Mitsuba has a modified core and a few plugins used for rendering microflakes volumes with the SGGX distribution and microfacet BSDFs using the ellipsoid NDF. It is also used for voxelizing the subresolution geometry in our prefiltering pipeline. This code can be compiled as a standard Mistuba renderer using [SCons](https://scons.org/). More information can be found in the [Mitsuba documentation](https://www.mitsuba-renderer.org/docs.html).

### Compiling the prefiltering tools

These tools depend on various external projects, including [OpenMesh](https://www.openmesh.org/) and [pngwriter](https://github.com/pngwriter/pngwriter). You can clone this git repository with these projects as submodules using the command:
```sh
git clone --recursive https://loubetinria@bitbucket.org/loubetinria/hybridlod.git
```

If you forgot the `--recursive` flag when cloning this repository you can initialize the submodules using:
```sh
cd hybridlod
git submodule update --init --recursive
```

To compile the tools, you may need to install other libraries such as Boost, Eigen3 and FreeType. If you use a Linux OS with `apt`, you can write:
```sh
sudo apt install libboost-all-dev
sudo apt install libeigen3-dev
sudo apt install libfreetype6-dev
```

Then, you can compile the project:
```sh
cd hybridlod
mkdir build
cd build
cmake .. 
make -j
```

This should compile several prefiltering tools used for the project: 

* `textureFiltering`
* `collapser`
* `voxels2vol`

For generating LODs, you need to add them to your shell environment.
```sh
cd ..
source setpath.sh
```

### Compiling Mitsuba

We refer to the [Mitsuba documentation](https://www.mitsuba-renderer.org/docs.html) for the compilation of
this part of the project.

The first step is choosing and copying a configuration file located in `build/`, for instance:
```sh
cd mitsuba
cp build/config-linux-gcc.py config.py
```
Make sure you have all the required libraries installed.
```sh
sudo apt-get install build-essential scons mercurial qt4-dev-tools libpng-dev libjpeg-dev libilmbase-dev libxerces-c-dev libboost-all-dev libopenexr-dev libglewmx-dev libxxf86vm-dev libpcrecpp0v5 libeigen3-dev libfftw3-dev
```
You can then compile using SCons.
```sh
scons
```
If you get syntax errors in Mitsuba config files, this may be because 
SCons is using Python3. You can force SCons to use Python2 by writing `python2` followed by the location of the SCons executable:
```sh
python2 /usr/local/bin/scons`
```

You can then import Mitsuba in your shell environment.
```sh
source setpath.sh
```
### Notes on compiling Mitsuba on Mac OS X

Compiling Mitsuba on MacOS is less straightforward. Wenzel Jakob provided a repository with some 
Mitsuba dependencies that can be easily downloaded.
```sh
hg clone https://www.mitsuba-renderer.org/hg/dependencies_macos
mv dependencies_macos dependencies
``` 

These dependencies are not completely up-to-date. You need to modify manually the version number
in `dependencies/version` to `0.6.0`. If you get some problems with some of the libraries, 
you can install the last versions on your machine and set paths manually in the file `config.py`
(most likely for Boost, Python, Boost-Python and OpenEXR).

## Generating and rendering LODs

Make sure you are back in the root directory of the project.
```sh
cd ..
```
### Input data

The pineline only supports very specific input files currently. Some examples can be found in `data/`.
Each asset has a root directory directly located in `data/`. For instance, the root directory of
the Oak Trunk asset is `data/oak_trunk/`. Then the input data should be organized as follows:

* `data/myasset/src/src.obj` should be the input geometry of the asset `myasset`. The mesh should ideally be a triangle and manifold mesh (otherwise it will be triangulated and cleaned during the prefiltering process). Currently the pipeline only supports one `.obj` file per asset. The mesh should be scaled and translated to fit in the cube of min corner `(-1, -1, 0)` and max corner `(1, 1, 2)`, the up axis being the Z axis.
* `data/myasset/src/src_diffuse_front.png` should be a texture for the asset  `myasset` containing the diffuse reflectance. Currently the pipeline only supports one texture file for the meshes contained in `src.obj`. The texture should contain
reflectance values in linear space, which is the space in which the data should be filtered. If this is not the case, you can use `gimp` to apply a gamma correction with a value of roughly `1/2.2=0.4545` to your image, for instance using the `convert` tool from [ImageMagick](https://imagemagick.org/script/command-line-options.php#gamma). 
```sh
convert input.png -gamma .45455 output.png
```
* `data/myasset/src/src_specular_front.png` should contain a reflectance value for the specular lobe of the object's BSDF. Again, this should be in linear space.

### Rendering the src data

You can render the `src` data using:
```sh
source scripts/renderSrc.sh data/myasset/
```

You should be able to render the `oak_trunk` asset, the rendered image should be written in the file `results/src.exr`.
Currently this renders the asset using a simple diffuse material. For visualizing `.exr` file, you can use `tev`: https://github.com/Tom94/tev.

### Generating a mesh with per face reflectance data

The hybridlod pipeline requires a triangle mesh with per face attributes (diffuse reflectance, specular reflectance and SGGX matrix).

To generate such data from the `src` data, you can run:
```sh
textureFiltering data/oak_trunk/ 0.005
```
where `0.005` is the initial roughness of the specular lobe (currently, the same for all the faces of the input asset). The roughness value should be 
higher that `0.001` to avoid numerical errors later in the pipeline.

This should generate the following files:

* `data/oak_trunk/geometry/input.obj`: a mesh, roughly the same as `src.obj`, eventually triangulated and cleaned, with new texture coordinates.
* `data/oak_trunk/maps/input_diffuse_front.png`: a texture file where each pixel correspond to one face of the mesh `input.obj`.
* `data/oak_trunk/maps/input_specular_front.png`: a texture file for specular reflectance.
* `data/oak_trunk/maps/input_sggx_sigma_front.png`: a texture file containing the `sigma` data representing a SGGX matrix as proposed in the [SGGX paper](https://research.nvidia.com/publication/2015-08_The-SGGX-microflake).
* `data/oak_trunk/maps/input_sggx_r_front.png`: a texture file containing the `r` data of the SGGX matrix.

You can check that the appearance of this preprocessed input data is correct by rendering it using:
```sh
source scripts/renderInput.sh data/oak_trunk/
```

You should obtain a file `results/input.exr`.

### Prefiltering the geometry and textures

The next step is to prefilter the geometry and the textures of the input data. For this, you can write:
```sh
collapser data/oak_trunk/
```

This will start the edge collpase process and generate lots of new files. It generates data for 7 LODs from `lod512` (finest LOD) to `lod8` (coarsest), where the
number is the resolution of the corresponding voxel grid. These LODs are intended to be renderer at an appropriate scale, for instance the lod `lod128` should be renderer such that it covers roughly `128x128` pixels or less.

The new files for the LODs are:

* `data/oak_trunk/geometry/lod128.obj`: the prefiltered macrosurfaces for `lod128`.
* `data/oak_trunk/geometry/lod128_detached.obj`: the subresolution geometry. This will get voxelized in the next step.
* `data/oak_trunk/geometry/lod128_0.obj` and `data/oak_trunk/geometry/lod128_1.obj`. These are used for seamless transitions. These meshes should have the same number of vertices and the same topology, such that morphing can be acheived using a simple linear interpolation of vertex positions. The mesh `lod128_0.obj` shoud be visually equivalent to `lod128.obj`, and `lod128_100.obj` should be visually equivalent to the mesh `lod64.obj` (the next LOD).
* `data/oak_trunk/maps/lod128_diffuse_front.png`, etc.:  textures for `lod128`.
* `data/oak_trunk/maps/lod128_diffuse_front_0.png`, etc.:  used for seamless transitions.
* `data/oak_trunk/maps/lod128_diffuse_front_detached.png`, etc.:  use for the voxelization of subresolution geometry.
* `data/oak_trunk/maps/lod128_mask.png`, ect.: used for the seamless transitions (some faces must become transparent if they are voxelized in the next LOD).
* `data/oak_trunk/scripts/voxelize128.sh`: script for voxelizing the subresolution geometry for this LOD.

### Voxelizing the subresolution geometry

Once the geometry has been processed, the volume data for one LOD can be generated using (for the `oak_trunk` asset):
```sh
source data/oak_trunk/scripts/voxelize128.sh
```

This calls the modified `mitsuba` for the voxelization. It generates a set of files `voxelized/[threadID].voxels` that are merged into a file `all.voxels`, that is then converted to Mitsuba volume files in the asset root directory:
* `data/oak_trunk/volume/lod_diffuse128.mtsvol`: 3D volume containing the diffuse reflectance of microflakes.
* `data/oak_trunk/volume/lod_specular128.mtsvol`: 3D volume containing the specular reflectance of microflakes.
* `data/oak_trunk/volume/lod_sggx_sigma129.mtsvol`: `char3` grid, sigma data of SGGX matrix.
* `data/oak_trunk/volume/lod_sggx_r128.mtsvol`: `char3` grid, r data of SGGX matrix.
* `data/oak_trunk/volume/lod_density129.mtsvol`: attenuation coefficient, in inverse scene units, `float` grid.

At this stage, all the LOD data has been generated!

### Rendering a LOD

Once generated, a LOD can be rendered using:
```sh
source scripts/renderLOD.sh 128 data/oak_trunk/
```
This should render the LOD in `results/lod128.exr`.

## TODOs

The supported input data is currently very limited, as well as the reflectance models in the LODs: diffuse lobe and a 
microfacet lobe, simple addition. This project mainly addresses the problem of the transition
between surface-like appearance and volume-like appearance. The pipeline could be extended with more complex BSDF models that can
be filtered easilly (for example the Disney BSDF, with coating, transmission component for tree leaves, etc.).

Compression and sparse volumetric data should be considered for reducing as much as possible the memory usage of LODs.

As you will notice the geometry prefiltering step is currently slow. The implementation is single threaded, mainly
because edge collapse is done in OpenMesh and is essentially an iterative operation, although it is local.
The macrosurface analysis could be parallelized as it is a per triangle operation.

We noticed robustness issues in our surface prefiltering technique (face materials being prefiltered 
as edges are collpased). Texture prefiltering is in general a more
robust and efficient way of prefiltering surface reflectance. During an internship at the Walt Disney Animation Studios,
I worked on another version of this pipeline based on [quad mesh simplification](https://vgc.poly.edu/~csilva/papers/siggraph-asia2008.pdf). 
Although it was only applicable to structured quad meshes, these meshes are ubiquitous in the film industry, and both 
macrosurface analysis and texture filtering can be done more easily and robustly in this framework.
Unfortunately there is no public code for this but feel free to contact me for a discussion if you are interested by the topic.

Seamless transitions have been working at the time the paper was published but it haven't been tested since then. It requires interpolating
volumes, meshes, textures, and some opacity textures so that triangles get some transparency when they become subresolution geometry.